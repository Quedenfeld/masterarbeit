\documentclass[a4paper]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage{geometry}
\geometry{a4paper, top=25mm, left=35mm, right=25mm, bottom=30mm}

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage{algorithm}
\usepackage[noend]{algorithmic}
\usepackage{lipsum}

%\usepackage{bibgerm}
% Einbindung des natbib-Pakets
%\usepackage[numbers,round]{natbib}
  
\title{Variantentolerantes Readmapping durch Locality Sensitive Hashing}
\author{Jens Quedenfeld}
\date{\today}

\begin{document}

\selectlanguage{ngerman}

\maketitle

\section{Thema und Motivation}

Ein zentraler Baustein des Lebens ist die Desoxyribonukleinsäure, kurz DNA. Sie ist der Träger der Erbinformation und bestimmt damit das Aussehen und die Eigenschaften von Lebewesen. Von daher ist es von großem wissenschaftlichen Interesse die DNA zu sequenzieren (d.h. die Abfolge der Basenpaare zu bestimmen). Die Kosten und der Aufwand für das Sequenzieren eines Genoms (also der gesamten DNA eines Lebewesens bzw. einer Zelle) sind in den letzten Jahren stark gesunken. Dadurch wird es immer einfacher und schneller möglich, die DNA eines Lebewesens, beispielsweise die des Menschen, zu analysieren. Allerdings liefern diese Verfahren nur kurze Fragmente, sogenannte Reads mit einer Länge von (je nach Verfahren) 50 bis 50000 Basenpaaren. Diese Reads müssen durch ein rechenintensives Verfahren wieder zu dem Genom zusammengesetzt werden. Da sich die DNA verschiedener Menschen stark ähnelt, ist es deutlich leichter, ein bereits sequenziertes Referenzgenom zu betrachten und die Reads in diesem wiederzufinden. Ein Programm, das dieses Problem löst, wird als Readmapper bezeichnet. 

Dadurch dass bereits die Genome von über 1000 Menschen sequenziert wurden, sind zahlreiche Varianten bekannt. Herkömmliche Readmapper wie BWA \cite{BWA2010} nutzen diese Zusatzinformation jedoch nicht aus. Ein variantentoleranter Readmapper (wie beispielsweise mrsFast-Ultra \cite{mrsFastUltra2014}) benutzt dagegen als zusätzliche Eingabe Informationen über häufig vorkommende Varianten enthält. Kommen in den Reads bekannte Varianten vor, werden diese Unterschiede nicht als Fehler gewertet. Variantentolerante Readmapper können dadurch solche Reads deutlich besser im Referenzgenom wiederfinden als herkömmliche Readmapper.

Die Projektgruppe 583 der TU Dortmund \cite{PG583} hat einen solchen variantentoleranten Readmapper namens VATRAM entwickelt, der auf einem Verfahren namens Locality Sensitive Hashing (kurz LSH) basiert \cite{Indyk1998,Leskovec2014}. %Die Funktionsweise von VATRAM soll im Folgenden kurz erklärt werden.
Das Referenzgenom wird dabei in (sich überlappende) Abschnitte unterteilt, dessen Länge etwa der Readlänge entsprechen. Anschließend wird die $q$-Grammmenge $Q(a_i)$ jedes Abschnitts $a_i$ berechnet, also die Menge der zusammenhängenden Substrings mit einer Länge von $q$ Basen. Es wird eine zufällige Permutation $\pi$ aller $q$-Gramme gewählt. Das bezüglich $\pi$ kleinste $q$-Gramm aus $Q(a_i)$ bildet beim sogenannten Min-Hashing den Schlüssel $x(a_i)$ des Abschnitts $a_i$. In einer geeigneten Datenstruktur $D$ (z.B. einer Hashtabelle) werden dann die Paare aus dem Schlüssel $x(a_i)$ und dem Index $i$ des jeweiligen Abschnitts $a_i$ abgespeichert. Um einen gegebenen Read $r$ im Referenzgenom wiederzufinden, werden analog zu den Referenzabschnitten die $q$-Gramme des Reads bestimmt und anschließend das kleinste $q$-Gramm $x(r)$ bezüglich $\pi$ berechnet. Alle Abschnitte, die unter dem Schlüssel $x(r)$ in $D$ abgespeichert wurden, sind mögliche Positionen für den Read $r$. Durch die Verwendung von mehreren Permutationen kann die Qualität des Readmappers erheblich gesteigert werden, wobei man beachten sollte, dass auch der Speicherverbrauch linear mit der Anzahl der verwendeten Permutationen ansteigt.

Die Ergebnisse der Projektgruppe haben gezeigt, dass das Locality Sensitive Hashing ein vielversprechender Ansatz ist. Teilweise mappt VATRAM sogar schneller und besser als State-of-the-art Readmapper wie BWA. Allerdings blieben bei den Forschungsergebnissen der Projektgruppe noch einige Fragestellungen offen, die in meiner Masterarbeit weiter untersucht werden sollen. Dabei soll ausschließlich auf das Mapping durch LSH eingegangen werden. Eine Weiterentwicklung des Aligners von VATRAM soll nicht stattfinden, da dies zu weit führen würde.

\section{Ziele und Fragestellungen}

Das LSH-Verfahren wurde von der Projektgruppe ausschließlich experimentell untersucht. Über die Jaccard-Ähnlichkeit kann das LSH-Verfahren und insbesondere die Behandlung der Varianten auch theoretisch untersucht werden. Durch das Hinzufügen der $q$-Gramme aller Variantenkombinationen von SNPs erhöht sich die $q$-Grammmenge eines Referenzabschnittes. Dadurch verringert sich die Wahrscheinlichkeit, einen variantenlosen Read zu finden. In welchen Situationen es sich lohnt, trotzdem SNP-Varianten hinzuzufügen, soll in meiner Masterarbeit geklärt werden. Darüber hinaus kann theoretisch untersucht werden, wie wahrscheinlich es ist, dass ein im Genom nicht vorkommender Read gemappt wird. Genauso kann berechnet werden, mit welcher Wahrscheinlichkeit die korrekte Position eines im Genom vorkommenden Reads gefunden wird. Sollte eine theoretische Analyse zu kompliziert werden, bietet es sich an, die Fälle zu simulieren, um so Wahrscheinlichkeiten näherungsweise zu bestimmen.

Darüber hinaus existieren in den VCF-Dateien auch Wahrscheinlichkeitswerte für Varianten. Diese wurden bisher nicht berücksichtigt. Man kann sich fragen, wie hoch die Wahrscheinlichkeit sein muss, damit sich das Hinzufügen der geänderten $q$-Gramme lohnt, wenn man davon ausgeht, dass ein gegebener Read die jeweilige Variante mit der entsprechenden Wahrscheinlichkeit enthält. Nach der theoretischen Analyse kann die Berücksichtigung der Wahrscheinlichkeitswerte für Varianten auch in VATRAM verwendet werden.

Die PG583 hat in ihren Experimenten ausschließlich Reads mit 100 Basenpaaren betrachtet. Hier stellt sich die Frage, inwiefern VATRAM mit längeren Reads zurecht kommt und wie die Parameter angepasst werden müssen. Die von der Projektgruppe durchgeführten Experimente können mit verschiedenen Readlängen erneut durchgeführt werden, um so optimale Parameterkonfigurationen für längere Reads zu bestimmen. Insbesondere bei der $q$-Grammlänge stellt sich die Frage, ob (und falls ja, wie) diese angepasst werden muss. Andere Parameter müssen dagegen vermutlich einfach um den Faktor vergrößert werden, um den auch die Reads länger geworden sind. Bei längeren Reads könnte außerdem untersucht werden, wie die Laufzeit skaliert. Da zur Berechnung der Trefferlisten ein aufwändiges Merging stattfindet, könnte die Laufzeit durch die größere Anzahl von Hash"=tabellen superlinear ansteigen. Auf der anderen Seite reduziert sich die Anzahl falscher Treffer, weswegen auch ein sublinearer Zusammenhang möglich wäre. 

Ein alternativer Ansatz zur Behandlung von langen Reads wäre, diese in kürzere Read-Fragmente zu unterteilen. Es wird dann nach den einzelnen Fragmenten gesucht und anschließend die Schnittmenge über die möglichen Positionen im Genom gebildet. Ob dieser Ansatz sinnvoll ist, müssen Experimente zeigen. Dieser Ansatz eignet sich auch zur Behandlung von Paired-End-Reads, die bisher von VATRAM nicht unterstützt werden. 

Bei VATRAM wurde bisher ausschließlich Min-Hashing als LSH-Verfahren verwendet. Eine Alternative wäre Sim-Hashing \cite{Charikar2002}. Ob sich der Einsatz von Sim-Hashing lohnt, soll ebenfalls in der Masterarbeit geklärt werden.

%\bibliographystyle{../abbrvnat}
\bibliographystyle{plaindin}
\bibliography{../Literatur/literatur}

\end{document}