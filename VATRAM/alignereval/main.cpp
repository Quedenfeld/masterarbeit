#include "AlignerNoRowSkip.h"
#include "AlignerNaive.h"
#include "align/Aligner.h"
#include "GenomeGenerator.h"
#include "PerformanceTimer.h"
#include "Logger.h"

#include <iostream>

int main() {
    PerformanceTimer timer;
    timer.startCounter();
    double time = 0.0;
    logger.info("Starting benchmark ..");

    GenomeGenerator gen(1337);

    // Parameters
    size_t readNumber100 = 10000;
    size_t readNumber250 = readNumber100;
    size_t readNumber350 = readNumber100;
    size_t readNumber500 = readNumber100;
    size_t readNumber750 = readNumber100;
    size_t readNumber1000 = readNumber100;
    size_t readNumber1500 = readNumber100;
    size_t readNumber2000 = readNumber100;
    size_t readNumber4000 = readNumber100/4;
    size_t readNumber8000 = readNumber100/8;
    size_t readNumber16000 = readNumber100/16;
    size_t readNumber32000 = readNumber100/32;
    ErrorCount maxError100 = 6;
    ErrorCount maxError250 = 6;
    ErrorCount maxError350 = 6;
    ErrorCount maxError500 = 6;
    ErrorCount maxError750 = 6;
    ErrorCount maxError1000 = 6;
    ErrorCount maxError1500 = 6;
    ErrorCount maxError2000 = 6;
    //ErrorCount maxError4000 = 6;
    //ErrorCount maxError8000 = 6;
    //ErrorCount maxError16000 = 6;
    //ErrorCount maxError32000 = 6;
    double snpProp100 = 0.027;
    double snpProp250 = snpProp100*100/250;
    double snpProp350 = snpProp100*100/350;
    double snpProp500 = snpProp100*100/500;
    double snpProp750 = snpProp100*100/750;
    double snpProp1000 = snpProp100*100/1000;
    double snpProp1500 = snpProp100*100/1500;
    double snpProp2000 = snpProp100*100/2000;
    double snpProp4000 = snpProp100*100/4000;
    double snpProp8000 = snpProp100*100/8000;
    double snpProp16000 = snpProp100*100/16000;
    double snpProp32000 = snpProp100*100/32000;
    double indelProp100 = 0.003;
    double indelProp250 = indelProp100*100/250;
    double indelProp350 = indelProp100*100/350;
    double indelProp500 = indelProp100*100/500;
    double indelProp750 = indelProp100*100/750;
    double indelProp1000 = indelProp100*100/1000;
    double indelProp1500 = indelProp100*100/1500;
    double indelProp2000 = indelProp100*100/2000;
    double indelProp4000 = indelProp100*100/4000;
    double indelProp8000 = indelProp100*100/8000;
    double indelProp16000 = indelProp100*100/16000;
    double indelProp32000 = indelProp100*100/32000;

    double falsePositionProb = 0.05;

    // Generate Genome or read from file
    logger.info("Generating genome ..");
    size_t genomeLength = 10000000;
    size_t indels = genomeLength/100;
    size_t indelLength = 6;
    Chromosome chrOrgn = gen.createChromosome(genomeLength);
    Chromosome chrRef(chrOrgn);

    time = timer.getCounter()/1000 -time;
    logger.info("Genome generation done in ", time, "ms");

    // Generate variant files
    logger.info("Generating variants ..");
    std::vector<Variant> varsRef = gen.generateIndels(chrRef, indels, indelLength);
    logger.info("1/12");
    std::vector<std::vector<Variant>> vars;
    for (unsigned int i = 0; i <= 10; i++) {
        vars.push_back(gen.generateIndels(chrRef, genomeLength*2*i/100, indelLength));
        logger.info((i+2), "/12");
    }
    time = timer.getCounter()/1000 -time;
    logger.info("Variant generation done in ", time, "ms");

    // Generate variant index
    logger.info("Generating variant indices ..");
    VariantIndex varIndexRef = VariantIndex(varsRef, seqan::length(chrRef));
    std::vector<VariantIndex> varIndexArray;
    for (unsigned int i = 0; i < vars.size(); i++) {
        varIndexArray.push_back(VariantIndex(vars[i], seqan::length(chrRef.data)));
    }
    time = timer.getCounter()/1000 -time;
    logger.info("Variant indices generation done in ", time, "ms");

    // Generate reads
    logger.info("Generating reads ..");
    std::vector<GenomeGenerator::Read> reads100 = gen.createReads(chrRef, readNumber100, 100, snpProp100, indelProp100);
    std::vector<GenomeGenerator::Read> reads100_e9 = gen.createReads(chrRef, readNumber100*10, 100, 9*snpProp100/3, 9*indelProp100/3);
    std::vector<GenomeGenerator::Read> reads250 = gen.createReads(chrRef, readNumber250, 250, snpProp250, indelProp250);
    std::vector<GenomeGenerator::Read> reads350 = gen.createReads(chrRef, readNumber350, 350, snpProp350, indelProp350);
    std::vector<GenomeGenerator::Read> reads500 = gen.createReads(chrRef, readNumber500, 500, snpProp500, indelProp500);
    std::vector<GenomeGenerator::Read> reads750 = gen.createReads(chrRef, readNumber750, 750, snpProp750, indelProp750);
    std::vector<GenomeGenerator::Read> reads1000 = gen.createReads(chrRef, readNumber1000, 1000, snpProp1000, indelProp1000);
    std::vector<GenomeGenerator::Read> reads1000_e6 = gen.createReads(chrRef, readNumber1000, 1000, 6*snpProp1000/3, 6*indelProp1000/3);
    std::vector<GenomeGenerator::Read> reads1500 = gen.createReads(chrRef, readNumber1500, 1500, snpProp1500, indelProp1500);
    std::vector<GenomeGenerator::Read> reads2000 = gen.createReads(chrRef, readNumber2000, 2000, snpProp2000, indelProp2000);
    std::vector<GenomeGenerator::Read> reads4000 = gen.createReads(chrRef, readNumber4000, 4000, snpProp4000, indelProp4000);
    std::vector<GenomeGenerator::Read> reads8000 = gen.createReads(chrRef, readNumber8000, 8000, snpProp8000, indelProp8000);
    std::vector<GenomeGenerator::Read> reads16000 = gen.createReads(chrRef, readNumber16000, 16000, snpProp16000, indelProp16000);
    std::vector<GenomeGenerator::Read> reads32000 = gen.createReads(chrRef, readNumber32000, 32000, snpProp32000, indelProp32000);
    std::vector<std::vector<GenomeGenerator::Read>> reads100_var;
    for (unsigned int i = 0; i <= 10; i++) {
        std::vector<GenomeGenerator::Read> varReads = gen.createReadsWithVariants(chrRef, varIndexArray[i], readNumber100, 100, .25, indelProp100, indelProp100, snpProp100, snpProp100);
        reads100_var.push_back(varReads);
    }

    time = timer.getCounter()/1000 -time;
    logger.info("Read generation done in ", time, "ms");

    // Generate aligner
    logger.info("Generating aligner ..");
    Aligner aligner(maxError100, 0);
    AlignerNoRowSkip alignerNoRowSkip(maxError100, 0);
    AlignerNaive alignerNaive(maxError100, 0);
    Alignment a;
    size_t found = 0;
    size_t foundNoRowSkip = 0;
    size_t foundNaive = 0;

    time = timer.getCounter()/1000 -time;
    logger.info("Alignment generation done in ", time, "ms");

    // Length scaling
    logger.info("Executing length scaling ..");
    found = 0;
    foundNoRowSkip = 0;
    foundNaive = 0;
    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    std::cout << "NEWDATA;LENGTH_SCALING (Length=100)"<<std::endl;
    #endif
    for (GenomeGenerator::Read &read : reads100) {
        size_t length = 100;
        size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
        if (gen.probability(falsePositionProb)) {
            startPos = (startPos + length*10) % genomeLength;
        }
        size_t endPos = std::min(genomeLength, startPos + length*2);
        a = aligner.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError100, 0);
        found += a.isValid();
        a = alignerNoRowSkip.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError100, 0);
        foundNoRowSkip += a.isValid();
        a = alignerNaive.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError100, 0);
        foundNaive += a.isValid();
    }
    logger.info("Reads of length 100 done");

    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    std::cout << "NEWDATA;LENGTH_SCALING (Length=250)"<<std::endl;
    #endif
    for (GenomeGenerator::Read &read : reads250) {
        size_t length = 250;
        size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
        if (gen.probability(falsePositionProb)) {
            startPos = (startPos + length*10) % genomeLength;
        }
        size_t endPos = std::min(genomeLength, startPos + length*2);
        a = aligner.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError250, 0);
        found += a.isValid();
        a = alignerNoRowSkip.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError250, 0);
        foundNoRowSkip += a.isValid();
        a = alignerNaive.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError250, 0);
        foundNaive += a.isValid();
    }
    logger.info("Reads of length 250 done");

    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    std::cout << "NEWDATA;LENGTH_SCALING (Length=350)"<<std::endl;
    #endif
    for (GenomeGenerator::Read &read : reads350) {
        size_t length = 350;
        size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
        if (gen.probability(falsePositionProb)) {
            startPos = (startPos + length*10) % genomeLength;
        }
        size_t endPos = std::min(genomeLength, startPos + length*2);
        a = aligner.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError350, 0);
        found += a.isValid();
        a = alignerNoRowSkip.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError350, 0);
        foundNoRowSkip += a.isValid();
        a = alignerNaive.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError350, 0);
        foundNaive += a.isValid();
    }
    logger.info("Reads of length 350 done");

    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    std::cout << "NEWDATA;LENGTH_SCALING (Length=500)"<<std::endl;
    #endif
    for (GenomeGenerator::Read &read : reads500) {
        size_t length = 500;
        size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
        if (gen.probability(falsePositionProb)) {
            startPos = (startPos + length*10) % genomeLength;
        }
        size_t endPos = std::min(genomeLength, startPos + length*2);
        a = aligner.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError500, 0);
        found += a.isValid();
        a = alignerNoRowSkip.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError500, 0);
        foundNoRowSkip += a.isValid();
        a = alignerNaive.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError500, 0);
        foundNaive += a.isValid();
    }
    logger.info("Reads of length 500 done");

    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    std::cout << "NEWDATA;LENGTH_SCALING (Length=750)"<<std::endl;
    #endif
    for (GenomeGenerator::Read &read : reads750) {
        size_t length = 750;
        size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
        if (gen.probability(falsePositionProb)) {
            startPos = (startPos + length*10) % genomeLength;
        }
        size_t endPos = std::min(genomeLength, startPos + length*2);
        a = aligner.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError750, 0);
        found += a.isValid();
        a = alignerNoRowSkip.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError750, 0);
        foundNoRowSkip += a.isValid();
        a = alignerNaive.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError750, 0);
        foundNaive += a.isValid();
    }
    logger.info("Reads of length 750 done");

    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    std::cout << "NEWDATA;LENGTH_SCALING (Length=1000)"<<std::endl;
    #endif
    for (GenomeGenerator::Read read : reads1000) {
        size_t length = 1000;
        size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
        if (gen.probability(falsePositionProb)) {
            startPos = (startPos + length*10) % genomeLength;
        }
        size_t endPos = std::min(genomeLength, startPos + length*2);
        a = aligner.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError1000, 0);
        found += a.isValid();
        a = alignerNoRowSkip.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError1000, 0);
        foundNoRowSkip += a.isValid();
        a = alignerNaive.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError1000, 0);
        foundNaive += a.isValid();
    }
    logger.info("Reads of length 1000 done");

    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    std::cout << "NEWDATA;LENGTH_SCALING (Length=1500)"<<std::endl;
    #endif
    for (GenomeGenerator::Read &read : reads1500) {
        size_t length = 1500;
        size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
        if (gen.probability(falsePositionProb)) {
            startPos = (startPos + length*10) % genomeLength;
        }
        size_t endPos = std::min(genomeLength, startPos + length*2);
        a = aligner.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError1500, 0);
        found += a.isValid();
        a = alignerNoRowSkip.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError1500, 0);
        foundNoRowSkip += a.isValid();
        a = alignerNaive.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError1500, 0);
        foundNaive += a.isValid();
    }
    logger.info("Reads of length 1500 done");

    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    std::cout << "NEWDATA;LENGTH_SCALING (Length=2000)"<<std::endl;
    #endif
    for (GenomeGenerator::Read read : reads2000) {
        size_t length = 2000;
        size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
        if (gen.probability(falsePositionProb)) {
            startPos = (startPos + length*10) % genomeLength;
        }
        size_t endPos = std::min(genomeLength, startPos + length*2);
        a = aligner.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError2000, 0);
        found += a.isValid();
        a = alignerNoRowSkip.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError2000, 0);
        foundNoRowSkip += a.isValid();
        a = alignerNaive.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError2000, 0);
        foundNaive += a.isValid();
    }
    logger.info("Reads of length 2000 done");

    #ifdef ALIGNER_USE_HASHING
    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    std::cout << "NEWDATA;LENGTH_SCALING (Length=4000)"<<std::endl;
    #endif
    for (GenomeGenerator::Read read : reads4000) {
        size_t length = 4000;
        size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
        if (gen.probability(falsePositionProb)) {
            startPos = (startPos + length*10) % genomeLength;
        }
        size_t endPos = std::min(genomeLength, startPos + length*2);
        a = aligner.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError4000, 0);
        found += a.isValid();
    }
    logger.info("Reads of length 4000 done");

    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    std::cout << "NEWDATA;LENGTH_SCALING (Length=8000)"<<std::endl;
    #endif
    for (GenomeGenerator::Read read : reads8000) {
        size_t length = 8000;
        size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
        if (gen.probability(falsePositionProb)) {
            startPos = (startPos + length*10) % genomeLength;
        }
        size_t endPos = std::min(genomeLength, startPos + length*2);
        a = aligner.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError8000, 0);
        found += a.isValid();
    }
    logger.info("Reads of length 8000 done");

    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    std::cout << "NEWDATA;LENGTH_SCALING (Length=16000)"<<std::endl;
    #endif
    for (GenomeGenerator::Read read : reads16000) {
        size_t length = 16000;
        size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
        if (gen.probability(falsePositionProb)) {
            startPos = (startPos + length*10) % genomeLength;
        }
        size_t endPos = std::min(genomeLength, startPos + length*2);
        a = aligner.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError16000, 0);
        found += a.isValid();
    }
    logger.info("Reads of length 16000 done");

    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    std::cout << "NEWDATA;LENGTH_SCALING (Length=32000)"<<std::endl;
    #endif
    for (GenomeGenerator::Read read : reads32000) {
        size_t length = 32000;
        size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
        if (gen.probability(falsePositionProb)) {
            startPos = (startPos + length*10) % genomeLength;
        }
        size_t endPos = std::min(genomeLength, startPos + length*2);
        a = aligner.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError32000, 0);
        found += a.isValid();
    }
    logger.info("Reads of length 32000 done");
    #endif

    logger.info("Found=", found, ";", foundNoRowSkip, ";", foundNaive);
    time = timer.getCounter()/1000 - time;
    logger.info("Length scaling done in ", time, "ms");

    // Error scaling
    logger.info("Executing error scaling ..");
    found = 0;
    foundNoRowSkip = 0;
    foundNaive = 0;
    for (unsigned int i = 0; i <= 10; i++) {
        #ifdef MEASURE_STATISTICS_IN_ALIGNER
        std::cout << "NEWDATA;ERROR_SCALING (Length=100, Error="<<i<<")"<<std::endl;
        #endif
        for (GenomeGenerator::Read &read : reads100) {
            size_t length = 100;
            size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
            if (gen.probability(falsePositionProb)) {
                startPos = (startPos + length*10) % genomeLength;
            }
            size_t endPos = std::min(genomeLength, startPos + length*2);
            a = aligner.align(chrRef, startPos, endPos, varIndexRef, read.readString, i, 0);
            found += a.isValid();
            a = alignerNoRowSkip.align(chrRef, startPos, endPos, varIndexRef, read.readString, i, 0);
            foundNoRowSkip += a.isValid();
        }
        logger.info("Reads of length 100 and ", i, " errors done");
    }

    for (unsigned int i = 0; i <= 20; i+=2) {
        #ifdef MEASURE_STATISTICS_IN_ALIGNER
        std::cout << "NEWDATA;ERROR_SCALING (Length=1000, Error="<<i<<")"<<std::endl;
        #endif
        for (GenomeGenerator::Read read : reads1000_e6) {
            size_t length = 1000;
            size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
            if (gen.probability(falsePositionProb)) {
                startPos = (startPos + length*10) % genomeLength;
            }
            size_t endPos = std::min(genomeLength, startPos + length*2);
            a = aligner.align(chrRef, startPos, endPos, varIndexRef, read.readString, i, 0);
            found += a.isValid();
            a = alignerNoRowSkip.align(chrRef, startPos, endPos, varIndexRef, read.readString, i, 0);
            foundNoRowSkip += a.isValid();
        }
        logger.info("Reads of length 1000 and ", i, " errors done");
    }
    logger.info("Found=", found, ";", foundNoRowSkip, ";", foundNaive);

    time = timer.getCounter()/1000 - time;
    logger.info("Error scaling done in ", time, "ms");

    // Variant scaling
    found = 0;
    logger.info("Executing variant scaling ..");
    for (unsigned int i = 0; i <=10; i++) {
        #ifdef MEASURE_STATISTICS_IN_ALIGNER
        std::cout << "NEWDATA;VARIANT_SCALING (Length=100, Density="<<(2*i)<<"%)"<<std::endl;
        #endif
        for (unsigned int j = 0; j < reads100_var[i].size(); j++) {            
            GenomeGenerator::Read &read = reads100_var[i][j];
            size_t length = 100;
            size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
            if (gen.probability(falsePositionProb)) {
                startPos = (startPos + length*10) % genomeLength;
            }
            size_t endPos = std::min(genomeLength, startPos + length*2);
            a = aligner.align(chrRef, startPos, endPos, varIndexArray[i], read.readString, maxError100, 0);
            found += a.isValid();
        }
        logger.info("Indel density of ",(2*i),"% done");
    }
    logger.info("Found=", found);

    time = timer.getCounter()/1000 - time;
    logger.info("Variant scaling done in ", time, "ms");

    // Clipping impact
    found = 0;
    logger.info("Executing clipping impact test ..");
    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    std::cout << "NEWDATA;CLIPPING_IMPACT (Length=100, ReadError=9)"<<std::endl;
    #endif
    for (unsigned int j = 0; j < reads100_e9.size(); j++) {
        GenomeGenerator::Read &read = reads100_e9[j];
        size_t length = 100;
        size_t startPos = (read.startPos)<(length/2)?0:(read.startPos - length/2);
        size_t endPos = std::min(genomeLength, read.startPos + length*3/2);
        a = aligner.align(chrRef, startPos, endPos, varIndexRef, read.readString, maxError100, 30);
        found += a.isValid();
    }
    logger.info("Found=", found);

    time = timer.getCounter()/1000 - time;
    logger.info("Clipping impact test done in ", time, "ms");

    return 0;
}
