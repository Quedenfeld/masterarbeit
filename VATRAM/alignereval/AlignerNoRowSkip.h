#ifndef ALIGNERNOROWSKIP_H
#define ALIGNERNOROWSKIP_H

#include "align/VariantIndex.h"
#include "align/Alignment.h"
#include "Chromosome.h"
#include "types.h"
#include "align/BacktracingMatrix.h"

#include <seqan/sequence.h>

#include <vector>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <functional>

using std::size_t;

class BacktracingMatrix;
class MatrixStorage;

/**
 * @brief Same as Aligner.h but without rowskip optimization.
 */
class AlignerNoRowSkip {
private:
    typedef seqan::Infix<const ReferenceString>::Type ReferenceInfix;
    typedef seqan::Infix<const ReadString>::Type ReadInfix;

    std::vector<Variant>::const_iterator variantIndexEnd;
    BacktracingMatrix b;
    ErrorCount maxError;
    ErrorCount maxClip;
    ErrorCount maxCurClipFront;
    ErrorCount maxCurClipBack;

    std::reference_wrapper<const ReadString> pattern;
    size_t patternLength;

    ReferenceInfix text;
    ReferenceInfix extendedText;
    size_t startPos;
    size_t endPos;

    RowIndex highestRow;
    ErrorCount bestMinimum;
    ColumnIndex bestMinimumPosition;

    RowIndex lastRowCurrent;
    RowIndex lastRowNext;
    size_t nextVarPos;

    std::vector<Variant>::const_iterator itVariant;
    std::vector<ColumnIndex> columns;
    std::vector<ColumnIndex> lastColumnOfVariant; // stored end position of each variant

    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    int numberOfHandledInsertions;
    int numberOfHandledDeletions;
    int numberOfComputedFields;
    int numberOfFoundAlignments;
    #endif

    Alignment align(const Chromosome &chromosome, size_t startPos, size_t endPos, const VariantIndex &variantIndex,
                    const ReadString &pattern, bool isInitialRun);

    void alignColumn(size_t refPos, size_t indelPos, const Variant &var, seqan::Iupac t, ColumnIndex previousColumn);

    void alignColumn(size_t refPos, size_t indelPos, const Variant &var, seqan::Iupac t);

    void processVariants(size_t refPos);

    ColumnIndex processInsertion(size_t refPos, const Variant &insertion, ColumnIndex columnBefore);

    ColumnIndex processDeletion(size_t refPos, const Variant &deletion, ColumnIndex columnBefore);

    ColumnIndex processSubstitution(size_t refPos, const Variant &substitution, ColumnIndex columnBefore);

    ColumnIndex alignInsertionString(size_t refPos, const Variant &var, ColumnIndex lastColumnBeforeVariants);

    /**
     * @brief Computes an alignment given a backtracing matrix and an end position.
     * @param position the end position, where the score was optimal
     * @return The alignment, which belongs the found optimum
     */
    Alignment backtrace(ColumnIndex position);

    /**
     * @brief printMatrix Prints the score, recDir, recColumns and nextRow matrices. Do only use for debugging purpose,
     * because this method is extremely slow (n*m runtime!).
     */
    void printMatrix();

public:
    /**
     * @brief Constructs a new semiglobal aligner.
     * @param maxError the maximum number of allowed errors for alignments
     */
    AlignerNoRowSkip(ErrorCount maxError, ErrorCount maxClip);

    /**
     * @brief align Computes all optimal alignments in an interval of the reference genome parameters.
     * @param chromosome the chromosome from the reference genome
     * @param startPos the start position of the interval
     * @param endPos the end position of the interval
     * @param variantIndex the index, which stores information of all variants
     * @param pattern the read to align
     * @return a vector, which contains optimal alignments
     */
    Alignment align(const Chromosome &chromosome, size_t startPos, size_t endPos, const VariantIndex &variantIndex,
                    const ReadString &pattern);

    /**
     * @brief align Computes all optimal alignments in an interval of the reference genome parameters using the specified
     * number of alignment errors. Implicitly sets the default number of alignment errors and clipped characters for
     * succeeding alignment runs to the specified value.
     * @param chromosome the chromosome from the reference genome
     * @param startPos the start position of the interval
     * @param endPos the end position of the interval
     * @param variantIndex the index, which stores information of all variants
     * @param pattern the read to align
     * @param maxError the maximum number of allowed errors for alignments
     * @param maxClip the maximum number clipped characters at beginning and end of read
     * @return a vector, which contains optimal alignments
     */
    Alignment align(const Chromosome &chromosome, size_t startPos, size_t endPos, const VariantIndex &variantIndex,
                    const ReadString &pattern, ErrorCount maxError, ErrorCount maxClip);

    /**
     * @brief charDistance Calculates the distance between the two characters with respect to the IUPAC alphabet.
     * @param p first character (usually from the pattern to match)
     * @param t second character (usually from the text to match)
     * @return 0 if characters do match, 1 else
     */
    inline ErrorCount charDistance(seqan::Iupac p, seqan::Iupac t) const;

    /**
     * @brief getMaxErrors Returns the maximum number of allowed errors for this aligner excluding any clipping.
     * @return the maximum number of allowed errors
     */
    ErrorCount getMaxErrors() const;

    /**
     * @brief getMaxClip Returns the maximum length of any prefix or suffix, which can be clipped from the read.
     * @return maximum number of clipped characters
     */
    ErrorCount getMaxClip() const;
};

#endif // PG583_ALIGN_ALIGNERNOROWSKIP_H
