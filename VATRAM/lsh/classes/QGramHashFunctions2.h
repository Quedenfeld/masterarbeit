#ifndef QGRAMHASHFUNCTIONS2_H
#define QGRAMHASHFUNCTIONS2_H

#include "map/LSH.h"

struct QGramHashFunction_m5 {
    typedef uint32_t QGramHash;
    QGramHash multiplicator = 5;
    template <class Iterator>
    QGramHash operator()(Iterator begin, Iterator end) {
        return std::accumulate(begin + 1, end, QGramHash(*begin), [this](QGramHash ret, unsigned char c) { return ret * multiplicator + c; });
    }
};

typedef LSH_Generic<uint32_t, uint32_t, CollisionFreeBHT<uint32_t>, QGramHashFunction_m5> LSH_qghf_m5;

struct QGramHashFunction_shift {
    typedef uint32_t QGramHash;
    template <class Iterator>
    QGramHash operator()(Iterator begin, Iterator end) {
        QGramHash result = *begin++;
        for (; begin < end; ++begin) {
            result = (result >> 30) ^ (result << 2) ^ unsigned(*begin);
        }
        return result;
    }
};

typedef LSH_Generic<uint32_t, uint32_t, CollisionFreeBHT<uint32_t>, QGramHashFunction_shift> LSH_qghf_shift;

template <class TQGramHash>
struct QGramHashFunction_shift_std {
    typedef TQGramHash QGramHash;
    std::hash<QGramHash> hash;
    template <class Iterator>
    QGramHash operator()(Iterator begin, Iterator end) {
        QGramHash result = *begin++;
        for (; begin < end; ++begin) {
            result = (result >> 30) ^ (result << 2) ^ unsigned(*begin);
        }
        return hash(result);
    }
};

typedef LSH_Generic<uint32_t, uint32_t, CollisionFreeBHT<uint32_t>, QGramHashFunction_shift_std<uint32_t>> LSH_qghf_shift_std;

struct QGramHashFunction_stdHash {
    typedef uint32_t QGramHash;
    std::hash<std::string> StringHashFunction;
    template <class Iterator>
    QGramHash operator()(Iterator begin, Iterator end) {
        std::string qGram(begin, end);
//        for (; begin < end; ++begin) {
//            qGram.push_back(*begin);
//        }
        return StringHashFunction(qGram);
    }
};

typedef LSH_Generic<uint32_t, uint32_t, CollisionFreeBHT<uint32_t>, QGramHashFunction_stdHash> LSH_qghf_stdHash;

struct GappedQGramHashFunction_sh {
    const unsigned char GAP = 0;
    const unsigned char VALUE = 1;
    std::vector<unsigned char> gapVector;

    GappedQGramHashFunction_sh(const std::string& str) {
        setGapVector(str);
    }
    GappedQGramHashFunction_sh(const std::vector<unsigned char>& gapVector) :
        gapVector(gapVector) {
    }

    typedef uint32_t QGramHash;
    template <class Iterator>
    QGramHash operator()(Iterator begin, Iterator end) {
        QGramHash result = *begin++;
        for (size_t i = 0; begin < end; ++begin, ++i) {
            result = gapVector[i] ?
                        (result >> 30) ^ (result << 2) ^ unsigned(*begin) //5 * result + QGramHash(*begin)
                      : result;
        }
        return result;
    }

    void setGapVector(std::string str) {
        gapVector.clear();
        for (char c: str) {
            unsigned char val;
            switch (c) {
            case '#': val = VALUE; break; // This is the common notation used in papers
            case '-': val = GAP; break;
            default: throw std::invalid_argument("setGapVector");
            }
            gapVector.push_back(val);
        }
    }
};

typedef LSH_Generic<uint32_t, uint32_t, CollisionFreeBHT<uint32_t>, GappedQGramHashFunction_sh> LSH_GappedQGram;

struct GappedQGramHashFunction_m5 : public GappedQGramHashFunction_sh {
    GappedQGramHashFunction_m5(const std::string& str) : GappedQGramHashFunction_sh(str) {}
    GappedQGramHashFunction_m5(const std::vector<unsigned char>& vec) : GappedQGramHashFunction_sh(vec) {}
    QGramHash multiplicator = 5;

    typedef uint32_t QGramHash;
    template <class Iterator>
    QGramHash operator()(Iterator begin, Iterator end) {
        QGramHash result = *begin++;
        for (size_t i = 0; begin < end; ++begin, ++i) {
            result = gapVector[i] ?
                        multiplicator * result + QGramHash(*begin)
                      : result;
        }
        return result;
    }

};

typedef LSH_Generic<uint32_t, uint32_t, CollisionFreeBHT<uint32_t>, GappedQGramHashFunction_m5> LSH_GappedQGram_m5;

struct GappedQGramHashFunction_std : public GappedQGramHashFunction_sh {
    GappedQGramHashFunction_std(const std::string& str) : GappedQGramHashFunction_sh(str) {}
    GappedQGramHashFunction_std(const std::vector<unsigned char>& vec) : GappedQGramHashFunction_sh(vec) {}
    std::hash<std::string> StringHashFunction;

    typedef uint32_t QGramHash;
    template <class Iterator>
    QGramHash operator()(Iterator begin, Iterator end) {
        std::string qGram(begin, end);
        return StringHashFunction(qGram);
    }

};

typedef LSH_Generic<uint32_t, uint32_t, CollisionFreeBHT<uint32_t>, GappedQGramHashFunction_std> LSH_GappedQGram_std;


#endif // QGRAMHASHFUNCTIONS2_H
