#ifndef READLENGTHEXP_H
#define READLENGTHEXP_H

#include "experiments/VaryParameters.h"
#include "Experiments.h"
#include "GenomeGenerator.h"
#include "classes/QGramHashFunctions2.h"
#include "classes/BandHashFunctions.h"
#include "MultiWindowBucketAnalysis.h"
#include "SpeedTest.h"
#include "io/VariantsReader.h"
#include "map/LSHimpl.h"
#include "GenomeGenerator.h"
#include "ReadGenerator.h"
#include "io/ReferenceReader.h"
#include "VariantGenerator.h"
#include "map/LSHio.h"

#include <iostream>
#include <ctime>
#include <iomanip>

#ifdef PROFILING
#include "gperftools/profiler.h"
#endif

std::string getTimeStr() {
    auto now = std::chrono::system_clock::now();
    time_t now_c = std::chrono::system_clock::to_time_t(now);
    std::string str(ctime(&now_c));
    if (str.back() == '\n') str = str.substr(0, str.length() - 1);
    return str;
}



template <class TLsh>
void resetLSH_readLngPara_single(TLsh& lsh, size_t readLng) {
    lsh.windowSize = LSH::DEFAULT_WINDOW_SIZE * readLng / 100.;
    lsh.windowOffset = LSH::DEFAULT_WINDOW_OFFSET * readLng / 100.;
    const double defaultMemory = calcMemory(100, LSH::DEFAULT_SIGNATURE_LENGTH);
    lsh.signatureLength = calcSignatureLength(readLng, defaultMemory);// LSH::DEFAULT_SIGNATURE_LENGTH * readLng / 100.; // TODOJQ
    lsh._mapOptions.pairedEndDistanceMin = LshMapOptions::DEFAULT_PAIRED_END_DISTANCE_MIN * readLng / 100.;
    lsh._mapOptions.pairedEndDistanceMax = LshMapOptions::DEFAULT_PAIRED_END_DISTANCE_MAX * readLng / 100.;
}

template <class TLsh>
void resetLSH_readLngPara_long(TLsh& lsh) {
    lsh.windowSize = LSH::DEFAULT_WINDOW_SIZE;
    lsh.windowOffset = LSH::DEFAULT_WINDOW_OFFSET;
    lsh.signatureLength = LSH::DEFAULT_SIGNATURE_LENGTH;
}

template <class TLsh>
void resetLSH_otherPara(TLsh& lsh) {
    lsh.qGramSize = LSH::DEFAULT_QGRAM_SIZE;
    lsh.bandSize = LSH::DEFAULT_BAND_SIZE;
    lsh.limit = LSH::DEFAULT_LIMIT;
    lsh.limit_skip = LSH::DEFAULT_LIMIT_SKIP;
    lsh._mapOptions.minCountSelection = LshMapOptions::DEFAULT_MIN_COUNT_SELECTION;
    lsh._mapOptions.minCountPrefiltering = LshMapOptions::DEFAULT_MIN_COUNT_PREFILTERING;
    lsh._mapOptions.maxSelect = LshMapOptions::DEFAULT_MAX_SELECT;
    lsh._mapOptions.nextFactor = LshMapOptions::DEFAULT_NEXT_FACTOR;
    lsh._mapOptions.contractBandMultiplicator = LshMapOptions::DEFAULT_CONTRACT_BAND_MULTIPLICATOR;
    lsh._mapOptions.extendBandMultiplicator = LshMapOptions::DEFAULT_EXTEND_BAND_MULTIPLICATOR;
    lsh._mapOptions.extendFactor = LshMapOptions::DEFAULT_EXTEND_FACTOR;
    lsh._mapOptions.maximumOfReturnedWindows = LshMapOptions::DEFAULT_MAXIMUM_OF_RETURNED_WINDOWS;
    lsh._mapOptions.longReadSplitLength = LshMapOptions::DEFAULT_LONG_READ_SPLIT_LENGTH;
    lsh._mapOptions.longReadSplitFactor = LshMapOptions::DEFAULT_LONG_READ_SPLIT_FACTOR;

}

struct AlignerOptionsFactor {
    double maxError_lowFactor;
    double maxClip_lowFactor;

    double maxError_highFactor;
    double maxClip_highFactor;

    size_t maxSelect;
    double nextFactor;

    AlignerOptionsFactor(double maxError_lowFactor, double maxClip_lowFactor, double maxError_highFactor, double maxClip_highFactor, size_t maxSelect, double nextFactor) :
        maxError_lowFactor(maxError_lowFactor),
        maxClip_lowFactor(maxClip_lowFactor),
        maxError_highFactor(maxError_highFactor),
        maxClip_highFactor(maxClip_highFactor),
        maxSelect(maxSelect),
        nextFactor(nextFactor) {
    }

    AlignerOptions createAlignerOptions(size_t readLng) {
        return AlignerOptions(maxError_lowFactor * readLng,
                              maxClip_lowFactor * readLng,
                              maxError_highFactor * readLng,
                              maxClip_highFactor * readLng,
                              maxSelect,
                              nextFactor);
    }

    friend inline
    std::ostream& operator<<(std::ostream& strm, const AlignerOptionsFactor& aof) {
        return strm << aof.maxError_lowFactor << ";" << aof.maxClip_lowFactor << ";" << aof.maxError_highFactor << ";" << aof.maxClip_highFactor << ";" << aof.maxSelect << ";" << aof.nextFactor;
    }
};

struct DEFAULT_ALIGNEROPTIONS {
    constexpr static double MAX_ERROR_LOW = 0.04;
    constexpr static double MAX_CLIP_LOW = 0;
    constexpr static double MAX_ERROR_HIGH = 0.10;
    constexpr static double MAX_CLIP_HIGH = 0.0;
    constexpr static double MAX_SELECT = LshMapOptions::DEFAULT_MAX_SELECT / 4;
    constexpr static double NEXT_FACTOR = LshMapOptions::DEFAULT_NEXT_FACTOR / 2;
};



template <class TLsh>
void resetExp_alignerOptions(Experiment<TLsh>& e) {
    AlignerOptions& ao = e.alignerOptions;
    ao.maxError_low = DEFAULT_ALIGNEROPTIONS::MAX_ERROR_LOW * e.currentReadLength;
    ao.maxClip_low = DEFAULT_ALIGNEROPTIONS::MAX_CLIP_LOW * e.currentReadLength;
    ao.maxError_high = DEFAULT_ALIGNEROPTIONS::MAX_ERROR_HIGH * e.currentReadLength;
    ao.maxClip_high = DEFAULT_ALIGNEROPTIONS::MAX_CLIP_HIGH * e.currentReadLength;
    ao.maxSelect = DEFAULT_ALIGNEROPTIONS::MAX_SELECT;
    ao.nextFactor = DEFAULT_ALIGNEROPTIONS::NEXT_FACTOR;
}

template <class TLsh>
void resetExp_single(Experiment<TLsh>& e) {
    resetLSH_readLngPara_single(e.lsh, e.currentReadLength);
    resetLSH_otherPara(e.lsh);
    resetExp_alignerOptions(e);
}

template <class TLsh>
void resetExp_long(Experiment<TLsh>& e) {
    resetLSH_readLngPara_long(e.lsh);
    resetLSH_otherPara(e.lsh);
    resetExp_alignerOptions(e);
}

struct LSHConfig {
    int cfg;
    int qGramSize;
    std::string name;
    LSHConfig(int cfg, int qGramSize, const std::string& name) : cfg(cfg), qGramSize(qGramSize), name(name) {}
    friend std::ostream& operator<<(std::ostream& strm, const LSHConfig& c) {
        return strm << c.name;
    }
};

struct LongConfig {
    int cfg;
    size_t splitLength;
    double splitFactor;
    std::string name;
    LongConfig(int cfg, size_t splitLength, double splitFactor, const std::string& name) : cfg(cfg), splitLength(splitLength), splitFactor(splitFactor), name(name) {}
    friend std::ostream& operator<<(std::ostream& strm, const LongConfig& c) {
        return strm << c.name;
    }
};

struct GapConfig {
    size_t min;
    size_t max;
    GapConfig(size_t min, size_t max) : min(min), max(max) {}
    friend std::ostream& operator<<(std::ostream& strm, const GapConfig& c) {
        return strm << c.min << ";" << c.max;
    }
};

void runReadLength(std::ostream& out,
                   Genome& genome,
                   const std::vector<ReadSet>& readSets,
                   uint64_t seed,
                   const std::vector<std::string>& varPaths,
                   bool enableLongReads,
                   bool pairedEnd,
                   bool skipSome,
                   bool debug = true,
                   int configValue = 0) {
    bool containsVariants = varPaths.size() > 0;
    double alignProbability = 0.1;

    std::cout << "Initilaize, " << getTimeStr() << std::endl;
    LSH lsh(seed);
    typedef Experiment<LSH> Exp;
    Exp exp(genome, lsh, enableLongReads ? resetExp_long<LSH> : resetExp_single<LSH>, out);
    for (const ReadSet& rs: readSets) {
        exp.addReadSet(rs);
    }
    exp.printLSHParameters(out, exp.readSets[0].length);

    std::cout << "Experiments, " << getTimeStr() << std::endl;

    if (pairedEnd) {
        std::cout << "MinCount (Pre), " << getTimeStr() << std::endl;
        std::vector<MinCountConfig> v;
        for (unsigned selection: {1,2,3,4,5,6}) {
            for (unsigned prefiltering: {1,2,3,4,5,6}) {
                v.push_back(MinCountConfig{selection, prefiltering});
            }
        }
        auto set = [](Exp& e, MinCountConfig mcc) {e.lsh._mapOptions.minCountSelection = mcc.minCountSelection;
                                                   e.lsh._mapOptions.minCountPrefiltering = mcc.minCountPrefiltering;};
        exp.varyParameter<MinCountConfig>("min count (selection);min count (prefiltering)", v, set, "\nMIN-COUNT\n", alignProbability, ParameterType::Query);
        return;
    }
    if (configValue == 8) {
        std::cout << "MinCount, " << getTimeStr() << std::endl;
        std::vector<unsigned> v = {1,2,3,4,5,6};
        auto set = [](Exp& e, unsigned mc) {e.lsh._mapOptions.minCountSelection = mc; e.lsh._mapOptions.minCountPrefiltering = mc/2; /*std::cerr << ">>> SET-MINCOUNT = " << mc << " <<<" << std::endl;*/};
        exp.varyParameter<unsigned>("min count", v, set, "\nMIN-COUNT\n", alignProbability, ParameterType::Query);
    }
    if (configValue == 7) {
        std::cout << "Default, " << getTimeStr() << std::endl;
        out << "ORIGINAL\n";
        out << RUN_EXPERIMENT_HEADLINE;
        exp.runExperiment(alignProbability);
        return;
    }
    if (false) {
        std::cout << "Load/Save, " << getTimeStr() << std::endl;

        out << "LOAD/SAVE\n";
        out << "io;time;" << RUN_EXPERIMENT_HEADLINE;
        LshIO io;
        std::string filepath("/scratch/JQ/lsh.bin");

        ReadSetReference& rsr = exp.readSets.front();
        exp.currentReadLength = rsr.length;
        exp.clearAfter(exp);

        out << "new index;;";
        exp.runExperiment(*rsr.reads, rsr.length, alignProbability, ParameterType::Index);
        out << std::endl;

        Timer<> t;
        io.write(lsh, filepath);
        t.stopTimer();

        out << "write;";
        out << t.getDuration().count() << ";";
        exp.runExperiment(*rsr.reads, rsr.length, alignProbability, ParameterType::Query);
        out << std::endl;

        t.resetTimer();
        io.read(filepath, lsh, genome);
        t.stopTimer();

        out << "read;";
        out << t.getDuration().count() << ";";
        exp.runExperiment(*rsr.reads, rsr.length, alignProbability, ParameterType::Query);
        out << std::endl;
    }



    // =======================================================================
    // DEFAULT
    // =======================================================================

    if (false) {
        std::cout << "Q-gram-Time, " << getTimeStr() << std::endl;
#ifdef PROFILING
        ProfilerStart("./lsh_buildindex.prof");
#endif
        exp.buildIndex();
#ifdef PROFILING
        ProfilerStop();
#endif
        LSH& lsh = exp.lsh;
        uint64_t doNotOptimize = 0;
        out << "\nQ-GRAM-CALCTIME\n";
        out << "method;time\n";
        Timer<> timer;
        #pragma omp parallel for
        for (size_t i = 0; i < lsh.referenceWindows.size(); i++) {
            typename LSH::ReferenceWindow& window = lsh.referenceWindows[i];
            seqan::String<ReferenceChar> seq = seqan::infixWithLength(lsh.chromosomes[window.chromosomeIndex].get().data, window.beginPosition, lsh.windowSize);
            std::vector<typename LSH::QGramHash> qGramHashes;
            lsh.createQGramHashes_iupac(seqan::begin(seq), seqan::end(seq), qGramHashes);
            doNotOptimize += qGramHashes.size();
        }
        out << "lsh-like;" << timer.stopTimer().count() << std::endl;
        std::cout << "    do-not-optimize: " << doNotOptimize << std::endl;

#ifdef PROFILING
        ProfilerStart("./lsh_complete.prof");
#endif
        exp.lsh.clear();
        exp.currentReadLength = exp.readSets.front().length;
        exp.clearAfter(exp);
        exp.runExperiment(*exp.readSets.front().reads, exp.readSets.front().length, alignProbability, ParameterType::Index);
#ifdef PROFILING
        ProfilerStop();
#endif

    }


    if (configValue == 5) {
        std::cout << "Superrank, " << getTimeStr() << std::endl;
        out << "SUPERRANK\n";
        out << "block size; " << RUN_EXPERIMENT_HEADLINE;

//        exp.runExperiment(alignProbability, ParameterType::Index, "64-Bit-Block;");
        exp.lsh.clear();
        exp.varIdxs.clear();
        {
            typedef LSH_Generic<uint32_t, uint64_t, SuperRank<uint32_t, 64, 64>, GappedQGramHashFunction<uint64_t>> LSH_SR64;
            typedef Experiment<LSH_SR64> Exp_SR64;
            LSH_SR64 lsh2(seed);
            Exp_SR64 exp2(genome, lsh2, enableLongReads ? resetExp_long<LSH_SR64> : resetExp_single<LSH_SR64>, out);
            for (const ReadSet& rs: readSets) {
                exp2.addReadSet(rs);
            }
            exp2.runExperiment(alignProbability, ParameterType::Index, "64-Bit-Block;");
        }
        {
            typedef LSH_Generic<uint32_t, uint64_t, SuperRank<uint32_t, 64, 32>, GappedQGramHashFunction<uint64_t>> LSH_SR32;
            typedef Experiment<LSH_SR32> Exp_SR32;
            LSH_SR32 lsh2(seed);
            Exp_SR32 exp2(genome, lsh2, enableLongReads ? resetExp_long<LSH_SR32> : resetExp_single<LSH_SR32>, out);
            for (const ReadSet& rs: readSets) {
                exp2.addReadSet(rs);
            }
            exp2.runExperiment(alignProbability, ParameterType::Index, "32-Bit-Block;");
        }
        {
            typedef LSH_Generic<uint32_t, uint64_t, SuperRank<uint32_t, 64, 16>, GappedQGramHashFunction<uint64_t>> LSH_SR16;
            typedef Experiment<LSH_SR16> Exp_SR16;
            LSH_SR16 lsh2(seed);
            Exp_SR16 exp2(genome, lsh2, enableLongReads ? resetExp_long<LSH_SR16> : resetExp_single<LSH_SR16>, out);
            for (const ReadSet& rs: readSets) {
                exp2.addReadSet(rs);
            }
            exp2.runExperiment(alignProbability, ParameterType::Index, "16-Bit-Block;");
        }
        {
            typedef LSH_Generic<uint32_t, uint64_t, SuperRank<uint32_t, 64, 8>, GappedQGramHashFunction<uint64_t>> LSH_SR8;
            typedef Experiment<LSH_SR8> Exp_SR8;
            LSH_SR8 lsh2(seed);
            Exp_SR8 exp2(genome, lsh2, enableLongReads ? resetExp_long<LSH_SR8> : resetExp_single<LSH_SR8>, out);
            for (const ReadSet& rs: readSets) {
                exp2.addReadSet(rs);
            }
            exp2.runExperiment(alignProbability, ParameterType::Index, "8-Bit-Block;");
        }

        exp.createVariantIndexes();
    }


    // ---
    if (configValue == 9){ // long reads
        std::cout << "WindowSize, " << getTimeStr() << std::endl;
        std::vector<double> v = {100,125,150,175,200,250,300,350,400,500,600,700,800,1000,1200};
        const double defaultMemory = calcMemory(LSH::DEFAULT_READ_LENGTH, LSH::DEFAULT_SIGNATURE_LENGTH);
        auto set = [defaultMemory](Exp& e, double rlng) {
            e.lsh.windowSize = std::round(1.40 * rlng);
            e.lsh.windowOffset = std::round(1.25 * rlng);
            e.lsh.signatureLength = calcSignatureLength(rlng, defaultMemory);
            e.lsh._mapOptions.longReadSplitLength = std::round(rlng);
        };
        exp.varyParameter<double>("Seeming read length", v, set, "\nLONG-WINDOW-SIZE\n", alignProbability, ParameterType::Index);
    }
    if (configValue == 6) {
        std::cout << "MinCount, " << getTimeStr() << std::endl;
        std::vector<unsigned> v = {1,2,3,4,5,6,7,8};
        for (double sigFactor: {0.25, 0.5, 0.7, 1.0}) {
            auto set = [sigFactor](Exp& e, unsigned mc) {
                e.lsh._mapOptions.minCountSelection = mc;
                e.lsh._mapOptions.minCountPrefiltering = mc/2;
                const double defaultMemory = calcMemory(100, LSH::DEFAULT_SIGNATURE_LENGTH);
                e.lsh.signatureLength = sigFactor * calcSignatureLength(e.currentReadLength, defaultMemory);
            };
            exp.varyParameter<unsigned>("min count", v, set, std::string("\nMIN-COUNT: SIGLNG-FACTOR=") + std::to_string(sigFactor) + "\n", alignProbability, ParameterType::Query);
        }

    }
    if (configValue == 0) {
        if (pairedEnd) {
            std::cout << "MinCount (Pre), " << getTimeStr() << std::endl;
            std::vector<MinCountConfig> v;
            for (unsigned selection: {1,2,3,4,5,6}) {
                for (unsigned prefiltering: {1,2,3}) {
                    v.push_back(MinCountConfig{selection, prefiltering});
                }
            }
            auto set = [](Exp& e, MinCountConfig mcc) {e.lsh._mapOptions.minCountSelection = mcc.minCountSelection;
                                                       e.lsh._mapOptions.minCountPrefiltering = mcc.minCountPrefiltering;};
            exp.varyParameter<MinCountConfig>("min count (selection);min count (prefiltering)", v, set, "\nMIN-COUNT\n", alignProbability, ParameterType::Query);
        } else  {
//            exp.extraInfoForMinCount = true;
//            exp.multiThreading = false;
            std::cout << "MinCount, " << getTimeStr() << std::endl;
            std::vector<unsigned> v = {1,2,3,4,5,6};
            auto set = [](Exp& e, unsigned mc) {e.lsh._mapOptions.minCountSelection = mc; e.lsh._mapOptions.minCountPrefiltering = mc/2; /*std::cerr << ">>> SET-MINCOUNT = " << mc << " <<<" << std::endl;*/};
            exp.varyParameter<unsigned>("min count", v, set, "\nMIN-COUNT\n", alignProbability, ParameterType::Query);
//            auto set2 = [](Exp& e, unsigned mc) {e.lsh._mapOptions.minCountSelection = mc; e.lsh._mapOptions.minCountPrefiltering = mc/2; e.lsh._mapOptions.contractBandMultiplicator = 0; std::cerr << ">>> SET-MINCOUNT = " << mc << " <<<" << std::endl;};
//            exp.varyParameter<unsigned>("min count", v, set2, "\nMIN-COUNT\n", alignProbability, ParameterType::UseLastIndex);
//            exp.extraInfoForMinCount = false;
//            exp.multiThreading = true;
        }
    }
    if (configValue == 1) {
        std::cout << "Limit, " << getTimeStr() << std::endl;
        uint64_t oldSeed = exp.lsh.initialRandomSeed;
        uint64_t baseSeed = 1445088900; // last experiment!!
        for (uint64_t seedOffset: {/*0, 1,*/ 2}) {
            std::vector<LimitConfig> v;
            for (unsigned limit: {1, 2, 3, 5, 7, 10}) {
                for (unsigned limitSkip: {1, 2, 3, 5, 7, 10, 15, 20}) {
                    if (limit <= limitSkip) {
                        v.push_back(LimitConfig{limit, limitSkip});
                    }
                }
            }
            auto set = [](Exp& e, LimitConfig lc) {
                e.lsh.limit = lc.limit;
                e.lsh.limit_skip = lc.limitSkip;
            };

            exp.lsh.initialRandomSeed = baseSeed + seedOffset;
            out << "==== SEED: " << exp.lsh.initialRandomSeed << " ====\n";
            exp.varyParameter<LimitConfig>("limit; limit-skip", v, set, "\nLIMIT & LIMIT-SKIP\n", alignProbability, ParameterType::Index);
        }
        exp.lsh.initialRandomSeed = oldSeed;
    }

    if (configValue == 2) {
        std::cout << "Offset+Overlap, " << getTimeStr() << std::endl;
        std::vector<WindowBandsFactorConfig> v;

        for (double offset: {0.9, 1.00, 1.1, 1.25, 1.5, 2., 3., 5.}) {
            for (double overlap: {.0, 0.05, 0.1, .15, 0.2, .25, 0.3, .35, 0.4, 0.45, .50, 0.6, 0.7, 0.8, 0.9, 1.0}) {
                v.push_back(WindowBandsFactorConfig{offset + overlap, offset}); // always use the same amount of memory!
            }
        }
        const double defaultMemory = calcMemory(100, LSH::DEFAULT_SIGNATURE_LENGTH);
        auto set = [defaultMemory](Exp& e, WindowBandsFactorConfig wb) {
            e.lsh.windowSize = std::round(wb.windowSizeFactor * e.currentReadLength);
            e.lsh.windowOffset = std::round(wb.windowOffsetFactor * e.currentReadLength);
            double seemingRlng = e.lsh.windowOffset / (double) (LSH::DEFAULT_WINDOW_OFFSET / (double) LSH::DEFAULT_READ_LENGTH);
            e.lsh.signatureLength = calcSignatureLength(seemingRlng, defaultMemory);
        };
        exp.varyParameter<WindowBandsFactorConfig>("overlap; window offset factor; window size factor", v, set, "\nWINDOW-SIZE & OVERLAPPING\n", alignProbability, ParameterType::Index);
    }

    if (debug) return;
    {
        std::cout << "Default, " << getTimeStr() << std::endl;
        out << "ORIGINAL\n";
        out << RUN_EXPERIMENT_HEADLINE;
        exp.runExperiment(alignProbability);
    }
    {
        auto set = [](Exp& e, double f) {e.lsh.signatureLength = e.currentReadLength * f;};
        std::vector<double> v;
        v.push_back(LSH::DEFAULT_SIGNATURE_LENGTH / 100.0);
        exp.varyParameter<double>("const-memory", v, set, "\nCONST MEMORY", alignProbability, ParameterType::Index);
    }


    // =======================================================================
    // CONFIG
    // =======================================================================
    if (enableLongReads) {
        std::cout << "Config (Long), " << getTimeStr() << std::endl;
        std::vector<LongConfig> v;
        for (double factor: {0.5, 0.6, 0.7, 0.8, 0.9}) {
            for (size_t length: {50, 60, 70, 80, 90, 100}) {
                std::string str = std::to_string(factor) + ";" + std::to_string(length);
                v.emplace_back(LSH_PE_FOR | LSH_PE_MEAN, length, factor, str + ";keepLength");
                v.emplace_back(LSH_PE_FOR | LSH_PE_MEAN | LSH_SPLIT_EQUAL, length, factor, str + ";splitEqual");
            }
        }

        auto set = [](Exp& e, LongConfig cfg) {
            global_LSH = cfg.cfg;
            e.lsh._mapOptions.longReadSplitLength = cfg.splitLength;
            e.lsh._mapOptions.longReadSplitFactor = cfg.splitFactor;
        };
        exp.varyParameter<LongConfig>("splitFactor;splitLength;config", v, set, "\nLONG-CONFIG\n", alignProbability, ParameterType::Query);

        global_LSH = LSH_PE_FOR | LSH_PE_MEAN;
    }
    if (pairedEnd) if (!skipSome) {

        std::cout << "Config, " << getTimeStr() << std::endl;
        std::vector<LSHConfig> v;
        for (int q: {15,16,17}) {
            v.emplace_back(LSH_PE_FOR | LSH_PE_PLUS, q, std::string("PE: plus;") + std::to_string(q));
            v.emplace_back(LSH_PE_FOR | LSH_PE_PLUS_D2, q, std::string("PE: plus-div-2;") + std::to_string(q));
            v.emplace_back(LSH_PE_FOR | LSH_PE_MEAN, q, std::string("PE: mean;") + std::to_string(q));
            v.emplace_back(LSH_PE_FOR | LSH_PE_MEAN | LSH_KTRS_1, q, std::string("KTRS (multi hash);") + std::to_string(q));
            v.emplace_back(LSH_PE_FOR | LSH_PE_MEAN | LSH_KTRS_2, q, std::string("KTRS (div, separate);") + std::to_string(q));
            v.emplace_back(LSH_PE_FOR | LSH_PE_MEAN | LSH_KTRS_1 | LSH_KTRS_2, q, std::string("KTRS (multi hash, separate);") + std::to_string(q));
        }

        auto set = [](Exp& e, LSHConfig cfg) {
            e.lsh.qGramSize = cfg.qGramSize;
            global_LSH = cfg.cfg;
        };
        exp.varyParameter<LSHConfig>("config;q", v, set, "\nCONFIG\n", alignProbability, ParameterType::Index);

        global_LSH = LSH_PE_FOR | LSH_PE_MEAN;
    }
    if (false) { // KTRS

        std::cout << "Config, " << getTimeStr() << std::endl;
        std::vector<LSHConfig> v;
        for (int q: {17,17,17}) {
//            v.emplace_back(LSH_PE_FOR | LSH_PE_PLUS, q, std::string("PE: plus;") + std::to_string(q));
//            v.emplace_back(LSH_PE_FOR | LSH_PE_PLUS_D2, q, std::string("PE: plus-div-2;") + std::to_string(q));
            v.emplace_back(LSH_PE_FOR | LSH_PE_MEAN, q, std::string("PE: mean;") + std::to_string(q));
//            v.emplace_back(LSH_PE_FOR | LSH_PE_MEAN | LSH_KTRS_1, q, std::string("KTRS (multi hash);") + std::to_string(q));
//            v.emplace_back(LSH_PE_FOR | LSH_PE_MEAN | LSH_KTRS_2, q, std::string("KTRS (div, separate);") + std::to_string(q));
            v.emplace_back(LSH_PE_FOR | LSH_PE_MEAN | LSH_KTRS_1 | LSH_KTRS_2, q, std::string("KTRS (multi hash, separate);") + std::to_string(q));
        }

        auto set = [](Exp& e, LSHConfig cfg) {
            e.lsh.qGramSize = cfg.qGramSize;
            global_LSH = cfg.cfg;
        };
        exp.varyParameter<LSHConfig>("config;q", v, set, "\nCONFIG\n", alignProbability, ParameterType::Index);

        global_LSH = LSH_PE_FOR | LSH_PE_MEAN | LSH_SPLIT_EQUAL;
    }

    // =======================================================================
    // Paired-End-Gap
    // =======================================================================
    if (pairedEnd) {
        std::vector<GapConfig> v;
        for (size_t mean: {250, 350, 450}) {
            for (size_t size: {0, 50, 100, 150, 250, 1000}) {
                v.emplace_back(mean-size, mean+size);
            }
        }

        auto set = [](Exp& e, GapConfig c) {
            e.lsh._mapOptions.pairedEndDistanceMin = c.min;
            e.lsh._mapOptions.pairedEndDistanceMax = c.max;
        };

        exp.varyParameter<GapConfig>("min-pe-distance;max-pe-distance", v, set, "\nPE-DISTANCE\n", alignProbability, ParameterType::Query);
    }


    // =======================================================================
    // MAX-ERROR
    // =======================================================================
    if (!skipSome) {
        std::cout << "AlignerOptions, " << getTimeStr() << std::endl;
        std::vector<AlignerOptionsFactor> v;
        for (double d: {0.00, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08}) {
            v.emplace_back(d, 0, 0.1, 0., 16, 2);
        }

        for (double d: {0.08, 0.09, 0.10, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16}) {
            v.emplace_back(0.04, 0, d, 0., 16, 2);
        }

        for (size_t x: {0, 1, 2, 3, 4, 8, 16, 32, 64}) {
            v.emplace_back(0.04, 0, 0.1, 0., x, 2);
        }

        for (double d: {1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5}) {
            v.emplace_back(0.04, 0, 0.1, 0., 16, d);
        }
        auto set = [](Exp& e, AlignerOptionsFactor aof){e.alignerOptions = aof.createAlignerOptions(e.currentReadLength);};
        exp.varyParameter<AlignerOptionsFactor>("max-error-low;max-clip-low;max-error-high;max-clip-high;max-select;next-factor", v, set, "\nALIGNER-OPTIONS\n", 1.0, ParameterType::Query);
    }

    // =======================================================================
    // VARIANT PROBABILITY THRESHOLD
    // =======================================================================
    {
        std::cout << "Variant histogram, " << getTimeStr() << std::endl;
        std::vector<size_t> varCount(101, 0);
        auto func = [&varCount](Chromosome&, size_t,
                const seqan::Infix<const seqan::CharString&>::Type& infixRef,
                const seqan::Infix<seqan::CharString&>::Type& infixAlt,
                double probability) {
            if (seqan::length(infixRef) == seqan::length(infixAlt)) {
                int p = probability * 100;
                varCount[p]++;
            }
        };
        VariantsReader vr(genome, std::unordered_set<std::string>());
        vr.readVariants(varPaths, func);

        out << "\nSNP-COUNT\n";
        out << "prob;count\n";
        for (size_t i = 0; i < varCount.size(); i++) {
            out << i << ";" << varCount[i] << "\n";
        }
        out << std::flush;
    }
    if (containsVariants) if (!skipSome) {
        std::cout << "Variant threshold, " << getTimeStr() << std::endl;
        std::vector<double> v = {0.0, 0.01, 0.02, 0.03, 0.05, 0.07, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.6, 0.7, 0.8, 0.9, 1};
        Genome genomeCopy = genome;
        auto set = [&varPaths, &genome](Exp& e, double pt) {
            for (Chromosome& chr: genome) {
                chr.data = chr.noVariantsString;
                chr.variants.clear();
            }
            VariantsReader vr(genome, pt);
            vr.readVariants(varPaths);
            e.createVariantIndexes();
        };
        exp.varyParameter<double>("variant probability threshold", v, set, "\nVARIANT-PROBABILITY-THRESHOLD\n", alignProbability, ParameterType::Index);
        exp.lsh.clear();
        genome = genomeCopy;
        exp.createVariantIndexes();
//        std::swap(genome, genomeCopy); // restore old genome

    }

    // =======================================================================
    // Q-GRAMS
    // =======================================================================
    if (!skipSome) {
        std::cout << "Q-Gram (mem-const)" << getTimeStr() << std::endl;
        std::vector<unsigned> vq = {12,  13,  14,  15,  16,  17,  18,  19,  20,   21,   22,   23,   24,   25};
        std::vector<double> vmem = {4.15,4.71,5.65,6.57,7.55,8.72,9.61,10.1,10.34,10.49,10.58,10.65,10.71,10.77};
        double memDefault = 8.72;
        std::vector<size_t> v;
        for (size_t i = 0; i < vq.size(); i++) {
            v.push_back(i);
        }
        auto set = [&](Exp& e, size_t i) {
            e.lsh.qGramSize = vq[i];
            const double defaultMemory = calcMemory(100, LSH::DEFAULT_SIGNATURE_LENGTH);
            e.lsh.signatureLength = memDefault / vmem[i] * calcSignatureLength(e.currentReadLength, defaultMemory);
        };
        exp.varyParameter<size_t>("q-gram size", v, set, "\nQ-GRAM-SIZE (memory const)\n", alignProbability, ParameterType::Index);
    }
    if (!skipSome) {
        std::cout << "Q-Grams, " << getTimeStr() << std::endl;
        std::vector<unsigned> v = {11,12,13,14,15,16,17,18,19,20,21,22};
        auto set = [](Exp& e, unsigned q) {e.lsh.qGramSize = q;};
        exp.varyParameter<unsigned>("q-gram size", v, set, "\nQ-GRAM-SIZE (64-bit q-grams, no gaps)\n", alignProbability, ParameterType::Index);
    }

    if (!skipSome) {
        std::cout << "Q-Grams (middle gap), " << getTimeStr() << std::endl;
        std::vector<unsigned> v = {12,13,14,15,16,17,18,19,20,21,22,23};
        auto set = [](Exp& e, unsigned q) {
            std::string s = std::string((q-1) / 2, '#') + "-" + std::string(q / 2, '#');
            e.lsh.qGramSize = q; e.lsh.qGramHashObject.setGapVector(s);
        };
        exp.varyParameter<unsigned>("q-gram size", v, set, "\nQ-GRAM-SIZE (middle gap)\n", alignProbability, ParameterType::Index);
    }

    if (!skipSome) {
        std::cout << "Q-Grams (left gap), " << getTimeStr() << std::endl;
        std::vector<unsigned> v = {12,13,14,15,16,17,18,19,20,21,22,23};
        auto set = [](Exp& e, unsigned q) {
            std::string s = std::string(5, '#') + "-" + std::string(q - 6, '#');
            e.lsh.qGramSize = q; e.lsh.qGramHashObject.setGapVector(s);
        };
        exp.varyParameter<unsigned>("q-gram size", v, set, "\nQ-GRAM-SIZE (single gap left: #[5]-#[q-6])\n", alignProbability, ParameterType::Index);
    }


    // =======================================================================
    // WINDOW SIZE ; WINDOW OFFSET ; BAND NUMBER
    // =======================================================================
    if (!skipSome) {
        std::cout << "WindowSize, " << getTimeStr() << std::endl;
        std::vector<double> v = {1.25, 1.30, 1.35, 1.40, 1.45, 1.50, 1.55, 1.60};
        auto set = [](Exp& e, double ws) {e.lsh.windowSize = std::round(ws * e.currentReadLength);};
        exp.varyParameter<double>("window size factor", v, set, "\nWINDOW-SIZE\n", alignProbability, ParameterType::Index);
    }

    // deprecated
//    if (!skipSome) {
//        std::cout << "Offset+Overlap, " << getTimeStr() << std::endl;
//        std::vector<WindowBandsFactorConfig> v;
//        for (double offset: {1.00, 1.25, 1.75, 2.25}) {
//            for (double overlap: {.0, .15, .25, .35, .50}) {
//                v.push_back(WindowBandsFactorConfig{offset + overlap, offset, (unsigned) std::round(LSH::DEFAULT_SIGNATURE_LENGTH * offset / LSH::DEFAULT_WINDOW_OFFSET * 100)}); // always use the same amount of memory!
//            }
//        }
//        auto set = [](Exp& e, WindowBandsFactorConfig wb) {
//            e.lsh.windowSize = std::round(wb.windowSizeFactor * e.currentReadLength);
//            e.lsh.windowOffset = std::round(wb.windowOffsetFactor * e.currentReadLength);
//            e.lsh.signatureLength = e.lsh.bandSize * wb.bandNumber;
//        };
//        exp.varyParameter<WindowBandsFactorConfig>("window size factor; window offset factor; band number", v, set, "\nWINDOW-SIZE & OVERLAPPING\n", alignProbability, ParameterType::Index);
//    }

    if (false) {
        std::cout << "Bandnumber, " << getTimeStr() << std::endl;
        std::vector<unsigned> v = {8, 10, 12, 14, 16, 20, 24, 28, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160};
        const double MAX_MEMORY = 0.36;
        auto set = [MAX_MEMORY](Exp& e, unsigned s) {
            if (s > std::round(MAX_MEMORY * e.currentReadLength)) {
                throw std::runtime_error("siglng too big.");
            } else {
                e.lsh.signatureLength = s;
            }
        };
        exp.varyParameter<unsigned>("band number", v, set, "\nBAND-NUMBER\n", alignProbability, ParameterType::Index);
    }


    // =======================================================================
    // MIN COUNT ; MAX SELECT ; NEXT FACTOR
    // =======================================================================
    if (!skipSome) {
        std::cout << "MinCount, " << getTimeStr() << std::endl;
        std::vector<unsigned> v = {1,2,3,4,5,6};
        auto set = [](Exp& e, unsigned mc) {e.lsh._mapOptions.minCountSelection = mc; e.lsh._mapOptions.minCountPrefiltering = mc/2;};
        exp.varyParameter<unsigned>("min count", v, set, "\nMIN-COUNT\n", alignProbability, ParameterType::Query);
    }
    if (!skipSome) {
        std::cout << "MinCount (Pre), " << getTimeStr() << std::endl;
        std::vector<MinCountConfig> v;
        for (unsigned selection: {1,2,3,4,5,6}) {
            for (unsigned prefiltering: {1,2,3}) {
                v.push_back(MinCountConfig{selection, prefiltering});
            }
        }
        auto set = [](Exp& e, MinCountConfig mcc) {e.lsh._mapOptions.minCountSelection = mcc.minCountSelection;
                                                   e.lsh._mapOptions.minCountPrefiltering = mcc.minCountPrefiltering;};
        exp.varyParameter<MinCountConfig>("min count (selection);min count (prefiltering)", v, set, "\nMIN-COUNT\n", alignProbability, ParameterType::Query);
    }

    if (!skipSome) {
        std::vector<MaxSeletNextFactor> v;
        for (unsigned maxSelect: {8, 16, 32, 64, 128, 256}) {
            for (double nextFactor: {2., 2.5, 3., 3.5, 4., 5., 6., 8.}) {
                v.push_back(MaxSeletNextFactor{maxSelect, nextFactor});
            }
        }
        auto set = [](Exp& e, MaxSeletNextFactor mn) {e.lsh._mapOptions.maxSelect = mn.maxSelect;
                                                      e.lsh._mapOptions.nextFactor = mn.nextFactor;};
        exp.varyParameter<MaxSeletNextFactor>("max select; next factor", v, set, "\nMAX-SELECT & NEXT-FACTOR\n", alignProbability, ParameterType::Query);
    }


    // =======================================================================
    // LIMIT ; LIMIT SKIP
    // =======================================================================
    if (containsVariants) if (!skipSome) {
        std::cout << "Limit, " << getTimeStr() << std::endl;
        std::vector<LimitConfig> v;
        for (unsigned limit: {1, 2, 3, 5, 7, 10}) {
            for (unsigned limitSkip: {1, 2, 3, 5, 7, 10, 15, 20}) {
                if (limit <= limitSkip) {
                    v.push_back(LimitConfig{limit, limitSkip});
                }
            }
        }
        auto set = [](Exp& e, LimitConfig lc) {
            e.lsh.limit = lc.limit;
            e.lsh.limit_skip = lc.limitSkip;
        };
        exp.varyParameter<LimitConfig>("limit; limit-skip", v, set, "\nLIMIT & LIMIT-SKIP\n", alignProbability, ParameterType::Index);
    }

    // =======================================================================
    // EXTEND NUMBER ; BAND MULTIPLICATOR
    // =======================================================================
    if (!skipSome) {
        std::cout << "Extend, " << getTimeStr() << std::endl;
        std::vector<ExtendConfig> v;
        for (double extend: {0., 0.15, 0.3, 0.45, 0.6}) {
            for (double mult: {0., 0.15, 0.3, 0.45}) {
                v.push_back(ExtendConfig{extend, mult});
            }
        }
        auto set = [](Exp& e, ExtendConfig ec) {e.lsh._mapOptions.extendFactor = ec.extendFactor;
                                                e.lsh._mapOptions.extendBandMultiplicator = ec.extendBandMultiplicator;};
        exp.varyParameter<ExtendConfig>("extend-number; extend-band-multiplicator", v, set, "\nEXTEND-NUMBER & EXTEND-BAND-MULTIPLICATOR\n", alignProbability, ParameterType::Query);

    }
    if (!skipSome) {
        std::cout << "Contract, " << getTimeStr() << std::endl;
        std::vector<double> v = {0.0, 0.1, 0.15, 0.2, 0.3, 0.4, 0.5};
        auto set = [](Exp& e, double v) {e.lsh._mapOptions.contractBandMultiplicator = v;};
        exp.varyParameter<double>("contract-band-multiplicator", v, set, "\nCONTRACT-BAND-MULTIPLICATOR\n", alignProbability, ParameterType::Query);
    }

    // =======================================================================
    // MAX-RETURNED-WINDOWS
    // =======================================================================
    if (!skipSome) {
        std::cout << "Returned windows, " << getTimeStr() << std::endl;
        std::vector<unsigned> v = {100, 200, 400, 800, 1600, 3200, 6400, 12800, 25600, 51200, 102400, 204800, 409600};
        auto set = [](Exp& e, unsigned mrw) {e.lsh._mapOptions.maximumOfReturnedWindows = mrw;};
        exp.varyParameter<unsigned>("max returned windows", v, set, "\nMAXIMUM-OF-RETURNED-WINDOWS\n", alignProbability, ParameterType::Query);
    }

    // =======================================================================
    // ITERATIONS
    // =======================================================================
    if (!debug) {
        std::cout << "Iterations, " << getTimeStr() << std::endl;
        std::vector<int> v = {1,1,1};//{1,2,3,4,5};
        auto set = [](Exp& e, size_t seed) {
            e.lsh.initialRandomSeed = seed;
            e.lsh.rngHashFunctions.seed(seed);
            e.lsh.rngLimits.clear();
            e.lsh.rngLimits.emplace_back(seed);
        };
        exp.varyParameter<int>("iteration", v, set, "\nITERATIONS\n", alignProbability, ParameterType::Index);
    }

    // =======================================================================
    // HIT-DISTRIBUTION ; EVIL Q-GRAMS ; MULTI WINDOW BUCKETS
    // =======================================================================
    if (!skipSome) {
        std::cout << "Hit Distribution, " << getTimeStr() << std::endl;
        exp.lsh.clear();
        exp.clearAfter(exp);
        exp.lsh._mapOptions.maximumOfReturnedWindows = std::numeric_limits<uint32_t>::max();
        exp.buildIndex();

        for (const ReadSetReference& rsr: exp.readSets) {
            out << "\nReadLength = " << rsr.length;
            hitDistribution(out, exp.lsh, *rsr.reads);
        }
    }
    {
        std::cout << "Multi Window Buckets, " << getTimeStr() << std::endl;

        exp.lsh.clear();
        exp.clearAfter(exp);
        exp.lsh._mapOptions.maximumOfReturnedWindows = std::numeric_limits<uint32_t>::max();
        exp.buildIndex();

        for (size_t div: {1000,100,10,1}) {
            size_t max = div < 1000 ? 100*div : std::numeric_limits<size_t>::max();
            WindowIndexData wiData = calcWindowIndexData(exp.lsh, div, false);
            multiWindowBuckets(out, wiData.wiCount, div, max);
        }
    }

    // =======================================================================
    // 64-BIT-CFBHT
    // =======================================================================
    if (!skipSome) {
        std::cout << "64-bit-CFBHT, " << getTimeStr() << std::endl;
        double sigLngFactor = .18;
        std::vector<unsigned> v = {14,15,16,17,18,19,20,21,22};
        {

            auto set = [sigLngFactor](Exp& e, unsigned q) {e.lsh.qGramSize = q; e.lsh.signatureLength = e.lsh.bandSize * sigLngFactor * e.currentReadLength;};
            exp.varyParameter<unsigned>("q-gram-size", v, set, "\nDEFAULT\n", alignProbability, ParameterType::Index);
            exp.lsh.clear();
            exp.varIdxs.clear();
        }
        {
            typedef Experiment<LSH_64bBands> Exp_64bBands;
            LSH_64bBands lsh2(seed);
            Exp_64bBands exp2(genome, lsh2, enableLongReads ? resetExp_long<LSH_64bBands> : resetExp_single<LSH_64bBands>, out);
            for (const ReadSet& rs: readSets) {
                exp2.addReadSet(rs);
            }
            auto set = [sigLngFactor](Exp_64bBands& e, unsigned q) {e.lsh.qGramSize = q; e.lsh.signatureLength = e.lsh.bandSize * sigLngFactor * e.currentReadLength;};
            exp2.varyParameter<unsigned>("q-gram-size", v, set, "\n64-Bit-Bands (CFBHT)\n", alignProbability, ParameterType::Index);
            exp2.lsh.clear();
        }
        exp.createVariantIndexes();
    }

    // =======================================================================
    // Superrank-Block-Size
    // =======================================================================
    {
        out << "\nSUPERRANK-INFO\n";
        out << "Read Length; Window Count; Signature Values; Block Count;\n";
        out << std::flush;
        std::vector<unsigned> v = {11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};
        for (const ReadSetReference& rsr: exp.readSets) {
            for (unsigned q: v) {
                exp.currentReadLength = rsr.length;
                exp.clearAfter(exp);
                exp.lsh.qGramSize = q;
                exp.buildIndex();
                out << rsr.length << ";";
                out << exp.lsh.referenceWindows.size() << ";";
                uint64_t avgBandhashCount = 0;
                uint64_t avgBlockCount = 0;
                for (const SuperRank<>& sr: exp.lsh.bandHashTables) {
                    avgBandhashCount += sr.getUniqueBandHashCount();
                    avgBlockCount += sr.getBlockCount();
                }
                avgBandhashCount /= exp.lsh.bandHashTables.size();
                avgBlockCount /= exp.lsh.bandHashTables.size();
                out << avgBandhashCount << ";";
                out << avgBlockCount << ";";
                out << std::endl;
            }
        }
    }
    if (!debug) {
        std::cout << "Superrank, " << getTimeStr() << std::endl;
        out << "SUPERRANK\n";
        out << "block size; " << RUN_EXPERIMENT_HEADLINE;

//        exp.runExperiment(alignProbability, ParameterType::Index, "64-Bit-Block;");
        exp.lsh.clear();
        exp.varIdxs.clear();
        {
            typedef LSH_Generic<uint32_t, uint64_t, SuperRank<uint32_t, 64, 64>, GappedQGramHashFunction<uint64_t>> LSH_SR64;
            typedef Experiment<LSH_SR64> Exp_SR64;
            LSH_SR64 lsh2(seed);
            Exp_SR64 exp2(genome, lsh2, enableLongReads ? resetExp_long<LSH_SR64> : resetExp_single<LSH_SR64>, out);
            for (const ReadSet& rs: readSets) {
                exp2.addReadSet(rs);
            }
            exp2.runExperiment(alignProbability, ParameterType::Index, "64-Bit-Block;");
        }
        {
            typedef LSH_Generic<uint32_t, uint64_t, SuperRank<uint32_t, 64, 32>, GappedQGramHashFunction<uint64_t>> LSH_SR32;
            typedef Experiment<LSH_SR32> Exp_SR32;
            LSH_SR32 lsh2(seed);
            Exp_SR32 exp2(genome, lsh2, enableLongReads ? resetExp_long<LSH_SR32> : resetExp_single<LSH_SR32>, out);
            for (const ReadSet& rs: readSets) {
                exp2.addReadSet(rs);
            }
            exp2.runExperiment(alignProbability, ParameterType::Index, "32-Bit-Block;");
        }
        {
            typedef LSH_Generic<uint32_t, uint64_t, SuperRank<uint32_t, 64, 16>, GappedQGramHashFunction<uint64_t>> LSH_SR16;
            typedef Experiment<LSH_SR16> Exp_SR16;
            LSH_SR16 lsh2(seed);
            Exp_SR16 exp2(genome, lsh2, enableLongReads ? resetExp_long<LSH_SR16> : resetExp_single<LSH_SR16>, out);
            for (const ReadSet& rs: readSets) {
                exp2.addReadSet(rs);
            }
            exp2.runExperiment(alignProbability, ParameterType::Index, "16-Bit-Block;");
        }
        {
            typedef LSH_Generic<uint32_t, uint64_t, SuperRank<uint32_t, 64, 8>, GappedQGramHashFunction<uint64_t>> LSH_SR8;
            typedef Experiment<LSH_SR8> Exp_SR8;
            LSH_SR8 lsh2(seed);
            Exp_SR8 exp2(genome, lsh2, enableLongReads ? resetExp_long<LSH_SR8> : resetExp_single<LSH_SR8>, out);
            for (const ReadSet& rs: readSets) {
                exp2.addReadSet(rs);
            }
            exp2.runExperiment(alignProbability, ParameterType::Index, "8-Bit-Block;");
        }

        exp.createVariantIndexes();
    }


    std::cout << "Finished, " << getTimeStr() << std::endl;
}


void runReadLengthExperiment_fast(const std::string& outPrefix,
                                  Genome& genome,
                                  const std::vector<std::string>& varPaths,
                                  ReadGenerator& rg,
                                  const std::string &/*samPathPrefix*/,
                                  size_t numReads,
                                  uint64_t seed) {
    std::cout << "FAST, " << getTimeStr() << std::endl;

    std::ofstream file_fast(outPrefix + "fast.csv");
    file_fast << "Fast test\n";

    std::vector<ReadSet> readSets;
    readSets.push_back(rg.createReads(numReads / 10, 100, 0.018*2, 0.002*2, 0.3));

    runReadLength(file_fast, genome, readSets, seed, varPaths, false, false, true);
}
void runReadLengthExperiment_vary(const std::string& outPrefix,
                                  Genome& genome,
                                  const std::vector<std::string>& varPaths,
                                  ReadGenerator& rg,
                                  const std::string &samPathPrefix,
                                  size_t numReads,
                                  uint64_t seed) {
    std::cout << "VARY, " << getTimeStr() << std::endl;

    std::ofstream file_rlng(outPrefix + "rlng.csv");
    file_rlng << "Vary Read length, with variants\n";

    std::vector<ReadSet> readSets;
    double errf = 2;

//    // Superrank
//    readSets.clear();
//    for (size_t lng: {100, 125, 150, 175, 200, 250, 300, 350, 400, 500/*, 500, 700, 1000*/}) {
//        readSets.push_back(rg.createReads(numReads, lng, 0.018*errf, 0.002*errf, 0.3));
//    }
//    runReadLength(file_rlng, genome, readSets, seed, varPaths, false, false, true, true, 5);

    // Mincount (500, vary siglng)
//    readSets.clear();
//    readSets.push_back(rg.createReads(numReads, 500, 0.018*errf, 0.002*errf, 0.3));
//    runReadLength(file_rlng, genome, readSets, seed, varPaths, false, false, true, true, 6);

//    for (double errf: {/*1., 2.,*/ 4.}) {
//        file_rlng << "==== ERROR FACTOR: " << errf << " ====" << "\n";

//        for (size_t lng: {/*100, 200, 300,*/ 500}) {
    //for (size_t lng: {100, 150, 200, 300/*, 500, 700, 1000*/}) {


//        double errf = 2;
//        readSets.push_back(rg.createReads(numReads, lng, 0.018*errf, 0.002*errf, 0.3));


//            readSets.push_back(rg.createReads(numReads, lng, 0.018*errf, 0.002*errf, 0.3));
//        }
//        runReadLength(file_rlng, genome, readSets, seed, varPaths, false, false, true);
//        rg.writeReads(readSets.back(), samPathPrefix + "rlng" + std::to_string(lng) + ".sam");
//    }

//    for (size_t lng: {100, 150}) {
//        readSets.push_back(rg.createReads(numReads*20, lng, 0.018*2, 0.002*2, 0.3));
//        //rg.writeReads(readSets.back(), samPathPrefix + "rlng" + std::to_string(lng) + ".sam");
//    }

    // window
    readSets.clear();
    for (size_t lng: {100, 200, 300, 500}) {

        readSets.push_back(rg.createReads(numReads, lng, 0.018*errf, 0.002*errf, 0.3));
    }
    runReadLength(file_rlng, genome, readSets, seed, varPaths, false, false, true, true, 2);

    // 30M, Rlng=100 (limit, limitSkip)
//    readSets.clear();
//    readSets.push_back(rg.createReads(numReads*30, 100, 0.018*2, 0.002*2, 0.3));
//    runReadLength(file_rlng, genome, readSets, seed, varPaths, false, false, true, true, 1);


}

void runReadLengthExperiment_pairedEnd(const std::string& outPrefix,
                                       Genome& genome,
                                       const std::vector<std::string>& varPaths,
                                       ReadGenerator& rg,
                                       const std::string &samPathPrefix,
                                       size_t numReads,
                                       uint64_t seed) {
    std::cout << "PAIRED, " << getTimeStr() << std::endl;

    std::ofstream file_pe(outPrefix + "pairedEnd.csv");
    file_pe << "Paired vs Single, with variants\n";

    std::mt19937 rnd(seed);


    std::vector<ReadSet> readSets;

    // Create Paired End
    //for (size_t lng: {100, 125, 150, 175, 200, 250, 300, 350, 400, 500}) {
    for (size_t lng: {100, 200, 300, 500}) {
        std::normal_distribution<double> peDist(3*lng, lng/2);
        auto distr = [&peDist, &rnd](){return peDist(rnd);};
        readSets.push_back(rg.createReads_pairedEnd(numReads/2, lng, distr, 0.018*2, 0.002*2, 0.3));
//        std::vector<ReadGenerator::Read> se;
//        for (ReadGenerator::Read rd: readSets.back()) {
//            se.push_back(*rd.next);
//            rd.next.reset();
//            se.push_back(rd);
//        }
//        readSets.push_back(std::move(se));
    }
//    rg.writeReads(readSets.back(), samPathPrefix + "paired.sam");

    // Create Single End Set


//    readSets.push_back(rg.createReads(numReads, 200, 0.018*2, 0.002*2, 0.3));
//    rg.writeReads(readSets.back(), samPathPrefix + "single.sam");

    // Run
    runReadLength(file_pe, genome, readSets, seed, varPaths, false, true, true, true, 4);
    std::vector<ReadSet> seSets;
    for (const ReadSet& rs: readSets) {
        std::vector<ReadGenerator::Read> se;
        for (ReadGenerator::Read rd: rs) {
            se.push_back(*rd.next);
            rd.next.reset();
            se.push_back(rd);
        }
        seSets.push_back(std::move(se));
    }
    runReadLength(file_pe, genome, seSets, seed, varPaths, false, true, true, true, 8);
//    global_LSH = LSH_PE_FOR | LSH_PE_PLUS_D2 | LSH_SPLIT_EQUAL;
//    runReadLength(file_pe, genome, readSets, seed, varPaths, false, true, true, true, 4);
//    global_LSH = LSH_PE_FOR | LSH_PE_MEAN | LSH_SPLIT_EQUAL;
}

void runReadLengthExperiment_long(const std::string& outPrefix,
                                  Genome& genome,
                                  const std::vector<std::string>& varPaths,
                                  ReadGenerator& rg,
                                  const std::string &/*samPathPrefix*/,
                                  size_t numReads,
                                  uint64_t seed) {
    std::cout << "LONG, " << getTimeStr() << std::endl;

    std::ofstream file_rlng(outPrefix + "long.csv");
    file_rlng << "Long Read Splitting, with variants\n";

    std::vector<ReadSet> readSets;

    // const-rlng, compare
//    readSets.clear();
//    for (size_t lng: {125, 150, 175, 200, 250, 300, 350, 400, 500}) {
//        readSets.push_back(rg.createReads(numReads, lng, 0.018*2, 0.002*2, 0.3));
//    }
//    runReadLength(file_rlng, genome, readSets, seed, varPaths, true, false, true, true, 7);
//    runReadLength(file_rlng, genome, readSets, seed, varPaths, false, false, true, true, 7);



    std::mt19937 rnd(seed);
    readSets.clear();
    for (size_t lng: {200, 450, 950, 1950}) {
        std::uniform_int_distribution<size_t> distr(50, lng);
        readSets.push_back(rg.createReads_differentLength(numReads, std::bind(std::ref(distr), std::ref(rnd)), 0.018*2, 0.002*2, 0.3));
    }
    runReadLength(file_rlng, genome, readSets, seed, varPaths, true, false, true, true, 9);

}


void runRealReadExperiment(const std::string& outPrefix,
                           Genome& genome,
                           const std::vector<std::string>& varPaths,
                           size_t numReads,
                           uint64_t seed) {
    std::vector<ReadSet> readSets;

    std::ofstream file_real(outPrefix + "real.csv");

    std::string prefix = "/scratch/JQ/sam/";
    readSets.emplace_back();
    extractAlignedReadsFromSAM(prefix + "optc_50_ERR385811.sam", genome, readSets.back());
    readSets.emplace_back();
    extractAlignedReadsFromSAM(prefix + "optc_100_ERR281333a.sam", genome, readSets.back());
    readSets.emplace_back();
    extractAlignedReadsFromSAM(prefix + "optc_150_SRR1374471.sam", genome, readSets.back());
    readSets.emplace_back();
    extractAlignedReadsFromSAM(prefix + "optc_250_ERR967952a.sam", genome, readSets.back());

    std::mt19937 rnd(seed);
    for (ReadSet& rs: readSets) {
        if (rs.size() > numReads) {
            std::uniform_int_distribution<size_t> distr(0, rs.size()-1);
            for (size_t i = 0; i < numReads; i++) {
                std::swap(rs[i], rs[distr(rnd)]);
            }
            rs.resize(numReads);
        }
    }

    runReadLength(file_real, genome, readSets, seed, varPaths, false, true, false);
}

void runRealReadExperiment_long(const std::string& outPrefix,
                           Genome& genome,
                           const std::vector<std::string>& varPaths,
                           uint64_t seed) {
    std::vector<ReadSet> readSets;

    std::ofstream file_reallong(outPrefix + "reallong.csv");

    std::string prefix = "/scratch/JQ/sam/";
    readSets.emplace_back();
    extractAlignedReadsFromSAM(prefix + "optv_180_DRR003760.sam", genome, readSets.back());
    readSets.emplace_back();
    extractAlignedReadsFromSAM(prefix + "optv_266_SRR003025.sam", genome, readSets.back());
    readSets.emplace_back();
    extractAlignedReadsFromSAM(prefix + "optv_565_SRR003174.sam", genome, readSets.back());

    runReadLength(file_reallong, genome, readSets, seed, varPaths, true, true, false);
}

void runArtificalVariantsExperiment(const std::string& outPrefix,
                                    Genome& genome,
                                    size_t numReads,
                                    uint64_t seed) {
    const size_t varCount = 30000000;
    const bool enableLongReads = false;
    const double alignProbability = 0.1;

    std::ofstream file_var(outPrefix + "var.csv");
    file_var << "Artifical Variants\n";

    std::cout << "VariantGenerator()" << std::endl;
    VariantGenerator vg(genome, seed);



//    Genome copy = genome;

    file_var << "ARTIFICAL VARIANTS\n";
    file_var << "variant probability; addVariants; " << RUN_EXPERIMENT_HEADLINE;
//    for (double prob: {0.0001, 0.05, 0.1}) {
    for (double errFactor: {/*1.,*/ 4.}) {
        for (size_t lng: {/*100, 150,*/ 200, 300, 500}) {
            vg.setSeed(seed);
            for (double prob: {0.0001, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5}) {
            //for (double prob: {0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5}) {
                // Create Variants
                std::vector<VcfVariant> variants = vg.createSNPs(varCount, [prob](){return prob;});

                // Create Reads
                ReadGenerator rg(genome, seed);
                rg.setVariants_destroyParameter(variants);
                std::vector<ReadSet> readSets;
        //        for (size_t lng: {100, 150, 200, 300, 500}) {
        //            readSets.push_back(rg.createReads(numReads, lng, 0.018*2, 0.002*2, 0.3 ));
        //        }
                readSets.push_back(rg.createReads(numReads, lng, 0.018*errFactor, 0.002*errFactor, 0.3 ));

                // Initialize LSH and Exp
                LSH lsh(seed);
                typedef Experiment<LSH> Exp;
                Exp exp(genome, lsh, enableLongReads ? resetExp_long<LSH> : resetExp_single<LSH>, file_var);
                for (const ReadSet& rs: readSets) {
                    exp.addReadSet(rs);
                }

                // Reset genome
                for (Chromosome& chr: genome) {
                    chr.data = chr.noVariantsString;
                }

                // Run Experiments
                for (bool addVariants: {false, true}) {
                    // Add Variants in second run
                    if (addVariants) {
                        for (const VcfVariant& v: rg.getVariants()) {
                            genome[v.chrIdx].addSNP(v.position, v.altString[0]);
                        }
                    }

                    // Experiment
                    {
                        std::ostringstream oss;
                        oss << prob << ";";
                        oss << addVariants << ";";
                        exp.runExperiment(alignProbability, ParameterType::Index, oss.str());
                        exp.clearAfter(exp);
                    }
                }
            }
        }
    }

    // Reset genome
    for (Chromosome& chr: genome) {
        chr.data = chr.noVariantsString;
    }


//    std::swap(copy, genome); // restore old genome
}

void runReadLengthExperiments(const std::string& outPrefix,
                              const std::vector<std::string> &refPaths,
                              const std::vector<std::string> &varPaths,
                              const std::string &samPathPrefix,
                              size_t numReads,
                              uint64_t seed) {
    Genome genome;
    std::unordered_set<std::string> filter;

    std::cout << "readReferences()" << std::endl;
    ReferenceReader rr(genome, filter);
    rr.readReferences(refPaths);

//    runArtificalVariantsExperiment(outPrefix, genome, numReads, seed);

    std::cout << "readVariants()" << std::endl;
    VariantsReader vr(genome, filter, DEFAULT_VARIANT_PROBABILITY_THRESHOLD);
    vr.readVariants(varPaths);

    std::cout << "ReadGenerator()" << std::endl;
    ReadGenerator rg(genome, varPaths, seed);
    rg.readVariants();


//    runReadLengthExperiment_fast(outPrefix, genome, varPaths, rg, samPathPrefix, numReads, seed);
//    runReadLengthExperiment_pairedEnd(outPrefix, genome, varPaths, rg, samPathPrefix, numReads, seed);
    runReadLengthExperiment_long(outPrefix, genome, varPaths, rg, samPathPrefix, numReads, seed);
    runReadLengthExperiment_vary(outPrefix, genome, varPaths, rg, samPathPrefix, numReads, seed);



//    runRealReadExperiment(outPrefix, genome, varPaths, numReads, seed);
//    runRealReadExperiment_long(outPrefix, genome, varPaths, numReads, seed);


}


#endif // READLENGTHEXP_H
