#ifndef READSAMIO_H
#define READSAMIO_H

#include "CompareStructs.h"
#include "io/SamWriter.h"
#include "GenomeGenerator.h"

template <class SeqanString>
std::string convertString(const SeqanString& seqanString) {
    return std::string(seqan::begin(seqanString), seqan::end(seqanString));
}


inline
std::vector<UnalignedRead> readReadFile(const std::string& readFile) {
    seqan::SequenceStream reader(readFile.c_str());
    std::vector<UnalignedRead> reads;

    while (!seqan::atEnd(reader)) {
        UnalignedRead r;
        if (seqan::readRecord(r.id, r.seq, reader) != 0) {
            throw std::runtime_error("readReadFile");
        }
        r.id = r.id.substr(0, r.id.find_first_of(' ', 0));
        reads.push_back(std::move(r));
    }
    std::sort(reads.begin(), reads.end());
    return reads;
}

inline
std::vector<AlignedRead> readSamFile(const std::string& samFile) {
    std::vector<AlignedRead> alignedReads;

    seqan::BamStream bam(samFile.c_str());
    if (!seqan::isGood(bam)) {
        throw std::runtime_error("SamCompare : error opening file");
    }
    seqan::BamAlignmentRecord record;
    while (!seqan::atEnd(bam)) {
        if (seqan::readRecord(record, bam) != 0) {
            throw std::runtime_error("SamCompare : error reading file");
        }
//            auto it = chrNameMap.find(Chromosome::generateName(convertString(bam.header.sequenceInfos[record.rID].i1)));
//            if (it != chrNameMap.end()) {
        if (record.rID != seqan::BamAlignmentRecord::INVALID_REFID) {
            AlignedRead read;
            read.id = convertString(record.qName);
            read.chr = Chromosome::generateName(convertString(bam.header.sequenceInfos[record.rID].i1)); /*it->second;*/
            read.beginPos = record.beginPos;
            read.revCompl = seqan::hasFlagRC(record); //seqan::hasFlagMultiple()
            read.seq = record.seq;
            if (read.revCompl) {
                seqan::reverseComplement(read.seq);
            }
            alignedReads.push_back(std::move(read));
        }
//            } // else chromosome not found -> skip
    }
    std::sort(alignedReads.begin(), alignedReads.end()); // sort according to id
    return alignedReads;
}


template <class Read>
void writeReadFile(const std::vector<Read>& reads, const std::string& path) {
    std::ofstream file(path);
    for (size_t i = 0; i < reads.size(); i++) {
        const UnalignedRead& r = reads[i];
        if (i == 0 || r.id != reads[i-1].id) {
            file << "@" << r.id << "\n";
            file << r.seq << "\n";
            file << "+\n";
            file << std::string(seqan::length(r.seq), 'J') << "\n";
        }
    }
}

template <class Read>
void writeReadFile_paired(const std::vector<Read>& reads, const std::string& path1, const std::string& path2) {
    std::ofstream file1(path1);
    std::ofstream file2(path2);

    for (size_t i = 0; i < reads.size(); i++) {
        const Read& r = reads[i];
        if (i % 2 == 0) {
            file1 << "@" << r.id << "\n";
            file1 << r.seq << "\n";
            file1 << "+\n";
            file1 << std::string(seqan::length(r.seq), 'J') << "\n";
        } else if (r.id == reads[i-1].id) {
            file2 << "@" << r.id << "\n";
            file2 << r.seq << "\n";
            file2 << "+\n";
            file2 << std::string(seqan::length(r.seq), 'J') << "\n";
        } else {
            throw std::runtime_error("writeReadFile_paired");
        }
    }
}


inline
bool exists(const std::string& name) {
    std::ifstream f(name);
    return f.good();
}

#endif // READSAMIO_H
