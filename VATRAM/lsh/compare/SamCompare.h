#ifndef SAMCOMPARE_H
#define SAMCOMPARE_H

#include "Experiments.h"
#include "CompareStructs.h"

class SamCompare {
public:

    struct Result {
        size_t readCount = 0;
        size_t correct = 0;
        size_t wrong = 0;
    };
    


    Result compareSam(const std::vector<AlignedRead>& correctReads, const std::vector<AlignedRead>& alignedReads, int distanceThreshold, std::vector<AlignedRead>* unmapped = NULL) {
        Result res;
        auto alignedIt = alignedReads.begin();
        auto alignedEnd = alignedReads.end();
        auto correctEnd = correctReads.end();
        const AlignedRead* lastRead = NULL;
        for (auto it = correctReads.begin(); it != correctEnd; ++it) {
            if (!lastRead || *lastRead < *it) { // ensure that the next read has a different id
                res.readCount++;
//                std::cout << readCount << "r";
                // search for read id
                while (alignedIt != alignedEnd && *alignedIt < *it) {
                    ++alignedIt;
                }
                if (alignedIt != alignedEnd) {
//                    std::cout << "e";
                    const AlignedRead& correct = *it;
                    const AlignedRead& aligned = *alignedIt;
                    if (correct.id == aligned.id && correct.seq == aligned.seq) {
//                        std::cout << "i";
                        int64_t distance = std::abs(correct.beginPos - (int64_t) aligned.beginPos);
                        if (correct.revCompl == aligned.revCompl && correct.chr == aligned.chr && distance < distanceThreshold) {
                            res.correct++;
                        } else {
                            res.wrong++;
                        }
                    } else {
                        if (unmapped) {
                            unmapped->push_back(correct);
                        }
                    }
                } else {
                    break; // all reads fount
                }
//                std::cout << "__" << std::flush;
                lastRead = &*it;
            }
        }
//        std::cout <<"\n";
        return res;
    }
};



#endif // SAMCOMPARE_H
