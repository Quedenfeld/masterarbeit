#ifndef RMEXPERIMENTPAIRED_H
#define RMEXPERIMENTPAIRED_H

#include "RMComparatorPaired.h"
#include "ReadGenerator.h"
#include "VatramCmdStrings.h"
#include "io/ReferenceReader.h"

#include <functional>


class RMExperimentPaired {
public:



    struct Experiment {
        RMComparatorPaired::ReadSet readSet;
        std::string vatramExtraParameters;
        double meanError;

        Experiment(const std::string& name, const std::string& samFile, const std::string& vatramExtraParameters, double meanError) :
            readSet(name, samFile), vatramExtraParameters(vatramExtraParameters), meanError(meanError) {}
        Experiment(const std::string& name, const std::string& inputFile1, const std::string& inputFile2, const std::string& vatramExtraParameters, double meanError) :
            readSet(name, inputFile1, inputFile2), vatramExtraParameters(vatramExtraParameters), meanError(meanError) {}
    };

    std::ofstream resultFile;
    std::vector<Experiment> experiments;
    uint64_t baseCountPerExperiment;
    uint64_t seed;
    std::mt19937 rnd;

    Genome genome;

    RMExperimentPaired(uint64_t baseCount, uint64_t seed) :
        resultFile("resultPaired.csv"),
        baseCountPerExperiment(baseCount),
        seed(seed),
        rnd(seed) {

    }


    void createExperiments(const std::vector<std::string> &refPaths,
                           const std::vector<std::string> &varPaths) {

        std::unordered_set<std::string> filter;
        ReferenceReader rr(genome, filter);
        rr.readReferences(refPaths);
        VariantsReader vr(genome, filter, 0.2);
        vr.readVariants(varPaths);
        ReadGenerator rg(genome, varPaths, seed);






        double realErrorFactor = 0.05;
        std::vector<double> verr = {0.02, 0.04, 0.08};

        for (size_t lng: {100, 150, 200, 300, 400, 500}) {
            for (double err: verr) {
                createPaired(rg, lng, lng*5, lng, err);
            }
        }

        createFastqPaired("pe_c_100_ERR281333",
                          "/scratch/JQ/reads/Illumina/ERR281333/ERR281333_1_2M.fastq",
                          "/scratch/JQ/reads/Illumina/ERR281333/ERR281333_2_2M.fastq",
                          "-e 10 --pe-distance-min 100 --pe-distance-max 900",
                          100*realErrorFactor); // 100 c pe
        createFastqPaired("pe_c_151_ERR259389",
                          "/scratch/JQ/reads/Illumina/ERR259389/ERR259389_1.fastq.gz",
                          "/scratch/JQ/reads/Illumina/ERR259389/ERR259389_2.fastq.gz",
                          "-e 15 -w 211 -W 189 -s 52 --pe-distance-min 150 --pe-distance-max 1350",
                          151*realErrorFactor); // 250 c pe
        createFastqPaired("pe_c_250_ERR967952",
                          "/scratch/JQ/reads/Illumina/ERR967952/ERR967952_1_1M.fastq",
                          "/scratch/JQ/reads/Illumina/ERR967952/ERR967952_2_1M.fastq",
                          "-e 25 -w 350 -W 312 -s 83 --pe-distance-min 250 --pe-distance-max 2250",
                          250*realErrorFactor); // 250 c pe

        // clear stuff that is not needed
        for (Chromosome& chr: genome) {
            chr.insertLowProbabilitySnps();
            decltype(chr.noVariantsString) empty;
            seqan::swap(chr.noVariantsString, empty);
        }




    }

    void createPaired(ReadGenerator& rg, size_t rlng, size_t gapMean, size_t gapStddev, double errorRate) {
        std::normal_distribution<double> gapDistr(gapMean, gapStddev);
        std::ostringstream oss;
        oss << "paired_" << rlng << "_" << std::setprecision(3) << errorRate;
        std::string file = oss.str() + ".sam";
        if (!exists(file)) {
            auto reads = rg.createReads_pairedEnd(baseCountPerExperiment / rlng, rlng, std::bind(gapDistr, std::ref(rnd)), .95*errorRate, .05*errorRate, 0.3);
            rg.writeReads(reads, file);
        }
        std::ostringstream cmd;
        cmd << " -w " << int(std::round(1.4*rlng))
            << " -W " << int(std::round(1.25*rlng))
            << " -s " << calcSignatureLength(rlng)
            << " -e " << int(2+std::round(2*rlng*errorRate))
            << " --pe-distance-min " << int(gapMean - 3*gapStddev)
            << " --pe-distance-max " << int(gapMean + 3*gapStddev)
            << " ";
        experiments.emplace_back(oss.str(), file, cmd.str(), errorRate*rlng);
    }

    void createFastqPaired(const std::string& name, const std::string& readFilePath1, const std::string& readFilePath2, const std::string& vatramParameters, double meanError) {
        experiments.emplace_back(name, readFilePath1, readFilePath2, vatramParameters, meanError);
    }

    void runAll(bool clear) {
        for (const Experiment& exp: experiments) {
            run(exp, clear);
        }
    }

    void run(const Experiment& exp, bool clear) {
        RMComparatorPaired rmc(genome, 3*exp.meanError);

        rmc.addReadmapper("BWA", "/home/quedenfe/bwa/bwa mem -t 8 /scratch/JQ/genome/GCA_000001405.14_GRCh37.p13_no_alt_analysis_set_MRN.fa", "", " > bwa.sam", "bwa.sam");
        rmc.addReadmapper("BWA-EDIT", "/home/quedenfe/bwa/bwa mem -t 8 -A1 -B1 -O1 -E1 /scratch/JQ/genome/GCA_000001405.14_GRCh37.p13_no_alt_analysis_set_MRN.fa", "", " > bwaedit.sam", "bwaedit.sam");
        rmc.addReadmapper("BWA-SW", "/home/quedenfe/bwa/bwa bwasw -t 8 /scratch/JQ/genome/GCA_000001405.14_GRCh37.p13_no_alt_analysis_set_MRN.fa", "", " > bwasw.sam", "bwasw.sam");
        rmc.addReadmapper("VATRAM", std::string("/home/quedenfe/masterarbeit/work/vatram align ") + vatramRef + vatramVar + " -x 1337 -o vatram.sam -p", "", exp.vatramExtraParameters, "vatram.sam");
        rmc.addReadmapper("VATRAM-NOVAR", std::string("/home/quedenfe/masterarbeit/work/vatram align") + vatramRef + " -x 1337 -o vatramnovar.sam -p", "", exp.vatramExtraParameters, "vatramnovar.sam");
        rmc.addReadmapper("Bowtie2", "/home/quedenfe/bowtie2/bowtie2 -p 8 -x /scratch/JQ/genome/index_bowtie -S bowtie2.sam -1", "-2", "", "bowtie2.sam", "bowtie2-align-s");
        rmc.addReadmapper("MRSfast", "/home/quedenfe/mrsfast/mrsfast --search /scratch/JQ/genome/GCA_000001405.14_GRCh37.p13_no_alt_analysis_set_MRN.fa --threads 0 --snp /scratch/JQ/genome/snp.index -o mrsfast.sam --best --pe --seq1", "--seq2", "", "mrsfast.sam");

        rmc.addReadSet(exp.readSet);
        rmc.evaluate(resultFile, clear);
        if (clear) {
//            std::remove(exp.samFile.c_str());
        }
    }


};

inline
void runRMexperimentPaired(const std::vector<std::string> &refPaths,
                           const std::vector<std::string> &varPaths,
                           uint64_t seed) {
    RMExperimentPaired rme(100ULL*1000*1000, seed);
    rme.createExperiments(refPaths, varPaths);
    rme.runAll(true);
}

#endif // RMEXPERIMENTPAIRED_H
