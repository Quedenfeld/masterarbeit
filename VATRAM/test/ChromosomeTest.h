#ifndef PG583_TEST_CHROMOSOMENAMEPARSETEST_H
#define PG583_TEST_CHROMOSOMENAMEPARSETEST_H

#include <QtTest>

Q_DECLARE_METATYPE(QString)

class ChromosomeTest : public QObject {
    Q_OBJECT

private Q_SLOTS:
    void testNameParsing();
    void testNameParsing_data();
};

#endif // PG583_TEST_CHROMOSOMENAMEPARSETEST_H
