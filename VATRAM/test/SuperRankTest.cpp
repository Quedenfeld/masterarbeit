#include "SuperRankTest.h"
#include "map/SuperRank.h"
#include <random>

SuperRankTest::SuperRankTest()
{
}

void SuperRankTest::testRandomNumbers()
{
	std::vector< std::pair<uint32_t, uint32_t> > hashList;
	std::mt19937 rnd;
	const size_t keyMax = 10000;
    
    SuperRank<uint32_t> superRank(keyMax);
    for (unsigned int i = 0; i < keyMax; i++)
	{
        if (rnd() % 100 < 40)
        {
            continue;
        }
        uint32_t bandHash = rnd() % keyMax;
        uint32_t windowIndex = rnd() % 1000;
        superRank.add(bandHash, windowIndex);
		hashList.emplace_back(bandHash, windowIndex);
        if (rnd() % 100 < 50)
        {
            continue;
        }
        superRank.add(bandHash, windowIndex + 1);
		hashList.emplace_back(bandHash, windowIndex+1);
//        qDebug() << bandHash << " --- " << windowIndex;
	}
    superRank.sort();
    superRank.build();
    qDebug() << "------";
	for (unsigned int i = 0; i < hashList.size(); i++)
	{
        std::vector<uint32_t> indexList;
        auto operation = [&indexList](uint32_t index){ 
            indexList.push_back(index); 
        };
        superRank.find(hashList[i].first, maximumOfReturnedWindows, operation);
        if (std::find(indexList.begin(), indexList.end(), hashList[i].second) == indexList.end())
        {
            for (uint32_t windowIndex : indexList)
            {
                qDebug() << windowIndex;
            }
            qDebug() << " - " << hashList[i].second;
            qDebug() << hashList[i].first;
            QFAIL("Did not find an entry");
            
        }
	}
}

void SuperRankTest::testNoCollision()
{
	std::vector< std::pair<uint32_t, uint32_t> > hashList;
//	std::mt19937 rnd;
//	const size_t keyMax = 10000;
//	for (unsigned int i = 0; i < keyMax; i++)
//	{
////        if (rnd() % 100 < 40)
////        {
////            continue;
////        }
//        SuperRank::BandHash bandHash = i;//rnd() % keyMax;
//        WindowIndex windowIndex = i;//rnd() % 1000;
//		hashList.emplace_back(bandHash, windowIndex);
//		hashList.emplace_back(bandHash, windowIndex + 1);
////        qDebug() << bandHash << " --- " << windowIndex;
//	}
    SuperRank<uint32_t> superRank(4);
    
    superRank.add(1, 2);
    superRank.add(1, 3);
    superRank.add(1, 4);
    superRank.add(1, 5);
    
    hashList.emplace_back(1, 2);
    hashList.emplace_back(1, 3);
    hashList.emplace_back(1, 4);
    hashList.emplace_back(1, 5);
    superRank.sort();
    superRank.build();
	for (unsigned int i = 0; i < 4; i++)
	{
        std::vector<uint32_t> indexList;
        auto operation = [&indexList](uint32_t i){ 
            indexList.push_back(i); 
        };
        superRank.find(hashList[i].first, maximumOfReturnedWindows, operation);
        if (std::find(indexList.begin(), indexList.end(), hashList[i].second) == indexList.end())
        {
            for (uint32_t windowIndex : indexList)
            {
                qDebug() << windowIndex;
            }
            qDebug() << " - " << hashList[i].second;
            qDebug() << hashList[i].first;
            QFAIL("Did not find an entry");
            
        }
	}
}


void SuperRankTest::superRankTest() {
    /*
    std::vector< std::pair<SuperRank::BandHash, WindowIndex> > hashList;
    hashList.emplace_back(0, 1);
    hashList.emplace_back(8, 2);
    hashList.emplace_back(16, 3);
    hashList.emplace_back(24, 4);
    hashList.emplace_back(32, 5);
    hashList.emplace_back(40, 6);
    hashList.emplace_back(48, 7);
    hashList.emplace_back(56, 8);
    hashList.emplace_back(64, 9);
    // hashList.emplace_back(1000, 3);
		// for (unsigned int i = 0; i < 1000; i++)
		// {
		// 	hashList.emplace_back(i, 3 + i);
		// }
    SuperRank superRank(100, hashList); */
//    QCOMPARE(superRank.getBandHashEntry(0).getValue(), (unsigned int)1);
//    QCOMPARE(superRank.getBandHashEntry(8).getValue(), (unsigned int)2);
//    QCOMPARE(superRank.getBandHashEntry(16).getValue(), (unsigned int)3);
//    QCOMPARE(superRank.getBandHashEntry(24).getValue(), (unsigned int)4);
//    QCOMPARE(superRank.getBandHashEntry(32).getValue(), (unsigned int)5);
//    QCOMPARE(superRank.getBandHashEntry(40).getValue(), (unsigned int)6);
//    QCOMPARE(superRank.getBandHashEntry(48).getValue(), (unsigned int)7);
//    QCOMPARE(superRank.getBandHashEntry(56).getValue(), (unsigned int)8);
//    QCOMPARE(superRank.getBandHashEntry(64).getValue(), (unsigned int)9);
		// for (unsigned int i = 0; i < 1000; i++)
		// {
		// 	QCOMPARE(superRank.getBandHashEntry(i).getValue(), (unsigned int)3 + i);
		// }
    // QCOMPARE(superRank.getBandHashEntry(17).getValue(), (unsigned int)1);
    // QCOMPARE(superRank.getBandHashEntry(18).getValue(), (unsigned int)2);
    // QCOMPARE(superRank.getBandHashEntry(1000).getValue(), (unsigned int)3);
}
