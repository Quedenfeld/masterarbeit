#ifndef LSHTEST_H
#define LSHTEST_H

#include <QtTest>

class LSHTest : public QObject {
    Q_OBJECT

private Q_SLOTS:
    void testLSH();
    void testConfigureInitialState();
    void testAddGenome();
    void testAddChromosome();
    void testAddReferenceWindow();
    void testCreateQGramHashes();
    void testCreateQGramHashes_iupac();
    void testCreateSignatures();
    void testCreateBandHashes();
    void testFindRead();
    void testFindRead_revCompl();
    void testQGramHashFunction();
    void testBandHashFunction();
    void testFindCombinationsOfQGrams();
    void testCFBHT();
    void testFindIntervals();
};

#endif // LSHTEST_H
