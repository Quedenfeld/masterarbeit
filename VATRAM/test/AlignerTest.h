#ifndef ALIGNERTEST_H
#define ALIGNERTEST_H

#include <QObject>
#include <QtTest>
#include <QString>

#include "align/Variant.h"
#include "align/Aligner.h"
#include "align/Alignment.h"
#include "Chromosome.h"

#include <seqan/sequence.h>
#include <seqan/file.h>

#include <memory>
#include <string>

Q_DECLARE_METATYPE(Variant)
Q_DECLARE_METATYPE(std::string)
Q_DECLARE_METATYPE(Alignment)
Q_DECLARE_METATYPE(std::shared_ptr<Chromosome>)
Q_DECLARE_METATYPE(ErrorCount)

class AlignerTest : public QObject {
    Q_OBJECT

public:
    AlignerTest();

private Q_SLOTS:
    void alignerTest();
    void alignerTest_data();
};

#endif // ALIGNERTEST_H
