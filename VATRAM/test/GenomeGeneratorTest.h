#ifndef GENOMEGENERATORTEST_H
#define GENOMEGENERATORTEST_H

#include <QString>
#include <QtTest>
#include "GenomeGenerator.h"

class GenomeGeneratorTest : public QObject {
    Q_OBJECT

public:
    GenomeGeneratorTest();

private Q_SLOTS:
    void genomeGeneratorTest_data();
    void genomeGeneratorTest();
};

#endif // GENOMEGENERATORTEST_H
