#include "AlignerTest.h"
#include "CollisionFreeBhtTest.h"
#include "GenomeGeneratorTest.h"
#include "LSHTest.h"
#include "SuperRankTest.h"
#include "ChromosomeTest.h"

#include <QTest>

int main(int argc, char *argv[])
{
    int status = 0;

    // Copy the following two lines and modify them adequately
    // for other new test classes

    CollisionFreeBhtTest collisionFreeBhtTest;
    status |= QTest::qExec(&collisionFreeBhtTest, argc, argv);
    
    AlignerTest alignerTest;
    status |= QTest::qExec(&alignerTest, argc, argv);

    GenomeGeneratorTest genomeGeneratorTest;
    status |= QTest::qExec(&genomeGeneratorTest, argc, argv);

    SuperRankTest superRankTest;
    status |= QTest::qExec(&superRankTest, argc, argv);

    QStringList chromosomeTestArgs;
    chromosomeTestArgs.append(argv[0]);
    chromosomeTestArgs.append("-silent");
    ChromosomeTest chromosomeTest;
    status |= QTest::qExec(&chromosomeTest, chromosomeTestArgs);

    LSHTest lshTest;
    status |= QTest::qExec(&lshTest, argc, argv);

    return status;
}
