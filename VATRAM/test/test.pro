TARGET_FILE_NAME = test_vatram
include(../dep-on-lib.pri)

TEMPLATE = app
CONFIG += console

CONFIG += qt
QT += testlib

SOURCES += \
    AlignerTest.cpp \
    CollisionFreeBhtTest.cpp \
    GenomeGeneratorTest.cpp \
    LSHTest.cpp \
    main.cpp \
    SuperRankTest.cpp \
    ChromosomeTest.cpp

HEADERS += \
    AlignerTest.h \
    CollisionFreeBhtTest.h \
    GenomeGeneratorTest.h \
    LSHTest.h \
    SuperRankTest.h \
    ChromosomeTest.h
