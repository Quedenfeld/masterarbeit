#include <QtTest>
#include <QString>
#include "AlignerTest.h"

#include "align/Variant.h"
#include "align/Alignment.h"
#include "align/Aligner.h"
#include <seqan/sequence.h>
#include <seqan/file.h>
#include <string>

AlignerTest::AlignerTest() {
}

void AlignerTest::alignerTest_data() {

   QTest::addColumn< std::shared_ptr<Chromosome> >("chr");
   QTest::addColumn< std::string >("read");
   QTest::addColumn< std::vector<Alignment> >("expAlignments");
   QTest::addColumn< ErrorCount >("maxErrors");
   QTest::addColumn< ErrorCount >("maxClip");

   // Test 01

   std::string ref1     = "ATTCGTAGCTAAATCG";
   std::string read1    =     "GTAGCT";
   ReferenceString refx1(ref1);

   std::vector<Variant> variants1;

   std::shared_ptr<Chromosome> chr1(new Chromosome("chr1", refx1, variants1));

   Alignment a1_1;
   a1_1.setCigar("6M");
   a1_1.setPosition(4);
   a1_1.setStartOffset(0);
   a1_1.setStartVariant(Variant::none);
   a1_1.setAlignmentString("GTAGCT");

   std::vector<Alignment> exp1;
   exp1.push_back(a1_1);

   // Test 02

   std::string ref2     = "TACGGACTCCCTGACCAGT";
   std::string read2    =      "ACTCACTG";
   ReferenceString refx2(ref2);

   std::vector<Variant> variants2;

   std::shared_ptr<Chromosome> chr2(new Chromosome("chr2", refx2, variants2));

   Alignment a2_1;
   a2_1.setCigar("4M1X3M");
   a2_1.setPosition(5);
   a2_1.setStartOffset(0);
   a2_1.setStartVariant(Variant::none);
   a2_1.setAlignmentString("ACTCCCTG");

   std::vector<Alignment> exp2;
   exp2.push_back(a2_1);

   // Test 03

   std::string ref3     = "CCGATTACCCCAATTG";
   std::string read3    =     "TTACCCCGGAAT";
   ReferenceString refx3(ref3);

   std::vector<Variant> variants3;

   std::shared_ptr<Chromosome> chr3 = std::make_shared<Chromosome>("chr3", refx3, variants3);

   Alignment a3_1;
   a3_1.setCigar("7M2I3M");
   a3_1.setPosition(4);
   a3_1.setStartOffset(0);
   a3_1.setStartVariant(Variant::none);
   a3_1.setAlignmentString("TTACCCCAAT");

   std::vector<Alignment> exp3;
   exp3.push_back(a3_1);

   // Test 04

   std::string ref4     = "GATTCAGGACTCTAGTC";
   std::string read4    =   "TTCAACTC";
   ReferenceString refx4(ref4);

   std::vector<Variant> variants4;

   std::shared_ptr<Chromosome> chr4 = std::make_shared<Chromosome>("chr4", refx4, variants4);

   Alignment a4_1;
   a4_1.setCigar("4M2D4M");
   a4_1.setPosition(2);
   a4_1.setStartOffset(0);
   a4_1.setStartVariant(Variant::none);
   a4_1.setAlignmentString("TTCAGGACTC");

   std::vector<Alignment> exp4;
   exp4.push_back(a4_1);

   // Test 05

   std::string ref5     = "CCTGCAAGCGTGAAAACTGGCTACGTTTCGAAGTTTC";
   std::string read5    =          "GTGAGAACTGGCACGTTTCCGAA";
   ReferenceString refx5(ref5);

   std::vector<Variant> variants5;

   std::shared_ptr<Chromosome> chr5 = std::make_shared<Chromosome>("chr5", refx5, variants5);

   Alignment a5_1;
   a5_1.setCigar("4M1X7M1D6M1I4M");
   a5_1.setPosition(9);
   a5_1.setStartOffset(0);
   a5_1.setStartVariant(Variant::none);
   a5_1.setAlignmentString("GTGAAAACTGGCTACGTTTCGAA");

   std::vector<Alignment> exp5;
   exp5.push_back(a5_1);

   // Test 06

   std::string ref6     = "ACCTTGGCGGCCTTACTGGC";
   std::string read6    = "ACCTGGC";
   ReferenceString refx6(ref6);

   std::vector<Variant> variants6;

   std::shared_ptr<Chromosome> chr6 = std::make_shared<Chromosome>("chr6", refx6, variants6);

   Alignment a6_1;
   a6_1.setCigar("3M1D4M");
   a6_1.setPosition(0);
   a6_1.setStartOffset(0);
   a6_1.setStartVariant(Variant::none);
   a6_1.setAlignmentString("ACCTTGGC");
   Alignment a6_2;
   a6_2.setCigar("1M1I5M");
   a6_2.setPosition(14);
   a6_2.setStartOffset(0);
   a6_2.setStartVariant(Variant::none);
   a6_2.setAlignmentString("ACTGGC");

   std::vector<Alignment> exp6;
   exp6.push_back(a6_1);
   exp6.push_back(a6_2);

   // Test 07

   std::string ref7     = "CGGTACAGATGCAGGTAA";
   std::string read7    =     "ACAA";
   ReferenceString refx7(ref7);

   std::vector<Variant> variants7;

   std::shared_ptr<Chromosome> chr7 = std::make_shared<Chromosome>("chr7", refx7, variants7);

//   Alignment a7_1;
//   a7_1.setCigar("3M1X");
//   a7_1.setPosition(4);
//   a7_1.setStartOffset(0);
//   a7_1.setStartVariant(Variant::none);
//   a7_1.setAlignmentString("ACAG");
//   Alignment a7_2;
//   a7_2.setCigar("3M1D1M");
//   a7_2.setPosition(4);
//   a7_2.setStartOffset(0);
//   a7_2.setStartVariant(Variant::none);
//   a7_2.setAlignmentString("ACAGA");
   Alignment a7_3;
   a7_3.setCigar("2M1I1M");
   a7_3.setPosition(4);
   a7_3.setStartOffset(0);
   a7_3.setStartVariant(Variant::none);
   a7_3.setAlignmentString("ACA");

   std::vector<Alignment> exp7;
//   exp7.push_back(a7_1);
//   exp7.push_back(a7_2);
   exp7.push_back(a7_3);

   // Test 08

   std::string ref8     = "AAAAAAAAAAAACCCCCCCCAAAAAAAAAAAA";
   std::string read8    =             "CCCCCCCC";
   ReferenceString refx8(ref8);

   std::vector<Variant> variants8;

   std::shared_ptr<Chromosome> chr8 = std::make_shared<Chromosome>("chr8", refx8, variants8);

   Alignment a8_1;
   a8_1.setCigar("8M");
   a8_1.setPosition(12);
   a8_1.setStartOffset(0);
   a8_1.setStartVariant(Variant::none);
   a8_1.setAlignmentString("CCCCCCCC");

   std::vector<Alignment> exp8;
   exp8.push_back(a8_1);

   // Test 09

   std::string ref9     = "AAACAAACAAACAAACCACCCCCAAACAAACAAACAAA";
   std::string read9    =                "CCCCCCCC";
   ReferenceString refx9(ref9);

   std::vector<Variant> variants9;

   std::shared_ptr<Chromosome> chr9 = std::make_shared<Chromosome>("chr9", refx9, variants9);

   Alignment a9_1;
   a9_1.setCigar("2M1X5M");
   a9_1.setPosition(15);
   a9_1.setStartOffset(0);
   a9_1.setStartVariant(Variant::none);
   a9_1.setAlignmentString("CCACCCCC");

   std::vector<Alignment> exp9;
   exp9.push_back(a9_1);

   // Test 10

   std::string ref10     = "GGAACTAAACGGGCTTCATTCAAACTTCTCAAAGGGACTGGAA";
   std::string read10    =              "CTTCTTC";
   ReferenceString refx10(ref10);

   std::vector<Variant> variants10;

   std::shared_ptr<Chromosome> chr10 = std::make_shared<Chromosome>("chr10", refx10, variants10);

   Alignment a10_1;
   a10_1.setCigar("4M1D3M");
   a10_1.setPosition(13);
   a10_1.setStartOffset(0);
   a10_1.setStartVariant(Variant::none);
   a10_1.setAlignmentString("CTTCATTC");
   Alignment a10_3;
   a10_3.setCigar("4M1I2M");
   a10_3.setPosition(24);
   a10_3.setStartOffset(0);
   a10_3.setStartVariant(Variant::none);
   a10_3.setAlignmentString("CTTCTC");

   std::vector<Alignment> exp10;
   exp10.push_back(a10_1);
   exp10.push_back(a10_3);

   // Test 11

   std::string ref11     = "ATGTAGCGAGCATGCATGCATCGTAGCTAATCGCATGCACCCTACTAACTGTAGATGCGATACAATGCATCGATGCTAGCACACTAGCTATAATGCGCTAGATACGTATGCACGATCGCCTAG";
   std::string read11    =                                         "CCTACTAACTGTAATGGGATACA";
   ReferenceString refx11(ref11);

   std::vector<Variant> variants11;

   std::shared_ptr<Chromosome> chr11 = std::make_shared<Chromosome>("chr11", refx11, variants11);

   Alignment a11_1;
   a11_1.setCigar("13M1D3M1X6M");
   a11_1.setPosition(40);
   a11_1.setStartOffset(0);
   a11_1.setStartVariant(Variant::none);
   a11_1.setAlignmentString("CCTACTAACTGTAGATGCGATACA");

   std::vector<Alignment> exp11;
   exp11.push_back(a11_1);

   // Test 12

   std::string ref12     = "ACGTRCGTAYGTACSTWCGTACGKMMGTACGT";
   std::string read12    =     "GCGTATGTACGTACGTACGGACGT";
   ReferenceString refx12(ref12);

   std::vector<Variant> variants12;

   std::shared_ptr<Chromosome> chr12 = std::make_shared<Chromosome>("chr12", refx12, variants12);

   Alignment a12_1;
   a12_1.setCigar("24M");
   a12_1.setPosition(4);
   a12_1.setStartOffset(0);
   a12_1.setStartVariant(Variant::none);
   a12_1.setAlignmentString("RCGTAYGTACSTWCGTACGKMMGT");

   std::vector<Alignment> exp12;
   exp12.push_back(a12_1);

   // Test 13

   std::string ref13     = "ACGTABBBDCDDHHGHVVVTACGT";
   std::string read13    =     "ACGTACGTACGTACGT";
   ReferenceString refx13(ref13);

   std::vector<Variant> variants13;

   std::shared_ptr<Chromosome> chr13 = std::make_shared<Chromosome>("chr13", refx13, variants13);

   Alignment a13_1;
   a13_1.setCigar("16M");
   a13_1.setPosition(0);
   a13_1.setStartOffset(0);
   a13_1.setStartVariant(Variant::none);
   a13_1.setAlignmentString("ACGTABBBDCDDHHGH");
   Alignment a13_2;
   a13_2.setCigar("16M");
   a13_2.setPosition(4);
   a13_2.setStartOffset(0);
   a13_2.setStartVariant(Variant::none);
   a13_2.setAlignmentString("ABBBDCDDHHGHVVVT");
   Alignment a13_3;
   a13_3.setCigar("16M");
   a13_3.setPosition(8);
   a13_3.setStartOffset(0);
   a13_3.setStartVariant(Variant::none);
   a13_3.setAlignmentString("DCDDHHGHVVVTACGT");

   std::vector<Alignment> exp13;
   exp13.push_back(a13_1);
   exp13.push_back(a13_2);
   exp13.push_back(a13_3);

   // Test 14

   std::string ref14     = "ACGTTCGATGCCCTAATACG";
   std::string read14    =     "CGATTGCCC";
   ReferenceString refx14(ref14);

   std::vector<Variant> variants14;
   variants14.emplace_back(9, "T");
   variants14.emplace_back(18, 2);

   std::shared_ptr<Chromosome> chr14 = std::make_shared<Chromosome>("chr14", refx14, variants14);

   Alignment a14_1;
   a14_1.setCigar("9M");
   a14_1.setPosition(5);
   a14_1.setStartOffset(0);
   a14_1.setStartVariant(Variant::none);
   a14_1.addUsedVariant(chr14->variants[0]);
   a14_1.setAlignmentString("CGATTGCCC");

   std::vector<Alignment> exp14;
   exp14.push_back(a14_1);

   // Test 15

   std::string ref15     = "CKTTCGGCCTCCGARTCGAAACGWT";
   std::string read15    =      "GGCCCCGAAATCG";
   ReferenceString refx15(ref15);

   std::vector<Variant> variants15;
   variants15.emplace_back(15, "A");

   std::shared_ptr<Chromosome> chr15 = std::make_shared<Chromosome>("chr15", refx15, variants15);

   Alignment a15_1;
   a15_1.setCigar("4M1D9M");
   a15_1.setPosition(5);
   a15_1.setStartOffset(0);
   a15_1.setStartVariant(Variant::none);
   a15_1.addUsedVariant(chr15->variants[0]);
   a15_1.setAlignmentString("GGCCTCCGARATCG");

   std::vector<Alignment> exp15;
   exp15.push_back(a15_1);

   // Test 16

   std::string ref16     = "ACCAGTACCGTACATTGGCGA";
   std::string read16    =           "TACACGTTGGC";
   ReferenceString refx16(ref16);

   std::vector<Variant> variants16;
   variants16.emplace_back(14, "C");
   variants16.emplace_back(14, "G");

   std::shared_ptr<Chromosome> chr16 = std::make_shared<Chromosome>("chr16", refx16, variants16);

   Alignment a16_1;
   a16_1.setCigar("5M1I5M");
   a16_1.setPosition(10);
   a16_1.setStartOffset(0);
   a16_1.setStartVariant(Variant::none);
   a16_1.addUsedVariant(chr16->variants[0]);
   a16_1.setAlignmentString("TACACTTGGC");

   Alignment a16_2;
   a16_2.setCigar("4M1I6M");
   a16_2.setPosition(10);
   a16_2.setStartOffset(0);
   a16_2.setStartVariant(Variant::none);
   a16_2.addUsedVariant(chr16->variants[1]);
   a16_2.setAlignmentString("TACAGTTGGC");

   std::vector<Alignment> exp16;
    exp16.push_back(a16_1);
//   exp16.push_back(a16_2); outcommented on purpose

    // Test 17

   std::string ref17     = "CGACACACATGCGTA";
   std::string read17    =      "GACTGCGT";
   ReferenceString refx17(ref17);

   std::vector<Variant> variants17;
   variants17.emplace_back(9, 1);
   variants17.emplace_back(9, 3);
   variants17.emplace_back(9, 5);

   std::shared_ptr<Chromosome> chr17 = std::make_shared<Chromosome>("chr17", refx17, variants17);

   Alignment a17_1;
   a17_1.setCigar("8M");
   a17_1.setPosition(1);
   a17_1.setStartOffset(0);
   a17_1.setStartVariant(Variant::none);
   a17_1.addUsedVariant(chr17->variants[2]);
   a17_1.setAlignmentString("GACTGCGT");

   std::vector<Alignment> exp17;
   exp17.push_back(a17_1);

   // Test 18

   std::string ref18     = "CGACCTACCAGTGGCA";
   std::string read18    =    "CCTACCA";
   ReferenceString refx18(ref18);

   std::vector<Variant> variants18;
   variants18.emplace_back(5, "TA");
   variants18.emplace_back(9, "T");
   variants18.emplace_back(11, 2);
   variants18.emplace_back(12, "A");

   std::shared_ptr<Chromosome> chr18 = std::make_shared<Chromosome>("chr18", refx18, variants18);

   Alignment a18_1;
   a18_1.setCigar("7M");
   a18_1.setPosition(3);
   a18_1.setStartOffset(0);
   a18_1.setStartVariant(Variant::none);
   a18_1.setAlignmentString("CCTACCA");

   std::vector<Alignment> exp18;
   exp18.push_back(a18_1);

   // Test 19

   std::string ref19     = "ATTAGACCGATGGCA";
   std::string read19    = "TTCCGAT";
   ReferenceString refx19(ref19);

   std::vector<Variant> variants19;
   variants19.emplace_back(6, "GGTT");

   std::shared_ptr<Chromosome> chr19 = std::make_shared<Chromosome>("chr19", refx19, variants19);

   Alignment a19_1;
   a19_1.setCigar("7M");
   a19_1.setPosition(6);
   a19_1.setStartOffset(2);
   a19_1.setStartVariant(chr19->variants[0]);
   a19_1.setAlignmentString("TTCCGAT");
   a19_1.addUsedVariant(chr19->variants[0]);

   std::vector<Alignment> exp19;
   exp19.push_back(a19_1);

   // Test 20

   std::string ref20     = "GGTCATTCAGCAC";
   std::string read20    = "TGATTCATT";
   ReferenceString refx20(ref20);

   std::vector<Variant> variants20;
   variants20.emplace_back(9, "TTCA");

   std::shared_ptr<Chromosome> chr20 = std::make_shared<Chromosome>("chr20", refx20, variants20);

   Alignment a20_1;
   a20_1.setCigar("1M1X7M");
   a20_1.setPosition(2);
   a20_1.setStartOffset(0);
   a20_1.setStartVariant(Variant::none);
   a20_1.setAlignmentString("TCATTCATT");
   a20_1.addUsedVariant(chr20->variants[0]);

   std::vector<Alignment> exp20;
   exp20.push_back(a20_1);

   // Test 21

   std::string ref21     = "CGCACGAAACCGACGGACGCGGACCGA";
   std::string read21    =      "GCAACCGACTTTCACGGA";
   ReferenceString refx21(ref21);

   std::vector<Variant> variants21;
   variants21.emplace_back(12, "ACTTTC");

   std::shared_ptr<Chromosome> chr21 = std::make_shared<Chromosome>("chr21", refx21, variants21);

   Alignment a21_1;
   a21_1.setCigar("1M1X16M");
   a21_1.setPosition(5);
   a21_1.setStartOffset(0);
   a21_1.setStartVariant(Variant::none);
   a21_1.setAlignmentString("GAAACCGACTTTCACGGA");
   a21_1.addUsedVariant(chr21->variants[0]);

   std::vector<Alignment> exp21;
   exp21.push_back(a21_1);

   // Test 22

   std::string ref22     = "CGCACGAAACCGACGGACGCGGACCGA";
   std::string read22    =      "GAAAAAAACGG";
   ReferenceString refx22(ref22);

   std::vector<Variant> variants22;
   variants22.emplace_back(12, "AAA", 3);

   std::shared_ptr<Chromosome> chr22 = std::make_shared<Chromosome>("chr22", refx22, variants22);

   Alignment a22_1;
   a22_1.setCigar("11M");
   a22_1.setPosition(5);
   a22_1.setStartOffset(0);
   a22_1.setStartVariant(Variant::none);
   a22_1.setAlignmentString("GAAAAAAACGG");
   a22_1.addUsedVariant(chr22->variants[0]);

   std::vector<Alignment> exp22;
   exp22.push_back(a22_1);

   // Test 23

   std::string ref23     = "ATTCGCCCGATGGCA";
   std::string read23    =  "TTATACCGAT";
   ReferenceString refx23(ref23);

   std::vector<Variant> variants23;
   variants23.emplace_back(6, "AAA", 3);

   std::shared_ptr<Chromosome> chr23 = std::make_shared<Chromosome>("chr23", refx23, variants23);

   Alignment a23_1;
   a23_1.setCigar("3M1X6M");
   a23_1.setPosition(1);
   a23_1.setStartOffset(0);
   a23_1.setStartVariant(Variant::none);
   a23_1.setAlignmentString("TTAAACCGAT");
   a23_1.addUsedVariant(chr23->variants[0]);

   std::vector<Alignment> exp23;
   exp23.push_back(a23_1);

   // Test 24

   std::string ref24     = "ATTCGCATGCATCGCTAGCATCCGATGGCA";
   std::string read24    =       "TGTGATCGCTATCC";
   ReferenceString refx24(ref24);

   std::vector<Variant> variants24;
   variants24.emplace_back(10, "GTG", 2);
   variants24.emplace_back(21, "T", 4);

   std::shared_ptr<Chromosome> chr24 = std::make_shared<Chromosome>("chr24", refx24, variants24);

   Alignment a24_1;
   a24_1.setCigar("14M");
   a24_1.setPosition(7);
   a24_1.setStartOffset(0);
   a24_1.setStartVariant(Variant::none);
   a24_1.setAlignmentString("TGTGATCGCTATCC");
   a24_1.addUsedVariant(chr24->variants[0]);
   a24_1.addUsedVariant(chr24->variants[1]);

   std::vector<Alignment> exp24;
   exp24.push_back(a24_1);

   // Test 25

   std::string ref25     = "ATTCGCATGCATCGCTAGCATCCGATGGCA";
   std::string read25    =       "GGGCATCGCGAGCAT";
   ReferenceString refx25(ref25);

   std::vector<Variant> variants25;
   variants25.emplace_back(8, "GGGG", 4);

   std::shared_ptr<Chromosome> chr25 = std::make_shared<Chromosome>("chr25", refx25, variants25);

   Alignment a25_1;
   a25_1.setCigar("9M1X5M");
   a25_1.setPosition(8);
   a25_1.setStartOffset(1);
   a25_1.setStartVariant(chr25->variants[0]);
   a25_1.setAlignmentString("GGGCATCGCTAGCAT");
   a25_1.addUsedVariant(chr25->variants[0]);

   std::vector<Alignment> exp25;
   exp25.push_back(a25_1);

   // Test 26

   std::string ref26     = "ACGATGCACGCGCGATAACGGCGCCGCATACTCTCGCAGAGAGATACGTAGCGATGCATGTCATGCTACGTAGCATCGAGCATATATATCGCGCGATATGCAGCATTGCAGTCAGAGTATCTCAGCGCGGAGCAGACGGTACGCACGAGCCATCTAGTACGTCAGCGCAGCTAGTACCGTACGATGCTAGACTACGTGCTATGACGTACGTCAGCTCATGACGTGACGACACGTACGTGTCATGACGTACTGACTACGACTGATGCACGTACTGGACTACTGGCTAGCTAGACTACGTACTTATCGACCTGAGCAGCTGCATGCATCGATGCATGCTAGATGCATGCTAGCACACGATGCATCATGCTACGCGCTAAATGACCGCTAGCTATTATAGCCTGCATGATCGTACGCATGCATCGAACTACGTAGCATGCTAGCGCGTCAGAGAGAGAGAATCTCGCATCGAGCATGCTACGTAGCTAG";
   std::string read26    =                                                                                                                                                                                                               "TACGTCAGCTCATGACGTGACGACACGTACGTGTCATGACGTACTGACTACGACTGATGCACGTACTGGACTACTGGCTAGCTAGACTACGTACTTATCGACCTGAGCAGCTGCATGCATCGATGCATGCTAGATGCATGCTAGCACACGAT";
   ReferenceString refx26(ref26);

   std::vector<Variant> variants26;

   std::shared_ptr<Chromosome> chr26 = std::make_shared<Chromosome>("chr26", refx26, variants26);

   Alignment a26_1;
   a26_1.setCigar("152M");
   a26_1.setPosition(206);
   a26_1.setStartOffset(0);
   a26_1.setStartVariant(Variant::none);
   a26_1.setAlignmentString("TACGTCAGCTCATGACGTGACGACACGTACGTGTCATGACGTACTGACTACGACTGATGCACGTACTGGACTACTGGCTAGCTAGACTACGTACTTATCGACCTGAGCAGCTGCATGCATCGATGCATGCTAGATGCATGCTAGCACACGAT");

   std::vector<Alignment> exp26;
   exp26.push_back(a26_1);

   // Test 27

   std::string ref27     = "ATTCAAA";
   std::string read27    = "TGACTTTTTTTTTTTTTT";
   ReferenceString refx27(ref27);

   std::vector<Variant> variants27;
   variants27.emplace_back(4, "AAT");
   variants27.emplace_back(4, "ATA");

   std::shared_ptr<Chromosome> chr27 = std::make_shared<Chromosome>("chr27", refx27, variants27);

   std::vector<Alignment> exp27;

   // Test 28

   std::string ref28     = "CACACACATATACACATATATTTTTATATRTACACATATTTATATATACACATATATTTTTATATACACACACACAYATTTATACACACACACACGCAGGTGGCATGCCTRTACACATATATATTTATACA";
   std::string read28    = "ATACACACACATATACACATATATTTTGATATATACACATATTTATATATACACATATATTTTTATATACACACACACATATTTATACACACACACACGC";
   ReferenceString refx28(ref28);

   std::vector<Variant> variants28;
   variants28.emplace_back(8, 2);
   variants28.emplace_back(16, 2);
   variants28.emplace_back(76, 2);
   variants28.emplace_back(76, "CA", 2);

   std::shared_ptr<Chromosome> chr28 = std::make_shared<Chromosome>("chr28", refx28, variants28);

   std::vector<Alignment> exp28;

   // Test 29

   std::string ref29     = "CCCCCCCCCCCCCAAAACCCCCCCCCCCC";
   std::string read29    =          "CCCCAAAACCCCTTTTT";
   ReferenceString refx29(ref29);

   std::vector<Variant> variants29;

   std::shared_ptr<Chromosome> chr29 = std::make_shared<Chromosome>("chr29", refx29, variants29);

   Alignment a29_1;
   a29_1.setCigar("12M5S");
   a29_1.setPosition(9);
   a29_1.setStartOffset(0);
   a29_1.setStartVariant(Variant::none);
   a29_1.setAlignmentString("CCCCAAAACCCC");

   std::vector<Alignment> exp29;
   exp29.push_back(a29_1);

   // Test 30

   std::string ref30     = "CCCCCCCCCCCCCAAAACCCCCCCCCCCC";
   std::string read30    =     "TTTTTCCCCAAAACCCC";
   ReferenceString refx30(ref30);

   std::vector<Variant> variants30;

   std::shared_ptr<Chromosome> chr30 = std::make_shared<Chromosome>("chr30", refx30, variants30);

   Alignment a30_1;
   a30_1.setCigar("5S12M");
   a30_1.setPosition(9);
   a30_1.setStartOffset(0);
   a30_1.setStartVariant(Variant::none);
   a30_1.setAlignmentString("CCCCAAAACCCC");

   std::vector<Alignment> exp30;
   exp30.push_back(a30_1);

   // Test 31

   std::string ref31     = "CCCCCCCCCCCCCAAAACCCCCCCCCCCC";
   std::string read31    =     "TTTTTCCCCAAAACCCCTTTTT";
   ReferenceString refx31(ref31);

   std::vector<Variant> variants31;

   std::shared_ptr<Chromosome> chr31 = std::make_shared<Chromosome>("chr31", refx31, variants31);

   Alignment a31_1;
   a31_1.setCigar("5S12M5S");
   a31_1.setPosition(9);
   a31_1.setStartOffset(0);
   a31_1.setStartVariant(Variant::none);
   a31_1.setAlignmentString("CCCCAAAACCCC");

   std::vector<Alignment> exp31;
   exp31.push_back(a31_1);

   // Test 32

   std::string ref32     = "CCCCCCCCCCCCCAAAACCCCGGCCCCCC";
   std::string read32    =          "CCCCAAAACCCCGGTTT";
   ReferenceString refx32(ref32);

   std::vector<Variant> variants32;

   std::shared_ptr<Chromosome> chr32 = std::make_shared<Chromosome>("chr32", refx32, variants32);

   Alignment a32_1;
   a32_1.setCigar("14M1I2S");
   a32_1.setPosition(9);
   a32_1.setStartOffset(0);
   a32_1.setStartVariant(Variant::none);
   a32_1.setAlignmentString("CCCCAAAACCCCGG");

   std::vector<Alignment> exp32;
   exp32.push_back(a32_1);

   // Test 33

   std::string ref33     = "CCCCCCCCCCGGCCCCAAAACCCCGGCCCCCC";
   std::string read33    =        "TTTGGCCCCAAAACCCC";
   ReferenceString refx33(ref33);

   std::vector<Variant> variants33;

   std::shared_ptr<Chromosome> chr33 = std::make_shared<Chromosome>("chr33", refx33, variants33);

   Alignment a33_1;
   a33_1.setCigar("3S14M");
   a33_1.setPosition(10);
   a33_1.setStartOffset(0);
   a33_1.setStartVariant(Variant::none);
   a33_1.setAlignmentString("GGCCCCAAAACCCC");

   std::vector<Alignment> exp33;
   exp33.push_back(a33_1);

   QTest::newRow("Exact alignment, no variants") << chr1 << read1 << exp1 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Alignment with mismatch, no variants") << chr2 << read2 << exp2 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Alignment with insertion, no variants") << chr3 << read3 << exp3 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Alignment with deletion, no variants") << chr4 << read4 << exp4 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Alignment with multiple error types, no variants") << chr5 << read5 << exp5 << (ErrorCount)3 << (ErrorCount)0;

   QTest::newRow("Multiple alignments, no variants") << chr6 << read6 << exp6 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Multiple alignments with different error types in same position, no variants") << chr7 << read7 << exp7 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Exact alignment with optimzer potential, no variants") << chr8 << read8 << exp8 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Alignment with error and optimizer potential, no variants") << chr9 << read9 << exp9 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Multiple alignments with erros and optimizer potential, no variants") << chr10 << read10 << exp10 << (ErrorCount)3 << (ErrorCount)0;

   QTest::newRow("Long alignment with two erros, no variants") << chr11<< read11 << exp11 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("IUPAC characters with two bases, only SNP variants") << chr12 << read12 << exp12 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Multiple alignments with IUPAC characters with three bases, only SNP variants") << chr13 << read13 << exp13 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Exact alignment with insertion variant") << chr14 << read14 << exp14 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Non-exact Alignment with insertion variant and SNPs") << chr15 << read15 << exp15 << (ErrorCount)3 << (ErrorCount)0;

   QTest::newRow("Multiple insertions at same position") << chr16 << read16 << exp16 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Multiple deletions at same position") << chr17 << read17 << exp17 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Many unused variants") << chr18 << read18 << exp18 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Alignment start in insertion") << chr19 << read19 << exp19 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Alignment start in insertion") << chr20 << read20 << exp20 << (ErrorCount)3 << (ErrorCount)0;

   QTest::newRow("Long alignment with optimization potential and insertion variants") << chr21 << read21 << exp21 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Exact alignment with substitution") << chr22 << read22 << exp22 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Substition with error") << chr23 << read23 << exp23 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Substition with length variation") << chr24 << read24 << exp24 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Alignment starting in substitution") << chr25 << read25 << exp25 << (ErrorCount)3 << (ErrorCount)0;

   QTest::newRow("Veeeeeeeeeeeeeeeeeeeeeeery long reference string and read") << chr26 << read26 << exp26 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("This test should just check whether the aligner crashes") << chr27 << read27 << exp27 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("This test should just check whether the aligner crashes") << chr28 << read28 << exp28 << (ErrorCount)3 << (ErrorCount)0;
   QTest::newRow("Simple suffix clipping") << chr29 << read29 << exp29 << (ErrorCount)0 << (ErrorCount)5;
   QTest::newRow("Simple prefix clipping") << chr30 << read30 << exp30 << (ErrorCount)0 << (ErrorCount)5;

   QTest::newRow("Simple prefix and suffix clipping") << chr31 << read31 << exp31 << (ErrorCount)0 << (ErrorCount)5;
   QTest::newRow("Unfull suffix clip") << chr32 << read32 << exp32 << (ErrorCount)1 << (ErrorCount)5;
   QTest::newRow("Unfull prefix clip") << chr33 << read33 << exp33 << (ErrorCount)1 << (ErrorCount)5;
}

void AlignerTest::alignerTest() {

   QFETCH(std::shared_ptr<Chromosome>, chr);
   QFETCH(std::string, read);
   QFETCH(std::vector<Alignment>, expAlignments);
   QFETCH(ErrorCount, maxErrors);
   QFETCH(ErrorCount, maxClip);

   ReferenceString readx(read);

   // Initialize read patterns
   ReadString pattern(readx);

   auto chromosomeLength = seqan::length(chr->data);
   auto startPos = maxClip;
   auto endPos = chromosomeLength;

   // Voodoo
   VariantIndex variantIndex(chr->variants, chromosomeLength - 1);
   Aligner aligner(maxErrors, maxClip);

   // Actual Alignment
   Alignment alignment = aligner.align(*chr, startPos, endPos, variantIndex, pattern);

   if (expAlignments.size() > 0) {
       QCOMPARE(expAlignments[0], alignment);
   } else {
       QVERIFY(!alignment.isValid());
   }
}
