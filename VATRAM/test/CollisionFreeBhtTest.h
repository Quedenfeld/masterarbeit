#ifndef COLLISIONFREEBHTTEST_H
#define COLLISIONFREEBHTTEST_H

#include <QString>
#include <QtTest>

class CollisionFreeBhtTest : public QObject {
    Q_OBJECT
    const size_t maximumOfReturnedWindows = 10;
public:
    CollisionFreeBhtTest();

private Q_SLOTS:
    void collisionFreeBhtTest();
    void collisionFreeBhtTest_data();
    void randomTest();
    void removeTest();
};
#endif // BANDHASHTABLETEST_H
