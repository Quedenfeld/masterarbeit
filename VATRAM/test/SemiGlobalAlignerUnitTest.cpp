#include "SemiGlobalAlignerUnitTest.h"

SemiGlobalAlignerUnitTest::SemiGlobalAlignerUnitTest() {
}

bool SemiGlobalAlignerUnitTest::equals(Alignment &a1, Alignment &a2) {
//    std::cout << "Compare alignmetns (exp vs real)" << std::endl;
//    std::cout << "Cigar   : " << toString(a1.getCigar()) << " : " << toString(a2.getCigar()) << std::endl;
//    std::cout << "Position: " << a1.getPosition() << " : " << a2.getPosition() << std::endl;
//    std::cout << "Align   : " << a1.getAlignmentString() << " : " << a2.getAlignmentString() << std::endl;
//    std::cout << "StartOff: " << a1.getStartOffset() << " : " << a2.getStartOffset() << std::endl;
//    std::cout << "StartVar: " << a1.getStartVariant() << " : " << a2.getStartVariant() << std::endl;
//    std::cout << "UsedVar: " << a1.getUsedVariants() << " : " << a2.getUsedVariants() << std::endl;
    if (a1.getCigar() != a2.getCigar())
        return false;
    if (a1.getPosition() != a2.getPosition())
        return false;
    if (a1.getAlignmentString() != a2.getAlignmentString())
        return false;
    if (a1.getStartOffset() != a1.getStartOffset())
        return false;
    if (!(a1.getStartVariant() == a2.getStartVariant())) //TODO: Implement comparison of variants
        return false;
    if (a1.getUsedVariants() != a2.getUsedVariants()) //TODO: Implement comparison of variant sets
        return false;
    return true;
}

bool SemiGlobalAlignerUnitTest::matches(std::vector<Alignment> &as1, std::vector<Alignment> &as2) {
    // as1 contained in as2?
    for (Alignment &a1 : as1) {
        bool contained = false;
        for (Alignment &a2 : as2) {
            if (equals(a1, a2)) {
                contained = true;
                break;
            }
        }
        if (!contained) {
            return false;
        }
    }

    // as2 contained in as2?
    for (Alignment &a2 : as2) {
        bool contained = false;
        for (Alignment &a1 : as1) {
            if (equals(a1, a2)) {
                contained = true;
                break;
            }
        }
        if (!contained) {
            return false;
        }
    }

    return true;
}

bool SemiGlobalAlignerUnitTest::test(std::vector<Variant> &variants, std::string &ref, std::string &read, std::vector<Alignment> &expAlignments) {
    int maxErrors = 3;

    // Generate Reference Object
    Chromosome chromosome("testChromosome01", ref, variants);

    // Initialize read patterns
    ReadString pattern(read);

    auto chromosomeLength = seqan::length(chromosome.data);
    auto startPos = 0;
    auto endPos = chromosomeLength;

    // Voodoo
    VariantIndex variantIndex(chromosome.variants, chromosomeLength - 1);
    SemiGlobalAligner aligner(variantIndex);

    // Actual Alignment
    std::vector<Alignment> alignments = aligner.align(chromosome, startPos, endPos, pattern, maxErrors);

    // Testing
    return matches(expAlignments, alignments);
}

bool SemiGlobalAlignerUnitTest::testCase01() {
    // Exact alignment, no variants
    std::string ref     = "ATTCGTAGCTAAATCG";
    std::string read    =     "GTAGCT";

    Alignment a1;
    a1.setCigar("6M");
    a1.setPosition(4);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("GTAGCT");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase02() {
    // Alignment with mismatch, no variants
    std::string ref     = "TACGGACTCCCTGACCAGT";
    std::string read    =      "ACTCACTG";

    Alignment a1;
    a1.setCigar("4M1X3M");
    a1.setPosition(5);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("ACTCCCTG");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase03() {
    // Alignment with insertion, no variants
    std::string ref     = "CCGATTACCCCAATTG";
    std::string read    =     "TTACCCCGGAAT";

    Alignment a1;
    a1.setCigar("7M2I3M");
    a1.setPosition(4);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("TTACCCCAAT");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase04() {
    // Alignment with deletion, no variants
    std::string ref     = "GATTCAGGACTCTAGTC";
    std::string read    =   "TTCAACTC";

    Alignment a1;
    a1.setCigar("4M2D4M");
    a1.setPosition(2);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("TTCAGGACTC");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase05() {
    // Alignment with multiple error types, no variants
    std::string ref     = "CCTGCAAGCGTGAAAACTGGCTACGTTTCGAAGTTTC";
    std::string read    =          "GTGAGAACTGGCACGTTTCCGAA";

    Alignment a1;
    a1.setCigar("4M1X7M1D7M1I3M");
    a1.setPosition(9);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("GTGAAAACTGGCTACGTTTCGAA");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase06() {
    // Multiple alignments, no variants
    std::string ref     = "ACCTTGGCGGCCTTACTGGC";
    std::string read    = "ACCTGGC";

    Alignment a1;
    a1.setCigar("4M1D3M");
    a1.setPosition(0);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("ACCTTGGC");
    Alignment a2;
    a2.setCigar("2M1I4M");
    a2.setPosition(14);
    a2.setStartOffset(0);
    a2.setStartVariant(Variant::none);
    a2.setAlignmentString("ACTGGC");
    Alignment a3;
    a3.setCigar("3M1D4M");
    a3.setPosition(0);
    a3.setStartOffset(0);
    a3.setStartVariant(Variant::none);
    a3.setAlignmentString("ACCTTGGC");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);
    expAlignments.push_back(a2);
    expAlignments.push_back(a3);

    std::vector<Variant> variants;

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase07() {
    // Multiple alignments with different error types in same position, no variants
    std::string ref     = "CGGTACAGATGCAGGTAA";
    std::string read    =     "ACAA";

    Alignment a1;
    a1.setCigar("3M1X");
    a1.setPosition(4);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("ACAG");
    Alignment a2;
    a2.setCigar("3M1D1M");
    a2.setPosition(4);
    a2.setStartOffset(0);
    a2.setStartVariant(Variant::none);
    a2.setAlignmentString("ACAGA");
    Alignment a3;
    a3.setCigar("3M1I");
    a3.setPosition(4);
    a3.setStartOffset(0);
    a3.setStartVariant(Variant::none);
    a3.setAlignmentString("ACA");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);
    expAlignments.push_back(a2);
    expAlignments.push_back(a3);

    std::vector<Variant> variants;

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase08() {
    // Exact alignment with optimzer potential, no variants
    std::string ref     = "AAAAAAAAAAAACCCCCCCCAAAAAAAAAAAA";
    std::string read    =             "CCCCCCCC";

    Alignment a1;
    a1.setCigar("8M");
    a1.setPosition(12);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("CCCCCCCC");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase09() {
    // Alignment with error and optimizer potential, no variants
    std::string ref     = "AAACAAACAAACAAACCACCCCCAAACAAACAAACAAA";
    std::string read    =                "CCCCCCCC";

    Alignment a1;
    a1.setCigar("2M1X5M");
    a1.setPosition(15);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("CCACCCCC");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase10() {
    // Multiple alignments with erros and optimizer potential, no variants
    std::string ref     = "GGAACTAAACGGGCTTCATTCAAACTTCTCAAAGGGACTGGAA";
    std::string read    =              "CTTCTTC";

    Alignment a1;
    a1.setCigar("4M1D3M");
    a1.setPosition(13);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("CTTCATTC");
    Alignment a2;
    a2.setCigar("5M1I1M");
    a2.setPosition(24);
    a2.setStartOffset(0);
    a2.setStartVariant(Variant::none);
    a2.setAlignmentString("CTTCTC");
    Alignment a3;
    a3.setCigar("4M1I2M");
    a3.setPosition(24);
    a3.setStartOffset(0);
    a3.setStartVariant(Variant::none);
    a3.setAlignmentString("CTTCTC");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);
    expAlignments.push_back(a2);
    expAlignments.push_back(a3);

    std::vector<Variant> variants;

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase11() {
    // Long alignment with two erros, no variants
    std::string ref     = "ATGTAGCGAGCATGCATGCATCGTAGCTAATCGCATGCACCCTACTAACTGTAGATGCGATACAATGCATCGATGCTAGCACACTAGCTATAATGCGCTAGATACGTATGCACGATCGCCTAG";
    std::string read    =                                         "CCTACTAACTGTAATGGGATACA";

    Alignment a1;
    a1.setCigar("13M1D3M1X6M");
    a1.setPosition(40);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("CCTACTAACTGTAGATGCGATACA");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase12() {
    // IUPAC characters with two bases, only SNP variants
    std::string ref     = "ACGTRCGTAYGTACSTWCGTACGKMMGTACGT";
    std::string read    =     "GCGTATGTACGTACGTACGGACGT";

    Alignment a1;
    a1.setCigar("24M");
    a1.setPosition(4);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("RCGTAYGTACSTWCGTACGKMMGT");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase13() {
    // Multiple alignments with IUPAC characters with three bases, only SNP variants
    std::string ref     = "ACGTABBBDCDDHHGHVVVTACGT";
    std::string read    =     "ACGTACGTACGTACGT";

    Alignment a1;
    a1.setCigar("16M");
    a1.setPosition(0);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("ACGTABBBDCDDHHGH");
    Alignment a2;
    a2.setCigar("16M");
    a2.setPosition(4);
    a2.setStartOffset(0);
    a2.setStartVariant(Variant::none);
    a2.setAlignmentString("ABBBDCDDHHGHVVVT");
    Alignment a3;
    a3.setCigar("16M");
    a3.setPosition(8);
    a3.setStartOffset(0);
    a3.setStartVariant(Variant::none);
    a3.setAlignmentString("DCDDHHGHVVVTACGT");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);
    expAlignments.push_back(a2);
    expAlignments.push_back(a3);

    std::vector<Variant> variants;

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase14() {
    // Exact alignment with insertion variant
    std::string ref     = "ACGTTCGATGCCCTAATACG";
    std::string read    =     "CGATTGCCC";

    Variant v0(9, "T");
    Variant v1(18, 2);

    Alignment a1;
    a1.setCigar("9M");
    a1.setPosition(5);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.addUsedVariant(v0);
    a1.setAlignmentString("CGATTGCCC");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;
    variants.push_back(v0);
    variants.push_back(v1);

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase15() {
    // Non-exact Alignment with insertion variant and SNPs
    std::string ref     = "CKTTCGGCCTCCGARTCGAAACGWT";
    std::string read    =      "GGCCCCGAAATCG";

    Variant v0(15, "A");

    Alignment a1;
    a1.setCigar("4M1D9M");
    a1.setPosition(5);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.addUsedVariant(v0);
    a1.setAlignmentString("GGCCTCCGARATCG");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;
    variants.push_back(v0);

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase16() {
    // Multiple insertions at same position
    std::string ref     = "ACCAGTACCGTACATTGGCGA";
    std::string read    =           "TACACGTTGGC";

    Variant v0(14, "C");
    Variant v1(14, "G");

    Alignment a1;
    a1.setCigar("5M1I5M");
    a1.setPosition(10);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.addUsedVariant(v0);
    a1.setAlignmentString("TACACTTGGCGA");

    Alignment a2;
    a2.setCigar("4M1I6M");
    a2.setPosition(10);
    a2.setStartOffset(0);
    a2.setStartVariant(Variant::none);
    a2.addUsedVariant(v1);
    a2.setAlignmentString("TACAGTTGGCGA");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);
    expAlignments.push_back(a2);

    std::vector<Variant> variants;
    variants.push_back(v0);
    variants.push_back(v1);

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase17() {
    // Multiple deletions at same position
    std::string ref     = "CGACACACATGCGTA";
    std::string read    =      "CACTGCGT";

    Variant v0(9, 1);
    Variant v1(9, 3);

    Alignment a1;
    a1.setCigar("8M");
    a1.setPosition(5);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.addUsedVariant(v0);
    a1.setAlignmentString("CACTGCGT");
    a1.addUsedVariant(v0);
    Alignment a2;
    a2.setCigar("8M");
    a2.setPosition(3);
    a2.setStartOffset(0);
    a2.setStartVariant(Variant::none);
    a2.addUsedVariant(v1);
    a2.setAlignmentString("CACTGCGT");
    a2.addUsedVariant(v1);

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);
    expAlignments.push_back(a2);

    std::vector<Variant> variants;
    variants.push_back(v0);
    variants.push_back(v1);

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase18() {
    // Many unused variants
    std::string ref     = "CGACCTACCAGTGGCA";
    std::string read    =    "CCTACCA";

    Variant v0(5, "TA");
    Variant v1(9, "T");
    Variant v2(11, 2);
    Variant v3(12, "A");

    Alignment a1;
    a1.setCigar("7M");
    a1.setPosition(3);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("CCTACCA");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;
    variants.push_back(v0);
    variants.push_back(v1);
    variants.push_back(v2);
    variants.push_back(v3);

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase19() {
    // Alignment start in insertion
    std::string ref     = "ATTAGACCGATGGCA";
    std::string read    = "TTCCGAT";

    Variant v0(6, "GGTT");

    Alignment a1;
    a1.setCigar("7M");
    a1.setPosition(6);
    a1.setStartOffset(2);
    a1.setStartVariant(v0);
    a1.setAlignmentString("TTCCGAT");
    a1.addUsedVariant(v0);

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;
    variants.push_back(v0);

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase20() {
    // Alignment start in insertion
    std::string ref     = "GGTCATTCAGCAC";
    std::string read    = "TGATTCATT";

    Variant v0(9, "TTCA");

    Alignment a1;
    a1.setCigar("1M1X7M");
    a1.setPosition(2);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("TCATTCATT");
    a1.addUsedVariant(v0);

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;
    variants.push_back(v0);

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase21() {
    // Long alignment with optimization potential and insertion variants
    std::string ref     = "CGCACGAAACCGACGGACGCGGACCGA";
    std::string read    =      "GCAACCGACTTTCACGGA";

    Variant v0(12, "ACTTTC");

    Alignment a1;
    a1.setCigar("1M1X16M");
    a1.setPosition(5);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.setAlignmentString("GAAACCGACTTTCACGGA");
    a1.addUsedVariant(v0);

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;
    variants.push_back(v0);

    return test(variants, ref, read, expAlignments);
}

bool SemiGlobalAlignerUnitTest::testCase22() {
    // Multiple insertions at same position
    std::string ref     = "AAAAAAAAAAAAAAAAAAAAAAAA";
    std::string read    =     "AAAAAACGTAAAAAA";

    Variant v0(10, "C");
    Variant v1(10, "G");
    Variant v2(10, "T");

    Alignment a1;
    a1.setCigar("15M");
    a1.setPosition(4);
    a1.setStartOffset(0);
    a1.setStartVariant(Variant::none);
    a1.addUsedVariant(v0);
    a1.addUsedVariant(v1);
    a1.addUsedVariant(v2);
    a1.setAlignmentString("AAAAAACGTAAAAAA");

    std::vector<Alignment> expAlignments;
    expAlignments.push_back(a1);

    std::vector<Variant> variants;
    variants.push_back(v0);
    variants.push_back(v1);
    variants.push_back(v2);

    return test(variants, ref, read, expAlignments);
}
