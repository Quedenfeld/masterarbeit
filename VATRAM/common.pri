CONFIG -= qt
CONFIG -= app_bundle
QT -= gui
CONFIG += c++11

CONFIG(release, release|debug) {
    QMAKE_CXXFLAGS += -O2 -g -march=native
    DEFINES += NDEBUG
}

QMAKE_CXXFLAGS += -Wall -Wextra
QMAKE_CXXFLAGS += -pedantic-errors

*-g++* {
    # Enable openmp (may not work with Clang and MSVC)
    QMAKE_CXXFLAGS += -fopenmp
    QMAKE_LFLAGS   += -fopenmp
}

# CONFIG += clang_doxygen_check
# CONFIG += enable_deprecated_warnings

clang_doxygen_check {
    !build_pass:warning("Warnings inside SeqAn are suppressed when checking Doxygen strings.")
    QMAKE_CXXFLAGS += -Wdocumentation --system-header-prefix=seqan/
}

enable_deprecated_warnings {
} else {
    # Disables "'auto_ptr' is deprecated" warnings in SeqAn.
    QMAKE_CXXFLAGS += -Wno-deprecated-declarations

    clang* {
        # Disables "'register' storage class is deprecated" warnings in SeqAn.
        QMAKE_CXXFLAGS += -Wno-deprecated-register
    }
}

macx {
    !host_build:QMAKE_MAC_SDK = macosx10.9
}

win32 {
    LIBS += -lpsapi
}

unix {
    DEFINES += PROFILING
    INCLUDEPATH += /home/quedenfe/gperftools/install/include
    LIBS += -L/home/quedenfe/gperftools/install/lib
    LIBS += -lprofiler
    LIBS += -L/home/quedenfe/gperftools/libunwind/lib
    LIBS += -lunwind
}

DEFINES += LSH_MAKE_VIRTUAL
#DEFINES += LSH_DEBUG
#DEFINES += MEASURE_STATISTICS_IN_ALIGNER
#DEFINES += ALIGNER_DEBUG_OUTPUT
#DEFINES += ALIGNER_USE_HASHING

# Enable gzip support in seqan. Disable if you're missing zlib.
DEFINES += SEQAN_HAS_ZLIB
LIBS += -lz
