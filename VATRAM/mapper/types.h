#ifndef PG583_TYPES_H
#define PG583_TYPES_H

#include <seqan/sequence.h>

typedef uint32_t ChromosomeIndex;
typedef uint32_t Position;


typedef seqan::Iupac ReferenceChar;
typedef seqan::String<ReferenceChar, seqan::Packed<>> ReferenceString;
typedef seqan::Dna5/*Iupac*/ ReadChar;
typedef seqan::String<ReadChar/*, seqan::Packed<>*/> ReadString;

typedef seqan::Value<ReferenceChar>::Type ReferenceCharValue;
typedef seqan::Value<ReadChar>::Type ReferenceCharValue;

#endif // PG583_TYPES_H
