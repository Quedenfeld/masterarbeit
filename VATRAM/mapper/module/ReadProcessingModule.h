#ifndef PG583_MODULE_READPROCESSINGMODULE_H
#define PG583_MODULE_READPROCESSINGMODULE_H

#include "module/ApplicationModule.h"
#include "module/ModuleOptions.h"
#include "io/SamWriter.h"
#include "align/Aligner.h"
#include "map/LSH.h" // only needed for MapInterval, ChromosomeIndex, LSH::Position
#include "parallel.h"

#include <seqan/seq_io.h>

#include <iostream>

class ReadProcessingModule : virtual public ApplicationModule {
public:
    ReadProcessingModule();
    virtual void addOptions(seqan::ArgumentParser &argParser) override;
    virtual void readOptions(seqan::ArgumentParser &argParser) override;

    struct AlignmentStats {
        size_t reads = 0, unmapped = 0, unaligned = 0, mappings = 0, alignments = 0, alignerCalls = 0, minErrors = 0,
               alignedIntervalSize = 0;
        void add(const AlignmentStats &increase) {
            reads += increase.reads;
            unmapped += increase.unmapped;
            unaligned += increase.unaligned;
            mappings += increase.mappings;
            alignments += increase.alignments;
            alignerCalls += increase.alignerCalls;
            minErrors += increase.minErrors;
            alignedIntervalSize += increase.alignedIntervalSize;
        }
    } totalStats;

    struct BamTagInterval {
        uint32_t chromosome, startPos, endPos, inverted;
    };

protected:
    std::vector<std::string> readsFilePaths;
    bool isPairedEnd;
    IntOption packageSizeOption;
    IntOption maxIntervalsOption;
    std::string outputFilePath;
    std::string unalignedOutputFilePath;
    SamWriter samWriter;
    SamWriter unalignedSamWriter;
    seqan::BamStream bstream;
    seqan::BamStream unalignedBamStream;
    Mutex outputMutex;
    bool shouldSeparateUnalignedOutput;

    bool noRemap = false;
    bool noRealign = false;

    void printStats(std::ostream &os, size_t packageStart, const AlignmentStats &packageStats) const;

    void processReadsFiles(const ChromosomeReferences &chromosomes);

    // readStreamCount is 1 (single end) or 2 (paired end)
    template<class TStream>
    void processReadsFile(TStream* readsStream, size_t readsStreamCount, const ChromosomeReferences &chromosomes,
                          const std::vector<ChromosomeIndex> &bamRefIdToChromosomeIndex = {});

    virtual AlignmentStats processRead(Aligner &aligner, PairedRead& read) = 0;

    template<class TStream>
    size_t readBatch(seqan::StringSet<seqan::CharString> &ids, seqan::StringSet<seqan::Dna5String> &seqs,
                     std::vector<seqan::BamAlignmentRecord> &bamRecords, TStream &stream,
                     size_t requestedPackageSize);

    template<class TStream>
    void prepareBatch(std::vector<seqan::StringSet<seqan::CharString>> &ids,
                      std::vector<seqan::StringSet<seqan::Dna5String>> &seqs,
                      std::vector<std::vector<seqan::BamAlignmentRecord>> &bamRecords,
                      std::vector<PairedRead> &out_reads,
                      const ChromosomeReferences &chromosomes,
                      const std::vector<ChromosomeIndex> &bamRefIdToChromosomeIndex,
                      const TStream&);

    ChromosomeIndex getChromosomeIndex(const ChromosomeReferences &chromosomes,
                                            const std::vector<ChromosomeIndex> &bamRefIdToChromosomeIndex,
                                            uint32_t rID, uint32_t startPos, uint32_t endPos = 0);

    void getMappingIntervals(std::vector<MapInterval> &intervals, seqan::BamAlignmentRecord &record,
                             const ChromosomeReferences &chromosomes,
                             const std::vector<ChromosomeIndex> &bamRefIdToChromosomeIndex);

    void writeIntervalsToBamTag(seqan::BamAlignmentRecord &record, const std::vector<MapInterval> &intervals);
};

std::ostream& operator<<(std::ostream &os, const ReadProcessingModule::AlignmentStats &stats);

#endif // PG583_MODULE_READPROCESSINGMODULE_H
