#include "module/AlignModule.h"
#include "Timer.h"

#include <vector>

AlignModule::AlignModule() :
    ReadProcessingModule() {
    for (Option &option : mappingOptions2ndTry.getOptions()) {
        seqan::ArgParseOption &argParseOption = option.getArgOpt();
        if (!argParseOption.shortName.empty()) {
            argParseOption.shortName += "-2nd-try";
        }
        if (!argParseOption.longName.empty()) {
            argParseOption.longName += "-2nd-try";
        }
        argParseOption.defaultValue.clear();
    }
}

std::string AlignModule::getName() const {
    return "align";
}

std::string AlignModule::getDescription() const {
    return "Maps and aligns reads to given reference and variants.";
}

void AlignModule::addOptions(seqan::ArgumentParser &argParser) {
    seqan::addSection(argParser, getName());

    AligningModule::addOptions(argParser);
    MappingModule::addOptions(argParser);
    mappingOptions2ndTry.addToParser(argParser);
    ReadProcessingModule::addOptions(argParser);
}

void AlignModule::readOptions(seqan::ArgumentParser &argParser) {
    MappingModule::readOptions(argParser);
    AligningModule::readOptions(argParser);
    mappingOptions2ndTry.read(argParser);
    ReadProcessingModule::readOptions(argParser);
}

int AlignModule::run() {
    Timer<> timer;

    prepareIndex();
    prepareAligning(index.chromosomes);

    mappingOptions.apply(mapOptions);
    shouldUse2ndTryMapping = !mappingOptions2ndTry.replaceUnsetValues(mappingOptions);
    mappingOptions2ndTry.apply(mapOptions2ndTry);


    processReadsFiles(chromosomes);


    logger.info("All done. ", "Elapsed time: ", timer);
    return 0;
}

ReadProcessingModule::AlignmentStats AlignModule::processRead(Aligner &aligner, PairedRead& read) {
    AlignmentStats readStats[2];

    if (noRealign && !seqan::empty(read.record[0].cigar) && (!read.isPairedEnd || !seqan::empty(read.record[1].cigar))) {
        // Copy previous valid alignments.
        return readStats[0]; // TODO: Was ist mit teilweiser Alignierung...
    }

    readStats[0].minErrors = alignOptions.maxErrors.value + 1;
    readStats[1].minErrors = alignOptions.maxErrors.value + 1;

    if (noRemap && read.intervals[0].size() && read.intervals[1].size()) {
        // If --no-remap and mapping intervals retrieved from record, only align read.
        for (size_t i = 0; i <= read.isPairedEnd; i++) {
            alignRead(aligner, read.seq[i], read.revSeq[i], read.intervals[i], read.record[i], readStats[i]);
        }
    } else if (noRemap) {
        std::vector<MapInterval> before[2] = {read.intervals[0], read.intervals[1]};
        mapRead(mapOptions, read);
        for (size_t i = 0; i <= read.isPairedEnd; i++) {
            if (!before[i].empty()) {
                std::swap(before[i], read.intervals[i]); // restore old interval-map, because we want no remapping
            }
            alignRead(aligner, read.seq[i], read.revSeq[i], read.intervals[i], read.record[i], readStats[i]);
        }
    } else {
        mapRead(mapOptions, read);
        for (size_t i = 0; i <= read.isPairedEnd; i++) {
            alignRead(aligner, read.seq[i], read.revSeq[i], read.intervals[i], read.record[i], readStats[i]);
        }
        if (shouldUse2ndTryMapping && readStats[0].alignments == 0) {
            mapRead(mapOptions2ndTry, read);
            for (size_t i = 0; i <= read.isPairedEnd; i++) {
                alignRead(aligner, read.seq[i], read.revSeq[i], read.intervals[i], read.record[i], readStats[i]);
            }
        }
    }

    AlignmentStats sumStats;
    for (size_t i = 0; i <= read.isPairedEnd; i++) {
        writeIntervalsToBamTag(read.record[i], read.intervals[i]);
        readStats[i].mappings = read.intervals[i].size();
        readStats[i].unmapped = readStats[i].mappings == 0;
        readStats[i].unaligned = readStats[i].alignments == 0;
        if (readStats[i].unaligned) {
            readStats[i].minErrors = 0;
        }
        sumStats.add(readStats[i]);
    }
    return sumStats;
}
