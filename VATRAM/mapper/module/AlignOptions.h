#ifndef PG583_MODULE_ALIGNOPTIONS_H
#define PG583_MODULE_ALIGNOPTIONS_H

#include "module/ModuleOptions.h"

class AlignOptions : public ModuleOptions {
public:
    IntOption maxErrors;
    IntOption maxClip;

    AlignOptions();

    virtual void checkValues() const;
};

#endif // PG583_MODULE_ALIGNOPTIONS_H
