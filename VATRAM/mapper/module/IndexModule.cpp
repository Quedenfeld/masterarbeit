#include "module/IndexModule.h"
#include "io/IndexWriter.h"
#include "Timer.h"

#include <iostream>
#include <fstream>
#include <cstddef>
using std::size_t;

std::string IndexModule::getName() const {
    return "index";
}

std::string IndexModule::getDescription() const {
    return "Creates an index file from reference and variants to be used for read mapping.";
}

void IndexModule::addOptions(seqan::ArgumentParser &argParser) {
    seqan::addSection(argParser, getName());

    IndexingModule::addOptions(argParser);

    seqan::ArgParseArgument outputFile(seqan::ArgParseArgument::OUTPUTFILE, "index-file");
    seqan::setHelpText(outputFile, "Write index to \\fIindex-file\\fP.");
    seqan::addArgument(argParser, outputFile);
}

void IndexModule::readOptions(seqan::ArgumentParser &argParser) {
    IndexingModule::readOptions(argParser);
    seqan::getArgumentValue(outputFilePath, argParser, 1);
}

int IndexModule::run() {
    Timer<> timer;
    index.open(outputFilePath);

    readReference();
    initializeIndex();

    logger.info("Saving index file header..");
    index.writeHeader(newlyIndexedChromosomes);

    readIndex();
    createIndex();

    logger.info("All done. ", "Elapsed time: ", timer);
    return 0;
}
