#ifndef PG583_MODULE_APPLICATIONMODULE_H
#define PG583_MODULE_APPLICATIONMODULE_H

#include <seqan/arg_parse.h>

#include <string>

class ApplicationModule {
public:
    virtual std::string generateUsageLine(seqan::ArgumentParser &argParser) const;
    virtual void addOptions(seqan::ArgumentParser &argParser) = 0;
    virtual void readOptions(seqan::ArgumentParser &argParser) = 0;
    virtual int run() = 0;
    virtual std::string getName() const = 0;
    virtual std::string getDescription() const = 0;
};

#endif // PG583_MODULE_APPLICATIONMODULE_H
