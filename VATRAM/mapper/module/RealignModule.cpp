#include "module/RealignModule.h"
#include "Timer.h"

RealignModule::RealignModule() :
    ReadProcessingModule() {
        noRemap = true;
}

std::string RealignModule::getName() const {
    return "realign";
}

std::string RealignModule::getDescription() const {
    return "Aligns reads to given reference and variants.";
}

void RealignModule::addOptions(seqan::ArgumentParser &argParser) {
    seqan::addSection(argParser, getName());

    ReferenceLoadingModule::addOptions(argParser);
    AligningModule::addOptions(argParser);
    ReadProcessingModule::addOptions(argParser);
}

void RealignModule::readOptions(seqan::ArgumentParser &argParser) {
    ReferenceLoadingModule::readOptions(argParser);
    AligningModule::readOptions(argParser);
    ReadProcessingModule::readOptions(argParser);
}

int RealignModule::run() {
    Timer<> timer;

    readReference();

    ChromosomeReferences chromosomesTmp;
    for (const Chromosome &chromosome : genome) {
        chromosomesTmp.emplace_back(chromosome);
    }
    prepareAligning(chromosomesTmp);

    processReadsFiles(chromosomes);

    logger.info("All done. ", "Elapsed time: ", timer);
    return 0;
}

ReadProcessingModule::AlignmentStats RealignModule::processRead(Aligner &aligner, PairedRead& read) {
    AlignmentStats stats;
    for (size_t i = 0; i <= read.isPairedEnd; i++) {
        ReadString& seq = read.seq[i];
        ReadString& revSeq = read.revSeq[i];
        std::vector<MapInterval>& intervals = read.intervals[i];
        seqan::BamAlignmentRecord& record = read.record[i];

        AlignmentStats readStats;

        if (noRealign && !seqan::empty(record.cigar)) {
            // Copy previous valid alignments.
            return readStats;
        }

        readStats.minErrors = alignOptions.maxErrors.value + 1;

        alignRead(aligner, seq, revSeq, intervals, record, readStats);

        writeIntervalsToBamTag(record, intervals);
        readStats.mappings = intervals.size();
        readStats.unmapped = readStats.mappings == 0;
        readStats.unaligned = readStats.alignments == 0;
        if (readStats.unaligned) {
            readStats.minErrors = 0;
        }
        stats.add(readStats);
    }
    return stats;
}
