#include "module/ReadProcessingModule.h"
#include "Timer.h"

ReadProcessingModule::ReadProcessingModule() :
    packageSizeOption("", "package-size", "package_size", 2000, 1),
    maxIntervalsOption("", "max-bam-intervals", "intervals", 0, 0),
    samWriter(bstream),
    unalignedSamWriter(unalignedBamStream) {
    packageSizeOption.setHelpText("Each thread processes chunks of reads. The chunk size is given by this parameter.");
}

void ReadProcessingModule::addOptions(seqan::ArgumentParser &argParser) {
    seqan::addOption(argParser, packageSizeOption.getArgOpt());

    seqan::ArgParseOption outputFile("o", "output", "Write alignments to \\fIoutput.sam\\fP.",
                                     seqan::ArgParseOption::OUTPUTFILE, "output.sam");
    seqan::setDefaultValue(outputFile, "-");
    seqan::addOption(argParser, outputFile);

    seqan::ArgParseOption unalignedOutputFile("u", "unaligned-output", "Write unaligned alignments to separate "
                                              "\\fIunaligned-output.sam\\fP.",
                                              seqan::ArgParseOption::OUTPUTFILE, "unaligned-output.sam");
    seqan::addOption(argParser, unalignedOutputFile);

    seqan::ArgParseOption pairedEndFlag("p", "paired-end", "Paired End Reads. Every two read files are interpreted as a paired end read set.");
    seqan::addOption(argParser, pairedEndFlag);

    seqan::ArgParseArgument readFiles(seqan::ArgParseArgument::INPUTFILE, "read-file", true);
    seqan::setHelpText(outputFile, "Input read files.");
    seqan::addArgument(argParser, readFiles);
}

void ReadProcessingModule::readOptions(seqan::ArgumentParser &argParser) {
    packageSizeOption.getValue(argParser);

    seqan::getOptionValue(outputFilePath, argParser, "o");
    seqan::getOptionValue(unalignedOutputFilePath, argParser, "u");
    isPairedEnd = seqan::isSet(argParser, "paired-end");
    readsFilePaths = seqan::getArgumentValues(seqan::getArgument(argParser, 1));
}

ChromosomeIndex ReadProcessingModule::getChromosomeIndex(
        const ChromosomeReferences &chromosomes,
        const std::vector<ChromosomeIndex> &bamRefIdToChromosomeIndex,
        uint32_t rID, uint32_t startPos, uint32_t endPos) {
    if (rID > bamRefIdToChromosomeIndex.size()) {
        return std::numeric_limits<ChromosomeIndex>::max();
    }
    const ChromosomeIndex chrIdx = bamRefIdToChromosomeIndex[rID];
    if (chrIdx >= chromosomes.size()) {
        return std::numeric_limits<ChromosomeIndex>::max();
    }
    const Position chrLen = seqan::length(chromosomes[chrIdx].get().data);
    if (startPos >= chrLen || endPos >= chrLen) {
        return std::numeric_limits<ChromosomeIndex>::max();
    }
    return chrIdx;
}

void ReadProcessingModule::getMappingIntervals(std::vector<MapInterval> &intervals, seqan::BamAlignmentRecord &record,
                                               const ChromosomeReferences &chromosomes,
                                               const std::vector<ChromosomeIndex> &bamRefIdToChromosomeIndex) {
    // If available, use mapping intervals from optional SAM tag "mi" (stored as instances of BamTagInterval).
    // Copy mapping intervals if they are valid. Adjust chromosome index to current lsh instance.
    seqan::BamTagsDict dict(record.tags);
    unsigned int idx;
    if (seqan::findTagKey(idx, dict, "mi")) {
        seqan::CharString intervalsTagValue = seqan::getTagValue(dict, idx);
        if (seqan::length(intervalsTagValue) > 2 + sizeof(uint32_t)
            && intervalsTagValue[0] == 'B' && seqan::toUpperValue(intervalsTagValue[1]) == 'I'
            && (seqan::length(intervalsTagValue) - 2 - sizeof(uint32_t)) % sizeof(BamTagInterval) == 0) {
            BamTagInterval interval;
            for (size_t i = 2 + sizeof(uint32_t); i < seqan::length(intervalsTagValue);) {
                char *pInterval = reinterpret_cast<char *>(&interval);
                for (size_t j = 0; j < sizeof interval; ++j, ++i) {
                    pInterval[j] = intervalsTagValue[i];
                }
                ChromosomeIndex chrIdx = getChromosomeIndex(chromosomes, bamRefIdToChromosomeIndex,
                                                                 interval.chromosome,
                                                                 interval.startPos, interval.endPos);
                if (chrIdx >= chromosomes.size()) {
                    continue;
                }
                intervals.emplace_back(MapInterval{chrIdx, interval.startPos, interval.endPos, interval.inverted != 0, 0});
            }
        }
    }

    // If no intervals in "mi" tag and valid chromosome index in record.rID, calculate interval from cigar string.
    if (intervals.empty() && record.rID >= 0 && record.beginPos >= 0) {
        ChromosomeIndex chrIdx = getChromosomeIndex(chromosomes, bamRefIdToChromosomeIndex,
                                                         record.rID, record.beginPos);
        if (chrIdx < chromosomes.size()) {
            Position startPosition = (Position) record.beginPos;
            Position endPosition = startPosition;
            auto it = seqan::begin(record.cigar), end = seqan::end(record.cigar);
            for (; it != end && it->operation == 'H'; ++it) {
            }
            for (; it != end && it->operation == 'S'; ++it) {
                startPosition -= std::min<Position>(it->count, startPosition);
            }
            for (; it != end && it->operation != 'H'; ++it) {
                if (it->operation != 'I') {
                    endPosition += it->count;
                }
            }
             // last resort: if interval from cigar string not valid, use startPosition + length(record.seq)
            if (endPosition <= startPosition) {
                endPosition = startPosition + seqan::length(record.seq);
            }
            endPosition = std::min<Position>(endPosition, seqan::length(chromosomes[chrIdx].get().data));
            intervals.emplace_back(MapInterval{chrIdx, startPosition, endPosition, seqan::hasFlagRC(record), (double)(unsigned)record.mapQ});
        }
    }
}

namespace {
template<class TTarget, class TSource>
inline void appendRaw(TTarget &target, const TSource &source) {
    const char *pSource = reinterpret_cast<const char *>(&source);
    for (const char *p = pSource; p < pSource + sizeof source; ++p) {
        seqan::appendValue(target, *p);
    }
}
} // namespace

template<>
size_t ReadProcessingModule::readBatch(seqan::StringSet<seqan::CharString> &ids,
                                       seqan::StringSet<seqan::Dna5String> &seqs,
                                       std::vector<seqan::BamAlignmentRecord> &,
                                       seqan::SequenceStream &readsStream,
                                       size_t requestedPackageSize) {
    seqan::readBatch(ids, seqs, readsStream, requestedPackageSize);
    return seqan::length(ids);
}

template<>
void ReadProcessingModule::prepareBatch(std::vector<seqan::StringSet<seqan::CharString>> &ids,
                                        std::vector<seqan::StringSet<seqan::Dna5String>> &seqs,
                                        std::vector<std::vector<seqan::BamAlignmentRecord>> &,
                                        std::vector<PairedRead> &reads,
                                        const ChromosomeReferences &,
                                        const std::vector<ChromosomeIndex> &,
                                        const seqan::SequenceStream &) {
    size_t lng = seqan::length(seqs[0]);
    reads.clear();
    reads.resize(lng);
//    bamRecords.clear();
//    bamRecords.resize(seqan::length(seqs));
//    mappingIntervals.clear();
//    mappingIntervals.resize(seqan::length(seqs));
//    for (seqan::BamAlignmentRecord &record : bamRecords) {
//        record.flag = 0; // flag is not cleared in constructor / clear function for seqan::BamAlignmentRecord
//    }
    // Make read IDs SAM-conforming: Trim non printable ASCII chars (+ '@') at front and back and limit length to 255.
    for (size_t k = 0; k < seqs.size(); k++) {
        for (seqan::CharString &id : ids[k]) {
            const size_t idLength = seqan::length(id);
            size_t validIdLength = 0;
            size_t i;
            for (i = 0; i < idLength; ++i) {
                char c = id[i];
                if ((c >= '!' && c <= '?') || (c >= 'A' && c <= '~')) {
                    id[validIdLength++] = c;
                    break;
                }
            }
            for (++i; i < idLength; ++i) {
                char c = id[i];
                if ((c >= '!' && c <= '?') || (c >= 'A' && c <= '~')) {
                    id[validIdLength++] = c;
                    if (validIdLength < 255) {
                        continue;
                    }
                }
                break;
            }
            if (validIdLength == 0) {
                id = '*';
            } else if (validIdLength != idLength) {
                seqan::resize(id, validIdLength);
            }
        }
    }
    for (size_t i = 0; i < lng; i++) {
        for (size_t k = 0; k < seqs.size(); k++) {
            reads[i].id[k] = /*seqan::move*/(ids[k][i]);
            reads[i].seq[k] = /*seqan::move*/(seqs[k][i]);
        }
        if (seqs.size() >= 2) {
            reads[i].isPairedEnd = true;
        }
        reads[i].setRevCompl();
    }
//    seqan::assign(revSeqs, seqs);
//    seqan::reverseComplement(revSeqs, seqan::Serial());
}

template<>
size_t ReadProcessingModule::readBatch(seqan::StringSet<seqan::CharString> &,
                                       seqan::StringSet<seqan::Dna5String> &,
                                       std::vector<seqan::BamAlignmentRecord> &bamRecords,
                                       seqan::BamStream &readsStream,
                                       size_t requestedPackageSize) {
    seqan::clear(bamRecords);
    for (size_t i = 0; i < requestedPackageSize; ++i) {
        if (seqan::atEnd(readsStream)) {
            break;
        }
        bamRecords.emplace_back();
        seqan::BamAlignmentRecord &record = bamRecords.back();
        if (seqan::readRecord(record, readsStream) != 0) {
            bamRecords.pop_back();
            break;
        }
    }
    return seqan::length(bamRecords);
}

template<>
void ReadProcessingModule::prepareBatch(std::vector<seqan::StringSet<seqan::CharString>> &,//ids_arr,
                                        std::vector<seqan::StringSet<seqan::Dna5String>> &,//seqs_arr,
                                        std::vector<std::vector<seqan::BamAlignmentRecord>> &bamRecords_arr,
                                        std::vector<PairedRead> &reads,
                                        const ChromosomeReferences &chromosomes,
                                        const std::vector<ChromosomeIndex> &bamRefIdToChromosomeIndex,
                                        const seqan::BamStream &readsStream) {
    const int32_t nameStoreSize = bamRefIdToChromosomeIndex.size();
    const ChromosomeIndex chrCount = chromosomes.size();

    // no paired end support
//    seqan::StringSet<seqan::CharString>& ids = ids_arr[0];
//    seqan::StringSet<seqan::Dna5String>& seqs = seqs_arr[0];
    std::vector<seqan::BamAlignmentRecord>& bamRecords = bamRecords_arr[0];

//    seqan::clear(ids);
//    seqan::clear(seqs);
    size_t packageSize = bamRecords.size();
    reads.clear();
    reads.resize(packageSize);


//    mappingIntervals.clear();
    for (size_t i = 0; i < packageSize; ++i) {
//        reads.push_back(PairedRead());
        PairedRead& read = reads[i];


        read.record[0] = bamRecords[i];
        seqan::BamAlignmentRecord &record = read.record[0];
        read.id[0] = record.qName;
//        seqan::appendValue(ids, record.qName);
        if (seqan::hasFlagRC(record)) {
            read.seq[0] = seqan::reverseString(record.seq);
            read.revSeq[0] = record.seq;
//            seqan::appendValue(seqs, seqan::reverseString(record.seq));
//            seqan::appendValue(revSeqs, record.seq);
        } else {
            read.seq[0] = record.seq;
            read.revSeq[0] = seqan::reverseString(record.seq);
//            seqan::appendValue(seqs, record.seq);
//            seqan::appendValue(revSeqs, seqan::reverseString(record.seq));
        }
        std::vector<MapInterval> &intervals = read.intervals[0];
        ChromosomeIndex chrIdx = getChromosomeIndex(chromosomes, bamRefIdToChromosomeIndex,
                                                         record.rID, record.beginPos);
        // If --no-realign and alignment has valid position and length, just copy it.
        if (noRealign && chrIdx < chrCount && seqan::getAlignmentLengthInRef(record) != 0) {
            record.rID = chrIdx;
            // Clear the "next" fields since we do not support mate pairs, split alignments etc..
            record.rNextId = seqan::BamAlignmentRecord::INVALID_REFID;
            record.pNext = seqan::BamAlignmentRecord::INVALID_POS;
        } else {
            seqan::CharString originalAlignmentBamTags;
            // Save previous mapping / alignment information if it is valid for bamIOContext.
            if (chrIdx < chrCount
                || (record.rID >= 0 && record.rID < nameStoreSize && record.beginPos > 0)) {
                seqan::append(originalAlignmentBamTags, "oRZ"); // "original reference"
                seqan::append(originalAlignmentBamTags, seqan::nameStore(readsStream.bamIOContext)[record.rID]);
                seqan::appendValue(originalAlignmentBamTags, '\0');

                seqan::append(originalAlignmentBamTags, "OPi"); // "original position"
                int32_t beginPos = record.beginPos + 1;
                appendRaw(originalAlignmentBamTags, beginPos);

                seqan::append(originalAlignmentBamTags, "oFI"); // "original flags"
                uint32_t flag = record.flag;
                appendRaw(originalAlignmentBamTags, flag);

                if (!seqan::empty(record.cigar)) {
                    seqan::append(originalAlignmentBamTags, "OCZ"); // "original cigar"
                    seqan::append(originalAlignmentBamTags, toString(record.cigar));
                    seqan::appendValue(originalAlignmentBamTags, '\0');
                }
            }
            if (noRemap) {
                getMappingIntervals(intervals, record, chromosomes, bamRefIdToChromosomeIndex);
            } else {
                intervals.clear();
            }
            record.rID = seqan::BamAlignmentRecord::INVALID_REFID;
            record.beginPos = seqan::BamAlignmentRecord::INVALID_POS;
            record.mapQ = 255;
            record.flag = 0;
            seqan::clear(record.cigar);
            record.bin = 0;
            record.rNextId = seqan::BamAlignmentRecord::INVALID_REFID;
            record.pNext = seqan::BamAlignmentRecord::INVALID_POS;
            record.tLen = seqan::BamAlignmentRecord::INVALID_LEN;
            seqan::clear(record.qual);
            seqan::swap(record.tags, originalAlignmentBamTags);
        }
    }
}

template<class TStream>
void ReadProcessingModule::processReadsFile(TStream* readsStream, size_t readStreamCount, const ChromosomeReferences &chromosomes,
                                            const std::vector<ChromosomeIndex> &bamRefIdToChromosomeIndex) {
    const size_t requestedPackageSize = (size_t) packageSizeOption.value;
    Mutex inputMutex;
    size_t readsCounter = 0;

    #pragma omp parallel
    {
        Aligner aligner(0, 0); // maxClip = 0, because not configurable yet);

        std::vector<seqan::StringSet<seqan::CharString>> ids(readStreamCount);
        std::vector<seqan::StringSet<seqan::Dna5String>> seqs(readStreamCount);
//        seqan::StringSet<seqan::Dna5String> revSeqs;
        std::vector<std::vector<seqan::BamAlignmentRecord>> bamRecords(readStreamCount);

//        std::vector<std::vector<MapInterval>> mappingIntervals;
        std::vector<PairedRead> reads;

        while (true) {
            size_t packageStart;
            AlignmentStats packageStats = {};
            {
                LockGuard lock(inputMutex);
                packageStats.reads = std::numeric_limits<size_t>::max();
                bool exit = false;
                for (size_t k = 0; k < readStreamCount; k++) {
                    TStream& strm = readsStream[k];
                    if (seqan::atEnd(strm)) {
                        exit = true;
                        break;
                    }

                    size_t readCount = readBatch(ids[k], seqs[k], bamRecords[k], readsStream[k], requestedPackageSize);
                    if (readCount == 0) {
                        exit = true;
                        break;
                    }
                    packageStats.reads = std::min(packageStats.reads, readCount);
                }
                if (exit) {
                    break;
                }
                packageStart = readsCounter;
                readsCounter += packageStats.reads;
            }
            prepareBatch(ids, seqs, bamRecords, reads, chromosomes, bamRefIdToChromosomeIndex, *readsStream);

            for (size_t i = 0; i < packageStats.reads; ++i) {
                packageStats.add(processRead(aligner, reads[i]));
            }
            {
                LockGuard lock(outputMutex);
                totalStats.add(packageStats);

                printStats(std::clog, packageStart, packageStats);

                for (size_t i = 0; i < packageStats.reads; ++i) {
                    for (size_t k = 0; k < readStreamCount; k++) {
                        seqan::BamAlignmentRecord &bestAlign = reads[i].record[k];
                        SamWriter &writer = shouldSeparateUnalignedOutput
                                            && seqan::empty(bestAlign.cigar) ? unalignedSamWriter : samWriter;
                        writer.writeRead(reads[i].id[k], reads[i].seq[k], reads[i].revSeq[k], bestAlign);
                    }
                }
            }
        }
    }
}

void ReadProcessingModule::processReadsFiles(const ChromosomeReferences &chromosomes) {
    seqan::open(bstream, outputFilePath.c_str(), seqan::BamStream::WRITE);
    samWriter.writeHeader(chromosomes);

    shouldSeparateUnalignedOutput = !unalignedOutputFilePath.empty() && unalignedOutputFilePath != outputFilePath;
    if (shouldSeparateUnalignedOutput) {
        seqan::open(unalignedBamStream, unalignedOutputFilePath.c_str(), seqan::BamStream::WRITE);
        unalignedSamWriter.writeHeader(chromosomes);
    }

    totalStats = {};

    Timer<> timer;
    logger.info("Processing reads..");

    if (isPairedEnd) {
        logger.info("Processing paired end reads..");
        for (size_t i = 0; i < readsFilePaths.size()-1; i+=2) {
            const std::string* readsFile = &readsFilePaths[i];

            for (size_t k: {0,1}) {
                if (seqan::endsWith(readsFile[k], ".bam") || seqan::endsWith(readsFile[k], ".sam")) {
                    logger.err("SAM/BAM-Format is not supported with paired end reads.");
                    return;
                }
            }
            seqan::SequenceStream readsStream[2] = {seqan::SequenceStream(readsFile[0].c_str()),
                                                    seqan::SequenceStream(readsFile[1].c_str())};
//            seqan::SequenceStream readsStream1(readsFile[1].c_str());
            if (!isGood(readsStream[0])) {
                logger.err("Could not open reads file ", '"', readsFile[0], '"');
                continue;
            }
            if (!isGood(readsStream[1])) {
                logger.err("Could not open reads file ", '"', readsFile[1], '"');
                continue;
            }
//            seqan::SequenceStream* readsStreamPt[2] = {&readsStream0, &readsStream1};
            processReadsFile(&readsStream[0], 2, chromosomes);
        }
    } else {
        for (const std::string &readsFile : readsFilePaths) {
            logger.info("Opening reads file ", '"', readsFile, '"');
            if (seqan::endsWith(readsFile, ".bam") || seqan::endsWith(readsFile, ".sam")) {
                seqan::BamStream readsStream(readsFile.c_str());
                if (!isGood(readsStream)) {
                    logger.err("Could not open reads file ", '"', readsFile, '"');
                    continue;
                }

                std::vector<ChromosomeIndex> bamRefIdToChromosomeIndex;
                for (seqan::CharString bamRefId : seqan::nameStore(readsStream.bamIOContext)) {
                    std::string bamRefName = Chromosome::generateName(seqan::toCString(bamRefId));
                    bamRefIdToChromosomeIndex.emplace_back(std::find_if(chromosomes.begin(), chromosomes.end(),
                            [&] (const Chromosome &chr) { return chr.name == bamRefName; }) - chromosomes.begin());
                }
                processReadsFile(&readsStream, 1, chromosomes, bamRefIdToChromosomeIndex);
            } else {
                seqan::SequenceStream readsStream(readsFile.c_str());
                if (!isGood(readsStream)) {
                    logger.err("Could not open reads file ", '"', readsFile, '"');
                    continue;
                }
                processReadsFile(&readsStream, 1, chromosomes);
            }
        }
    }

    logger.info("Done processing reads. ", "Elapsed time: ", timer);
}

void ReadProcessingModule::writeIntervalsToBamTag(seqan::BamAlignmentRecord &record,
                                                  const std::vector<MapInterval> &intervals) {
    size_t maxIntervals = std::min<size_t>(intervals.size(), maxIntervalsOption.value);
    if (maxIntervals > 0) {
        // No "mi" in record.tags since tags were cleared before (and noRealign + valid alignment => intervals.empty()).
        seqan::append(record.tags, "miBI");
        const uint32_t uint32_tCount = maxIntervals * (sizeof(BamTagInterval) / sizeof(uint32_t));
        appendRaw(record.tags, uint32_tCount);
        for (size_t i = 0; i < maxIntervals; ++i) {
            const MapInterval &interval = intervals[i];
            BamTagInterval intervalTagValue{interval.chromosome,
                                            interval.startPosition,
                                            interval.endPosition,
                                            (uint32_t) interval.inverted};
            appendRaw(record.tags, intervalTagValue);
        }
    }
}

void ReadProcessingModule::printStats(std::ostream &os, size_t packageStart, const AlignmentStats &packageStats) const {
    int nThread = omp_get_thread_num();
    os << "t="  << std::right << std::setw(2) << nThread
       << " batch" << packageStats
       << " start=" << std::right << std::setw(9) << packageStart
       << std::endl
       << "t="  << std::right << std::setw(2) << nThread
       << " total" << totalStats
       << std::endl;
}

std::ostream& operator<<(std::ostream &os, const ReadProcessingModule::AlignmentStats &stats) {
    auto s = [&os](const std::string &prefix, size_t value, int digits) {
        os << ' ' << prefix << std::right << std::setw(digits) << value; };
    const size_t mapped = stats.reads - stats.unmapped;
    const size_t aligned = stats.reads - stats.unaligned;
    const size_t unsuccessfulAlignerCalls = stats.alignerCalls - stats.alignments;
    s("", stats.reads, 9);
    s("align=", aligned, 9);
    s("", stats.unaligned, 9);
    s("map=", mapped, 9);
    s("", stats.unmapped, 9);
    s("intervals=", stats.mappings, 9+1);
    s("alignments=", stats.alignments, 9);
    s("", unsuccessfulAlignerCalls, 9+1);
    s("errors=", stats.minErrors, 9);
    s("intervalSize=", stats.alignedIntervalSize, 9+2);
    return os;
}
