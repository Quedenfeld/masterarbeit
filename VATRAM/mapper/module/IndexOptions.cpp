#include "module/IndexOptions.h"

#include <stdexcept>
#include <ctime>

// TODO: check if given default values are valid and if their respective hard limits are reasonable
IndexOptions::IndexOptions() :
    qGramSize              ("q", "qgram-size",                 "size",        LSH::DEFAULT_QGRAM_SIZE,  1, MAX_Q_GRAM_SIZE),
    signatureLength        ("s", "signature-length",           "length",      LSH::DEFAULT_SIGNATURE_LENGTH,  2),
    bandSize               ("S", "band-size",                  "band_size",   LSH::DEFAULT_BAND_SIZE,  1,  50),
    windowSize             ("w", "window-size",                "window_size", LSH::DEFAULT_WINDOW_SIZE,  2),
    windowOffset           ("W", "window-offset",              "offset",      LSH::DEFAULT_WINDOW_OFFSET,  1),
    snpCombinationLimit    ("l", "snp-combination-limit",      "limit",       LSH::DEFAULT_LIMIT,  1),
    snpCombinationLimitSkip("L", "snp-combination-skip-limit", "skip_limit",  LSH::DEFAULT_LIMIT_SKIP,  2),
    randomSeed             ("x", "random-seed",                "seed",        IntOption::UNSET),
    qGramForm              ("Q", "qgram-form",                 "form",        std::string(MAX_Q_GRAM_SIZE, '#')) {
    options.assign({qGramSize,
                    signatureLength,
                    bandSize,
                    windowSize,
                    windowOffset,
                    snpCombinationLimit,
                    snpCombinationLimitSkip,
                    randomSeed,
                    qGramForm});
    qGramSize              .setHelpText("The length of the q-grams.");
    signatureLength        .setHelpText("The number of signature values for a read or reference window.");
    bandSize               .setHelpText("The number of signature values that a connected to one band value.");
    windowSize             .setHelpText("The length of the reference windows.");
    windowOffset           .setHelpText("The distance from the begin of one window to the begin of the next window.");//" Defaults to \\fIwindow_size\\fP / 2.");
    snpCombinationLimit    .setHelpText("If the number of SNP combinations at a given q-gram position is less than this parameter, all q-grams are added. "
                                        "If the number is greater, only a random set of combinations is added. The size of the random set is equal to this parameter.");
    snpCombinationLimitSkip.setHelpText("If the number of SNP combinations at a given q-gram position is greater than this parameter, no q-grams are added.");
    randomSeed             .setHelpText("Initial seed that is used for the random number generators.");
    qGramForm              .setHelpText("Gapped qgram, e.g. ###-##-###--#.");
}

void IndexOptions::addToParser(seqan::ArgumentParser &argParser) {
    ModuleOptions::addToParser(argParser);
    seqan::ArgParseOption index("i", "index-file", "Index input file.", seqan::ArgParseOption::INPUTFILE, "index-file");
    seqan::addOption(argParser, index);
}

void IndexOptions::read(seqan::ArgumentParser &argParser) {
    ModuleOptions::read(argParser);

    if (!seqan::getOptionValue(indexFile, argParser, "i")) {
        indexFile.clear();
    }

    if (!windowOffset.isSet() && windowSize.isSet()) {
        windowOffset.value = std::max<IntOption::ValueType>(1, windowSize.value / 2);
    }
    if (!randomSeed.isSet()) {
        randomSeed.value = std::time(nullptr);
    }
    if (qGramForm.value.size() < (size_t)qGramSize.value) {
        throw std::invalid_argument("Invalid index options: q-gram form too small");
    }
    if (qGramForm.value.find_first_not_of("#-") != std::string::npos) {
        throw std::invalid_argument("Invalid index options: invalid q-gram form: " + qGramForm.value);
    }
    checkValues();
}

void IndexOptions::checkValues() const {
    std::ostringstream err;

    try {
        ModuleOptions::checkValues();
    } catch(const std::invalid_argument &ex) {
        err << ex.what();
    }

    for (auto pair : { std::pair<const IntOption&, const IntOption&>{windowSize, windowOffset},
                       std::pair<const IntOption&, const IntOption&>{signatureLength, bandSize}}) {
        if (pair.first.isSet() && pair.second.isSet() && pair.first.value < pair.second.value) {
            err << pair.first.getArgOptName() << " < " << pair.second.getArgOptName()
                << " (" << pair.first.value << " < " << pair.second.value << ")." << std::endl;
        }
    }

    std::string errors(err.str());
    if (!errors.empty()) {
        throw std::invalid_argument("Invalid index options: " + errors);
    }
}
