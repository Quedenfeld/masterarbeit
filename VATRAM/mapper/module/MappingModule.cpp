#include "module/MappingModule.h"

void MappingModule::addOptions(seqan::ArgumentParser &argParser) {
    IndexingModule::addOptions(argParser);
    mappingOptions.addToParser(argParser);
    seqan::addOption(argParser, seqan::ArgParseOption("", "no-remap", "If set, an already mapped read in the SAM file will not be remapped."));
}

void MappingModule::readOptions(seqan::ArgumentParser &argParser) {
    IndexingModule::readOptions(argParser);
    mappingOptions.read(argParser);
    noRemap = seqan::isSet(argParser, "no-remap");
}

void MappingModule::prepareIndex() {
    Timer<> timer;

    readReference();
    initializeIndex();
    readIndex();
    createIndex();

    index.printHashTableStatistics();
    logger.info("Done preparing index. ", "Elapsed time: ", timer);
}

void MappingModule::mapRead(const LshMapOptions &mapOptions, PairedRead& read) {
    read.intervals[0].clear();
    read.intervals[1].clear();
    index.findInterval(read, mapOptions);
}
