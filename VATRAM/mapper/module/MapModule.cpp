#include "module/MapModule.h"
#include "Timer.h"

#include <vector>

MapModule::MapModule() :
    ReadProcessingModule() {
    maxIntervalsOption.value = 10;
}

std::string MapModule::getName() const {
    return "map";
}

std::string MapModule::getDescription() const {
    return "Maps and aligns reads to given reference and variants.";
}

void MapModule::addOptions(seqan::ArgumentParser &argParser) {
    seqan::addSection(argParser, getName());

    MappingModule::addOptions(argParser);
    ReadProcessingModule::addOptions(argParser);
}

void MapModule::readOptions(seqan::ArgumentParser &argParser) {
    MappingModule::readOptions(argParser);
    ReadProcessingModule::readOptions(argParser);
}

int MapModule::run() {
    Timer<> timer;

    prepareIndex();

    mappingOptions.apply(mapOptions);

    processReadsFiles(index.chromosomes);

    logger.info("All done. ", "Elapsed time: ", timer);
    return 0;
}

ReadProcessingModule::AlignmentStats MapModule::processRead(Aligner &, PairedRead& read) {
    AlignmentStats readStats;


    if (noRemap && read.intervals[0].size() && read.intervals[1].size()) {
         // If --no-remap and mapping intervals retrieved from record, do nothing.
    } else if (noRemap) {
        // map and restore old non-empty interval-list if neccessary
        std::vector<MapInterval> before[2] = {read.intervals[0], read.intervals[1]};
        mapRead(mapOptions, read);
        for (size_t i = 0; i <= read.isPairedEnd; i++) {
            if (!before[i].empty()) {
                std::swap(before[i], read.intervals[i]); // restore old interval-map, because we want no remapping
            }
        }
    } else {
        mapRead(mapOptions, read);
    }

    for (size_t i = 0; i <= read.isPairedEnd; i++) {
        // TODOJQ: sam info über pe-reads
        writeIntervalsToBamTag(read.record[i], read.intervals[i]);
    }

    readStats.mappings = read.intervals[0].size();
    readStats.unmapped = read.intervals[0].empty();
    readStats.unaligned = 1;
    if (read.isPairedEnd) {
        readStats.mappings += read.intervals[1].size();
        readStats.unmapped += read.intervals[1].empty();
        readStats.unaligned += 1;
    }

    return readStats;
}
