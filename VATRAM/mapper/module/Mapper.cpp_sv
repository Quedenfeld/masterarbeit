#include "module/Mapper.h"
#include "io/SamWriter.h"
//#include "align/BacktracingMatrix.h"
#include "align/BacktracingMatrix2.h"
#include "Timer.h"
#include "parallel.h"

#include <seqan/seq_io.h>

#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <limits>
#include <random>
#include <ctime>
#include <cstddef>
using std::size_t;

std::string Mapper::getName() const {
    return "mapreads";
}

std::string Mapper::getDescription() const {
    return "Maps and aligns reads to given reference and variants.";
}

MappingOptions::MappingOptions() :
    maxErrors               ("e", "max-errors",   "errors",           3,  0, 100),
    packageSize             ("p", "package-size", "package_size", 10000,  1     ),
    minCount                ("",  "min-count",                        2,  1     ),
    maxSelect               ("",  "max-select",                      50,  1     ),
    nextFactor              ("",  "next-factor",                    2.0,  1.0   ),
    bandMultiplicator       ("",  "band-multiplicator",             1.0,  1.0   ),
    extendNumber            ("",  "extend-number",                   40,  0     ),
    maximumOfReturnedWindows("",  "max-returned-windows",            50,  1     ) {
    intOptions.assign({maxErrors, packageSize, minCount, maxSelect, extendNumber, maximumOfReturnedWindows});
    floatOptions.assign({nextFactor, bandMultiplicator});
    maxErrors               .setHelpText("Some help text for max-errors.");
    packageSize             .setHelpText("Some help text for package-size.");
    minCount                .setHelpText("Some help text for min-count.");
    maxSelect               .setHelpText("Some help text for max-select.");
    nextFactor              .setHelpText("Some help text for next-factor.");
    bandMultiplicator       .setHelpText("Some help text for band-multiplicator.");
    extendNumber            .setHelpText("Some help text for extend-number.");
    maximumOfReturnedWindows.setHelpText("Some help text for max-returned-windows.");
}

void MappingOptions::checkValues() const {
    try {
        ModuleOptions::checkValues();
    } catch(const std::invalid_argument &ex) {
        throw std::invalid_argument(std::string("Invalid mapping options: ") + ex.what());
    }
}

void Mapper::addOptions(seqan::ArgumentParser &argParser) {
    seqan::addSection(argParser, getName());

    Indexer::addOptions(argParser);

    mappingOptions.addToParser(argParser);

    seqan::ArgParseOption outputFile("o", "output", "Write alignments to \\fIoutput.sam\\fP.",
                                     seqan::ArgParseOption::OUTPUTFILE, "output.sam");
    seqan::setDefaultValue(outputFile, "-");
    seqan::addOption(argParser, outputFile);

    seqan::ArgParseArgument readFiles(seqan::ArgParseArgument::INPUTFILE, "read-file", true);
    seqan::setHelpText(outputFile, "Input read files.");
    seqan::addArgument(argParser, readFiles);
}

void Mapper::readOptions(seqan::ArgumentParser &argParser) {
    Indexer::readOptions(argParser);
    mappingOptions.read(argParser);
    seqan::getOptionValue(outputFilePath, argParser, "o");
    readsFilePaths = seqan::getArgumentValues(seqan::getArgument(argParser, 1));
}

int Mapper::run() {
    Timer<> timer;
    readReference(lsh);
    loadIndex(lsh);
    createIndex(lsh);

    lsh.printHashTableStatistics();

    mappingOptions.apply(lsh);

    const size_t packageSize = (size_t) mappingOptions.packageSize.value;

    std::vector<VariantIndex> variantIndexes;
    for (const Chromosome &chromosome : lsh.chromosomes) {
        variantIndexes.emplace_back(chromosome.variants, seqan::length(chromosome.data) - 1);
    }

    logger.info("Done preparing index. ", "Elapsed time: ", timer);
    logger.info("Starting to map and align reads..");

    Timer<> mappingTimer;

    seqan::BamStream bstream(outputFilePath.c_str(), seqan::BamStream::WRITE);

    SamWriter samWriter(bstream);
    samWriter.writeHeader(lsh.chromosomes);
    omp_std::mutex samWriterMutex;
    size_t totalReadsMapped = 0;
    size_t totalReadsUnmapped = 0;
    size_t totalReadsAligned = 0;
    size_t totalReadsUnaligned = 0;
    size_t totalMappings = 0;
    size_t totalAlignerCalls = 0;
    size_t totalAlignments = 0;

    for (const std::string &readsFile : readsFilePaths) {
        logger.info("Opening reads file ", '"', readsFile, '"');
        seqan::SequenceStream readsStream(readsFile.c_str());
        if (!isGood(readsStream)) {
            logger.err("Could not open reads file ", '"', readsFile, '"');
            continue;
        }
        omp_std::mutex readStreamMutex;
        size_t totalReadsCounter = 0;

//        #pragma omp parallel
        {
//            int nThread = omp_get_thread_num();

//            std::vector<SemiGlobalAligner> aligners;
            std::vector<Aligner> aligners;
            for (const VariantIndex &variantIndex : variantIndexes) {
//                aligners.emplace_back(variantIndex);
                aligners.emplace_back(variantIndex, 3);
            }

            // Share storage between all aligner instances.
//            MatrixStorage storage;

            seqan::StringSet<seqan::CharString> ids;
            seqan::StringSet<seqan::Dna5String> seqs;
            std::vector<BestAlign> alignmentOutput;

            while (true) {
                size_t readsMapped = 0;
                size_t readsUnmapped = 0;
                size_t readsAligned = 0;
                size_t readsUnaligned = 0;
                size_t mappings = 0;
                size_t alignerCalls = 0;
                size_t alignments = 0;

                seqan::clear(seqs);
                seqan::clear(ids);
                alignmentOutput.clear();

                size_t readsCounter;
                size_t currentPackageSize;
                {
//                    omp_std::lock_guard<omp_std::mutex> lock(readStreamMutex);
                    if (seqan::atEnd(readsStream)) {
                        break;
                    }
                    seqan::readBatch(ids, seqs, readsStream, packageSize);
                    currentPackageSize = seqan::length(seqs);
                    readsCounter = totalReadsCounter;
                    totalReadsCounter += currentPackageSize;
                }

                for (size_t i = 0; i < currentPackageSize; ++i) {
                    auto &read = seqs[i];

                    size_t readMappings = 0;
                    MinMaxSum readAlignErrors, readAlignCount;
                    std::vector<LSH::Result> res;
                    alignmentOutput.emplace_back(BestAlign{false, 0, 0, {}});
                    BestAlign &bestAlign = alignmentOutput.back();

                    size_t readAlignerCalls = 0;

                    readAlignerCalls += mapAndAlignDirected(aligners/*, storage*/, read, res,
                                                            bestAlign, readAlignCount, readAlignErrors);
                    readMappings += res.size();

                    mappings += readMappings;
                    alignerCalls += readAlignerCalls;
                    alignments += readAlignCount.sum;

                    if (readMappings == 0) {
                        ++readsUnmapped;
                    } else {
                        ++readsMapped;
                        if (readAlignCount.sum == 0) {
                            ++readsUnaligned;
                        } else {
                            ++readsAligned;
                        }
                    }
                    if (false) {
                        std::ostringstream oss;
//                        oss << "thread=" << std::right << std::setw(2) << nThread
//                            << " read="  << std::right << std::setw(9) << readsCounter + i
//                            << " map="   << std::right << std::setw(6) << readMappings
//                            << " alignerCalls =" << std::right << std::setw(6) << readAlignerCalls;
                        if (readAlignCount.sum != 0) {
                            oss << " align/map: " << readAlignCount.str(5)
                                << " errors/align: " << readAlignErrors.str(5)
                                << " chr=" << std::right << std::setw(2)
                                                         << lsh.chromosomes[bestAlign.chromosomeIndex].get().name
                                << " pos=" << std::right << std::setw(9) << bestAlign.beginPos + 1
                                << " " << (bestAlign.isReverseComplement ? "rev" : "fwd")
                                << " cigar: " << bestAlign.cigar;
                        }
                        oss << " read: " << ids[i] << std::endl;
                        std::cout << oss.str() << std::flush;
                    }
                }
                {
//                    omp_std::lock_guard<omp_std::mutex> lock(samWriterMutex);
                    totalReadsMapped += readsMapped;
                    totalReadsUnmapped += readsUnmapped;
                    totalReadsAligned += readsAligned;
                    totalReadsUnaligned += readsUnaligned;
                    totalMappings += mappings;
                    totalAlignerCalls += alignerCalls;
                    totalAlignments += alignments;

                    if (true) {
                        std::ostringstream oss;
//                        oss << "thread="        << std::right << std::setw(2) << nThread
//                            << " reads="        << std::right << std::setw(9) << readsCounter + currentPackageSize
//                            << " mapped="       << std::right << std::setw(9) << totalReadsMapped
//                            << " unmapped="     << std::right << std::setw(9) << totalReadsUnmapped
//                            << " aligned="      << std::right << std::setw(9) << totalReadsAligned
//                            << " unaligned="    << std::right << std::setw(9) << totalReadsUnaligned
//                            << " mappings="     << std::right << std::setw(9+3) << totalMappings
//                            << " alignerCalls=" << std::right << std::setw(9+3) << totalAlignerCalls
//                            << " alignments="   << std::right << std::setw(9) << totalAlignments
//                            << std::endl;
                        std::cout << oss.str() << std::flush;
                    }

                    for (size_t i = 0; i < currentPackageSize; ++i) {
                        BestAlign &bestAlign = alignmentOutput[i];
                        samWriter.writeRead(ids[i], seqs[i], bestAlign.isReverseComplement,
                                            bestAlign.chromosomeIndex, bestAlign.beginPos, bestAlign.cigar);
                    }
                }
            }
        }
    }

    logger.info("Done mapping reads. ", "Elapsed time: ", mappingTimer);

    logger.info("All done. ", "Elapsed time: ", timer);

    return 0;
}

size_t Mapper::mapAndAlignDirected(std::vector<Aligner> &aligners/*, MatrixStorage &storage*/,
                                   const ReadString &read, std::vector<LSH::Result> &res, BestAlign &bestAlign,
                                   MinMaxSum &readAlignCount, MinMaxSum &readAlignErrors) {
    // NOTE: Remove " - 1" to be able to obtain all optimal alignments.
    size_t maxErrors = std::min<size_t>(readAlignErrors.min - 1, mappingOptions.maxErrors.value);
    res.clear();
    ReadString revComplement(read);
    seqan::reverseComplement(revComplement);
    lsh.findInterval(read, res);

    for (size_t i = 0; i < res.size(); ++i) {
        LSH::ChromosomeIndex chrIdx = res[i].chromosome;
//        auto alignments = aligners[chrIdx].align(storage, lsh.chromosomes[res[i].chromosome], res[i].startPosition,
//                res[i].endPosition,
//                res[i].inverted ? revComplement : read, maxErrors);
        auto alignments = aligners[chrIdx].align(lsh.chromosomes[res[i].chromosome], res[i].startPosition,
                res[i].endPosition, res[i].inverted ? revComplement : read, maxErrors);
        size_t numAlignments = alignments.size();
        readAlignCount.update(numAlignments);
        if (numAlignments != 0) {
            auto &alignment = alignments[0];
            size_t curAlignErrors = alignment.getErrorCount();
            if (curAlignErrors < readAlignErrors.min) { // Should always be true if maxErrors < readAlignErrors.min.
                readAlignErrors.update(curAlignErrors);
                bestAlign = {res[i].inverted, chrIdx, res[i].startPosition + alignment.getPosition(),
                             alignment.getCigar()};
                if (curAlignErrors == 0) {
                    return i + 1;
                }
                // NOTE: Remove " - 1" to be able to obtain all optimal alignments.
                maxErrors = curAlignErrors - 1;
            } else {
                readAlignErrors.update(curAlignErrors);
            }
        }
    }
    return res.size();
}


template<class T, class Engine = std::minstd_rand>
class UniformIntRng {
public:
    typedef std::uniform_int_distribution<T> Distribution;
    Distribution distribution;
    Engine engine;

    UniformIntRng(Distribution distribution, Engine engine = Engine(std::time(nullptr))) :
        distribution(distribution),
        engine(engine) {
    }

    template<class ...DistributionArgs>
    UniformIntRng(DistributionArgs... distributionArgs) :
        UniformIntRng(Distribution(distributionArgs...)) {
    }

    T operator()() {
        return distribution(engine);
    }
};

template<typename TValue, typename TSpec, class Engine>
class UniformIntRng<seqan::SimpleType<TValue, TSpec>, Engine> {
    typedef seqan::SimpleType<TValue, TSpec> T;
public:
    typedef std::uniform_int_distribution<typename seqan::Value<T>::Type> Distribution;
    Distribution distribution;
    Engine engine;

    UniformIntRng(Distribution distribution, Engine engine = Engine(std::time(nullptr))) :
        distribution(distribution),
        engine(engine) {
    }

    template<class ...DistributionArgs>
    UniformIntRng(DistributionArgs... distributionArgs) :
        UniformIntRng(Distribution(distributionArgs...)) {
    }

    UniformIntRng() : UniformIntRng(seqan::MinValue<T>::VALUE, seqan::MaxValue<T>::VALUE) {
    }

    T operator()() {
        return distribution(engine);
    }
};

void generateRandomChromosome(size_t genomeLength = 1000 /* 1000*1000*30 */,
                              const std::string &filename = "./genome.fasta") {
    std::ofstream genomeOutputFile(filename);

    typedef seqan::Dna RefChar;
    UniformIntRng<RefChar> baseRng;
    seqan::String<RefChar> genome;
    seqan::resize(genome, genomeLength);
    for (auto &c : genome) {
        seqan::assign(c, baseRng());
    }
    seqan::writeRecord(genomeOutputFile, "0", genome, seqan::Fasta());
    genomeOutputFile.flush();
}

template<class TString>
void generateReads(const TString &genome, size_t numReads, size_t maxErrors = 4, size_t readLength = 100,
                   const std::string &filename = "./reads.fasta") {
    if(seqan::length(genome) < readLength) {
        return;
    }

    typedef seqan::Dna ReadChar;

    UniformIntRng<size_t> startPosRng(0, length(genome) - readLength);
    UniformIntRng<size_t> errorPosRng(0, readLength - 1);
    UniformIntRng<ReadChar> baseRng;

    std::ofstream readsOutputFile(filename);
    for (size_t readIndex = 0; readIndex < numReads; readIndex++) {
        seqan::String<ReadChar> read = seqan::infixWithLength(genome, startPosRng(), readLength);
        for (size_t errorCounter = 0; errorCounter < maxErrors; ++errorCounter) {
            read[errorPosRng()].value ^= baseRng().value;
        }
        seqan::writeRecord(readsOutputFile, std::to_string(readIndex), read, seqan::Fasta());
    }
    readsOutputFile.flush();
}
