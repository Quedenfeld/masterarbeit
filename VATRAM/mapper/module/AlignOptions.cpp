#include "module/AlignOptions.h"

AlignOptions::AlignOptions() :
    maxErrors("e", "max-errors", "errors", 6, 0),
    maxClip("c", "max-clip", "length", 0, 0) {
    options.assign({maxErrors, maxClip});
    maxErrors.setHelpText("Maximal number of errors in a read.");
    maxClip.setHelpText("The maximal number of bases at each end of the read that can be ignored by the aligner.");
}

void AlignOptions::checkValues() const {
    try {
        ModuleOptions::checkValues();
        // TODO:: Is a check for maxErrors < maxClip maybe reasonable?
    } catch(const std::invalid_argument &ex) {
        throw std::invalid_argument(std::string("Invalid alignment options: ") + ex.what());
    }
}
