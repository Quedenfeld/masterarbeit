#ifndef PG583_MODULE_MAPPINGMODULE_H
#define PG583_MODULE_MAPPINGMODULE_H

#include "module/IndexingModule.h"
#include "module/ReadProcessingModule.h"
#include "module/MappingOptions.h"
#include "map/LSH.h"

class MappingModule : virtual public IndexingModule<LSH>, virtual public ReadProcessingModule {
protected:
    MappingOptions mappingOptions;
    LshMapOptions mapOptions;
    void prepareIndex();
    void mapRead(const LshMapOptions &mapOptions, PairedRead& read);

public:
    virtual void addOptions(seqan::ArgumentParser &argParser);
    virtual void readOptions(seqan::ArgumentParser &argParser);
    virtual int run() = 0;
    virtual std::string getName() const = 0;
    virtual std::string getDescription() const = 0;
};

#endif // PG583_MODULE_MAPPINGMODULE_H
