#ifndef PG583_MODULE_MAPMODULE_H
#define PG583_MODULE_MAPMODULE_H

#include "module/MappingModule.h"

class MapModule : public MappingModule {
public:
    MapModule();
    virtual void addOptions(seqan::ArgumentParser &argParser);
    virtual void readOptions(seqan::ArgumentParser &argParser);
    virtual int run();
    virtual std::string getName() const;
    virtual std::string getDescription() const;

private:
    virtual AlignmentStats processRead(Aligner &, PairedRead& read) override;
};

#endif // PG583_MODULE_MAPMODULE_H
