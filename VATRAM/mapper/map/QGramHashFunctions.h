#ifndef PG583_MAP_QGRAMHASHFUNCTIONS_H
#define PG583_MAP_QGRAMHASHFUNCTIONS_H

#include <vector>
#include <string>

constexpr size_t MAX_Q_GRAM_SIZE = 64;

/**
 * @brief Default q-gram hash function for non-gapped q-grams
 */
template <class TQramHash>
struct DefaultQGramHashFunction {
    typedef TQramHash QGramHash;
    const static unsigned backShift = sizeof(QGramHash) * 8 - 2;

    /// Function for calculating the hash value for the string starting at begin and ending before end
    template <class Iterator>
    QGramHash operator()(Iterator begin, Iterator end) {
        QGramHash result = *begin++;
        for (; begin < end; ++begin) {
            // shifting circularly and adds the new value with xor
//            result = (result >> backShift) ^ (result << 2) ^ QGramHash(*begin);
            result = result * 33767 + QGramHash(*begin);
        }
        return result;
    }
};

/**
 * @brief Q-gram hash function for gapped q-grams
 */
template <class TQramHash>
struct GappedQGramHashFunction {
    typedef TQramHash QGramHash;
    const static unsigned backShift = sizeof(QGramHash) * 8 - 2;
    enum : unsigned char {
        GAP = 0,
        VALUE = 1
    };

    /// Vector storing the gap pattern (the vector contains a sequence of the values GAP and VALUE )
    std::vector<unsigned char> gapVector;

    /// Default constructor: no gap
    GappedQGramHashFunction() :
        gapVector(MAX_Q_GRAM_SIZE, VALUE) {
    }

    /// Constructor taking a gap string ('-' denotes a gap and '#' denotes non-gap)
    GappedQGramHashFunction(const std::string& str) {
        setGapVector(str);
    }

    /// Constructor taking a gap-vector (use the constants GAP and VALUE)
    GappedQGramHashFunction(const std::vector<unsigned char>& gapVector) :
        gapVector(gapVector) {
    }

    /// Function for calculating the hash value for the string starting at begin and ending before end
    template <class Iterator>
    QGramHash operator()(Iterator begin, Iterator end) {
        QGramHash result = *begin++;
        for (size_t i = 0; begin < end; ++begin, ++i) {
            assert(i < gapVector.size());
            // shifting circularly and adds the new value with xor (if there is no gap)
            result = gapVector[i] ?
//                        (result >> backShift) ^ (result << 2) ^ QGramHash(*begin)
                        result * 33767 + QGramHash(*begin)
                      : result;
        }
        return result;
    }

    /// Sets a the gapped q-gram ('-' denotes a gap and '#' denotes non-gap)
    void setGapVector(const std::string& str) {
        gapVector.clear();
        for (char c: str) {
            unsigned char val;
            switch (c) {
            case '#': val = VALUE; break; // This is the common notation used in papers
            case '-': val = GAP; break;
            default: throw std::invalid_argument("setGapVector");
            }
            gapVector.push_back(val);
        }
    }

    /// Returns the gapped q-gram as string ('-' denotes a gap and '#' denotes non-gap)
    std::string getGapVector() const {
        std::string ret;
        for (auto c : gapVector) {
            switch (c) {
            case VALUE:
                ret += '#';
                break;
            case GAP:
                ret += '-';
                break;
            }
        }
        return ret;
    }
};

#endif // PG583_MAP_QGRAMHASHFUNCTIONS_H
