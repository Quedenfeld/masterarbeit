#ifndef LSHIMPL_H
#define LSHIMPL_H

#include "map/LSH.h"

/// Merge function with special access function
template <class InputIterator1, class AccessFunction1, class InputIterator2, class AccessFunction2, class OutputIterator>
OutputIterator merge(InputIterator1 first1, InputIterator1 last1, AccessFunction1 acc1,
           InputIterator2 first2, InputIterator2 last2, AccessFunction2 acc2,
           OutputIterator result) {
    while (first1 != last1 || first2 != last2) {
        if (first1==last1) {
            while (first2 != last2) {
                *result++ = acc2(first2++);
            }
        } else if (first2==last2) {
            while (first1 != last1) {
                *result++ = acc1(first1++);
            }
        } else {
            if (acc2(first2) < acc1(first1)) {
                *result++ = acc2(first2);
                ++first2;
            } else {
                *result++ = acc1(first1);
                ++first1;
            }
        }
    }
    return result;
}

// =====================================================================================
// ===== I M P L E M E N T A T I O N S
// =====================================================================================

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
auto LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::getWseqBeginPos(const WindowSequence& ws) const -> Position {
    return referenceWindows[ws.begin & WI_VALUE_MASK].beginPosition;
}
template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
auto LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::getWseqEndPos(const WindowSequence& ws) const -> Position {
    return referenceWindows[ws.end & WI_VALUE_MASK].beginPosition + windowSize;
}
template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
auto LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::getWseqMidPos(const WindowSequence& ws) const -> Position {
    return (getWseqBeginPos(ws) + getWseqEndPos(ws)) / 2;
}
template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
auto LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::getWseqMidPosWeighted(const WindowSequence& ws) const -> Position {
    switch (ws.length()) {
    case 1: return (getWseqBeginPos(ws) + getWseqEndPos(ws)) / 2;
    case 2: return (ws.countLeft  * (uint64_t) referenceWindows[ws.begin & WI_VALUE_MASK].beginPosition
                  + ws.countRight * (uint64_t) referenceWindows[ws.end   & WI_VALUE_MASK].beginPosition)
                  / (ws.countLeft + ws.countRight) + windowSize / 2;
    case 3: return (ws.countLeft      * (uint64_t) referenceWindows[ws.begin     & WI_VALUE_MASK].beginPosition
                  + ws.countSndToLast * (uint64_t) referenceWindows[(ws.begin+1) & WI_VALUE_MASK].beginPosition
                  + ws.countRight     * (uint64_t) referenceWindows[ws.end       & WI_VALUE_MASK].beginPosition)
                / (ws.countLeft + ws.countSndToLast + ws.countRight) + windowSize / 2;
    default: return getWseqMidPos(ws);
    }
}
template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
auto LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::getWseqChrIdx(const WindowSequence& ws) const -> ChromosomeIndex {
    return referenceWindows[ws.begin & WI_VALUE_MASK].chromosomeIndex;
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::LSH_Generic(QGramHashFunction qghf) :
    LSH_Generic(std::time(nullptr), qghf) {
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::LSH_Generic(unsigned long long initialRandomSeed, QGramHashFunction qghf) :
//    windowSize(125),
//    windowOffset(100),
//    qGramSize(16),
//    signatureLength(32),
//    bandSize(1),
//    limit(3),
//    limit_skip(8),
    initialRandomSeed(initialRandomSeed),
    rngHashFunctions(initialRandomSeed),
    qGramHashObject(qghf) {
    rngLimits.emplace_back(initialRandomSeed);
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::buildIndex() {
    #pragma omp parallel for
    for (size_t bhtIndex = 0; bhtIndex < bandHashTables.size(); ++bhtIndex) {
        bandHashTables[bhtIndex].sort();
    }

    // limit maximum number of threads because there is maybe not enough memory
    // this is about 3s / 170s faster (1,8%)
    const size_t splitCount = 3;
    int threadLimit = std::min<int>(omp_get_max_threads(), bandHashTables.size() / splitCount);
    #pragma omp parallel for num_threads(threadLimit)
    for (size_t bhtIndex = 0; bhtIndex < bandHashTables.size(); ++bhtIndex) {
        bandHashTables[bhtIndex].build();
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::initialize(size_t windowCount){
    // create hash functions
    hashFunctions.clear();
    rngHashFunctions.seed(initialRandomSeed);
    // This was the old initialization of the hash functions
    for (size_t i = 0; i < signatureLength; i++) {
        hashFunctions.push_back(rngHashFunctions());
    }

    // new initialization of the hash functions
//    double big = exp2(2 * qGramSize); // for a shifting q-gram hash function, we only use that bits at positions lower than 2*qGramSize
//    double div = big / signatureLength; // we have "big" different senseful values for hash functions
//                                        // we need "signatureLength" of them, so two hash functions should have an expected distance of "div"
//    double div_half = std::max(1.0, div / 2);
//    for (size_t i = 0; i < signatureLength; i++) {
//        hashFunctions.push_back(uint64_t(i*div) // the lowest possible value for the hash functions
//                                                // the highest possible vlaue should be lower than "(i+1)*div"
//                                + rngHashFunctions() % uint64_t(div_half)   // an uniform distribution is not so good here
//                                + rngHashFunctions() % uint64_t(div_half)); // therefore we use two random values to create a triangle shaped function
//    }

    // initialize rngLimits for each thread
    for (auto &rng : rngLimits) {
        rng.seed(initialRandomSeed);
    }

    // set initial size of band hash table
    size_t bandHashTableCount = signatureLength / bandSize;
    bandHashTableMutex = std::vector<Mutex>(bandHashTableCount);
    bandHashTables.clear();
    bandHashTables.reserve(bandHashTableCount);
    for (size_t i = 0; i < bandHashTableCount; i++) {
        bandHashTables.emplace_back(windowCount);
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::clear() {
    for (auto &rng : rngLimits) {
        rng.seed(initialRandomSeed);
    }

    rngHashFunctions.seed(initialRandomSeed);

    chromosomes.clear();
    referenceWindows.clear();
    bandHashTables.clear();
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::addGenome(Iterator begin_it, Iterator end_it) {
    for (; begin_it != end_it; ++begin_it) {
//        std::cout << "_add" << begin_it->name << "," << seqan::length(begin_it->data) << std::endl;
        addChromosome(*begin_it);
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::addChromosome(const Chromosome &chromosome) {
    if (hashFunctions.size() == 0) {
        // Ihr glaubt gar nicht, wie oft ich schon vergessen hab, initialize() aufzurufen und mich dann über den Seg-Fault gewundert hab...
        std::cout << "hey, you forgot to initialize me" << std::endl;
        throw std::runtime_error("LSH is not initialized");
    }
    size_t chromosomeIndex = chromosomes.size();
    chromosomes.push_back(chromosome);

    const WindowIndex firstWindowIndex = referenceWindows.size();
    addReferenceWindows(chromosomeIndex);
    const WindowIndex lastWindowIndex = referenceWindows.size();
    createReferenceWindowBandHashes(firstWindowIndex, lastWindowIndex);
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::addReferenceWindows(ChromosomeIndex chromosomeIndex) {
    const Chromosome &chromosome = chromosomes[chromosomeIndex];
    if (seqan::length(chromosome.data) >= windowSize) {
        Position lastWindowPos = seqan::length(chromosome.data) - windowSize + 1;
        for (Position windowStartPos = 0; windowStartPos < lastWindowPos; windowStartPos += windowOffset) {
            auto windowStartPosIt = seqan::begin(chromosome.data) + windowStartPos;
            auto isOnlyNs = [windowStartPosIt] (Position first, Position last ) {
                return std::all_of(windowStartPosIt + first, windowStartPosIt + last, [](ReferenceChar c) { return c == 'N'; });
            };
            if (!isOnlyNs(0, windowOffset) && !isOnlyNs(windowSize - windowOffset, windowSize)) {
                referenceWindows.emplace_back(chromosomeIndex, windowStartPos);
            }
        }
    } else {
        logger.warn("LSH: Length of chromosome ", chromosome.name, " is ", seqan::length(chromosome.data));
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::createReferenceWindowBandHashes(WindowIndex first, WindowIndex last) {
    size_t chunkSize = 1000; // process chunkSize many windows in one thread
    WindowIndex wholeChunksEndIndex = last - (last - first) % chunkSize;
    #pragma omp parallel
    {
        #pragma omp single
        {
            int numThreads = omp_get_num_threads();
            while (int(rngLimits.size()) < numThreads) {
                rngLimits.emplace_back(initialRandomSeed);
            }
        }
        std::vector<std::vector<BandHash>> bandHashesPerWindow(chunkSize);
        #pragma omp for schedule(dynamic, 1) nowait
        for (WindowIndex chunkStartIndex = first; chunkStartIndex < wholeChunksEndIndex; chunkStartIndex += chunkSize) {
            createReferenceWindowBandHashes(chunkStartIndex, chunkStartIndex + chunkSize, bandHashesPerWindow);
        }
        #pragma omp single nowait
        createReferenceWindowBandHashes(wholeChunksEndIndex, last, bandHashesPerWindow);
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::createReferenceWindowBandHashes(WindowIndex chunkStartIndex, WindowIndex chunkEndIndex,
                                                                                               std::vector<std::vector<BandHash>> &bandHashesPerWindow) {
    bandHashesPerWindow.resize(chunkEndIndex- chunkEndIndex);
    for (WindowIndex windowIndex = chunkStartIndex; windowIndex < chunkEndIndex; ++windowIndex) {
        auto &bandHashes = bandHashesPerWindow[windowIndex - chunkStartIndex];
        bandHashes.clear();
        createWindowBandHashes(referenceWindows[windowIndex].chromosomeIndex,
                               referenceWindows[windowIndex].beginPosition, bandHashes);
    }
    for (size_t t = 0; t < bandHashTables.size(); ++t) {
        LockGuard lock(bandHashTableMutex[t]);
        for (WindowIndex windowIndex = chunkStartIndex; windowIndex < chunkEndIndex; ++windowIndex) {
            addReferenceWindowHash(t, bandHashesPerWindow[windowIndex - chunkStartIndex][t], windowIndex);
        }
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::createWindowBandHashes(ChromosomeIndex chromosomeIndex, Position beginPosition, std::vector<BandHash> &out_bandHashes) {
    seqan::String<ReferenceChar> window = seqan::infixWithLength(chromosomes[chromosomeIndex].get().data, beginPosition, windowSize);
    std::vector<QGramHash> qGramHashes;
    createQGramHashes_iupac(seqan::begin(window), seqan::end(window), qGramHashes);

    std::vector<SignatureHash> signatures;
    createSignatures(qGramHashes.begin(), qGramHashes.end(), signatures);

    createBandHashes(signatures.begin(), signatures.end(), out_bandHashes);
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::addReferenceWindowHash(size_t bandIndex, BandHash bucketHash, WindowIndex referenceWindowIndex) {
    bandHashTables[bandIndex].add(bucketHash, referenceWindowIndex);
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
WindowIndex LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::addReferenceWindow(
        ChromosomeIndex chromosomeIndex, Position beginPosition, const std::vector<BandHash> &bandHashes) {
    WindowIndex windowIndex = referenceWindows.size();
    referenceWindows.emplace_back(chromosomeIndex, beginPosition);

    for (size_t i = 0; i < bandHashes.size(); i++) {
        addReferenceWindowHash(i, bandHashes[i], windowIndex);
    }

    return windowIndex;
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::findRead(const ReadString &read, const ReadString &reversedRead,
                   const LshMapOptions &mapOptions,
                   std::vector<WindowIndex> &out_referenceWindowIndices,
                   std::vector<WindowIndex> &out_referenceWindowIndices_revComplement) {
//    findReadDirected(seqan::begin(read), seqan::end(read), out_referenceWindowIndices);
//    findReadDirected(seqan::begin(reversedRead), seqan::end(reversedRead), out_referenceWindowIndices_revComplement);
    std::vector<std::pair<WindowIndex, unsigned>> occ = findReadOccurrences(read, reversedRead, mapOptions.minCountSelection, mapOptions.maximumOfReturnedWindows);
    if (occ.size() == 0) {
        return;
    }
    std::vector<WindowSequence> wseqs = compressOccurrences(occ);
    for (WindowSequence& ws: wseqs) {
        bool inverted = false;
        if (ws.begin >= WI_INVERTED_MASK) {
            ws.begin -= WI_INVERTED_MASK;
            ws.end -= WI_INVERTED_MASK;
            inverted = true;
        }
        WindowIndex wi = (ws.end + ws.begin) / 2;
        if ((ws.end + ws.begin) % 2 == 1) { // there is a division rest
            if (occ[wi] < occ[wi+1]) { // ceil is bigger than floor
                wi++; // use ceil
            }
        }
        if (inverted) {
            out_referenceWindowIndices_revComplement.push_back(wi);
        } else {
            out_referenceWindowIndices.push_back(wi);
        }
    }
}

//extern std::unordered_map<unsigned, unsigned> lsh_debug_wsSize;
template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::findInterval(PairedRead& read, const LshMapOptions& mapOptions) {
    if (read.isPairedEnd) {
        findInterval_pairedEnd(read.seq[0], read.revSeq[0], read.seq[1], read.revSeq[1], mapOptions, read.intervals[0], read.intervals[1]);
    } else {
        // Single End
        if (seqan::length(read.seq[0]) > windowSize * mapOptions.longReadSplitFactor) {
            findInterval_longRead(read.seq[0], read.revSeq[0], mapOptions, read.intervals[0]);
        } else {
            findInterval_singleEnd(read.seq[0], read.revSeq[0], mapOptions, read.intervals[0]);
        }
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::findInterval_singleEnd(
        const ReadString &read, const ReadString &reversedRead,
        const LshMapOptions &mapOptions,
        std::vector<MapInterval>& out_result) {
    std::vector<std::pair<WindowIndex, unsigned>> map = findReadOccurrences(read, reversedRead, mapOptions.minCountSelection, mapOptions.maximumOfReturnedWindows);
    if (map.size() == 0) {
        return;
    }

    std::vector<WindowSequence> compressed = compressOccurrences(map);

    // select interesting windows
    std::vector<WindowSequence> selected = selectWindows(compressed, mapOptions);


    // calculate begin and end where the aligner will search for the read
    size_t readLength = seqan::length(read);
    for (WindowSequence ws: selected) {
        createResult(ws, readLength, mapOptions, out_result);
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::findInterval_pairedEnd(
        const ReadString &read1, const ReadString &read1_reversed,
        const ReadString &read2, const ReadString &read2_reversed,
        const LshMapOptions &mapOptions,
        std::vector<MapInterval>& out_result1,
        std::vector<MapInterval>& out_result2) {//, ChromosomeIndex /*debugChrIdx*/, Position debugPos1, Position debugPos2) {

    size_t readLength[2] = {seqan::length(read1), seqan::length(read2)};
    std::vector<std::pair<WindowIndex, unsigned>> maps[2];
    maps[0] = findReadOccurrences(read1, read1_reversed, mapOptions.minCountPrefiltering, mapOptions.maximumOfReturnedWindows);
    maps[1] = findReadOccurrences(read2, read2_reversed, mapOptions.minCountPrefiltering, mapOptions.maximumOfReturnedWindows);

    if (maps[0].empty() && maps[1].empty()) {
//        std::cout << "Warning: both maps are empty" << std::endl;
        return;
    }

//    const WindowIndex LAST_WINDOW_INDEX_MARKER = std::numeric_limits<WindowIndex>::max();
    std::vector<WindowSequence> compressed[2];
    double scoreMean[2] = {0,0};

    for (size_t i: {0,1}) {
        if (maps[i].size()) {
            compressed[i] = compressOccurrences(maps[i]);
            for (WindowSequence& ws: compressed[i]) {
                scoreMean[i] += ws.countScore;
                ws.pairedEndOriginalScore = ws.countScore;
            }
            scoreMean[i] /= compressed[i].size();
        }
    }
//    std::cout << "scores: " << scoreMean[0] << "," << scoreMean[1] << std::endl;

    std::vector<MapInterval>* out_results_pt[2] = {&out_result1, &out_result2};

    const double normalizationFactor = 1 / (1 / scoreMean[0] + 1 / scoreMean[1]);
    const int64_t MAX_WINDOW_INDEX_DISTANCE = std::ceil(mapOptions.pairedEndDistanceMax / (double) windowOffset);
    const WindowIndex LAST_WINDOW_INDEX_MARKER = std::numeric_limits<WindowIndex>::max() - MAX_WINDOW_INDEX_DISTANCE;
    for (size_t i: {0,1}) {
        if (compressed[i].size()) {
            size_t other = 1 - i;
            compressed[other].emplace_back(LAST_WINDOW_INDEX_MARKER, 0); // add dummy
            typename std::vector<WindowSequence>::iterator otherBegin = compressed[other].begin();
            auto jtLast = compressed[i].end();
            for (auto jt = compressed[i].begin(); jt != jtLast; ++jt) {
                WindowSequence& ws = *jt;

                int64_t compWidx = ws.begin - MAX_WINDOW_INDEX_DISTANCE;
                while (otherBegin->end < compWidx) {
                    ++otherBegin;
                }
                // find best partner
                unsigned opt = 0;
                if (global_LSH & LSH_PE_FOR) if (otherBegin->begin - MAX_WINDOW_INDEX_DISTANCE <= ws.end) {

                    ChromosomeIndex iChr = getWseqChrIdx(ws);
                    bool iInverted = ws.getInverted();
                    bool inv = (i==1) ^ iInverted;
                    int64_t invValue = 1 - int(inv) * 2; // false -> 1, true -> -1
                    int64_t iMidPos = getWseqMidPosWeighted(ws);

                    auto othIt = otherBegin;
                    do {

                        int64_t diff = getWseqMidPosWeighted(*othIt) - iMidPos;
                        int64_t diffInv = diff * invValue;

                        if (iChr == getWseqChrIdx(*othIt)
                                && iInverted == othIt->getInverted()
                                && diffInv >= mapOptions.pairedEndDistanceMin
                                && diffInv <= mapOptions.pairedEndDistanceMax) {
                            opt = std::max(opt, othIt->pairedEndOriginalScore);

                        }

                        ++othIt;
                    } while (othIt->begin - MAX_WINDOW_INDEX_DISTANCE <= ws.end);
                }
                if (global_LSH & LSH_PE_PLUS)
                    ws.countScore = (ws.pairedEndOriginalScore + opt) ; // increase the score
                else if (global_LSH & LSH_PE_PLUS_D2)
                    ws.countScore = (ws.pairedEndOriginalScore + opt) / 2.;
                else if (global_LSH & LSH_PE_MEAN)
                    ws.countScore = (ws.pairedEndOriginalScore / scoreMean[i] + opt / scoreMean[other]) * normalizationFactor;
            }
            compressed[other].pop_back(); // delete dummy

            LshMapOptions mo = mapOptions;
            if (global_LSH & LSH_PE_MINCOUNT_ZERO) {
                mo.minCountPrefiltering = 0;
                mo.minCountSelection = 0;
            }
            if (global_LSH & LSH_PE_NEXTFACTOR_MAX)
                mo.nextFactor = 9999;
            if (global_LSH & LSH_PE_MAXSELECT_MAX)
                mo.maxSelect = 9999;

            std::vector<WindowSequence> selected = selectWindows(compressed[i], mo);

            // calculate begin and end where the aligner will search for the read
            for (WindowSequence ws: selected) {
                ws.countScore = ws.pairedEndOriginalScore; // restore old value
                createResult(ws, readLength[i], mapOptions, *out_results_pt[i]);
            }

            if (global_LSH & LSH_PE_OUT) {
                for (int k: {0, 1}) {
                    std::vector<double> values;
                    for (const WindowSequence& ws: compressed[i]) {
                        values.push_back(k ? ws.countScore : ws.pairedEndOriginalScore);
                    }
                    std::sort(values.begin(), values.end(), std::greater<double>());
                    std::cout << (k ? "paired: " : "single: ");
                    for (double d: values) {
                        std::cout << d << ",";
                    }
                    std::cout << std::endl;
                }
            }

        }
    }

}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::findInterval_longRead(
        const ReadString &read, const ReadString &reversedRead, const LshMapOptions &mapOptions, std::vector<MapInterval>& out_result) {

    typedef uint32_t FragmentID;
    constexpr uint64_t CHROMFACTOR = 1ull << 32;
    constexpr uint64_t POSITION_MASK = CHROMFACTOR - 1;
    struct WinSeqWithID : public WindowSequence {
        FragmentID fragmentID;
        uint64_t normalizedMidPosPlusChrOffset;
        WinSeqWithID(const WindowSequence& ws, FragmentID fragmentID, Position normalizedMidPos, ChromosomeIndex chrIdx) :
            WindowSequence(ws),
            fragmentID(fragmentID),
            normalizedMidPosPlusChrOffset(normalizedMidPos + chrIdx * CHROMFACTOR) {
        }
        Position getNormalizedMidPos() const {
            return normalizedMidPosPlusChrOffset & POSITION_MASK;
        }
        bool operator<(const WinSeqWithID& oth) const {
            return normalizedMidPosPlusChrOffset < oth.normalizedMidPosPlusChrOffset;
        }
    };

    std::vector<WinSeqWithID> windowSeqs;

    size_t rLng = seqan::length(read);
    size_t splitLng;
    size_t splitCount;
    if (global_LSH & LSH_SPLIT_EQUAL) {
        splitCount = std::round(rLng / (double) mapOptions.longReadSplitLength);
        splitLng = std::ceil(rLng / (double) splitCount);
    } else {
        splitLng = mapOptions.longReadSplitLength;
        splitCount = std::round(rLng / (double) splitLng); // last read fragment has at least half as long as the other fragments
    }

    for (size_t i = 0; i < splitCount; i++) {
        size_t begin = i*splitLng;
        size_t end = std::min((i+1)*splitLng, rLng);
        std::vector<std::pair<WindowIndex, unsigned>> occ = findReadOccurrences(seqan::infix(read, begin, end),
                                                                                seqan::infix(reversedRead, begin, end),
                                                                                mapOptions.minCountPrefiltering,
                                                                                mapOptions.maximumOfReturnedWindows);
        if (occ.size()) {
            std::vector<WindowSequence> wseqs = compressOccurrences(occ);
            for (const WindowSequence& ws: wseqs) {
                int64_t normalizedPos = int64_t(getWseqMidPosWeighted(ws)) - int64_t(i * splitLng);
                normalizedPos = std::min<int64_t>(std::max<int64_t>(normalizedPos, 0), seqan::length(chromosomes[getWseqChrIdx(ws)].get().data));
                windowSeqs.emplace_back(ws, i, normalizedPos, getWseqChrIdx(ws));
            }
        }
    }

    // sort windowSeqs according to the center of the respective WindowSequence minus the "read fragment offset"
    if (windowSeqs.empty()) {
        return;
    }
    std::sort(windowSeqs.begin(), windowSeqs.end());

    // compress
    struct LongReadWSequence {
        typedef typename WindowSequence::Score Score;
        Score score;
        Position begin;
        Position end;
        std::vector<WinSeqWithID*>  wseqs;
        LongReadWSequence(WinSeqWithID& wseq, const LSH_This& lsh, size_t splitLng) :
            score(wseq.countScore),
            begin(lsh.getWseqBeginPos(wseq) - wseq.fragmentID * splitLng),
            end(lsh.getWseqEndPos(wseq) - wseq.fragmentID * splitLng) {
            wseqs.push_back(&wseq);
        }
        Score getScore() const {
            return score;
        }
        bool contains(Position mid) {
            return begin <= mid && mid <= end;
        }
        const WinSeqWithID& firstWSeq() const {
            return *wseqs.front();
        }
        void add(WinSeqWithID& wseq, const LSH_This& lsh, size_t splitLng) {
            wseqs.push_back(&wseq);
            score += wseq.countScore;
            begin = std::min<Position>(begin, lsh.getWseqBeginPos(wseq) - wseq.fragmentID * splitLng);
            end = std::max<Position>(end, lsh.getWseqEndPos(wseq) - wseq.fragmentID * splitLng);
        }
    };

    std::vector<LongReadWSequence> compressed;
    compressed.emplace_back(windowSeqs.front(), *this, splitLng);
    for (size_t i = 1; i < windowSeqs.size(); i++) {
        WinSeqWithID& entry = windowSeqs[i];

        LongReadWSequence& last = compressed.back();
        if (getWseqChrIdx(entry) == getWseqChrIdx(last.firstWSeq())
                && entry.getInverted() == last.firstWSeq().getInverted()
                && last.contains(entry.getNormalizedMidPos())) {
            last.add(entry, *this, splitLng);
        } else {
            // a new window sequence
            compressed.emplace_back(entry, *this, splitLng);
        }
    }
    //std::sort(compressed.begin(), compressed.end(), [](const LongReadWSequence& a, const LongReadWSequence& b){return a.count < b.count;});

//    std::cerr << omp_get_thread_num() << ": selectWindows" << std::endl;
    std::vector<LongReadWSequence> selected = selectWindows(compressed, mapOptions);
//    std::cerr << omp_get_thread_num() << ": createIntervals" << std::endl;
    for (LongReadWSequence& entry: selected) {

        double mean = 0;
//        std::cout << "nor:";
        for (const WinSeqWithID* ws: entry.wseqs) {
            mean += ws->getNormalizedMidPos();
//            std::cout << ws->getNormalizedMidPos() << " (" << ws->fragmentID << ") " << ws->begin << "-" << ws->end << ";";
        }
//        std::cout << std::endl;
        mean /= entry.wseqs.size();
//        std::cout << "Begin of first: " << getWseqBeginPos(entry.firstWSeq()) << " (" << entry.firstWSeq().fragmentID << ")" << std::endl;
//        std::cout << "Entry: " << uint64_t(mean) << "," << entry.firstWSeq().getNormalizedMidPos() << std::endl;
        int64_t begin = mean - windowSize * (0.5 + mapOptions.extendFactor);
        int64_t end = mean + (splitCount-1)*splitLng + windowSize * (0.5 + mapOptions.extendFactor);
//        std::cout << "Res: " << begin << "-" << end << std::endl;

        MapInterval res;
        res.inverted = entry.firstWSeq().getInverted();
        res.chromosome = getWseqChrIdx(entry.firstWSeq());
        res.startPosition = std::max<int64_t>(0, begin);
        res.endPosition = std::min<int64_t>(end, seqan::length(chromosomes[res.chromosome].get().data));
//        res.startPosition = begin;
//        res.endPosition = end;
        res.score = entry.getScore();
        out_result.push_back(res);
    }
}


template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::createQGramHashes(Iterator window_begin, Iterator window_end, std::vector<QGramHash> &out_hashes) {
    out_hashes.clear();
    if (window_begin + qGramSize <= window_end) { // if the window is bigger than the q-gram
        out_hashes.reserve(window_end - window_begin - qGramSize + 1);
        for (auto qgram_end = window_begin + qGramSize; qgram_end <= window_end; ++window_begin, ++qgram_end) {
            // window_begin is the start position of the current q-gram.
            out_hashes.push_back(qGramHashFunction(window_begin, qgram_end));
        }
        // Remove duplicates.
        std::sort(out_hashes.begin(), out_hashes.end());
        if (global_LSH & (LSH_KTRS_1 | LSH_KTRS_2)) {} else {
            out_hashes.erase(std::unique(out_hashes.begin(), out_hashes.end()), out_hashes.end());
        }
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::createQGramHashes_iupac(Iterator window_begin, Iterator window_end, std::vector<QGramHash> &out_hashes) {
    // number of variants for a iupac symbol
    const static auto nv = [](unsigned x) {return (x & 1)
                                                + ((x & 2) >> 1)
                                                + ((x & 4) >> 2)
                                                + ((x & 8) >> 3);};
    const static unsigned long long NUMBER_OF_VARIANTS[16] = {nv(0), nv(1), nv(2), nv(3), nv(4), nv(5), nv(6), nv(7),
                                                              nv(8), nv(9), nv(10),nv(11),nv(12),nv(13),nv(14),nv(15)};

    out_hashes.clear();
    out_hashes.reserve(3 * qGramSize);
    typedef typename std::array<seqan::Dna, MAX_Q_GRAM_SIZE>::iterator OpIt;

    uint_fast16_t combinationCount[5] = {0,0,0,0,0}; // we count the number of bases with 0,1,2,3,4 variants
    const double lb3 = log2(3);
    const double lb_limit = log2(limit + 0.001);
    const double lb_limit_skip = log2(limit_skip + 0.001);
    auto func_lb_combinationNumber = [&combinationCount, lb3]() { // calculates the binary logarithm of the number of combinations for the current q-gram
        return combinationCount[2] + lb3 * combinationCount[3] + 2 * combinationCount[4];
    };
    auto func_combinationNumber = [&combinationCount]() {
        unsigned long long combinations = 1ull << (combinationCount[2] + 2*combinationCount[4]);
        for (uint_fast16_t i = 0; i < combinationCount[3]; i++) {
            combinations *= 3;
        }
        return combinations;
    };

    auto qgram_end = window_begin + qGramSize;
    for (auto it = window_begin; it < qgram_end && it < window_end; ++it) {
        combinationCount[NUMBER_OF_VARIANTS[unsigned(seqan::Iupac(*it))]]++;
    }

    // the normal operation to add all combinations
    auto operation = [&out_hashes, this](OpIt beginIt, OpIt endIt) {
        out_hashes.emplace_back(this->qGramHashFunction(beginIt, endIt));
    };

    // the operation that adds on average only "limit" combinations
    auto &rngLimit = rngLimits[omp_get_thread_num()];
    const double rngMax = rngLimit.max();
    const double limitAsFloat = limit;
    unsigned long long combinations; // must be set before executing operation_limit
    auto operation_limit = [&out_hashes, this, &rngLimit, rngMax, &combinations, limitAsFloat](OpIt beginIt, OpIt endIt) {
        if (double(rngLimit()) / rngMax < limitAsFloat / combinations) {
            out_hashes.emplace_back(this->qGramHashFunction(beginIt, endIt));
        }
    };

    auto lastQgram_begin = window_end - qGramSize;
    for (auto curQgram_begin = window_begin; curQgram_begin <= lastQgram_begin; ++curQgram_begin) {
        double lb_combinations = func_lb_combinationNumber();
        if (lb_combinations <= lb_limit) {
            // add all combinations
            findCombinationsOfQGram(curQgram_begin, operation);
        } else if (lb_combinations <= lb_limit_skip) {
            // add a random set of (on average) "limit" combinations
            combinations = func_combinationNumber(); // combinations is used by findCombinationsOfQGram!
            findCombinationsOfQGram(curQgram_begin, operation_limit);
        } else {
            // skip this q-gram (because there are too many combinations)
        }

        // update the number of combinations for the next q-gram
        if (curQgram_begin < lastQgram_begin) { // true, if this is not the last iteration
            combinationCount[NUMBER_OF_VARIANTS[unsigned(seqan::Iupac(*curQgram_begin))]]--;
            combinationCount[NUMBER_OF_VARIANTS[unsigned(seqan::Iupac(*(curQgram_begin + qGramSize)))]]++;
        }
    }

    std::sort(out_hashes.begin(), out_hashes.end());
    if (global_LSH & (LSH_KTRS_1 | LSH_KTRS_2)) {} else {
        out_hashes.erase(std::unique(out_hashes.begin(), out_hashes.end()), out_hashes.end());
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::createSignatures(Iterator qGramHashes_begin, Iterator qGramHashes_end, std::vector<SignatureHash> &out_signatures) {
    if (global_LSH & LSH_KTRS_1) {
        if (global_LSH & LSH_KTRS_2) {
            // multihash, separate
            out_signatures.resize(signatureLength);
            QGramHash lastQGram = ~*qGramHashes_begin; // just take another q-gram than the first one
            std::vector<SignatureHash> lastHashes(signatureLength+1); // +1 is important, see below!
            std::vector<SignatureHash> minValues(signatureLength, std::numeric_limits<SignatureHash>::max());
            for (; qGramHashes_begin != qGramHashes_end; ++qGramHashes_begin) {
                QGramHash qGramHash = *qGramHashes_begin;
                if (qGramHash != lastQGram) {
                    for (size_t hashFuncIndex = 0; hashFuncIndex < signatureLength; hashFuncIndex++) {
                        SignatureHash& hash = lastHashes[hashFuncIndex]; // write to lastHashes
                        hash = qGramHash ^ hashFunctions[hashFuncIndex];
                        if (hash < minValues[hashFuncIndex]) {
                            out_signatures[hashFuncIndex] = hash;
                            minValues[hashFuncIndex] = hash;
                        }
                    }
                } else {
                    // same q-gram
                    lastHashes[signatureLength] = lastHashes[0]; // this avoids an expensive modulo-operation
                    for (size_t hashFuncIndex = 0; hashFuncIndex < signatureLength; hashFuncIndex++) {
                        SignatureHash& hash = lastHashes[hashFuncIndex];
                        hash = lastHashes[hashFuncIndex+1] ^ hashFunctions[hashFuncIndex]; // offset access, no modulo necessary (see above)
                        if (hash < minValues[hashFuncIndex]) {
                            out_signatures[hashFuncIndex] = qGramHash ^ hashFunctions[hashFuncIndex];
                            minValues[hashFuncIndex] = hash;
                        }
                    }
                }
                lastQGram = *qGramHashes_begin;
            }
        } else {
            // multi hash
            out_signatures.assign(signatureLength, std::numeric_limits<SignatureHash>::max());
            QGramHash lastQGram = ~*qGramHashes_begin; // just take another q-gram than the first one
            std::vector<SignatureHash> lastHashes(signatureLength+1); // +1 is important, see below!
            for (; qGramHashes_begin != qGramHashes_end; ++qGramHashes_begin) {
                QGramHash qGramHash = *qGramHashes_begin;
                if (qGramHash != lastQGram) {
                    for (size_t hashFuncIndex = 0; hashFuncIndex < signatureLength; hashFuncIndex++) {
                        SignatureHash& hash = lastHashes[hashFuncIndex]; // write to lastHashes
                        hash = qGramHash ^ hashFunctions[hashFuncIndex];
                        out_signatures[hashFuncIndex] = hash < out_signatures[hashFuncIndex] ? hash : out_signatures[hashFuncIndex];
                    }
                } else {
                    // same q-gram
                    lastHashes[signatureLength] = lastHashes[0]; // this avoids an expensive modulo-operation
                    for (size_t hashFuncIndex = 0; hashFuncIndex < signatureLength; hashFuncIndex++) {
                        SignatureHash& hash = lastHashes[hashFuncIndex];
                        hash = lastHashes[hashFuncIndex+1] ^ hashFunctions[hashFuncIndex]; // offset access, no modulo necessary (see above)
                        out_signatures[hashFuncIndex] = hash < out_signatures[hashFuncIndex] ? hash : out_signatures[hashFuncIndex];
                    }
                }
                lastQGram = *qGramHashes_begin;
            }
        }
    } else if (global_LSH & LSH_KTRS_2) {
        // div, separate
        out_signatures.resize(signatureLength);
        std::vector<SignatureHash> minValues(signatureLength, std::numeric_limits<SignatureHash>::max());
        QGramHash lastQGram = ~*qGramHashes_begin; // just take another q-gram than the first one
        int repetitionCount = 0;
        for (; qGramHashes_begin != qGramHashes_end; ++qGramHashes_begin) {
            QGramHash qGramHash = *qGramHashes_begin;
            if (qGramHash != lastQGram) {
                repetitionCount = 1;
                for (size_t hashFuncIndex = 0; hashFuncIndex < signatureLength; hashFuncIndex++) {
                    // Create a normal hash value and XOR it with one of the randomly generated integers.
                    // This will serve well enough as "another" hash function.
                    SignatureHash hash = qGramHash ^ hashFunctions[hashFuncIndex];
                    if (hash < minValues[hashFuncIndex]) {
                        out_signatures[hashFuncIndex] = hash;
                        minValues[hashFuncIndex] = hash;
                    }
                }
            } else {
                repetitionCount++;
                for (size_t hashFuncIndex = 0; hashFuncIndex < signatureLength; hashFuncIndex++) {
                    // Create a normal hash value and XOR it with one of the randomly generated integers.
                    // This will serve well enough as "another" hash function.
                    SignatureHash hash = qGramHash ^ hashFunctions[hashFuncIndex];
                    SignatureHash value = hash / repetitionCount;
                    if (value < minValues[hashFuncIndex]) {
                        out_signatures[hashFuncIndex] = hash;
                        minValues[hashFuncIndex] = value;
                    }
                }
            }

        }


    } else {
        // OLD IMPLEMENTATION
        out_signatures.assign(signatureLength, std::numeric_limits<SignatureHash>::max());
        for (; qGramHashes_begin != qGramHashes_end; ++qGramHashes_begin) {
            QGramHash qGramHash = *qGramHashes_begin;
            for (size_t hashFuncIndex = 0; hashFuncIndex < signatureLength; hashFuncIndex++) {
                // Create a normal hash value and XOR it with one of the randomly generated integers.
                // This will serve well enough as "another" hash function.
                SignatureHash hash = qGramHash ^ hashFunctions[hashFuncIndex];
                out_signatures[hashFuncIndex] = hash < out_signatures[hashFuncIndex] ? hash : out_signatures[hashFuncIndex];
            }
        }
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::createBandHashes(Iterator sig_begin, Iterator sig_end, std::vector<BandHash> &out_bandHashes) {
    out_bandHashes.clear();
    out_bandHashes.reserve((sig_end - sig_begin) / bandSize);
    // FIXME: signatureLength should be a multiple of bandSize.
    //        If not, the last 'signatureLength mod bandSize' signatures will be ignored!
    //        Should be enforced in constructor and / or CLI etc.
    for (; sig_begin < (sig_end - bandSize + 1); sig_begin = sig_begin + bandSize) {
        out_bandHashes.push_back(bandHashFunction(sig_begin, sig_begin + bandSize));
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator> // an iterator pointing to seqan::Dna or seqan::Dna5
typename LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::QGramHash LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::qGramHashFunction(Iterator begin, Iterator end) {
    static_assert(std::is_same<const typename std::remove_reference<decltype(*begin)>::type, const seqan::Dna>::value ||
                  std::is_same<const typename std::remove_reference<decltype(*begin)>::type, const seqan::Dna5>::value,
                  "Iterator is pointing to the wrong type");
    return qGramHashObject(begin, end);
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
BandHash LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::bandHashFunction(Iterator begin, Iterator end) {
    // diese Hashfunction könnte möglichweise zu schwach sein!
    // 33767 is a prime number
    return std::accumulate(begin + 1, end, BandHash(*begin), [](BandHash ret, SignatureHash c) { return ret * 33767 + c; });
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
BandHash LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::bandHashFunction(typename std::vector<SignatureHash>::iterator begin,
                                                                                    typename std::vector<SignatureHash>::iterator end) {
    // TODO: diese Hashfunction könnte möglichweise zu schwach sein!
    // 33767 is a prime number
    return std::accumulate(begin + 1, end, BandHash(*begin), [](BandHash ret, SignatureHash c) { return ret * 33767 + c; });
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator, class Function>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::findCombinationsOfQGram(Iterator iupacQgram_begin, Function &operation) {
    std::array<char, sizeof(seqan::Dna) * MAX_Q_GRAM_SIZE> _curQGram;
    // goddamn ugly, but prevents default initialization of non-plain data type. Other applicable suggestions welcome!
    auto &curQGram = (std::array<seqan::Dna, MAX_Q_GRAM_SIZE>&) _curQGram;
    std::array<char, MAX_Q_GRAM_SIZE + 1> curVariant;

    int curIndex = 0; // current index in the current q-gram
    curVariant[0] = 0; // begin with variant id 0 at position 0

    const int qGramSizeMinusOne = (int) qGramSize - 1;

    while (true) {
        while (curVariant[curIndex] == 4) {
            // Alle Varianten wurden für das aktuelle Zeichen abgearbeitet
            curIndex--;
            if (curIndex < 0) {
                // Alle Varianten des aktuellen Q-Gramms wurden berücksichtigt -> break und nächstes Q-Gramm
                return;
            }
        }

        // extract the current iupac-base
        seqan::Iupac iupacSymbol = *(iupacQgram_begin + curIndex);
        seqan::Iupac dnaChar = iupacSymbol.value & (1 << curVariant[curIndex]);
        curVariant[curIndex]++; // next variant for the current position
        if (dnaChar.value != 0) {
            // an der Position "curIndex" im Q-Gramm kann die Base mit ID "curVariant[curIndex]" stehen
            curQGram[curIndex] = dnaChar;
            if (curIndex == qGramSizeMinusOne) {
                // we are at the end of the q-gram, so add the current q-gram to the document-q-gram-set
                operation(curQGram.begin(), curQGram.begin() + qGramSize);
           } else {
                curIndex++; // next position
                curVariant[curIndex] = 0; // current variant at the new position is set to zero
            }
        }
    }

}



template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
size_t LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::memoryUsage() const {
    size_t sum = 0;
    sum += sizeof(*this);
    for (const BHT& bht: bandHashTables) {
        sum += bht.memoryUsage();
    }
    sum += referenceWindows.capacity() * sizeof(ReferenceWindow);
    std::cout << "__memoryUsage(): " << referenceWindows.size() << "," << referenceWindows.capacity() << "," << windowOffset << "," << sum << std::endl;
    return sum;
}


// TODO: DEPRECATED
//
template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::printHashTableStatistics() {
    for (auto& bht: bandHashTables) {
        bht.printHashTableStatistics();
    }
    logger.info("LSH: ", "genome size: ",
                std::accumulate(chromosomes.begin(), chromosomes.end(), (size_t) 0,
                                [&](size_t s, const Chromosome &c) { return s + seqan::length(c.data); }),
                ", reference windows: ", referenceWindows.size());
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::calcReadBandHashes(Iterator read_begin, Iterator read_end, std::vector<BandHash>& out_bandHashes) {
    std::vector<QGramHash> qGramHashes;
    createQGramHashes(read_begin, read_end, qGramHashes);
    std::vector<SignatureHash> signatures;
    createSignatures(qGramHashes.begin(), qGramHashes.end(), signatures);
    createBandHashes(signatures.begin(), signatures.end(), out_bandHashes);
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
//std::unordered_map<WindowIndex, unsigned>
std::vector<std::pair<WindowIndex, unsigned>>
LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::findReadOccurrences(
        const ReadString &read, const ReadString &reversedRead, size_t minCount, size_t maximumOfReturnedWindows) {
    // ====================
    // NEUE IMPLEMENTIERUNG
    // ====================


    // We use size_t instead of WindowIndex* to add a constant
    typedef std::pair<size_t, size_t> WindowIteratorPair;

    // for each query in the band hash tables we safe a pair of two pointers (begin and end)
    // pointing to the Window Indices in the bucket.
    //std::clog << "__frocc" << std::endl;
    //std::clog << WP_INVERTED_OFFSET << std::endl;
    //std::clog << WP_POINTER_MASK << std::endl;
    std::vector<WindowIteratorPair> iteratorPairs;
    iteratorPairs.reserve(bandHashTables.size() * 2);


    std::vector<BandHash> bandHashes;

    // Search for the read
    calcReadBandHashes(seqan::begin(read), seqan::end(read), bandHashes);
    for (size_t i = 0; i < bandHashes.size(); i++) {
        auto pair = bandHashTables[i].findIterators(bandHashes[i], maximumOfReturnedWindows);
        if (pair.first != NULL) {
            iteratorPairs.push_back(std::make_pair(size_t(pair.first), size_t(pair.second)));
        }
    }

    // Search for the reversed complement
    calcReadBandHashes(seqan::begin(reversedRead), seqan::end(reversedRead), bandHashes);
    for (size_t i = 0; i < bandHashes.size(); i++) {
        auto pair = bandHashTables[i].findIterators(bandHashes[i], maximumOfReturnedWindows);
        if (pair.first != NULL) {
            iteratorPairs.push_back(std::make_pair(size_t(pair.first) + WP_INVERTED_OFFSET, size_t(pair.second) + WP_INVERTED_OFFSET));
        }
    }

    // Number of all returned window indices
//        std::clog << "  sum=";
    size_t wiSum = 0;
    for (WindowIteratorPair& wip: iteratorPairs) {
        //std::clog << "    (" << wip.first << "->" << wiPt(wip.first) << " , " << wip.second << "->" << wiPt(wip.second) << " = " << wiPt(wip.second) - wiPt(wip.first) << ")" << std::endl;
        wiSum += wiPt(wip.second) - wiPt(wip.first);
    }
//        std::clog << wiSum << std::endl;

    // Sort the pointer-pairs by length
    auto iteratorPairComparer = [](const WindowIteratorPair& a, const WindowIteratorPair& b){
        return a.second - a.first > b.second - b.first;};
    std::sort(iteratorPairs.begin(), iteratorPairs.end(), iteratorPairComparer);

    std::vector<WindowIndex> occurrences;
    bool minCountOptimization = false;
    if (wiSum < wiSumThreshold) {
        // small array
        occurrences.reserve(wiSum);
        for (WindowIteratorPair& wip: iteratorPairs) {
            WindowIndex* last = wiPt(wip.second);
            if (wip.first & WP_INVERTED_OFFSET) {
                for (WindowIndex* wp = wiPt(wip.first); wp < last; ++wp) {
                    occurrences.push_back(*wp + WI_INVERTED_MASK);
                }
            } else {
                for (WindowIndex* wp = wiPt(wip.first); wp < last; ++wp) {
                    occurrences.push_back(*wp);
                }
            }
        }
        std::sort(occurrences.begin(), occurrences.end());
    } else {
        // big array
        minCountOptimization = minCount >= 2;
        size_t wiSumReduced = minCountOptimization ? wiSum - (wiPt(iteratorPairs.front().second) -
                                                              wiPt(iteratorPairs.front().first))
                                                   : wiSum;

        occurrences.reserve(wiSumReduced);

        std::vector<size_t> positions; // stores the first index of each subarray in occurrences
        positions.push_back(0); // the first subarray begins with index 0
        auto it = iteratorPairs.begin() + minCountOptimization; // the current (begin,end)-pointer-pair
        auto end = iteratorPairs.end(); // the last pair
        std::back_insert_iterator<std::vector<WindowIndex>> occBackIt(occurrences);
        // while there are further pairs that are big enough, merge them
        while (it+1 < end && (it+1)->second - (it+1)->first >= wipLngThreshold) {
            // MERGE and set INVERTED_WI_OFFSET bit if neccessary
            auto normalAccess = [](WindowIndex* wi){return *wi;};
            auto invAccess = [](WindowIndex* wi){return *wi + WI_INVERTED_MASK;};

            occBackIt = merge(wiPt(it->first), wiPt(it->second),
                                        it->first & WP_INVERTED_OFFSET ? invAccess : normalAccess,
                              wiPt((it+1)->first), wiPt((it+1)->second),
                                        (it+1)->first & WP_INVERTED_OFFSET ? invAccess : normalAccess,
                              occBackIt);

            // the next array will begin at the specified position
            positions.push_back(occurrences.size());
            // we merge two pointer-pairs, so increase current-pair-iterator by 2
            it += 2;
        }

        // if the are pointer-pairs left, copy them into occurrences
        auto firstItNonMerge = it;
        WindowIndex* firstWiNonMerge = &*occurrences.end();
        for (; it != end; ++it) {
            WindowIndex* last = wiPt(it->second);
            if (it->first & WP_INVERTED_OFFSET) {
                for (WindowIndex* wp = wiPt(it->first); wp < last; ++wp) {
                    occurrences.push_back(*wp + WI_INVERTED_MASK);
                }
            } else {
                for (WindowIndex* wp = wiPt(it->first); wp < last; ++wp) {
                    occurrences.push_back(*wp);
                }
            }
        }
        // if there was more than one pair, then sort
        if (end - firstItNonMerge >= 1) {
            if (end - firstItNonMerge >= 2) {
                std::sort(firstWiNonMerge, &*occurrences.end());
            }
            positions.push_back(occurrences.size());
        }

        // we need a second occurrences vector because merge-sort does not work in-place
        std::vector<WindowIndex> occurrences2(wiSumReduced);
        // the following two pointers denote from which vector the
        std::vector<WindowIndex>* copyFrom = &occurrences;
        std::vector<WindowIndex>* copyTo = &occurrences2;

        // merge sort
        while (positions.size() > 2) {
            // the last entry in positions is the end of the last subarray, so calc -1
            // we always look at two subarrays, so calc -1 again
            const size_t lastPosIdx = positions.size() - 2;
            auto it = copyFrom->begin(); // current WindowIndex-Iterator
            size_t curPosIdx = 0; // current index in positions
            while (curPosIdx < lastPosIdx) {
                // merge two subarray
                std::merge(it + positions[curPosIdx],   it + positions[curPosIdx+1],
                           it + positions[curPosIdx+1], it + positions[curPosIdx+2],
                           copyTo->begin() + positions[curPosIdx]);
                curPosIdx += 2;
                // set new array end inplace
                positions[curPosIdx / 2] = positions[curPosIdx];
            }
            // if there is one subarray left, just copy it
            if (curPosIdx == lastPosIdx) {
                //std::clog << "    copy" << std::flush;
                std::copy(it + positions[curPosIdx], it + positions[curPosIdx+1], copyTo->begin() + positions[curPosIdx]);
                positions[curPosIdx / 2 + 1] = positions[curPosIdx + 1];
                curPosIdx++;
            }
            // resize positions array
            positions.resize((curPosIdx+1) / 2 + 1);

            // swap the two vector-pointer
            std::swap(copyFrom, copyTo);
        }
        // ensure that occurrences contains the sorted data
        if (copyFrom != &occurrences) {
            //std::clog << "swap" << std::flush;
            std::swap(occurrences, occurrences2);
        }
    }

    if (false) {
        std::vector<WindowIndex> check;
        calcReadBandHashes(seqan::begin(read), seqan::end(read), bandHashes);
        for (size_t i = 0; i < bandHashes.size(); i++) {
            bandHashTables[i].find(bandHashes[i], maximumOfReturnedWindows,
                                   [&check](WindowIndex wi){check.push_back(wi);}
            );
        }

//            std::clog << "pre=" << check.size() << std::endl;

        // Search for the reversed complement
        calcReadBandHashes(seqan::begin(reversedRead), seqan::end(reversedRead), bandHashes);
        for (size_t i = 0; i < bandHashes.size(); i++) {
            bandHashTables[i].find(bandHashes[i], maximumOfReturnedWindows,
                                   [&check](WindowIndex wi){check.push_back(wi + WI_INVERTED_MASK);}
            );
        }

        // sort the window indices
        std::sort(check.begin(), check.end());

        auto it2 = check.begin();
        for (auto it = occurrences.begin(); it < occurrences.end(); ++it, ++it2) {
            if (*it != *it2) {
                std::cout << "__error" << std::endl;
                std::cout << occurrences.size() << ": ";
                for (WindowIndex wi: occurrences)
                    std::cout << wi << ",";
                std::cout << std::endl;
                std::cout << check.size() << ": ";
                for (WindowIndex wi: check)
                    std::cout << wi << ",";
                std::cout << std::endl;
                break;
            }
        }
    }

    // sort the window indices
    std::vector<std::pair<WindowIndex, unsigned>> result;
    if (occurrences.size() == 0) {
        // return an empty vector if nothing was found
        return result;
    }

    // lets count the number of hits for each window
    WindowIndex curIndex = occurrences.front();
    unsigned curCount = 1;
    WindowIndex* extraBegin;
    WindowIndex* extraEnd;
    WindowIndex extraAdd;
    if (minCountOptimization) {
        // the biggest multi window bucket was not used yet
        extraBegin = wiPt(iteratorPairs.front().first);
        extraEnd = wiPt(iteratorPairs.front().second);
        extraAdd = iteratorPairs.front().first & WP_INVERTED_OFFSET ? WI_INVERTED_MASK : 0;

        // find the first window that is equal or bigger than the first window in occurrences
        while (extraBegin != extraEnd && *extraBegin + extraAdd < curIndex) {
            ++extraBegin;
        }
    } else {
        extraBegin = extraEnd = NULL;
        extraAdd = 0;
    }

    for (size_t i = 1; i < occurrences.size(); i++) {
        if (occurrences[i] == curIndex) {
            curCount++;
        } else {
            // there is a new window index
            // check if the biggest multi window bucket contains the current window index
            if (extraBegin != extraEnd && *extraBegin + extraAdd == curIndex) {
                // if so then add 1 to the count variable
                curCount += 1;
            }
            if (curCount >= minCount) {
                // we only add the window if there are enough occurences
                result.emplace_back(curIndex, curCount);
            }
            curIndex = occurrences[i];
            curCount = 1;


            while (extraBegin != extraEnd && *extraBegin + extraAdd < curIndex) {
                ++extraBegin;
            }
        }
    }
    // add the last index that was not added yet
    result.emplace_back(curIndex, curCount);

    return result;

    // ====================
    // ALTE IMPLEMENTIERUNG
    // ====================
/*
    std::vector<WindowIndex> occurrences;
    occurrences.reserve(signatureLength / bandSize * 4); // reserve enough space
    std::vector<BandHash> bandHashes;

    // Search for the read
    calcReadBandHashes(seqan::begin(read), seqan::end(read), bandHashes);
    for (size_t i = 0; i < bandHashes.size(); i++) {
        bandHashTables[i].find(bandHashes[i], mapOptions.maximumOfReturnedWindows,
                               [&occurrences](WindowIndex wi){occurrences.push_back(wi);}
        );
    }

    // Search for the reversed complement
    calcReadBandHashes(seqan::begin(reversedRead), seqan::end(reversedRead), bandHashes);
    for (size_t i = 0; i < bandHashes.size(); i++) {
        bandHashTables[i].find(bandHashes[i], mapOptions.maximumOfReturnedWindows,
                               [&occurrences](WindowIndex wi){occurrences.push_back(wi + INVERTED_WI_OFFSET);}
        );
    }

    // sort the window indices
    std::sort(occurrences.begin(), occurrences.end());
    std::vector<std::pair<WindowIndex, unsigned>> result;
    if (occurrences.size() == 0) {
        // return an empty vector if nothing was found
        return result;
    }

    // lets count the number of hits for each window
    WindowIndex curIndex = occurrences.front();
    unsigned curCount = 1;
    for (size_t i = 1; i < occurrences.size(); i++) {
        if (occurrences[i] == curIndex) {
            curCount++;
        } else {
            if (curCount >= mapOptions.minCount) {
                // we only add the window if there are enough occurences
                result.emplace_back(curIndex, curCount);
            }
            curIndex = occurrences[i];
            curCount = 1;
        }
    }
    // add the last index that was not added yet
    result.emplace_back(curIndex, curCount);

    return result; */
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
std::vector<typename LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::WindowSequence>
                            LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::compressOccurrences(const std::vector<std::pair<WindowIndex, unsigned> > &occurrences) {
    typedef std::pair<WindowIndex, unsigned> WICPair; // WindowIndex-Count-Pair
    std::vector<WICPair> wiSorted(occurrences.begin(), occurrences.end());
    std::sort(wiSorted.begin(), wiSorted.end(), [](const WICPair& a, const WICPair& b){return a.first < b.first;});

    // compress a sequence of windows to one Compressed object
    std::vector<WindowSequence> compressed;
    compressed.push_back(WindowSequence(wiSorted.front()));
    for (size_t i = 1; i < wiSorted.size(); i++) {
        WICPair& entry = wiSorted[i];
        WindowIndex curWI = entry.first;
        WindowIndex lastWI = compressed.back().end;
        ReferenceWindow& curRefWindow = referenceWindows[curWI & WI_VALUE_MASK];
        ReferenceWindow& lastRefWindow = referenceWindows[lastWI & WI_VALUE_MASK];
        if (curWI == lastWI + 1
                    && curRefWindow.chromosomeIndex == lastRefWindow.chromosomeIndex
                    && curRefWindow.beginPosition == lastRefWindow.beginPosition + windowOffset) {
            compressed.back().expand(entry);
        } else {
            // a new window sequence
            compressed.push_back(WindowSequence(entry));
        }
    }

    return compressed;
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class ScoreObject>
std::vector<ScoreObject> LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::selectWindows(std::vector<ScoreObject> &windowSeqs,
                                                                        const LshMapOptions &mapOptions) {
    // sort the compressed windows by count
    std::sort(windowSeqs.begin(), windowSeqs.end(),
              [](const ScoreObject& a, const ScoreObject& b){
        return a.getScore() > b.getScore();
    });

    std::vector<ScoreObject> selected;
    selected.push_back(windowSeqs[0]);
    for (size_t i = 1; i < std::min<size_t>(mapOptions.maxSelect, windowSeqs.size()); i++) {
        if (windowSeqs[i].getScore() < mapOptions.minCountSelection) {
            break;
        } else if (windowSeqs[i].getScore() * mapOptions.nextFactor < windowSeqs[0].getScore()) {
            break;
        } else {
            // the next interval is added, if the count value is significant large
            selected.push_back(windowSeqs[i]);
        }
    }
    return selected;
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSH_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::createResult(WindowSequence ws, size_t readLength, const LshMapOptions& mapOptions, std::vector<MapInterval>& out_result) {
    const double bandNumber = signatureLength / bandSize; // double because we divide by this value

    const double extendMult = mapOptions.extendBandMultiplicator / bandNumber * windowSize;
    const double contractReadOverlappingLength = (windowSize - windowOffset + readLength - 2*qGramSize) / 2.0;
    const double contractMult = mapOptions.contractBandMultiplicator / bandNumber * contractReadOverlappingLength;

    // calculate begin and end where the aligner will search for the read
    MapInterval res;
    res.inverted = ws.getInverted();
    res.chromosome = getWseqChrIdx(ws);
    res.score = ws.getScore();

    // count_begin replaces map[ws.begin] if map were an unordered_map
    // count_end replaces map[ws.end] if map were an unordered_map
    unsigned count_begin = ws.countLeft;//std::lower_bound(map.begin(), map.end(), ws.begin, [](std::pair<WindowIndex, unsigned>& a, WindowIndex b){return a.first < b;})->second;
    unsigned count_end = ws.countRight;//std::lower_bound(map.begin(), map.end(), ws.end, [](std::pair<WindowIndex, unsigned>& a, WindowIndex b){return a.first < b;})->second;

    // switch for the number of windows in the sequence
    int64_t left, right;
    switch(ws.length()) {
    case 1: {
        left = getWseqBeginPos(ws) - std::max<int64_t>(0, mapOptions.extendFactor * windowSize - count_begin * extendMult);
        right = getWseqEndPos(ws) + std::max<int64_t>(0, mapOptions.extendFactor * windowSize - count_end * extendMult);
        break;
    }
    case 2: {
        left = referenceWindows[ws.end & WI_VALUE_MASK].beginPosition - readLength + qGramSize + count_end * contractMult;
        right = referenceWindows[ws.begin & WI_VALUE_MASK].beginPosition + windowSize + readLength - (int64_t) qGramSize - count_begin * contractMult;
        break;
    }
    default: {
        left = getWseqBeginPos(ws);
        right = getWseqEndPos(ws);
        break;
    }
    }

    res.startPosition = std::max<int64_t>(0, left);
    res.endPosition = std::min<int64_t>(right, seqan::length(chromosomes[res.chromosome].get().data));

    if (res.startPosition > res.endPosition || res.endPosition - res.startPosition + 1 < readLength) {
        if (res.startPosition > res.endPosition) {
            std::cout << "___so goes that but not:" << res.startPosition << "," << res.endPosition << std::endl;
        } else {
            std::cout << "___sense - this result makes none" << std::endl;
        }
        std::cout << "ws: " << ws.begin << " - " << ws.end << ", #=" << ws.countScore << std::endl;
        std::cout << "case: " << ws.length() << std::endl;
        std::cout << "l/r: " << left << " - " << right << std::endl;
        std::cout << "read: " << readLength << std::endl;
        std::cout << "mult (c,e): " << contractMult << "," << extendMult << std::endl;
        std::cout << "windowSize, qGramSize: " << windowSize << "," << qGramSize << std::endl;
        std::cout << "count (b,e): " << count_begin << "," << count_end << std::endl;
//        std::cout << "map: [";
//        for (size_t i = ws.begin; i <= ws.end; i++) {
//            unsigned count = std::lower_bound(map.begin(), map.end(), i, [](std::pair<WindowIndex, unsigned>& a, WindowIndex b){return a.first < b;})->second;
//            std::cout << "(" << i << ": " << count << "), ";
//        }
//        std::cout << "]" << std::endl;
        std::cout << "begin: " << getWseqBeginPos(ws) << " - " << getWseqEndPos(ws) << std::endl;
        std::cout << "chr: " << getWseqChrIdx(ws) << std::endl;
        //            std::cout << "_" << leftExtend << "," << leftMax << "," << std::max(leftExtend, leftMax) << ";" <<std::max<int64_t>(0, std::max(leftExtend, leftMax)) << std::endl;
        //            std::cout << "_" << rightExtend << "," << rightMax << std::endl;
        //            std::cout << referenceWindows[ws.begin].beginPosition << ";" << referenceWindows[ws.end].beginPosition << std::endl;
        //            std::cout << map[ws.begin] << ";" << map[ws.end] << std::endl;
    } else {
        // only add the result if start and end position are valid
        Position length = res.endPosition - res.startPosition;
        Position overlap = (3*readLength)/2;
        Position stride = (17*readLength)/2;
        if (length > overlap+stride) {
            // split interval into smaller ones
            int splits = 1+(length-overlap-1)/stride;
            for (int i = 0; i < splits; i++) {
                MapInterval split;
                split.startPosition = res.startPosition+stride*i;
                split.endPosition = std::min(split.startPosition+stride+overlap, res.endPosition);
                split.chromosome = res.chromosome;
                split.inverted = res.inverted;
                out_result.push_back(split);
            }
        } else {
            // return whole interval
            out_result.push_back(res);
        }
    }
}

#endif // LSHIMPL_H
