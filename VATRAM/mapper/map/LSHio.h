#ifndef LSHIO_H
#define LSHIO_H

#include "LSH.h"
#include <fstream>

class LshIO {
private:
    typedef typename LSH::BHT Superrank;

    template <class T>
    void write(std::ofstream& file, const T& obj) {
        file.write(reinterpret_cast<const char*>(&obj), sizeof(T));
    }

    template <class T>
    void read(std::ifstream& file, T& obj) {
        file.read(reinterpret_cast<char*>(&obj), sizeof(T));
    }

    template <class T>
    void writeVector(std::ofstream& file, const std::vector<T>& v) {
        size_t size = v.size();
        write(file, size);
        file.write(reinterpret_cast<const char*>(v.data()), size * sizeof(T));
    }

    template <class T>
    void readVector(std::ifstream& file, std::vector<T>& v) {
        size_t size;
        read(file, size);
        v.resize(size);
        file.read(reinterpret_cast<char*>(v.data()), size * sizeof(T));
    }

    void writeString(std::ofstream& file, const std::string& str) {
        size_t size = str.size();
        write(file, size);
        file.write(str.data(), size);
    }

    void readString(std::ifstream& file, std::string& str) {
        size_t size;
        read(file, size);
        str.resize(size);
        file.read(&str[0], size);
    }

    template <class SeqanString>
    void writeSeqanString(std::ofstream& file, const SeqanString& str) {
        size_t length = seqan::length(str);
        size_t size = seqan::length(str.data_host);
        write(file, length);
        write(file, size);
        file.write(reinterpret_cast<const char*>(&str.data_host[0]), size * sizeof(str.data_host[0]));
    }

    template <class SeqanString>
    void readSeqanString(std::ifstream& file, SeqanString& str) {
        size_t length;
        read(file, length);
        size_t size;
        read(file, size);
        seqan::resize(str, length);
        file.read(reinterpret_cast<char*>(&str.data_host[0]), size * sizeof(str.data_host[0]));
    }

    void writeVariant(std::ofstream& file, const Variant& var) {
        size_t pos = var.getPosition();
        size_t delLng = var.getDeletionLength();
        write(file, pos);
        write(file, delLng);
        writeSeqanString(file, var.getInsertionString());
    }

    void readVariant(std::ifstream& file, Variant& var) {
        read(file, var.position);
        read(file, var.deletionLength);
        readSeqanString(file, var.insertionString);
    }

    void writeVariantVector(std::ofstream& file, const std::vector<Variant>& v) {
        size_t size = v.size();
        write(file, size);
        for (const Variant& var: v) {
            writeVariant(file, var);
        }
    }

    void readVariantVector(std::ifstream& file, std::vector<Variant> v) {
        size_t size;
        read(file, size);
        v.resize(size);
        for (Variant& var: v) {
            readVariant(file, var);
        }
    }


    void writeChromosome(std::ofstream& file, const Chromosome& chr) {
        writeString(file, chr.id);
        writeString(file, chr.name);
        writeSeqanString(file, chr.data);
        writeVariantVector(file, chr.variants);
        writeVector(file, chr.lowProbabilitySnps);
        writeVector(file, chr.highProbabilitySnps);
        writeSeqanString(file, chr.noVariantsString);
    }

    void readChromosome(std::ifstream& file, Chromosome& chr) {
        readString(file, chr.id);
        readString(file, chr.name);
        readSeqanString(file, chr.data);
        readVariantVector(file, chr.variants);
        readVector(file, chr.lowProbabilitySnps);
        readVector(file, chr.highProbabilitySnps);
        readSeqanString(file, chr.noVariantsString);
    }

    void writeGenome(std::ofstream& file, const ChromosomeReferences& chrRefs) {
        size_t size = chrRefs.size();
        file.write(reinterpret_cast<const char*>(&size), sizeof(size));
        for (const Chromosome& chr: chrRefs) {
            writeChromosome(file, chr);
        }
    }

    void readGenome(std::ifstream& file, Genome& genome) {
        size_t size;
        read(file, size);
        genome.resize(size, Chromosome(""));
        for (Chromosome& chr: genome) {
            readChromosome(file, chr);
        }
    }

    template <class T>
    void writeDeque(std::ofstream& file, const std::deque<T>& d) {
        size_t size = d.size();
        write(file, size);
        for (const T& obj: d) {
            write(file, obj);
        }
    }

    template <class T>
    void readDeque(std::ifstream& file, std::deque<T>& d) {
        size_t size;
        read(file, size);
        d.resize(size);
        for (size_t i = 0; i < size; i++) {
            read(file, d[i]);
        }
    }

    template <class T>
    void writeDequeVector(std::ofstream& file, const std::deque<std::vector<T>>& d) {
        size_t size = d.size();
        write(file, size);
        for (const std::vector<T>& v: d) {
            writeVector(file, v);
        }
    }

    template <class T>
    void readDequeVector(std::ifstream& file, std::deque<std::vector<T>>& d) {
        size_t size;
        read(file, size);
        d.resize(size);
        for (size_t i = 0; i < size; i++) {
            readVector(file, d[i]);
        }
    }

    void writeWindowManager(std::ofstream& file, const MultiWindowManager& mgr) {
        writeDeque(file, mgr.doubleWindowBuckets);
        writeDeque(file, mgr.quadrupleWindowBuckets);
        writeDequeVector(file, mgr.multiWindowBuckets);
        writeVector(file, mgr.freeDoubleWindowBuckets);
        writeVector(file, mgr.freeQuadrupleWindowBuckets);
        writeVector(file, mgr.freeMultiWindowBuckets);
    }

    void readWindowManager(std::ifstream& file, MultiWindowManager& mgr) {
        readDeque(file, mgr.doubleWindowBuckets);
        readDeque(file, mgr.quadrupleWindowBuckets);
        readDequeVector(file, mgr.multiWindowBuckets);
        readVector(file, mgr.freeDoubleWindowBuckets);
        readVector(file, mgr.freeQuadrupleWindowBuckets);
        readVector(file, mgr.freeMultiWindowBuckets);
    }

    void writeSuperrank(std::ofstream& file, const Superrank& sr) {
        write(file, sr.uniqueBandHashCount);
        write(file, sr.blockCount);
        writeVector(file, sr.bandHashBuffer);
        // must not be written: bandHashEntryList
        writeVector(file, sr.blockBitVector);
        writeVector(file, sr.listBlockPositions);
        writeVector(file, sr.bandHashBitVector);
        writeVector(file, sr.listBandHashPositions);
        writeWindowManager(file, sr.windowManager);
    }

    void readSuperrank(std::ifstream& file, Superrank& sr) {
        read(file, sr.uniqueBandHashCount);
        read(file, sr.blockCount);
        readVector(file, sr.bandHashBuffer);
        sr.bandHashEntryList = reinterpret_cast<decltype(sr.bandHashEntryList)>(&sr.bandHashBuffer[0]);
        readVector(file, sr.blockBitVector);
        readVector(file, sr.listBlockPositions);
        readVector(file, sr.bandHashBitVector);
        readVector(file, sr.listBandHashPositions);
        readWindowManager(file, sr.windowManager);
    }


    void writeSuperrankArray(std::ofstream& file, const std::vector<Superrank>& v) {
        size_t size = v.size();
        write(file, size);
        for (const Superrank& sr: v) {
            writeSuperrank(file, sr);
        }
    }

    void readSuperrankArray(std::ifstream& file, std::vector<Superrank>& v) {
        size_t size;
        read(file, size);
        v.resize(size, Superrank(0));
        for (Superrank& sr: v) {
            readSuperrank(file, sr);
        }
    }


public:



    inline
    void write(const LSH& lsh, const std::string& filepath) {
        std::ofstream file(filepath, std::ios::out | std::ios::binary);

        // write parameters
        write(file, lsh.windowSize);
        write(file, lsh.windowOffset);
        write(file, lsh.qGramSize);
        write(file, lsh.signatureLength);
        write(file, lsh.bandSize);
        write(file, lsh.limit);
        write(file, lsh.limit_skip);
        write(file, lsh.wiSumThreshold);
        write(file, lsh.wipLngThreshold);

        // write simple data
        write(file, lsh._mapOptions);
        write(file, lsh.initialRandomSeed);

        // must not be written: rngLimits, rngHashFunctions

        // write hash functions, genome, windows
        writeVector(file, lsh.hashFunctions);
        writeGenome(file, lsh.chromosomes);
        writeVector(file, lsh.referenceWindows);

        // write bandHashTables
        writeSuperrankArray(file, lsh.bandHashTables);

        // must not be written: bandHashTableMutex

        // write gap pattern
        writeVector(file, lsh.qGramHashObject.gapVector);

    }

    inline
    void read(const std::string& filepath, LSH& lsh, Genome& genome) {
        std::ifstream file(filepath, std::ios::in | std::ios::binary);
        lsh.clear();
        genome.clear();

        // read parameters
        read(file, lsh.windowSize);
        read(file, lsh.windowOffset);
        read(file, lsh.qGramSize);
        read(file, lsh.signatureLength);
        read(file, lsh.bandSize);
        read(file, lsh.limit);
        read(file, lsh.limit_skip);
        read(file, lsh.wiSumThreshold);
        read(file, lsh.wipLngThreshold);

        // read simple data
        read(file, lsh._mapOptions);
        read(file, lsh.initialRandomSeed);

        // must not be read: rngLimits
        // restore rngHashFunctions
        lsh.rngHashFunctions.seed(lsh.initialRandomSeed);

        // read hash functions, genome, windows
        readVector(file, lsh.hashFunctions);
        readGenome(file, genome);
        for (Chromosome& chr: genome) {
            lsh.chromosomes.push_back(chr);
        }
        readVector(file, lsh.referenceWindows);

        // read bandHashTables
        readSuperrankArray(file, lsh.bandHashTables);

        // create mutexes
        lsh.bandHashTableMutex = std::vector<Mutex>(lsh.bandHashTables.size());

        // read gap pattern
        readVector(file, lsh.qGramHashObject.gapVector);
    }



};



#endif // LSHIO_H
