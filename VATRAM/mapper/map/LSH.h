#ifndef PG583_MAP_LSH_H
#define PG583_MAP_LSH_H

#include "map/CollisionFreeBHT.h"
#include "map/SuperRank.h"
#include "map/HashDataStructure.h"
#include "map/QGramHashFunctions.h"
#include "Chromosome.h"
#include "parallel.h"
#include "Logger.h"
#include "types.h"
#include "Read.h"

#include <seqan/modifier.h>

#include <vector>
#include <random>
#include <ctime>
#include <cstdint>
#include <cmath>
#include <numeric>
#include <limits>
#include <functional>
#include <type_traits>
#include <utility>
#include <cstddef>
#include <unordered_map>

constexpr int LSH_PE_FOR = 0x1;
constexpr int LSH_PE_PLUS = 0x2;
constexpr int LSH_PE_PLUS_D2 = 0x4;
constexpr int LSH_PE_MEAN = 0x8;
constexpr int LSH_PE_MINCOUNT_ZERO = 0x10;
constexpr int LSH_PE_MAXSELECT_MAX = 0x20;
constexpr int LSH_PE_NEXTFACTOR_MAX = 0x40;
constexpr int LSH_PE_OUT = 0x80;
constexpr int LSH_KTRS_1 = 0x100;
constexpr int LSH_KTRS_2 = 0x200;
constexpr int LSH_SPLIT_EQUAL = 0x400;
extern int global_LSH;

using std::size_t;





struct LshMapOptions {
    const static size_t DEFAULT_MIN_COUNT_SELECTION = 2;
    const static size_t DEFAULT_MIN_COUNT_PREFILTERING = 2;
    const static size_t DEFAULT_MAX_SELECT = 64;
    constexpr static double DEFAULT_NEXT_FACTOR = 4;
    constexpr static double DEFAULT_CONTRACT_BAND_MULTIPLICATOR = 0.3;
    constexpr static double DEFAULT_EXTEND_BAND_MULTIPLICATOR = 0.3;
    constexpr static double DEFAULT_EXTEND_FACTOR = 0.43;
    const static size_t DEFAULT_MAXIMUM_OF_RETURNED_WINDOWS = 1000;
    const static size_t DEFAULT_PAIRED_END_DISTANCE_MIN = 100;
    const static size_t DEFAULT_PAIRED_END_DISTANCE_MAX = 700;
    const static size_t DEFAULT_LONG_READ_SPLIT_LENGTH = 100;
    constexpr static double DEFAULT_LONG_READ_SPLIT_FACTOR = 0.8;

    size_t minCountSelection = DEFAULT_MIN_COUNT_SELECTION; // findInterval() returns only intervals with hits in at least minCound band hash tables for one read.
    size_t minCountPrefiltering = DEFAULT_MIN_COUNT_PREFILTERING; // minCount prefiltering value used for paired end and long reads
    size_t maxSelect       = DEFAULT_MAX_SELECT; // findInterval() returns at most maxSelect different intervals for one read
    double nextFactor      = DEFAULT_NEXT_FACTOR; // in findInteral() the number of hits for the second, third (and so one) window sequence must not be smaller than the first window sequence by a factor of nextFactor
    double contractBandMultiplicator = DEFAULT_CONTRACT_BAND_MULTIPLICATOR; // for the interval calculation the number of band hash hits is multiplied with this attribute
    double extendBandMultiplicator   = DEFAULT_EXTEND_BAND_MULTIPLICATOR;
    double extendFactor              = DEFAULT_EXTEND_FACTOR; // a single window is extended by this number of bases in findInterval()
    /**
     * @brief Maximum number of window indices returned by HashDataStructure::find()
     * @details  If there are more window indices stored for a specific band hash value than the value of this variable,
     *      then no window indices are returned.
     */
    size_t maximumOfReturnedWindows  = DEFAULT_MAXIMUM_OF_RETURNED_WINDOWS;

    size_t pairedEndDistanceMin = DEFAULT_PAIRED_END_DISTANCE_MIN;
    size_t pairedEndDistanceMax = DEFAULT_PAIRED_END_DISTANCE_MAX;

    size_t longReadSplitLength = DEFAULT_LONG_READ_SPLIT_LENGTH;


    double longReadSplitFactor = DEFAULT_LONG_READ_SPLIT_FACTOR;
};



/** If this macro is defined, all methods of the LSH class are virtual */
//#define LSH_MAKE_VIRTUAL

#ifdef LSH_MAKE_VIRTUAL
#define LSH_VIRTUAL virtual
#else
#define LSH_VIRTUAL
#endif

/**
 * @brief Generic LSH class
 * @details If the macro LSH_MAKE_VIRTUAL is defined, several methods are declared as virtual.
 * @param TBandHash BandHash-Type, usually a 32bit (or 64 bit) unsigned integer
 * @param TQGramHash QGramHash-Type, usually a 32bit (or 64 bit) unsigned integer
 * @param BHT band hash table, for example the CollisionFreeBHT or the BandHashTable. The class has to implement
 *      add(BandHash_, WindowIndex), find(BandHash_, std::vector<WindowIndex>&) and a constructor taking the intial size
 * @param QGramHashFunction, a class template that overloads the operator() taking two iterator objects (called begin and end)
 *      pointing to a part of a Dna- or Dna5-String. The function calculates a hash value for the q-gram
 *      between begin (inclusive) and end (exclusive). See DefaultQGramHashFunction for an example
 */
template <class TBandHash,
          class TQGramHash,
          class TBHT,
          class QGramHashFunction>
class LSH_Generic {
public:
    typedef LSH_Generic<TBandHash, TQGramHash, TBHT, QGramHashFunction> LSH_This;

    typedef TQGramHash HashFunction;  // sollte kleiner gleich decltype(rngHashFunctions()) sein
    typedef TQGramHash QGramHash;     // sollte gleich HashFunction sein
    typedef TQGramHash SignatureHash; // sollte gleich QGramHash und HashFunction sein
    typedef TBandHash BandHash;
    typedef TBHT BHT;

    /**
     * @brief Represents a window of the genome
     * @details This struct stores a pointer to the respective chromosome and the begin position of the window (aka document)
     */
    struct ReferenceWindow {
        ChromosomeIndex chromosomeIndex;
        Position beginPosition;
        ReferenceWindow() {}
        ReferenceWindow(ChromosomeIndex chromosomeIndex, Position beginPosition) :
            chromosomeIndex(chromosomeIndex),
            beginPosition(beginPosition) {
        }
    };





    const static size_t DEFAULT_WINDOW_SIZE = 140;
    const static size_t DEFAULT_WINDOW_OFFSET = 125;
    const static size_t DEFAULT_QGRAM_SIZE = 17;
    const static size_t DEFAULT_SIGNATURE_LENGTH = 36; // TODOJQ: Sig-Lng = 50, falls Speicher ausreichend
    const static size_t DEFAULT_BAND_SIZE = 1;
    const static size_t DEFAULT_LIMIT = 3;
    const static size_t DEFAULT_LIMIT_SKIP = 3;
    const static size_t DEFAULT_READ_LENGTH = 100;

    size_t windowSize      = DEFAULT_WINDOW_SIZE; /// the length of a window
    size_t windowOffset    = DEFAULT_WINDOW_OFFSET; /// the offset between two windows (two consecutive windows have "windowSize - windowOffset" common bases)
    size_t qGramSize       = DEFAULT_QGRAM_SIZE; /// the length of a q-gram (inclusive gaps!)
    size_t signatureLength = DEFAULT_SIGNATURE_LENGTH; /// the number of hash functions
    size_t bandSize        = DEFAULT_BAND_SIZE; /// denotes how many signatures are put together to one band
    size_t limit           = DEFAULT_LIMIT; /// The number of combinations that is at most added
    size_t limit_skip      = DEFAULT_LIMIT_SKIP; /// If the number of combinations in a q-gram is bigger than this value, no combination is added

    size_t wiSumThreshold = 500; /// used in findReadOccurences: window-index-sum-threshold (determines if std::sort or merge-sort is used, only important for runtime)
    size_t wipLngThreshold = 10; /// used in findReadOccurences: window-index-pointerpair-length-threashold (determines if we use copy+sort or merge-sort, only important for runtime)

    // TODO: This member is actually not used, apart from in the test / experiment classes.
    // The sound method to resolve this is to split LSH into LshIndex and LshMap / LshMapper or the like (which should
    // hold the find methods). LshMapOptions could then reside in the LshMap* class.
    LshMapOptions _mapOptions;

    // This random generator is used to select some q-grams if there are more qgrams than "limit".
    // The generator should be fast, so don't use std::mt19937
    std::minstd_rand::result_type initialRandomSeed; /// initial random seed used for rngLimits and rngHashFunctions
    std::vector<std::minstd_rand> rngLimits; /// random number generator to select a random subset of the q-gram combinations (one generator for each thread)

    std::mt19937_64 rngHashFunctions;

    std::vector<HashFunction> hashFunctions; /// hash functions
    ChromosomeReferences chromosomes; /// references to the chromosomes
    std::vector<ReferenceWindow> referenceWindows; /// a vector to convert the window indices to a chromosome and a begin position
    
    // Our New custom data structure
    std::vector<BHT> bandHashTables; /// data structures to store the band-hash window-index pairs (one table for each band)
    std::vector<Mutex> bandHashTableMutex; /// one mutex for each band hash table
    QGramHashFunction qGramHashObject; /// a instance of a q-gram hash function
    

    /**
     * @brief Default constructor. Uses the current time as seed.
     */
    LSH_Generic(QGramHashFunction qghf = QGramHashFunction());
    LSH_Generic(unsigned long long initialRandomSeed, QGramHashFunction qghf = QGramHashFunction());



    /**
     * @brief initialize the hash functions needed by LSH and sets
     * @param windowCount number of windows which will be added (may be estimated, preferably exact)
     */
    void initialize(size_t windowCount);


    /**
     * @brief Post processing after adding all chromosomes
     * @details This method should be called after the reference genome is added.
     */
    void buildIndex();


    /**
     * @brief Clears all data structures such that the state of the lsh object is the same as before calling initialize.
     */
    void clear();


    /**
     * @brief Adds all chromosomes between begin_it and end_it.
     */
    template <class Iterator>
    void addGenome(Iterator begin_it, Iterator end_it);


    /**
     * @brief Adds a chromosome
     * @details Creates overlapping windows of the chromosome (using the attributes windowSize and windowOffset).
     *      Each window is stored as a Reference object and is added to the hash table via addReference().
     * @param chromosome
     */
    void addChromosome(const Chromosome &chromosome);

    void addReferenceWindows(ChromosomeIndex chromosomeIndex);
    void createReferenceWindowBandHashes(WindowIndex firstWindowIndex, WindowIndex lastWindowIndex);
    void createReferenceWindowBandHashes(WindowIndex chunkStartIndex, WindowIndex chunkEndIndex,
                                         std::vector<std::vector<BandHash>> &bandHashesPerWindow);
    void createWindowBandHashes(ChromosomeIndex chromosomeIndex, Position beginPosition,
                                std::vector<BandHash> &out_bandHashes);

    /**
     * @brief Adds a pair consisting of a band hash value and a window index to the specified band hash table
     * @param bandIndex the index of the band hash function respecively band hash table
     * @param bucketHash the band hash value
     * @param referenceWindowIndex the window index
     */
    void addReferenceWindowHash(size_t bandIndex, BandHash bucketHash, WindowIndex referenceWindowIndex);

    WindowIndex addReferenceWindow(ChromosomeIndex chromosomeIndex, Position beginPosition,
                                   const std::vector<BandHash> &bandHashes);

    /**
     * @brief Finds the read and its reverse complement in the genome.
     * @param out_referenceIDs Positions where the read was found.
     * @param out_referenceIDs_revComplement Positions where the reverse complement of the read was found.     */
    LSH_VIRTUAL void findRead(const ReadString &read, const ReadString &reversedRead,
                              const LshMapOptions &mapOptions,
                              std::vector<WindowIndex> &out_referenceWindowIndices,
                              std::vector<WindowIndex> &out_referenceWindowIndices_revComplement);

    LSH_VIRTUAL void findInterval(PairedRead& read, const LshMapOptions& mapOptions);

    /**
     * @brief Finds the read and its reverse complement in the genome.
     * @param read a reference to the read string
     * @param reversedRead a reference to the reverse complement of the read string
     * @param out_result The intervals where the read was found
     */
    LSH_VIRTUAL void findInterval_singleEnd(const ReadString &read, const ReadString &reversedRead,
                                  const LshMapOptions &mapOptions, std::vector<MapInterval>& out_result);

    LSH_VIRTUAL void findInterval_pairedEnd(const ReadString &read1, const ReadString &read1_reversed,
                                            const ReadString &read2, const ReadString &read2_reversed,
                                            const LshMapOptions &mapOptions,
                                            std::vector<MapInterval>& out_result1,
                                            std::vector<MapInterval>& out_result2);
    //, ChromosomeIndex debugChrIdx = 0, Position debugPos1 = -1, Position debugPos2 = -1);

    LSH_VIRTUAL void findInterval_longRead(const ReadString &read, const ReadString &reversedRead,
                                           const LshMapOptions &mapOptions, std::vector<MapInterval>& out_result);

    /**
     * @brief creates the q-gram hashes of a DnaString or Dna5String
     * @param window_begin
     * @param window_end
     * @param out_hashes After execution of this function, this variable contains the q-grams (respectively their hash values)
     */
    template <class Iterator>
    void createQGramHashes(Iterator window_begin, Iterator window_end, std::vector<QGramHash> &out_hashes);

    /**
     * @brief creates the q-gram hashes of a IupacString
     * @param window_begin
     * @param window_end
     * @param out_hashes After execution of this function, this variable contains the q-grams (respectively their hash values)
     */
    template <class Iterator>
    void createQGramHashes_iupac(Iterator window_begin, Iterator window_end, std::vector<QGramHash> &out_hashes);

    template <class Iterator>
    void createSignatures(Iterator qGramHashes_begin, Iterator qGramHashes_end, std::vector<SignatureHash> &out_signatures);

    template <class Iterator>
    void createBandHashes(Iterator signature_begin, Iterator signature_end, std::vector<BandHash> &out_bandHashes);

    template <class Iterator> // an iterator pointing to seqan::Dna or seqan::Dna5
    QGramHash qGramHashFunction(Iterator qgram_begin, Iterator qgram_end);

    template <class Iterator>
    BandHash bandHashFunction(Iterator signature_begin, Iterator signature_end);

    // This function is virtual because it is overloaded in the sub project "lsh" to do some experiments
    // If you measure runtime, you can remove the "virtual", however don't commit this chance or tell me (Jens), if you commit it.
    LSH_VIRTUAL BandHash bandHashFunction(typename std::vector<SignatureHash>::iterator sig_begin,
                                          typename std::vector<SignatureHash>::iterator sig_end);

    /** Find all variant combinations of the substring "iupacString[beginPos...beginPos+qGramSize]"
     *  and execute the function operation on the founds.
     *  @param curQGram arbitrary space of length qGramSize
     *  @param curVariant arbitrary space of length qGramSize+1
     *  @param operation a object that overloads operator()(Iterator, Iterator) with Iterator = std::vector<seqan::DNA>::iterator
     */
    template <class Iterator, class Function>
    void findCombinationsOfQGram(Iterator iupacQgram_begin, Function &operation);

    /**
     * @brief current memory usage for this data structure
     * @return an approximation for the number of bytes used by this data structure
     */
    LSH_VIRTUAL size_t memoryUsage() const;

    /**
     * @brief Prints some statistics
     */
    void printHashTableStatistics();


//protected:
    /**
     * @brief This window index offset denotes that the reversed complement of the window was found
     */
    const static WindowIndex WI_INVERTED_MASK = 1 << 31;
    const static WindowIndex WI_VALUE_MASK = ~WI_INVERTED_MASK;

    /**
     * @brief A sequence of window indices
     */
    struct WindowSequence {
        typedef double Score;
        WindowIndex begin;
        WindowIndex end;
        unsigned countLeft; /// number of hits for the first window index (begin)
        union {
            unsigned countSndToLast; /// number of hits for the second to last window index (end-1)
            unsigned pairedEndOriginalScore;
        };
        unsigned countRight; /// number of hits for the last window index (end)
        Score countScore; /// score value for the number of hits (maximum over each pair)

        WindowSequence(const std::pair<WindowIndex, unsigned>& pair) :
            WindowSequence(pair.first, pair.second) {
        }

        WindowSequence(WindowIndex first, unsigned count) :
            begin(first),
            end(first),
            countLeft(count),
            countSndToLast(0),
            countRight(count),
            countScore(count) {
        }

        Score getScore() const {
            return countScore;
        }

        void expand(const std::pair<WindowIndex, unsigned>& pair) {
            expand(pair.first, pair.second);
        }

        void expand(WindowIndex next, unsigned count) {
            countSndToLast = countRight;
            countRight = count;
            countScore = std::max<double>(countScore, countRight + countSndToLast);
            end = next;
        }

        int length() const {
            return int(end & WI_VALUE_MASK) - int(begin & WI_VALUE_MASK) + 1;
        }

        bool getInverted() const {
            return begin & WI_INVERTED_MASK;
        }
    };

    Position getWseqBeginPos(const WindowSequence& ws) const;
    Position getWseqEndPos(const WindowSequence& ws) const;
    Position getWseqMidPos(const WindowSequence& ws) const;
    Position getWseqMidPosWeighted(const WindowSequence& ws) const;
    ChromosomeIndex getWseqChrIdx(const WindowSequence& ws) const;
    /**
     * @brief Calculates the band hashes of a read
     * @param read_begin the begin of the read
     * @param read_end the end of the read
     * @param out_bandHashes a vector (will be cleared before) into which the calculated band hashes are added
     */
    template <class Iterator>
    void calcReadBandHashes(Iterator read_begin, Iterator read_end, std::vector<BandHash>& out_bandHashes);

    /**
     * @brief Finds the read and its reverse complement in the genome.
     * @param read a reference to the read string
     * @param reversedRead a reference to the reverse complement of the read string
     * @return a map with the window indices and the corresponding numbers of band hash tables where the read was found
     * @details The reverse complement is represented by a WindowIndex where INVERTED_WI_OFFSET is added. */
    LSH_VIRTUAL std::vector<std::pair<WindowIndex, unsigned>> findReadOccurrences(
            const ReadString &read, const ReadString &reversedRead, size_t minCount, size_t maximumOfReturnedWindows);

    /**
     * @brief Compresses the window indices where a read was found
     * @param occurrences usually the map returned by findReadOccurrences
     * @details Compresses a sequence of window indices to one WindowSequence object
     * @return a vector with window sequences     */
    LSH_VIRTUAL std::vector<WindowSequence> compressOccurrences(const std::vector<std::pair<WindowIndex, unsigned>>& occurrences);


    template <class ScoreObject>
    std::vector<ScoreObject> selectWindows(std::vector<ScoreObject>& windowSeqs, const LshMapOptions &mapOptions);

    LSH_VIRTUAL void createResult(WindowSequence windowSeq, size_t readLength, const LshMapOptions& mapOptions, std::vector<MapInterval>& out_result);

private:
    /**
     * @brief This bit denotes the reversed complement
     * @details If this bit is set in a pointer to a window index (aka WindowIndex*), then the reversed complement was found.
     *      This trick works on 64-bit systems, since usually only 48 bits are used. There will be problems,
     *      if a pointer variable use only 32 bit.
     */
    const static size_t WP_INVERTED_OFFSET = (size_t)(1) << (8*sizeof(WindowIndex*) - 1); // This bit denotes the reversed complement
    const static size_t WP_POINTER_MASK = ~WP_INVERTED_OFFSET; /// Bit mask to extract the pointer

    /**
     * @brief Converts a pointer with meta information into a normal pointer
     * @param pt a pointer with meta information
     * @return a pointer without meta information
     */
    inline
    WindowIndex* wiPt(size_t pt) const {
        return (WindowIndex*) (pt & WP_POINTER_MASK);
    }

};

typedef LSH_Generic<uint32_t, uint64_t, SuperRank<uint32_t>, GappedQGramHashFunction<uint64_t>> LSH;



#endif // PG583_MAP_LSH_H
