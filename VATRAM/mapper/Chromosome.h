#ifndef PG583_CHROMOSOME_H
#define PG583_CHROMOSOME_H

#include "align/Variant.h"
#include "types.h"

#include <memory>
#include <string>
#include <vector>
#include <cstddef>

using std::size_t;

class Chromosome {
public:
    std::string id;
    std::string name;
    ReferenceString data;
    std::vector<Variant> variants;
    std::vector<SnpVariant> highProbabilitySnps;
    std::vector<SnpVariant> lowProbabilitySnps;
    seqan::String<seqan::Dna5, seqan::Packed<>> noVariantsString;

    Chromosome(const Chromosome &) = default;
    Chromosome& operator=(const Chromosome &) = default;
    ~Chromosome() = default;
    Chromosome(Chromosome &&rhs) noexcept;
    Chromosome& operator=(Chromosome &&rhs);

    Chromosome(const std::string &id);
    Chromosome(const std::string &id, const ReferenceString &data);
    Chromosome(const std::string &id, const ReferenceString &data, const std::vector<Variant> &variants);

    void addSNP(size_t pos, ReferenceChar snp);
    void addLowProbabilitySNP(size_t pos, ReferenceChar snp);
    void addInsertion(size_t pos, const ReferenceString &insertionString);
    void addDeletion(size_t posAfterDeletion, size_t deletionLength);
    void addVariant(size_t posAfterDeletion, size_t deletionLength, const ReferenceString &insertionString);

    void insertLowProbabilitySnps();
    void removeLowProbabilitySnps();

    std::string getOriginalName() const;

    /// @brief Extracts chromosome name from FASTA ID.
    /// @details Returns NCBI's GenBank accession ID if @p id does not describe a chromosome but contains such an ID.
    ///          Returns "MT" for the Mitochondrion. Returns an empty string if no name or GenBank ID could be deduced.
    /// @param id FASTA ID.
    static std::string generateName(const std::string &id);
};

typedef std::vector<Chromosome> Genome;
typedef std::vector<std::reference_wrapper<const Chromosome>> ChromosomeReferences;

template <class TIterator>
class DereferenceIterator : public TIterator {
public:
    DereferenceIterator() : TIterator() {}
    DereferenceIterator(const TIterator& rhs) : TIterator(rhs) {}
    DereferenceIterator(TIterator&& rhs) : TIterator(rhs) {}
    auto operator*() -> decltype(*this->TIterator::operator*()) { return *TIterator::operator*(); }
    auto operator*() const -> decltype(*this->TIterator::operator*()) { return *TIterator::operator*(); }
    auto operator->() -> decltype(&**this->TIterator::operator->()) { return &**TIterator::operator->(); }
    auto operator->() const -> decltype(&**this->TIterator::operator->()) { return &**TIterator::operator->(); }
};

template <class TContainer>
class DereferenceIterable {
public:
    TContainer &container;
    DereferenceIterable(TContainer &container) : container(container) {}
    DereferenceIterator<decltype(container.begin())> begin() { return container.begin(); }
    DereferenceIterator<decltype(container.begin())> begin() const { return container.begin(); }
    DereferenceIterator<decltype(container.end())> end() { return container.end(); }
    DereferenceIterator<decltype(container.end())> end() const { return container.end(); }
};

template <class TContainer>
DereferenceIterable<TContainer> getDereferenceIterators(TContainer &container) {
    return container;
}

#endif // PG583_CHROMOSOME_H
