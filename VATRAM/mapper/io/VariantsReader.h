#ifndef PG583_IO_VARIANTSREADER_H
#define PG583_IO_VARIANTSREADER_H

#include "Chromosome.h"
#include "Logger.h"
#include "parallel.h"

#include <seqan/sequence.h>
#include <seqan/vcf_io.h>

#include <unordered_map>
#include <unordered_set>
#include <string>
#include <vector>
#include <cstddef>
#include <functional>
using std::size_t;

/// @brief Reads short variants from VCF files and applies them to ::Chromosome instances.
/// @details Single nucleotide polymorphisms are added to Chromosome::data via the IUPAC encoding.
/// Short indel and substitution variants get stored in Chromosome::variants.
class VariantsReader {
public:
    typedef std::function<void(Chromosome& chromosome,
                               size_t pos,
                               const seqan::Infix<const seqan::CharString&>::Type& infixRef,
                               const seqan::Infix<seqan::CharString&>::Type& infixAlt,
                               double probability)> AddVariantFunction;

    /// @brief Construct a ReferenceReader associated with an instance of ::Genome and a filter on chromosome names.
    VariantsReader(Genome &genome, const std::unordered_set<std::string> &chromosomeNameFilter, double probabilityThreshold);
    VariantsReader(Genome &genome, const std::unordered_set<std::string> &chromosomeNameFilter);
    /// @brief Construct a ReferenceReader associated with an instance of ::Genome.
    VariantsReader(Genome &genome, double probabilityThreshold = 0.0);

    /// @brief Reads all variants in the given files and adds them to corresponding chromosomes in #genome.
    /// @details The variants' CHROM fields must be equal to the Chromosome::name attributes. Reading of variants is
    /// faster if they are provided in multiple files since the files are being processed in parallel.
    /// @param referenceFiles A list of file paths to (optionally gzipped) VCF files.
    int readVariants(const std::vector<std::string> &variantFiles);

    int readVariants(const std::vector<std::string> &variantFiles, AddVariantFunction function);

    size_t getIndexOfChromosome(Chromosome& chr) const {
        auto it = chromosomeNameToIndex.find(chr.name);
        return it == chromosomeNameToIndex.end() ? -1 : it->second;
    }
    /// Prefix for log messages.
    static constexpr const char *LOG_PREFIX = "VCF: ";
private:
    /// Associated ::Genome instance which holds the chromosomes to which the variants will be added.
    Genome &genome;
    /// Mutexes used to synchronize access on chromosomes inside #genome when reading multiple files.
    std::vector<Mutex> chromosomesMutexes;
    /// Mapping of chromosome names to corresponding indices in #genome.
    std::unordered_map<std::string, size_t> chromosomeNameToIndex;
    /// Set of chromosome names. If not empty then only chromosome with those names will be added to #genome.
    std::unordered_set<std::string> chromosomeNameFilter;

    double probabilityThreshold;

    /// Enum class to filter out unsupported variant types.
    enum class AltType {
        SUPPORTED, MISSING_ALLELE, BREAKEND, STRUCTURAL, UNKNOWN, SAME_AS_REFERENCE, LOWPROBABILITY
    };

    /// Opens file @p variantFile and calls readVariantsFromReader().
    int readVariantsFromReader(const std::string &variantFile, AddVariantFunction function);
    /// Reads instances of seqan::VcfRecord and calls processRecord() on them.
    template<class TReader>
    int readVariantsFromReader(TReader &reader, AddVariantFunction function);
    /// Extracts all ALT strings from a seqan::VcfRecord an processes them via processVariant().
    int processRecord(seqan::VcfRecord &record, const seqan::VcfIOContext &context, AddVariantFunction function);
    /// Adds supported variants to @p chromosome.
    AltType processVariant(Chromosome &chromosome, size_t beginPos, const seqan::CharString &ref,
                           seqan::CharString &alt, double probability, AddVariantFunction function);
    void addVariant(Chromosome& chromosome,
                    size_t pos,
                    const seqan::Infix<const seqan::CharString&>::Type& infixRef,
                    const seqan::Infix<seqan::CharString&>::Type& infixAlt,
                    double probability);

    /// Checks if @p alt represents a supported variant and converts it to upper case.
    AltType checkAndConvertAltToUpper(seqan::CharString &alt);
};

#endif // PG583_IO_VARIANTSREADER_H
