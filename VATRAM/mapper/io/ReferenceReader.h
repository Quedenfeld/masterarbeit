#ifndef PG583_IO_REFERENCEREADER_H
#define PG583_IO_REFERENCEREADER_H

#include "Chromosome.h"
#include "parallel.h"

#include <unordered_map>
#include <unordered_set>
#include <string>
#include <vector>
#include <cstddef>
using std::size_t;

/// @brief Reads reference sequences from FASTA files into ::Chromosome instances.
class ReferenceReader {
public:
    /// @brief Construct a ReferenceReader associated with an instance of ::Genome and a filter on chromosome names.
    /// @param genome ::Genome instance to which the chromosomes will be added.
    /// @param chromosomeNameFilter If not empty then only chromosome with given names will be added to @p genome.
    ReferenceReader(Genome &genome, const std::unordered_set<std::string> &chromosomeNameFilter);

    /// @brief Construct a ReferenceReader associated with an instance of ::Genome.
    /// @param genome ::Genome instance to which the chromosomes will be added.
    ReferenceReader(Genome &genome);

    /// @brief Reads all sequences in the given files and adds them to #genome.
    /// @details If multiple sequences with the same chromosome name are read then only one of them is added to #genome
    /// ensuring chromosome names are unique in one. Reading of references is faster if chromosomes are provided in
    /// multiple files since the files are being processed in parallel.
    /// @param referenceFiles A list of file paths to (optionally gzipped) FASTA files.
    int readReferences(const std::vector<std::string> &referenceFiles);

    size_t getChrIdxByName(const std::string& str) {
        return chromosomeNameToIndex.at(str);
    }

private:
    /// Prefix for log messages.
    static constexpr const char *LOG_PREFIX = "FASTA: ";
    /// Associated ::Genome instance to which chromosomes will be added.
    Genome &genome;
    /// Mutex used to synchronize access on #genome when reading multiple files.
    Mutex genomeMutex;
    /// Mapping of chromosome names to corresponding indices in #genome.
    std::unordered_map<std::string, size_t> chromosomeNameToIndex;
    /// Set of chromosome names. If not empty then only chromosome with those names will be added to #genome.
    std::unordered_set<std::string> chromosomeNameFilter;

    /// @brief Reads all sequences from one file and adds them to #genome.
    /// @param referenceFile Path to the FASTA file.
    int readReferenceFile(const std::string &referenceFile);
};

#endif // PG583_IO_REFERENCEREADER_H
