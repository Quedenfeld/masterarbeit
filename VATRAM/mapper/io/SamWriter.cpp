#include "io/SamWriter.h"
#include "Chromosome.h"

SamWriter::SamWriter(seqan::BamStream &bamStream)
    : bamStream(bamStream) {
}

void SamWriter::writeHeader(const ChromosomeReferences &chromosomes) {
    seqan::BamHeader header;
    // Fill header records.
    resize(header.records, 2 + chromosomes.size());
    // @HD header.
    header.records[0].type = seqan::BAM_HEADER_FIRST;
    resize(header.records[0].tags, 1);
    // @HD header, tag/value: VN:1.4.
    header.records[0].tags[0].i1 = "VN";
    header.records[0].tags[0].i2 = "1.4";
    // @PG header
    header.records[1].type = seqan::BAM_HEADER_PROGRAM;
    resize(header.records[1].tags, 1);
    header.records[1].tags[0].i1 = "ID";
    header.records[1].tags[0].i2 = "PG583";
    // @SQ header.
    resize(header.sequenceInfos, chromosomes.size());
    for (int i = 0, e = chromosomes.size(); i != e; ++i) {
        size_t chromoLen = seqan::length(chromosomes[i].get().data);
        header.sequenceInfos[i].i1 = chromosomes[i].get().getOriginalName();  // add the removed prefix "chr" again
        header.sequenceInfos[i].i2 = chromoLen;

        auto &r = header.records[i+2];
        resize(r.tags, 2);
        r.type = seqan::BAM_HEADER_REFERENCE;
        r.tags[0].i1 = "SN";
        r.tags[0].i2 = chromosomes[i].get().getOriginalName(); // add the removed prefix "chr" again

        r.tags[1].i1 = "LN";
        r.tags[1].i2 = std::to_string(chromoLen);
    }

    bamStream.header = std::move(header);
}

int SamWriter::writeRead(const seqan::CharString &readName, const ReadString &read, const ReadString &reversedRead,
                          bool isReverseComplement, size_t chromosomeIndex,
                          size_t beginPos, CigarString &cigar) {
    seqan::BamAlignmentRecord record;
    setAttributes(record, isReverseComplement, chromosomeIndex, beginPos, cigar);
    return writeRead(readName, read, reversedRead, record);
}

int SamWriter::writeRead(const seqan::CharString &readName, const ReadString &read, const ReadString &reversedRead,
                         seqan::BamAlignmentRecord &record) {
    record.qName = readName;

    // TOOD: more flags?

    record.mapQ = 255;

    if (seqan::empty(record.cigar)) {
        // Unmapped read.
        record.flag |= seqan::BAM_FLAG_UNMAPPED;
        record.rID = seqan::BamAlignmentRecord::INVALID_REFID;
        record.beginPos = 0;
    }

    // Write read data.
    record.seq = seqan::hasFlagRC(record) ? reversedRead : read;

    if (seqan::length(record.qual) != seqan::length(record.seq)) {
        seqan::clear(record.qual);
    }
    return seqan::writeRecord(bamStream, record);
}

void SamWriter::setAttributes(seqan::BamAlignmentRecord &record, bool isReverseComplement, size_t chromosomeIndex,
                              size_t beginPos, CigarString &cigar) {
    record.flag = 0;
    if (isReverseComplement) {
        record.flag |= seqan::BAM_FLAG_RC;
    }
    record.beginPos = beginPos;
    record.rID = chromosomeIndex;
    seqan::swap(record.cigar, cigar);
}
