#ifndef PG583_PARALLEL_H
#define PG583_PARALLEL_H

#include <seqan/parallel.h>

// provides std::mutex and std::lock_guard like interfaces for OpenMP's locks
namespace omp_std {

#ifdef _OPENMP
class mutex {
public:
    typedef omp_lock_t native_handle_type;
    mutex(const mutex&) = delete;
    mutex() { omp_init_lock(&omp_lock); }
    ~mutex() { omp_destroy_lock(&omp_lock); }
    void lock() { omp_set_lock(&omp_lock); }
    void unlock() { omp_unset_lock(&omp_lock); }
    bool try_lock() { return omp_test_lock(&omp_lock); }
    native_handle_type native_handle() { return omp_lock; }
private:
    native_handle_type omp_lock;
};
#else
class mutex {
public:
    typedef mutex native_handle_type;
    mutex(const mutex&) = delete;
    mutex() {}
    ~mutex() {}
    void lock() {}
    void unlock() {}
};
#endif

template <class TMutex>
class lock_guard {
public:
    explicit lock_guard(TMutex& m) : m(m) { m.lock(); }
    ~lock_guard() { m.unlock(); }
    void operator=(const lock_guard&) = delete;
    lock_guard(const lock_guard&) = delete;
private:
    TMutex& m;
};

} // namespace omp_std

using Mutex = omp_std::mutex;
using LockGuard = omp_std::lock_guard<Mutex>;

#endif // PG583_PARALLEL_H
