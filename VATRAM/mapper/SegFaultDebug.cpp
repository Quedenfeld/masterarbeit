#include "SegFaultDebug.h"
#include "map/LSH.h"

#include <iostream>

DebugInfos debugInfos;
int global_LSH = LSH_PE_FOR | LSH_PE_MEAN | LSH_SPLIT_EQUAL;

void setSegFaultAction() {
#ifdef __linux__
    struct sigaction sa;
    memset(&sa, 0, sizeof(struct sigaction));
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = segfault_sigaction;
    sa.sa_flags   = SA_SIGINFO;
    sigaction(SIGSEGV, &sa, NULL);
#endif
}

#ifdef __linux__
void segfault_sigaction(int signal, siginfo_t* si, void*) {
    std::cerr << "Signal: " << signal << "\n";
    std::cerr << "Adress: " << si->si_addr << "\n";
//    printDebugInfo();
    std::cerr << "Segmentation Fault" << std::endl;
    exit(0);
}
#endif

void printDebugInfo() {
    std::cerr << "in_addChromsome" << ": " << debugInfos.in_addChromsome << "\n";
    std::cerr << "in_addReferenceWindows" << ": " << debugInfos.in_addReferenceWindows << "\n";
    std::cerr << "    " << "chrIdx" << ": " << debugInfos.chrIdx << "\n";
    std::cerr << "    " << "lastWinPos" << ": " << debugInfos.lastWinPos << "\n";
    std::cerr << "    " << "winStartPos" << ": " << debugInfos.winStartPos << "\n";
    std::cerr << "    " << "pos_addReferenceWindows" << ": " << debugInfos.pos_addReferenceWindows << "\n";
    std::cerr << "    " << "first" << ": " << debugInfos.first << "\n";
    std::cerr << "    " << "last" << ": " << debugInfos.last << "\n";
    std::cerr << "in_createReferenceWindowBandHashes" << ": " << debugInfos.in_createReferenceWindowBandHashes << "\n";
    std::cerr << std::flush;
    for (size_t i = 0; i < debugInfos.thread.size(); i++) {
        DebugInfos::ThreadInfo& ti = debugInfos.thread[i];
        if (ti.used) {
            std::cerr << "Thread " << i << "\n";
            std::cerr << "    " << "in_createWindowBandHashes" << ": " << ti.in_createWindowBandHashes << "\n";
            std::cerr << "    " << "in_createQGramHashes_iupac" << ": " << ti.in_createQGramHashes_iupac << "\n";
            std::cerr << "    " << "in_addReferenceWindowHash" << ": " << ti.in_addReferenceWindowHash << "\n";
            std::cerr << "    " << "bht_num" << ": " << ti.bht_num << "\n";
            std::cerr << "    " << "pos_bht_add" << ": " << ti.pos_bht_add << "\n";
            std::cerr << std::flush;
        }
    }
}

