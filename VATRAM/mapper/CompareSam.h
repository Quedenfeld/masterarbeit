#ifndef COMPARESAM_H
#define COMPARESAM_H

#include "types.h"

#include <seqan/bam_io.h>

class CompareSam
{
public:
    std::string perfectSamPath;
    std::string compareSamPath;

    size_t unmappedThreshold;

    std::vector<size_t> editDistances;
    std::vector<size_t> hammingDistances;

    size_t totalNum = 0;
    size_t numUnmapped = 0;  // False negative
    size_t numCorrectMapped = 0; // True Positives
    size_t numCorrectMappedThreshold = 0;
    size_t numIncorrectMapped = 0; // False Positive
    size_t numIncorrectByGoldStandard = 0; // True Negative

    struct Row {
        std::string qName;
        bool unmapped;
        size_t flag;
        size_t pos;
        size_t length;
        seqan::CigarElement<> cigar;
        seqan::CharString sequence;
        seqan::BamAlignmentRecord alignmentCopy;
        size_t edit_distance;
        size_t hamming_distance;
    };

    CompareSam();
    CompareSam(std::string perfectSamPath, std::string compareSamPath);

    void startComparison();

    size_t calculateEditDistance(seqan::CharString seq1, seqan::CharString seq2);
    size_t calculateHammingDistance(seqan::CharString seq1, seqan::CharString seq2);

    double calculateAccuracy();

    double calculateSensitivity();
    double calculateSpecitivity();

    double calculatePrecision();
    double calculateRecall();

    double calculateFalseDiscoveryRate();
    double calculateFalseOmissionRate();
    double calculateNegativePredictionValue();


    double calculateF1();
    double calculateAverageDistance(std::vector<size_t> distances);


    void printStats();
};

#endif // COMPARESAM_H
