#ifndef READGENERATOR_H
#define READGENERATOR_H

#include "Chromosome.h"
#include "GenomeGenerator.h"
#include "io/VariantsReader.h"
#include "io/SamWriter.h"
#include "VcfVariant.h"

class ReadGenerator {
public:
    typedef GenomeGenerator::Read Read;
//    typedef std::pair<Read, Read> PairedEndRead;
//    using GenomeGenerator::Read;

    constexpr static double MAX_N_RATIO = 0.1;

    ReadGenerator(const Genome& genome, uint64_t seed) :
        ReadGenerator(genome, std::vector<std::string>(), seed) {
    }

    ReadGenerator(const Genome& genome, const std::vector<std::string>& varPaths, uint64_t seed) :
        genome(genome),
        varPaths(varPaths),
        rnd(seed),
        u01(0, 1) {
//        std::cout << "ReadGenerator()" << std::endl;

        size_t sum = 0;
        for (const Chromosome& chr: genome) {
            sum += seqan::length(chr.data);
            cumulativeChrLng.push_back(sum);
        }
        chrLngDistr = std::uniform_int_distribution<size_t>(0, sum-1);

        for (size_t i = 0; i < genome.size(); ++i) {
            chromosomeNameToIndex[genome[i].name] = i;
        }
    }

    void readVariants() {
        Mutex mutex;
//        std::cout << "readVariants()" << std::endl;
        VariantsReader vr(const_cast<Genome&>(genome));
        vr.readVariants(varPaths, [this, &vr, &mutex](Chromosome& chromosome,
                                                      size_t pos,
                                                      const seqan::Infix<const seqan::CharString&>::Type& infixRef,
                                                      const seqan::Infix<seqan::CharString&>::Type& infixAlt,
                                                      double probability) {
            LockGuard lock(mutex); // needed because of multi threading in VariantsReader
            variants.emplace_back(vr.getIndexOfChromosome(chromosome), pos, probability, seqan::length(infixRef), infixAlt);
        });
        std::sort(variants.begin(), variants.end());
    }

    const std::vector<VcfVariant>& getVariants() const {
        return variants;
    }

    void setVariants(std::vector<VcfVariant> newVariants) {
        std::swap(variants, newVariants);
        std::sort(variants.begin(), variants.end());
    }

    void setVariants_destroyParameter(std::vector<VcfVariant>& newVariants) {
        std::swap(variants, newVariants);
        std::sort(variants.begin(), variants.end());
    }

    ChromosomeIndex sampleChromosome() {
        size_t random = chrLngDistr(rnd);
        for (size_t i = 0; i < genome.size(); i++) {
            if (cumulativeChrLng[i] > random) {
                return i;
            }
        }
        return genome.size() - 1;
    }

    std::vector<VcfVariant>::iterator getNextVariant(ChromosomeIndex chrIdx, Position pos) {
        VcfVariant v(chrIdx, pos);
        return std::lower_bound(variants.begin(), variants.end(), v);
    }

    seqan::Dna randomNucleotid() {
        return rnd() & 0x3;
    }

    template <class TString>
    void takeChar(ReadString& read, const TString& ref, Position& offset, size_t& errorCount, double snpProbability, double indelProbability, double indelEnlargeProbability) {
//        std::cout << "    takeChar()" << std::endl;
        if (u01(rnd) <= snpProbability) {
            // snp
            read += randomNucleotid();
            offset++;
            errorCount++;
        } else if (u01(rnd) <= indelProbability) {
            if (rnd() & 1) {
                // insertion
                do {
                    read += randomNucleotid();
                    errorCount++;
                } while (u01(rnd) <= indelEnlargeProbability);
            } else {
                // deletion
                do {
                    offset++;
                    errorCount++;
                } while (u01(rnd) <= indelEnlargeProbability);
            }
        } else if (offset >= seqan::length(ref)) {
            read += randomNucleotid();
            errorCount++;
        } else {
            read += ref[offset++];
        }
    }

    Read createString(ChromosomeIndex chrIdx, Position pos, uint32_t readLength, double snpProbability, double indelProbability, double indelEnlargeProbability) {
//        std::cout << "createString()" << std::endl;
        std::vector<VcfVariant>::iterator varIt = getNextVariant(chrIdx, pos);

        Position curPos = pos;

        Read read;
        read.chromosome = &genome[chrIdx];
        read.startPos = pos;

        while (seqan::length(read.readString) < readLength) {
            // find next variant
            while (varIt != variants.end() && varIt->position < curPos) {
                ++varIt;
            }
            if (varIt != variants.end() && varIt->position == curPos && varIt->probability >= u01(rnd)) {
                // use variant
//                std::cout << "  use variant" << std::endl;
                curPos += varIt->refLength;
                Position offset = 0;
                while (offset < seqan::length(varIt->altString)) {
                    takeChar(read.readString, varIt->altString, offset, read.errorCount, snpProbability, indelProbability, indelEnlargeProbability);
                }
            } else {
                // use ref
//                std::cout << "  use ref" << std::endl;
                takeChar(read.readString, genome[chrIdx].noVariantsString, curPos, read.errorCount, snpProbability, indelProbability, indelEnlargeProbability);
            }
        }

        if (seqan::length(read.readString) > readLength) {
//            std::cout << "  cut" << std::endl;
            read.readString = seqan::infix(read.readString, 0, readLength);
        }

        read.reversedComplement = false;
        return read;
    }

    void reverseReadMaybe(Read& read) {
        if (rnd() & 0x1) {
            read.reversedComplement = !read.reversedComplement;
            seqan::reverseComplement(read.readString);

            if (read.next) {
                Read& snd = *read.next;
                snd.reversedComplement = snd.reversedComplement;
                seqan::reverseComplement(snd.readString);

                std::swap(read.readString, snd.readString);
                std::swap(read.startPos, snd.startPos);
                std::swap(read.chromosome, snd.chromosome);
                std::swap(read.reversedComplement, snd.reversedComplement);
                std::swap(read.errorCount, snd.errorCount);
            }
        }
    }

    void samplePosition(uint32_t readLength, ChromosomeIndex& out_chrIdx, Position& out_pos) {
        bool manyNs;
        do {
            out_chrIdx = sampleChromosome();
            std::uniform_int_distribution<Position> posDistr(0, seqan::length(genome[out_chrIdx].data) - readLength);
            out_pos = posDistr(rnd);
            size_t numberNs = 0;
            for (size_t i = 0; i < readLength; i++) {
                numberNs += genome[out_chrIdx].noVariantsString[out_pos+i] == 'N';
            }
            manyNs = numberNs / double(readLength) > MAX_N_RATIO;
        } while(manyNs);
    }

    Read createRead(uint32_t readLength, double snpProbability, double indelProbability, double indelEnlargeProbability) {
//        std::cout << "createRead()" << std::endl;
        ChromosomeIndex chrIdx;
        Position pos;
        samplePosition(readLength, chrIdx, pos);
        Read read = createString(chrIdx, pos, readLength, snpProbability, indelProbability, indelEnlargeProbability);
        reverseReadMaybe(read);
        return read;
    }

    Read createRead_pairedEnd(uint32_t readLength, uint32_t gapLength, double snpProbability, double indelProbability, double indelEnlargeProbability) {
        ChromosomeIndex chrIdx;
        Position pos;
        samplePosition(readLength*2 + gapLength, chrIdx, pos);

        Read read = createString(chrIdx, pos, readLength, snpProbability, indelProbability, indelEnlargeProbability);
        read.next.reset(new Read());
        Read& snd = *read.next;
        snd = createString(chrIdx, pos + readLength + gapLength, readLength, snpProbability, indelProbability, indelEnlargeProbability);

        reverseReadMaybe(read);
        return read;
    }

    std::vector<Read> createReads(size_t count, uint32_t readLength, double snpProbability, double indelProbability, double indelEnlargeProbability) {
        std::vector<Read> reads;
        for (size_t i = 0; i < count; i++) {
            reads.push_back(createRead(readLength, snpProbability, indelProbability, indelEnlargeProbability));
        }
        return reads;
    }

    template <class Distribution>
    std::vector<Read> createReads_differentLength(size_t count, Distribution readLength, double snpProbability, double indelProbability, double indelEnlargeProbability) {
        std::vector<Read> reads;
        for (size_t i = 0; i < count; i++) {
            reads.push_back(createRead(readLength(), snpProbability, indelProbability, indelEnlargeProbability));
        }
        return reads;
    }

    std::vector<Read> createReads_position(size_t count, uint32_t readLength, ChromosomeIndex chrIdx, Position posMin, Position posMax, double snpProbability, double indelProbability, double indelEnlargeProbability) {
        std::uniform_int_distribution<Position> distr(posMin, posMax - readLength);
        std::vector<Read> reads;
        for (size_t i = 0; i < count; i++) {
            reads.push_back(createString(chrIdx, distr(rnd), readLength, snpProbability, indelProbability, indelEnlargeProbability));
        }
        return reads;
    }

    struct Interval {
        ChromosomeIndex chrIdx;
        Position begin;
        Position length;
        Interval(ChromosomeIndex chrIdx, Position begin, Position length) : chrIdx(chrIdx), begin(begin), length(length) {}
    };

    std::vector<Read> createReads_position(size_t count, uint32_t readLength, const std::vector<Interval>& intervals, double snpProbability, double indelProbability, double indelEnlargeProbability) {
        size_t sumIntervalLength = 0;
        for (const Interval& ival: intervals) {
            sumIntervalLength += ival.length;
        }
        std::vector<Read> reads;
        for (const Interval& ival: intervals) {
            size_t iCount = std::round(count * (double) ival.length / (double) sumIntervalLength);
            auto set = createReads_position(iCount, readLength, ival.chrIdx, ival.begin, ival.begin + ival.length, snpProbability, indelProbability, indelEnlargeProbability);
            for (const Read& r: set) {
                reads.push_back(r);
            }
        }
        return reads;
    }

    template <class Distribution>
    std::vector<Read> createReads_pairedEnd(size_t count, uint32_t readLength, Distribution pairedDistance, double snpProbability, double indelProbability, double indelEnlargeProbability) {
        std::vector<Read> reads;
        for (size_t i = 0; i < count; i++) {
            uint32_t dist = pairedDistance();
            reads.push_back(createRead_pairedEnd(readLength, dist, snpProbability, indelProbability, indelEnlargeProbability));
//            Read raw = createRead(2*readLength + dist, snpProbability, indelProbability, indelEnlargeProbability);

//            Read fst;
//            fst.next.reset(new Read());
//            Read& snd = *fst.next;

//            fst.readString = seqan::infix(raw.readString, 0, readLength);
//            snd.readString = seqan::infix(raw.readString, readLength + dist, 2*readLength + dist);
//            fst.chromosome = snd.chromosome = raw.chromosome;
//            fst.reversedComplement = snd.reversedComplement = raw.reversedComplement;
//            fst.startPos = raw.startPos;
//            snd.startPos = raw.startPos + readLength + dist;
//            if (raw.reversedComplement) {
//                std::swap(fst.startPos, snd.startPos);
//            }
//            fst.errorCount = snd.errorCount = raw.errorCount; // TODOJQ: PE: Create: errorCount: bessere Lösung?
        }
        return reads;
    }

    SamWriter createSamWriter(seqan::BamStream& bamstream) {
        ChromosomeReferences cr;
        for (const Chromosome& chr: genome) {
            cr.push_back(chr);
        }
        SamWriter sw(bamstream);
        sw.writeHeader(cr);
        return sw;
    }

    void writeRead(SamWriter& sw, const std::string& readID, const Read& read) {
        ReadString revComplStr = read.readString;
        seqan::reverseComplement(revComplStr);

        CigarString cs;
        seqan::CigarElement<> manyElements('M', seqan::length(read.readString));
        seqan::append(cs, manyElements);

        sw.writeRead(readID, read.readString, revComplStr, read.reversedComplement, chromosomeNameToIndex[read.chromosome->name], read.startPos, cs);
    }

    void writeReads(const std::vector<Read>& reads, const std::string& samPath) {
        seqan::BamStream bamstream(samPath.c_str(), seqan::BamStream::WRITE);

        SamWriter sw = createSamWriter(bamstream);


        size_t idCount = 0;
        for (const Read& read: reads) {
            std::string readID = std::string("read") + std::to_string(idCount++);

            // TODOJQ: PE: Create (SAM): support paired end reads! (and more...)
            for (const Read* r = &read; r != NULL; r = r->next.get()) {
                writeRead(sw, readID, *r);
            }
        }
    }

    size_t getChromsomeIndex(const std::string& name) {
        auto it = chromosomeNameToIndex.find(name);
        return it == chromosomeNameToIndex.end() ? -1 : it->second;
    }

private:
    const Genome& genome;
    std::vector<size_t> cumulativeChrLng;
    std::unordered_map<std::string, size_t> chromosomeNameToIndex;
    const std::vector<std::string>& varPaths;
    std::vector<VcfVariant> variants;
    std::mt19937 rnd;
    std::uniform_int_distribution<size_t> chrLngDistr;
    std::uniform_real_distribution<double> u01;
};


#endif // READGENERATOR_H
