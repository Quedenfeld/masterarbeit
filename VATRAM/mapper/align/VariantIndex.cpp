#include "align/VariantIndex.h"
#include "align/Variant.h"

#include <algorithm>

RankVariantIndex::RankVariantIndex(const std::vector<Variant> &variantList, size_t highestPosition) :
    variantList(variantList) {
    // initialize private members
    // one block for each BLOCK_SIZE positions in reference
    size_t blockCount = (highestPosition >> seqan::Log2<BLOCK_SIZE>::VALUE) + 1;
    existsVariant = std::vector<BlockType>(blockCount, 0);
    listPositions = std::vector<size_t>(blockCount);
    listOffset = std::vector<std::vector<size_t>>(blockCount, std::vector<size_t>(0));

    // iterate over variant list
    size_t oldBlock = 0;
    size_t oldPosition = 0;
    size_t oldOffset = 0;
    size_t variantsInCurrentBlock = 0;

    for (size_t i = 0; i < variantList.size(); i++) {
        size_t currentPosition = variantList[i].getPosition();
        assert(currentPosition >= oldPosition);
        size_t currentBlock = currentPosition >> seqan::Log2<BLOCK_SIZE>::VALUE;
        size_t currentOffset = currentPosition & (BLOCK_SIZE - 1);
        if (currentBlock > oldBlock) {
            // shift bitvector for remaining number of positions in this block
            existsVariant[oldBlock] <<= (BLOCK_SIZE - 1) - oldOffset;
            oldOffset = 0;
            // new block entered: reset variant counter for current block and set start indes
            listPositions[currentBlock] = i;
            variantsInCurrentBlock = 0;
            oldBlock = currentBlock;
        }

        if (currentPosition > oldPosition) {
            // shift bitvector by difference of old and new offset
            existsVariant[currentBlock] <<= (currentOffset - oldOffset);
            // mark current offset for existing variant
            existsVariant[currentBlock]++;
            oldOffset = currentOffset;
            // position changed: add a new entry into offset list
            listOffset[currentBlock].push_back(variantsInCurrentBlock);
            oldPosition = currentPosition;
        }
        variantsInCurrentBlock++;

    }
    // bitvector of last block has to be shifted for missing number of positions in it
    existsVariant[oldBlock] <<= (BLOCK_SIZE - 1) - oldOffset;
}

std::vector<Variant>::const_iterator RankVariantIndex::getNextVariant(size_t pos, size_t lastPos) const {
    size_t block = pos >> seqan::Log2<BLOCK_SIZE>::VALUE;
    size_t offset = pos & (BLOCK_SIZE - 1);
    if (existsVariant[block] & ((~BlockType(0) >> (8 * sizeof(BlockType) - BLOCK_SIZE)) >> offset)) {
        BlockType bitsUpToAndIncludingOffset = existsVariant[block] >> (BLOCK_SIZE - offset - 1);
        return variantList.begin() + listPositions[block]
                + listOffset[block][seqan::popCount(bitsUpToAndIncludingOffset >> 1)];
    }
    while (++block << seqan::Log2<BLOCK_SIZE>::VALUE < lastPos) {
        if (existsVariant[block]) {
            return variantList.begin() + listPositions[block];
        }
    }
    return variantList.end();
}

BinarySearchVariantIndex::BinarySearchVariantIndex(const std::vector<Variant> &variantList, size_t) :
    variantList(variantList) {
}
std::vector<Variant>::const_iterator BinarySearchVariantIndex::getNextVariant(size_t pos, size_t) const {
    return std::lower_bound(variantList.begin(), variantList.end(), Variant::none,
                            [pos](const Variant &v, const Variant&) { return v.getPosition() < pos; });
}
