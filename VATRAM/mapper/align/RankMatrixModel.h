#ifndef PG583_ALIGN_HASHMATRIXMODEL_H
#define PG583_ALIGN_HASHMATRIXMODEL_H

#include "align/MatrixModel.h"

#include <vector>
#include <cstddef>
#include <cstdint>
#include <cassert>
#include <functional>
#include <unordered_map>
#include <seqan/basic.h>
#include <seqan/misc/misc_bit_twiddling.h>

class Variant;

const static size_t MATRIX_MODEL_CACHE_WIDTH = 100;

typedef uint64_t Superblock;

/**
 * @class HashMatrixStorage
 * @brief A container for memory allocations in the BacktracingMatix. This is used to allow reusing memory across
 * multiple matrix instances.
 */
class RankMatrixModel {

public:
    RankMatrixModel(size_t h, size_t w);

    std::vector<Superblock> superBlocks;
    std::vector<size_t> superOffset;
    std::vector<size_t> blocks;
    std::vector<size_t> offset;
    std::vector<MatrixModel::Cell> cells;

    void writeCell(ColumnIndex col, RowIndex row, MatrixModel::Cell cell);
    MatrixModel::Cell &readCell(ColumnIndex col, RowIndex row);
    void clear();
    MatrixModel::Cell defaultCell;

    /**
     * @brief cellCache An array for faster read and write access than the hashtable. Has limited amount of columns.
     */
    std::vector<MatrixModel::Cell> cellCache;

    std::vector<MatrixModel::ColumnHeader> headers;

    /**
     * @brief allocated height of this matrix
     */
    size_t height;

    /**
     * @brief the initial number of columns, for which space is allocated
     */
    size_t width;

    /**
     * @brief the column id of the given position (index) in the reference
     * For every column of the matrix we have to store to which index of the reference it belongs. Because of insertion
     * variants the column index is not necessarily equal to the actual index of the reference.
     */
    std::vector<ColumnIndex> columnOfRefInIndex;

    /**
     * @brief resize Enlarges the matrix storage to accomodate height.
     */
    void resizeHeight(size_t newHeight);

    /**
     * @brief resize Enlarges the matrix storage to accomodate width.
     */
    void resizeWidth(size_t newWidth);
};

#endif // PG583_ALIGN_MATRIXMODEL_H
