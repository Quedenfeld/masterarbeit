#ifndef PG583_ALIGN_ALIGNER_H
#define PG583_ALIGN_ALIGNER_H

#include "align/VariantIndex.h"
#include "align/Alignment.h"
#include "Chromosome.h"
#include "types.h"
#include "align/BacktracingMatrix.h"
#include "align/RankBacktracingMatrix.h"

#include <seqan/sequence.h>

#include <vector>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <functional>

using std::size_t;

class BacktracingMatrix;
class HashMatrix;

/**
 * @brief The semiglobal aligner computes alignments between reads and a reference genome.
 *
 * This aligner can be used to align many reads against a reference genome. The aligner is fully aware of genomic
 * variants and will consider all insertion and deletion variants, which lie inside the currently viewed reference part.
 * It can also handle IUPAC-encoded strings as reference string, which can be used to handle SNP variants.
 *
 * The underlying algorithm is based on dynamic programming following the ideas of Ukkonen's alignment algorithm. The
 * worst case runtime is [reference length]*[read length]. Although there are some optimizations, this aligner is
 * supposed to handle only small pieces of the reference genome at once. The mapping between reads and reference parts
 * can be done by another algorithm and only promising candidate pairs should be fed to this aligner.
 *
 * The scoring for the best alignment is the edit distance. If there is more than one best alignment for the read, the
 * aligner will output all of them, unless they have the same end position within the reference genome. The aligner
 * tries to minimize the position of insertions and deletions in the output cigar string. If e.g. 48M3I45M and 50M3I43M
 * are possible outputs, the aligner will choose the first one.
 *
 * The aligner is initialized with a variant index, which cannot be modified afterwards. The maxError argument cannot
 * be changed directly afterwards, but implicitly by calling the alternative align-method. Every alignment run stores
 * temporary information in member variables, so the aligner is not thread save! Do not align multiple reads on the
 * same aligner instance simultaneously!
 */
class Aligner {
private:
    typedef seqan::Infix<const ReferenceString>::Type ReferenceInfix;
    typedef seqan::Infix<const ReadString>::Type ReadInfix;

    std::vector<Variant>::const_iterator variantIndexEnd;
    #ifndef ALIGNER_USE_HASHING
    BacktracingMatrix b;
    #else
    HashBacktracingMatrix b;
    #endif
    ErrorCount maxError;
    ErrorCount maxClip;
    ErrorCount maxCurClipFront;
    ErrorCount maxCurClipBack;

    std::reference_wrapper<const ReadString> pattern;
    size_t patternLength;

    ReferenceInfix text;
    ReferenceInfix extendedText;
    size_t startPos;
    size_t endPos;

    RowIndex highestRow;
    ErrorCount bestMinimum;
    ColumnIndex bestMinimumPosition;

    RowIndex lastRowCurrent;
    RowIndex lastRowNext;
    size_t nextVarPos;

    std::vector<Variant>::const_iterator itVariant;
    std::vector<ColumnIndex> columns;
    std::vector<ColumnIndex> lastColumnOfVariant; // stored end position of each variant

    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    int numberOfHandledInsertions;
    int numberOfHandledDeletions;
    int numberOfComputedFields;
    int numberOfFoundAlignments;
    #endif

    Alignment align(const Chromosome &chromosome, size_t startPos, size_t endPos, const VariantIndex &variantIndex,
                    const ReadString &pattern, bool isInitialRun);

    void alignColumn(size_t refPos, size_t indelPos, const Variant &var, seqan::Iupac t, ColumnIndex previousColumn);

    void alignColumn(size_t refPos, size_t indelPos, const Variant &var, seqan::Iupac t);

    void processVariants(size_t refPos);

    ColumnIndex processInsertion(size_t refPos, const Variant &insertion, ColumnIndex columnBefore);

    ColumnIndex processDeletion(size_t refPos, const Variant &deletion, ColumnIndex columnBefore);

    ColumnIndex processSubstitution(size_t refPos, const Variant &substitution, ColumnIndex columnBefore);

    ColumnIndex alignInsertionString(size_t refPos, const Variant &var, ColumnIndex lastColumnBeforeVariants);

    /**
     * @brief Computes an alignment given a backtracing matrix and an end position.
     * @param position the end position, where the score was optimal
     * @return The alignment, which belongs the found optimum
     */
    Alignment backtrace(ColumnIndex position);

    /**
     * @brief printMatrix Prints the score, recDir, recColumns and nextRow matrices. Do only use for debugging purpose,
     * because this method is extremely slow (n*m runtime!).
     */
    void printMatrix(std::ostream& out = std::cout);

public:
    /**
     * @brief Constructs a new semiglobal aligner.
     * @param maxError the maximum number of allowed errors for alignments
     */
    Aligner(ErrorCount maxError, ErrorCount maxClip);

    /**
     * @brief align Computes all optimal alignments in an interval of the reference genome parameters.
     * @param chromosome the chromosome from the reference genome
     * @param startPos the start position of the interval
     * @param endPos the end position of the interval
     * @param variantIndex the index, which stores information of all variants
     * @param pattern the read to align
     * @return a vector, which contains optimal alignments
     */
    Alignment align(const Chromosome &chromosome, size_t startPos, size_t endPos, const VariantIndex &variantIndex,
                    const ReadString &pattern);

    /**
     * @brief align Computes all optimal alignments in an interval of the reference genome parameters using the specified
     * number of alignment errors. Implicitly sets the default number of alignment errors and clipped characters for
     * succeeding alignment runs to the specified value.
     * @param chromosome the chromosome from the reference genome
     * @param startPos the start position of the interval
     * @param endPos the end position of the interval
     * @param variantIndex the index, which stores information of all variants
     * @param pattern the read to align
     * @param maxError the maximum number of allowed errors for alignments
     * @param maxClip the maximum number clipped characters at beginning and end of read
     * @return a vector, which contains optimal alignments
     */
    Alignment align(const Chromosome &chromosome, size_t startPos, size_t endPos, const VariantIndex &variantIndex,
                    const ReadString &pattern, ErrorCount maxError, ErrorCount maxClip);

    /**
     * @brief charDistance Calculates the distance between the two characters with respect to the IUPAC alphabet.
     * @param p first character (usually from the pattern to match)
     * @param t second character (usually from the text to match)
     * @return 0 if characters do match, 1 else
     */
    inline ErrorCount charDistance(seqan::Iupac p, seqan::Iupac t) const;

    /**
     * @brief getMaxErrors Returns the maximum number of allowed errors for this aligner excluding any clipping.
     * @return the maximum number of allowed errors
     */
    ErrorCount getMaxErrors() const;

    /**
     * @brief getMaxClip Returns the maximum length of any prefix or suffix, which can be clipped from the read.
     * @return maximum number of clipped characters
     */
    ErrorCount getMaxClip() const;
};

#endif // PG583_ALIGN_ALIGNER_H
