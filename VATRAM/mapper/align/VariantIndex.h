#ifndef PG583_ALIGN_VARIANTINDEX_H
#define PG583_ALIGN_VARIANTINDEX_H

#include <seqan/basic.h>
#include <seqan/misc/misc_bit_twiddling.h>

#include <vector>
#include <cassert>
#include <cstddef>
using std::size_t;

class Variant;

class RankVariantIndex {
public:
    RankVariantIndex(const std::vector<Variant> &variantList, size_t highestPosition);

    /**
     * @brief Gets first variant in chromosome position range [\p pos, \p lastPos).
     * @param pos Minimum chromosome position for returned variant.
     * @param lastPos Upper bound (exclusive) chromosome position.
     * @return Iterator to first variant with chromosome position >= \p pos,
     *         or variantList.end() if there is no variant in range [\p pos, \p lastPos).
     *         If \p lastPos is in the same block as \p pos,
     *         the iterator may point to a variant beyond \p lastPos in that block.
     */
    std::vector<Variant>::const_iterator getNextVariant(size_t pos, size_t lastPos) const;

    size_t getVariantCount() const {
        return variantList.size();
    }
    const std::vector<Variant>& getVariantList() const {
        return variantList;
    }
private:
    const std::vector<Variant> &variantList;

    typedef uint64_t BlockType;
    static constexpr size_t BLOCK_SIZE = 64;
    static_assert(8 * sizeof(BlockType) >= BLOCK_SIZE, "BLOCK_SIZE too big.");
    static_assert(size_t(1) << seqan::Log2<BLOCK_SIZE>::VALUE == BLOCK_SIZE, "BLOCK_SIZE is no power of 2.");
    std::vector<BlockType> existsVariant;
    std::vector<size_t> listPositions;
    std::vector<std::vector<size_t>> listOffset;
};

class BinarySearchVariantIndex {
public:
    /**
     * @brief BinarySearchVariantIndex uses std::lower_bound for getNextVariant.
     * @param variantList
     * @param ignored_highestPosition Parameter not neccessary and ignored.
     */
    BinarySearchVariantIndex(const std::vector<Variant> &variantList, size_t ignored_highestPosition);

    /**
     * @brief Gets first variant at chromosome position >= \p pos.
     * @param pos Minimum chromosome position for returned variant.
     * @param ignored_lastPos Specified upper bound is ignored!
     * @return Iterator to first variant with chromosome position >= \p pos,
     *         or variantList.end() if no variant in range [\p pos, length(chromosome)).
     */
    std::vector<Variant>::const_iterator getNextVariant(size_t pos, size_t ignored_lastPos) const;

    size_t getVariantCount() const {
        return variantList.size();
    }

    const std::vector<Variant>& getVariantList() const {
        return variantList;
    }
private:
    const std::vector<Variant> &variantList;
};

typedef BinarySearchVariantIndex VariantIndex;

#endif // PG583_ALIGN_VARIANTINDEX_H
