#ifndef PG583_ALIGN_ALIGNMENT_H
#define PG583_ALIGN_ALIGNMENT_H

#include "types.h"
#include "align/Variant.h"

#include <seqan/store.h>

#include <string>
#include <vector>
#include <set>
#include <functional>
#include <cstddef>
using std::size_t;

typedef seqan::String<seqan::CigarElement<>> CigarString;
std::string toString(const CigarString &cigarString);

class Alignment {
public:
    Alignment();

    /**
     * @brief getCigar Returns the current cigar string. This method may require to invert the internally stored
     * inversed cigar string on the first call.
     * @return the current cigar string
     */
    CigarString& getCigar();

    /**
     * @brief getAlignmentString Returns the actual reference string, which has been used for this alignment. This string
     * may differ from the reference genome, if variants are used.
     * @return reference string for alignment
     */
    const seqan::String<ReferenceChar>& getAlignmentString();

    /**
     * @brief getUsedVariants Returns a list of variants, which have been used for this alignment. Applying these variants
     * on the reference genome, will result in the alignment string, on which the read has been aligned.
     * @return list of used variants
     */
    const std::set<std::reference_wrapper<const Variant>>& getUsedVariants() const;

    /**
     * @brief getPosition Returns the starting position of this alignment within a chromosome.
     * @return start position of alignment
     */
    size_t getPosition() const;

    /**
     * @brief getStartVariant If the alignment begins within an insertion string of a variant, this variant will be returned.
     * Otherwise Variant::none will be returned.
     * @return variant in which the alignment begins
     */
    const Variant& getStartVariant() const;

    /**
     * @brief getStartOffset If the alignment begins within an insertion string of a variant, this the offset within this
     * insertion string will be returned. Otherwise it returns 0.
     * @return offset within starting variant.
     */
    int getStartOffset() const;

    /**
     * @brief getErrorCount Returns the number of errors for the aligned read. Error count is measured as edit distance.
     * @return number of errors for aligned read
     */
    size_t getErrorCount();

    /**
     * @brief addCigarChar Inserts a new cigar character in front of the existing one.
     * @param c the new cigar character
     */
    void addCigarChar(char c);

    /**
     * @brief addAlignmentChar Inserts a new reference genome character in front of the stored reference string of this
     * alignment.
     * @param c the new reference character
     */
    void addAlignmentChar(ReferenceChar c);

    /**
     * @brief addUsedVariant Adds a new variant to the list of used variants.
     * @param var the new variant
     */
    void addUsedVariant(const Variant &var);

    /**
     * @brief setCigarString Replaces the cigar string with a new one. Also updates the internally stored reversed cigar string.
     * @param newCigar the new cigar string
     */
    void setCigarString(const CigarString &newCigar);

    /**
     * @brief setCigar Replaces the cigar string with a new one. Also updates the internally stored reversed cigar string.
     * @param newCigar the new cigar string
     */
    void setCigar(const std::string &newCigar);

    /**
     * @brief setAlignmentString Replaces the alignment string with a new one. Also updates the internally stored reversed
     * alignment string.
     * @param align the new alignment string
     */
    void setAlignmentString(const std::string &align);

    /**
     * @brief setPosition Sets the starting position to the specified value.
     * @param pos the new starting position
     */
    void setPosition(size_t pos);

    /**
     * @brief setStartVariant Sets the specified variant as starting variant of this alignment
     * @param var the new starting variant
     */
    void setStartVariant(const Variant &var);

    /**
     * @brief setStartOffset Sets the offset within the starting variant to the specified value.
     * @param off the new offset within the starting variant
     */
    void setStartOffset(int off);

    /**
     * @brief incrementErrorCount Increases the numer of errors for the aligned read by one.
     */
    void incrementErrorCount();

    /**
     * @brief isValid Checks whether this alignment encodes a valid alignment. False means "no alignment".
     * @return whether this alignment is a valid one
     */
    bool isValid() const;

    // Used for the qCompare operation in Qt Unit Tests
    bool operator==(const Alignment &a2) const;
    bool operator<(const Alignment &a2) const;

private:
    // cigar string for alignment
    CigarString cigar;
    // used alignment string
    seqan::String<ReferenceChar> alignmentString;
    // the list of variants used for this alignment
    std::set<std::reference_wrapper<const Variant>> usedVariants;
    // start index in reference
    size_t position;
    // the variant in which the alignments starts (or Variant::none if it starts in reference)
    std::reference_wrapper<const Variant> startVariant;
    // the position in this variant
    int startOffset;
    size_t errorCount;
    bool cigarInvalid;
    bool alignmentStringInvalid;
    CigarString reversedCigar;
    seqan::String<ReferenceChar> reversedAlignmentString;
    void flush();
    const CigarString& getReversedCigar() const;
    const seqan::String<ReferenceChar>& getReversedAlignmentString() const;
    std::string cigarToString(const CigarString &cs) const;
};

bool operator==(const std::vector<Alignment> &vec1, const std::vector<Alignment> &vec2);

#endif // PG583_ALIGN_ALIGNMENT_H
