#include "align/RankBacktracingMatrix.h"
#include "align/Variant.h"
#include "Logger.h"

#include <cassert>

RankBacktracingMatrix::RankBacktracingMatrix(ErrorCount maxError) :
    height(5),
    rowSize(5),
    storage(height, rowSize),
    nextRowCombiner(height),
    locationInCache(rowSize),
    cacheIndex(1),
    invalidCacheIndex(std::numeric_limits<size_t>::max()),
    maxError(maxError) {

    for (size_t i = 0; i < height; i++) {
        storage.cellCache[i].score = i;
        storage.cellCache[i].recDir = RecursionType::AL_INSERTEDCHAR;
        storage.cellCache[i].columnForRecursion = 0;
        storage.cellCache[i].nextRow = i+1;
        storage.cellCache[height+i].nextRow = i+1;
//        setScore(0, i, i);
//        setDir(0, i, RecursionType::AL_INSERTEDCHAR);
//        setBtColumn(0, i, 0);
//        setNextRow(0, i, i+1);
//        setNextRow(1, i, i+1);
    }
    storage.headers[0].scoreFill = height;
}

void RankBacktracingMatrix::init(size_t readLength) {
    size_t oldHeight = height;
    height = readLength+2;
    storage.resizeHeight(height);

    // initialize first column with scores (0th row is init'ed when new column is added)
    for (size_t i = oldHeight; i < height; i++) {
        storage.cellCache[i].score = i;
        storage.cellCache[i].recDir = RecursionType::AL_INSERTEDCHAR;
        storage.cellCache[i].columnForRecursion = 0;
        storage.cellCache[i].nextRow = i+1;
//        setScore(0, i, i);
//        setDir(0, i, RecursionType::AL_INSERTEDCHAR);
//        setBtColumn(0, i, 0);
//        setNextRow(0, i, i+1);
    }

    // initialize seconds column with valid next-row-information for the aligner
    for (size_t i = 0; i < height; i++) {
        storage.cellCache[height+i].nextRow = i+1;
//        setNextRow(1, i, i+1);
    }

    storage.headers[0].scoreFill = height - 1;
    rowSize = 0;
    locationInCache[0] = 0;
    locationInCache[1] = 1;
    cacheIndex = 1;
}

void RankBacktracingMatrix::cleanup() {
    storage.clear();
}

void RankBacktracingMatrix::newColumn(size_t pos) {
    newColumn(pos, Variant::none, 0); // means "no variant"
}

void RankBacktracingMatrix::newColumn(size_t pos, const Variant &variant, size_t offset) {

    if (variant == Variant::none)
        storage.columnOfRefInIndex[pos] = rowSize+1;

    // increase size if necessary
    if (rowSize + 2 >= storage.width) {
        storage.resizeWidth(2*(rowSize+2));
        locationInCache.resize(2*(rowSize+2));
    }

    // initialize new column
    rowSize++;
//    assert(rowSize < storage.cells.size());
    assert(rowSize < storage.headers.size());

    // copy victim column from cache to hashtable
    if (rowSize + 1 >= MATRIX_MODEL_CACHE_WIDTH) {
        ColumnIndex victimColumn = rowSize + 1 - MATRIX_MODEL_CACHE_WIDTH;

        size_t cacheLocation = locationInCache[victimColumn];
        for (RowIndex row = 0; row <= getScoreFill(victimColumn); row = storage.cellCache[cacheLocation*height+row].nextRow) {
            storage.writeCell(victimColumn, row, storage.cellCache[cacheLocation*height+row]);
        }

        locationInCache[victimColumn] = invalidCacheIndex;
    }
    cacheIndex = (cacheIndex+1)%MATRIX_MODEL_CACHE_WIDTH;
    locationInCache[rowSize + 1] = cacheIndex;

    storage.cellCache[locationInCache[rowSize]*height].columnForRecursion = rowSize-1;
    storage.cellCache[locationInCache[rowSize]*height].nextRow = 1;
    storage.cellCache[locationInCache[rowSize+1]*height].nextRow = 1;
    storage.cellCache[locationInCache[rowSize]*height].recDir = RecursionType::AL_DELETEDCHAR;
    storage.cellCache[locationInCache[rowSize]*height].score = 0;

//    setScore(rowSize, 0, 0);
//    setDir(rowSize, 0, RecursionType::AL_DELETEDCHAR);
//    setBtColumn(rowSize, 0, rowSize-1);
//    setNextRow(rowSize, 0, 1);
//    setNextRow(rowSize+1, 0, 1);

    storage.headers[rowSize].variant = variant;
    storage.headers[rowSize].indexOfColumnInRef = pos;
    storage.headers[rowSize].offset = offset;
    storage.headers[rowSize].scoreFill = 0;
    storage.headers[rowSize].lastRowNext = 1;
}

void RankBacktracingMatrix::ensureScoreColumnFill(ColumnIndex column, RowIndex maxRow) {
    if (maxRow < storage.headers[column].scoreFill) {
        for (size_t i = 1; i < maxRow; i = getNextRow(column, i)) {
            RowIndex limit = getNextRow(column, i);
            for (RowIndex j = i+1; j < limit; j++) {
                defineScoreEntry(column, j);
            }
        }
    } else {
        for (size_t i = 1; i < storage.headers[column].scoreFill; i = getNextRow(column, i)) {
            RowIndex limit = getNextRow(column, i);
            for (RowIndex j = i+1; j < limit; j++) {
                defineScoreEntry(column, j);
            }
        }
        for (size_t i = storage.headers[column].scoreFill + 1; i <= maxRow; i++) {
            defineScoreEntry(column, i);
        }
    }
}

void RankBacktracingMatrix::ensureScoreColumnFillNoRowSkip(ColumnIndex column, RowIndex maxRow) {
    for (size_t i = 1; i <= maxRow; i++) {
        defineScoreEntry(column, i);
    }
}

void RankBacktracingMatrix::copyNextRowColumn(ColumnIndex column_from, ColumnIndex column_to, RowIndex rowFillToEnsure) {
    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout<<"copyNextRowColumn "<<column_from<<" -> "<<column_to<<std::endl;
    #endif
    // copy the old column
    for (size_t row = 1; row <= getScoreFill(column_from); row = getNextRow(column_from, row)) {
        setNextRow(column_to, row, getNextRow(column_from, row));
        defineScoreEntry(column_to, row-1);
    }

    // if guaranteed column fill not reached -> link remaining rows linearly
    for (size_t row = getScoreFill(column_from) + 1; row <= rowFillToEnsure; row++) {
        setNextRow(column_to, row, row+1);
    }
}

void RankBacktracingMatrix::initNextRowCombiner(ColumnIndex referenceCol, RowIndex maxRow) {
    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout<<"begin initNextRowCombiner"<<std::endl;
    std::cout<<"referenceCol="<<referenceCol<<std::endl;
    #endif
    nextRowCombiner.clear();
    for (size_t row = 1; row <= maxRow; row = getNextRow(referenceCol, row)) {
        #ifdef ALIGNER_DEBUG_OUTPUT
        std::cout<<"row="<<row<<std::endl;
        #endif
        nextRowCombiner.push_back(row);
        #ifdef ALIGNER_DEBUG_OUTPUT
        if (getNextRow(referenceCol, row) <= row) {
            std::cout<<"deadlock"<<std::endl;
        }
        #endif
    }
    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout<<"end initNextRowCombiner"<<std::endl;
    #endif
}

void RankBacktracingMatrix::combineNextRowColumn(ColumnIndex newCol, RowIndex maxRow) {
    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout<<"begin combineNextRowColumn"<<std::endl;
    std::cout<<"nextRowCombiner="<<nextRowCombiner<<std::endl;
    std::cout<<"newCol="<<newCol<<std::endl;
    #endif
    setNextRow(newCol, maxRow, maxRow+1); //TODO: Really necessary?
    std::vector<RowIndex> nextRows;

    // copy sorted row sequence to vector
    for (unsigned int row = 1; row <= maxRow; row = getNextRow(newCol, row)) {
        nextRows.push_back(row);
        #ifdef ALIGNER_DEBUG_OUTPUT
        if (getNextRow(newCol, row) <= row) {
            std::cout<<"deadlock"<<std::endl;
            std::cout<<nextRows<<std::endl;
            std::cout<<nextRowCombiner<<std::endl;
            exit(1);
        }
        #endif
    }

    // merge old and new array (both are in ascending order)
    std::vector<RowIndex> newCombiner;
    newCombiner.push_back(0);
    unsigned int a = 0, b = 0;
    while (a < nextRowCombiner.size() || b < nextRows.size()) {
        #ifdef ALIGNER_DEBUG_OUTPUT
        std::cout<<"a="<<a<<" b="<<b<<std::endl;
        std::cout<<"nextRowCombiner.size()="<<nextRowCombiner.size()<<" nextRows.size()="<<nextRows.size()<<std::endl;
        #endif
        if (a >= nextRowCombiner.size()) {
            if (nextRows[b] != newCombiner[newCombiner.size()-1]) {
                #ifdef ALIGNER_DEBUG_OUTPUT
                std::cout<<"pushback="<<nextRows[b]<<std::endl;
                #endif
                newCombiner.push_back(nextRows[b]);
            }
            b++;
        } else if (b >= nextRows.size()) {
            if (nextRowCombiner[a] != newCombiner[newCombiner.size()-1]) {
                #ifdef ALIGNER_DEBUG_OUTPUT
                std::cout<<"pushback="<<nextRowCombiner[a]<<std::endl;
                #endif
                newCombiner.push_back(nextRowCombiner[a]);
            }
            a++;
        } else if (nextRowCombiner[a] < nextRows[b]) {
            if (nextRowCombiner[a] != newCombiner[newCombiner.size()-1]) {
                #ifdef ALIGNER_DEBUG_OUTPUT
                std::cout<<"pushback="<<nextRowCombiner[a]<<std::endl;
                #endif
                newCombiner.push_back(nextRowCombiner[a]);
            }
            a++;
        } else {
            if (nextRows[b] != newCombiner[newCombiner.size()-1]) {
                #ifdef ALIGNER_DEBUG_OUTPUT
                std::cout<<"pushback="<<nextRows[b]<<std::endl;
                #endif
                newCombiner.push_back(nextRows[b]);
            }
            b++;
        }
    }

    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout<<"merging finished"<<b<<std::endl;
    #endif

    // copy new data into combiner
    nextRowCombiner.clear();
    for (unsigned int i = 0; i < newCombiner.size(); i++) {
        nextRowCombiner.push_back(newCombiner[i]);
    }

    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout<<"end combineNextRowColumn"<<std::endl;
    #endif
}

void RankBacktracingMatrix::writeBackCombinedNextRowColumn(ColumnIndex targetCol, RowIndex rowFillToEnsure, std::vector<ColumnIndex> &columns) {
    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout<<"begin writeBackCombinedNextRowColumn"<<std::endl;
    #endif
    // copy next-row-column and define necessary score entries
    for (size_t i = 2; i < nextRowCombiner.size(); i++) {
        setNextRow(targetCol, nextRowCombiner[i-1], nextRowCombiner[i]);
        defineScoreEntry(targetCol, nextRowCombiner[i-1]-1);
    }

    // determine necessary score entries for predecessor columns
    std::vector<RowIndex> rowsToDefine;
    for (size_t i = 1; i < nextRowCombiner.size(); i++) {
        if (!rowsToDefine.empty() && rowsToDefine.back() != nextRowCombiner[i]-1) {
            rowsToDefine.push_back(nextRowCombiner[i]-1);
        }
        rowsToDefine.push_back(nextRowCombiner[i]);
    }

    // define score entries for predecessor columns
    for (size_t j = 0; j < columns.size(); j++) {
        RowIndex row = 1;
        size_t i = 1;
        while (row <= getScoreFill(columns[j]) && i < rowsToDefine.size()) {
            if (row < rowsToDefine[i]) {                
                row = getNextRow(columns[j], row);
            } else if (row > rowsToDefine[i]) {
                defineScoreEntry(columns[j], rowsToDefine[i]);
                i++;
            } else {
                row = getNextRow(columns[j], row);
                i++;
            }
        }
        for (size_t ii = i; ii < rowsToDefine.size(); ii++) {
            defineScoreEntry(columns[j], rowsToDefine[ii]);
        }
        for (size_t row = nextRowCombiner[nextRowCombiner.size()-1]+1; row <= rowFillToEnsure; row++) {
            defineScoreEntry(columns[j], row);
        }
    }

    // if guaranteed column fill not reached -> link remaining rows linearly
    for (size_t row = nextRowCombiner[nextRowCombiner.size()-1]; row <= rowFillToEnsure; row++) {
        defineScoreEntry(targetCol, row-1);
        setNextRow(targetCol, row, row+1);
    }
    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout<<"end writeBackCombinedNextRowColumn"<<std::endl;
    #endif
}
