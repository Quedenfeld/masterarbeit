#include "align/BacktracingMatrix.h"
#include "align/Variant.h"

#include <cassert>

BacktracingMatrix::BacktracingMatrix(ErrorCount maxError) :
    height(5),
    rowSize(5),    
    storage(height, rowSize),
    maxError(maxError) {

    for (size_t i = 0; i < height; i++) {
        assert(height + i < storage.cells.size());
        storage.cells[i].score = i;
        storage.cells[i].recDir = RecursionType::AL_INSERTEDCHAR;
        storage.cells[i].columnForRecursion = 0;
        storage.cells[i].nextRow = i+1;
        storage.cells[height+i].nextRow = i+1;
    }
    storage.headers[0].scoreFill = - 1;
}

void BacktracingMatrix::init(size_t readLength) {
    size_t oldHeight = height;
    height = readLength+2;

    // configure matrix model
    if (storage.height < height) {
        storage.resizeHeight(height);
    }
    storage.height = height;

    // initialize first column with scores (0th row is init'ed when new column is added)
    for (size_t i = oldHeight; i < height; i++) {
        assert(height + i < storage.cells.size());
        storage.cells[i].score = i;
        storage.cells[i].recDir = RecursionType::AL_INSERTEDCHAR;
        storage.cells[i].columnForRecursion = 0;
        storage.cells[i].nextRow = i+1;
    }

    for (size_t i = 0; i < height; i++) {
        storage.cells[i].nextRow = i+1;
        storage.cells[height+i].nextRow = i+1;
    }

    storage.headers[0].scoreFill = height - 1;
    rowSize = 0;
}

void BacktracingMatrix::cleanup() {

}

void BacktracingMatrix::newColumn(size_t pos) {    
    newColumn(pos, Variant::none, 0); // means "no variant"
}

void BacktracingMatrix::newColumn(size_t pos, const Variant &variant, size_t offset) {

    if (variant == Variant::none)
        storage.columnOfRefInIndex[pos] = rowSize+1;

    // increase size if necessary
    if (rowSize + 2 >= storage.width)
        storage.resizeWidth(2*(rowSize+2));

    // initialize new column
    rowSize++;
    assert(height*rowSize < storage.cells.size());
    assert(rowSize < storage.headers.size());
    storage.cells[height*rowSize].columnForRecursion = rowSize-1;
    storage.cells[height*rowSize].nextRow = 1;
    storage.cells[height*rowSize].recDir = RecursionType::AL_DELETEDCHAR;
    storage.cells[height*rowSize].score = 0;
    storage.headers[rowSize].variant = variant;
    storage.headers[rowSize].indexOfColumnInRef = pos;
    storage.headers[rowSize].offset = offset;
    storage.headers[rowSize].scoreFill = 0;
    storage.headers[rowSize].lastRowNext = 1;
}

void BacktracingMatrix::ensureScoreColumnFill(ColumnIndex column, RowIndex maxRow) {
    int maxErrorpp = maxError + 1;
    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout << "fill ";
    #endif
    if (maxRow < storage.headers[column].scoreFill) {
        for (size_t i = 1; i < maxRow; i = storage.cells[column*height+i].nextRow) {
            for (RowIndex j = i+1; j < storage.cells[column*height+i].nextRow; j++) {
                assert(column*height+j < storage.cells.size());
                storage.cells[column*height+j].score = maxErrorpp;
                #ifdef ALIGNER_DEBUG_OUTPUT
                std::cout << "("<<column<<","<<j<<"~>"<<maxErrorpp<<") " << std::flush;
                #endif
            }
        }
    } else {
        for (size_t i = 1; i < storage.headers[column].scoreFill; i = storage.cells[column*height+i].nextRow) {
            for (RowIndex j = i+1; j < storage.cells[column*height+i].nextRow; j++) {
                assert(column*height+j < storage.cells.size());
                storage.cells[column*height+j].score = maxErrorpp;
                #ifdef ALIGNER_DEBUG_OUTPUT
                std::cout << "("<<column<<","<<j<<"~>"<<maxErrorpp<<") " << std::flush;
                #endif
            }
        }
        for (size_t i = storage.headers[column].scoreFill + 1; i <= maxRow; i++) {
            assert(column*height+i < storage.cells.size());
            storage.cells[column*height+i].score = maxErrorpp;
            #ifdef ALIGNER_DEBUG_OUTPUT
            std::cout << "("<<column<<","<<i<<"~>"<<maxErrorpp<<") " << std::flush;
            #endif
        }
    }
    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout << "endfill ";
    #endif
}

void BacktracingMatrix::ensureScoreColumnFillNoRowSkip(ColumnIndex column, RowIndex maxRow) {
    int maxErrorpp = maxError + 1;
    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout << "fill ";
    #endif
    for (size_t i = storage.headers[column].scoreFill + 1; i <= maxRow; i++) {
        assert(column*height+i < storage.cells.size());
        storage.cells[column*height+i].score = maxErrorpp;
    }
    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout << "endfill ";
    #endif
}

void BacktracingMatrix::copyNextRowColumn(ColumnIndex column_from, ColumnIndex column_to, RowIndex rowFillToEnsure) {
    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout << "copy " << column_from << "->" << column_to << std::flush;
    #endif
    // copy the old column
    for (size_t row = 1; row <= storage.headers[column_from].scoreFill; row = storage.cells[column_from*height+row].nextRow) {
        #ifdef ALIGNER_DEBUG_OUTPUT
        std::cout << "["<<column_to<<","<<row<<"~>"<<storage.cells[column_from*height + row].nextRow<<"] " << std::flush;
        #endif
        storage.cells[column_to*height + row].nextRow = storage.cells[column_from*height + row].nextRow;
        defineScoreEntry(column_to, row-1);
    }

    // if guaranteed column fill not reached -> link remaining rows linearly
    for (size_t row = storage.headers[column_from].scoreFill + 1; row <= rowFillToEnsure; row++) {
        storage.cells[column_to*height + row].nextRow = row+1;
    }
    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout << std::endl;
    #endif
}

void BacktracingMatrix::initNextRowCombiner(ColumnIndex /*referenceCol*/, RowIndex /*maxRow*/) {
    return;
}

void BacktracingMatrix::combineNextRowColumn(ColumnIndex /*newCol*/, RowIndex /*maxRow*/) {
    return;
}

void BacktracingMatrix::writeBackCombinedNextRowColumn(ColumnIndex targetCol, RowIndex rowFillToEnsure, std::vector<ColumnIndex> &columns) {
    // After variant: Always calculate whole column
    for (RowIndex i = 1; i <= rowFillToEnsure; i++) {
        setNextRow(targetCol, i, i+1);
    }

    // Ensure that all column for dynamic programming are filled up to highest row of any insertion
    for (size_t i = 0; i < columns.size(); i++) {
        ensureScoreColumnFill(columns[i], rowFillToEnsure);
    }
}
