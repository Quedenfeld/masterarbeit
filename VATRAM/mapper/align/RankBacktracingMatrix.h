#ifndef PG583_ALIGN_HASHBACKTRACINGMATRIX_H
#define PG583_ALIGN_HASHBACKTRACINGMATRIX_H

#include "align/RankMatrixModel.h"

#include <cassert>
#include <vector>
#include <functional>
#include <cstddef>
#include <iostream>
#include <fstream>
#include <cmath>

/**
 * @class HashBacktracingMatrix
 * @brief A class to store backtracing information for the semiglobal aligner.
 * Data storage is implemented as a single array, in which 2D-accesses are mapped into. Therefore the matrix needs an
 * initial column height for correct address arithmetics.
 */
class RankBacktracingMatrix {
private:

    /**
     * @brief height of this matrix
     */
    RowIndex height;

    /**
     * @brief the number of used columns
     */
    ColumnIndex rowSize;

    /**
     * @brief contains all allocated storage for this matrix.
     */
    RankMatrixModel storage;

    /**
     * @brief nextRowCombiner array used to merge next-row-columns
     */
    std::vector<RowIndex> nextRowCombiner;

    /**
     * @brief locationInCache an array, which maps all columns indices to their location in the cache
     */
    std::vector<size_t> locationInCache;

    /**
     * @brief cacheIndex indicated, which column of the cache is overwritten next (cache is overwritten in cyclic order)
     */
    size_t cacheIndex;

    /**
     * @brief invalidCacheIndex indicates, that a column is not present in the column cache
     */
    const size_t invalidCacheIndex;

public:
    /**
     * @brief Constructs a new matrix for backtracing information.
     * @param readLength the length of the read for the alignment
     * @param maxError the maximum number of errors for the current alignment
     * @param storage the storage object for this matrix
     *
     * The matrix is initialized with a height of readLength+1 (to store a 0th row) and a width of 200. The first
     * column is initialized with i in the i-th row, so the first column does not have to (and should not) be filled.
     * For every new column, it is adviced to use the methods for adding columns, so the matrix is aware of the actual
     * fill progress and may allocate new space, if the initial matrix width was not sufficient.
     */
    RankBacktracingMatrix(ErrorCount maxError);

    /**
     * @brief maxError The maximum number of allowed errors for read alignment.
     */
    ErrorCount maxError;

    /**
     * @brief init Initializes the matrix to prepare for a new alignment. Must be called for each new alignment.
     * @param readLength the length of the next read
     */
    void init(size_t readLength);

    /**
     * @brief cleanup Deletes all temporary data to save memory. Must be called at the end of each alignment to guarantee
     * correctness.
     */
    void cleanup();

    /**
     * @brief Read access for the score
     * @param column column index
     * @param row row index
     * @return the score of the specified field
     */
    ErrorCount getScore(ColumnIndex column, RowIndex row) {
        if (locationInCache[column] != invalidCacheIndex) {
            return storage.cellCache[locationInCache[column]*height + row].score;
        } else {
            return storage.readCell(column, row).score;
        }
    }

    /**
     * @brief Write access for the score
     * @param column column index
     * @param row row index
     * @param score the score for the specified field
     */
    void setScore(ColumnIndex column, RowIndex row, ErrorCount value) {
        #ifdef ALIGNER_DEBUG_OUTPUT
        std::cout << "("<<column<<","<<row<<"->"<<value<<") "<< std::flush;
        #endif
        if (locationInCache[column] != invalidCacheIndex) {
            storage.cellCache[locationInCache[column]*height + row].score = value;
        } else {
            storage.readCell(column, row).score = value;
        }
        storage.headers[column].scoreFill = std::max(storage.headers[column].scoreFill, (RowIndex)row);
    }

    /**
     * @brief Sets the specified score entry to maxError+1. Should be used to explicitly define an entry, which is not
     * computed by the aligner, but could be accessed.
     * @param column column index
     * @param row row index
     * @param maxError the max error for the current alignment
     */
    void defineScoreEntry(ColumnIndex column, RowIndex row) {
        #ifdef ALIGNER_DEBUG_OUTPUT
        std::cout << "("<<column<<","<<row<<"~>"<<maxError+1<<") " << std::flush;
        #endif
        if (locationInCache[column] != invalidCacheIndex) {
            storage.cellCache[locationInCache[column]*height + row].score = maxError+1;
        }
    }

    /**
     * @brief Read access for recursion direction
     * @param column column index
     * @param row row index
     * @return the type of alignment for the specified field
     */
    RecursionType getDir(ColumnIndex column, RowIndex row) {
        if (locationInCache[column] != invalidCacheIndex) {
            return storage.cellCache[locationInCache[column]*height + row].recDir;
        } else {
            return storage.readCell(column, row).recDir;
        }
    }

    /**
     * @brief Write access for recursion direction
     * @param column column index
     * @param row row index
     * @param dir the backtrace direction for the specified field
     */
    void setDir(ColumnIndex column, RowIndex row, RecursionType dir) {
        if (locationInCache[column] != invalidCacheIndex) {
            storage.cellCache[locationInCache[column]*height + row].recDir = dir;
        } else {
            storage.readCell(column, row).recDir = dir;
        }
    }

    /**
     * @brief Read access for column of the used recursion
     * @param column column index
     * @param row row index
     * @return the column index inside this matrix from which the specified field has been filles through recursion
     */
    ColumnIndex getBtColumn(ColumnIndex column, RowIndex row) {
        if (locationInCache[column] != invalidCacheIndex) {
            return storage.cellCache[locationInCache[column]*height + row].columnForRecursion;
        } else {
            return storage.readCell(column, row).columnForRecursion;
        }
    }

    /**
     * @brief Write access for backtracing column
     * @param column column index
     * @param row row index
     * @param col the backtracing column for the specified field
     */
    void setBtColumn(ColumnIndex column, RowIndex row, ColumnIndex col) {
        if (locationInCache[column] != invalidCacheIndex) {
            storage.cellCache[locationInCache[column]*height + row].columnForRecursion = col;
        } else {
            storage.readCell(column, row).columnForRecursion = col;
        }
    }

    /**
     * @brief Read access for next row to calculate
     * @param column column index
     * @param row row index
     * @return the next row, which has to be calculated in the specified column after the specified row
     */
    RowIndex getNextRow(ColumnIndex column, RowIndex row) {
        if (locationInCache[column] != invalidCacheIndex) {
            return storage.cellCache[locationInCache[column]*height + row].nextRow;
        } else {
            return storage.readCell(column, row).nextRow;
        }
    }

    /**
     * @brief Write access for next row to calculate
     * @param column column index
     * @param row row index
     * @param nrow the next row, which has to be calculated in the specified column after the specified row
     */
    void setNextRow(ColumnIndex column, RowIndex row, RowIndex nrow) {
        #ifdef ALIGNER_DEBUG_OUTPUT
        std::cout << "["<<column<<","<<row<<"->"<<nrow<<"] " << std::flush;
        #endif
        if (locationInCache[column] != invalidCacheIndex) {
            storage.cellCache[locationInCache[column]*height + row].nextRow = nrow;
        } else {
            storage.readCell(column, row).nextRow = nrow;
        }
    }

    /**
     * @brief Read access for the row size of this matrix
     * @return the number of columns in this matrix
     *
     * This size is not necessarily equal to the length of the reference because of insertion variants.
     */
    ColumnIndex getLastColumn() const {
        return rowSize;
    }

    /**
     * @brief Read access for variant in column
     * @param column column index
     * @return the variant to which the specified column belongs
     *
     * For the absense of a variant in the column this function returns Variant::none.
     */
    const Variant& getVariantAt(ColumnIndex column) const {
        assert(column < storage.headers.size());
        return storage.headers[column].variant;
    }

    /**
     * @brief Read access for the position of the reference
     * @param column column index
     * @return the index of the reference to which the specified column belongs
     *
     * This index is not necessarily equal to the column index because of insertion variants.
     */
    size_t getIndexOfColumnInRef(ColumnIndex column) const {
        assert(column < storage.headers.size());
        return storage.headers[column].indexOfColumnInRef;
    }

    /**
     * @brief Read access for the column id of reference position
     * @param pos position in reference
     * @return the column which belongs to the reference position
     *
     * This index is not necessarily equal to the column index because of insertion variants.
     */
    ColumnIndex getColumnOfRefInIndex(size_t pos) const {
        assert(pos < storage.columnOfRefInIndex.size());
        return storage.columnOfRefInIndex[pos];
    }

    /**
     * @brief Read access for offset inside an insertion variant
     * @param column column index
     * @return the offset inside the current insertion variant
     *
     * If the specified column belongs to an insertion variant, then the position inside this variant is returned.
     * Otherwise it returns 0.
     */
    size_t getOffset(ColumnIndex column) const {
        assert(column < storage.headers.size());
        return storage.headers[column].offset;
    }

    /**
     * @brief Returns the size of a column, which has been filled with (meaningful) scores.
     * @param column column index
     * @return the size of a column, which has been filled with (meaningful) scores.
     */
    RowIndex getScoreFill(ColumnIndex column) const {
        assert(column < storage.headers.size());
        return storage.headers[column].scoreFill;
    }

    /**
     * @brief Sets the last row, which has to be computed for the next column (not the current one).
     * @param column column index
     * @param the last row to compute for next column
     */
    void setLastRowNext(ColumnIndex column, RowIndex lrow) {
        assert(column < storage.headers.size());
        storage.headers[column].lastRowNext = lrow;
    }

    /**
     * @brief Returns the last row, which has to be computed for the next column (not the current one).
     * @param column column index
     * @return the last row to compute for next column
     */
    RowIndex getLastRowNext(ColumnIndex column) const {
        assert(column < storage.headers.size());
        return storage.headers[column].lastRowNext;
    }

    /**
     * @brief newColumn Inserts a new column to the matrix
     * @param pos the position of the reference, which corresponds to the new column
     */
    void newColumn(size_t pos);

    /**
     * @brief newColumn Inserts a new column to the matrix
     * @param pos the position of the reference, which corresponds to the new column
     * @param variant the variant present in this column
     * @param offset the offset inside an insertion variant for the new column
     */
    void newColumn(size_t pos, const Variant &variant, size_t offset);

    /**
     * @brief ensureScoreColumnFill Ensures that the specified column is filled with maxError+1 up to the specified row
     * @param column the ecolumn to fill
     * @param maxRow last entry, which must be filled
     * @param maxError maximum number of errors, undefined entries are filled with maxError+1
     */
    void ensureScoreColumnFill(ColumnIndex column, RowIndex maxRow);

    /**
     * @brief ensureScoreColumnFill Ensures that the specified column is filled with maxError+1 up to the specified row
     * ignoring any unfilled rows in between (should only be called by AlignerNoRowSkip).
     * @param column the ecolumn to fill
     * @param maxRow last entry, which must be filled
     * @param maxError maximum number of errors, undefined entries are filled with maxError+1
     */
    void ensureScoreColumnFillNoRowSkip(ColumnIndex column, RowIndex maxRow);


    /**
     * @brief copies the content of one "nextRow" column to another.
     * @param column_from index of source column
     * @param column_to index of target column
     * @param rowFillToEnsure index of the column which must be guaranteed to have a successor
     */
    void copyNextRowColumn(ColumnIndex column_from, ColumnIndex column_to, RowIndex rowFillToEnsure);

    /**
     * @brief initNextRowCombiner For variants, several "nextRow"-information has to be merged for the column, which follows
     * the variants. Initializes the next-row-combiner using the column before the variants.
     * @param referenceCol column index of column before the variants
     * @param maxRow size of this column
     */
    void initNextRowCombiner(ColumnIndex referenceCol, RowIndex maxRow);

    /**
     * @brief combineNextRowColumn Merges a new column into the next-row-combiner. This must be called for each column, on
     * which the column after the variant has read access. For example: last column of insertion string or some older column,
     * which is used to simulate a deletion.
     * @param newCol column index of column to merge
     * @param maxRow size of this column
     */
    void combineNextRowColumn(ColumnIndex newCol, RowIndex maxRow);

    /**
     * @brief writeBackCombinedNextRowColumn Writes the merged information into the target column, i.e. the column after the
     * variants. Also ensures, that all predecendant columns (which have to be specified as well) will have valid entries
     * for read access.
     * @param targetCol column index for column after variants
     * @param rowFillToEnsure row index, to which this and all other columns must have valid entries for read access
     * @param columns all predecendant columns for the target column
     */
    void writeBackCombinedNextRowColumn(ColumnIndex targetCol, RowIndex rowFillToEnsure, std::vector<ColumnIndex> &columns);
};

#endif // PG583_ALIGN_HASHBACKTRACINGMATRIX_H
