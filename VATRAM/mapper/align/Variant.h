#ifndef PG583_ALIGN_VARIANT_H
#define PG583_ALIGN_VARIANT_H

#include "types.h"

#include <cstddef>
using std::size_t;

struct SnpVariant {
    size_t position;
    ReferenceChar snp;

    bool operator<(const SnpVariant& v) const {return position < v.position;}
};

class Variant {
public:
    // Constructor was actually private and only used for static instance "none",
    // but the QtTest can not create an instance then..

    Variant();
    static const Variant none;

    enum Type {
        INSERTION, DELETION, SUBSTITUTION
    };

    Variant(size_t position, size_t deletionLength);//, double probability);
    Variant(size_t position, const ReferenceString &insertionString);//, double probability);
    Variant(size_t position, const ReferenceString &insertionString, size_t deletionLength);//, double probability);

    // move constructor and assignment operator need for std::sort
    Variant(Variant &&) = default;
    Variant& operator=(Variant &&) = default;

    Variant(const Variant &) = default;
    Variant& operator=(const Variant &) = default;
    ~Variant() = default;

    size_t getPosition() const;
    const ReferenceString& getInsertionString() const;
    size_t getDeletionLength() const;

    Type getType() const;

//    double getProbability() const;

    // needed for std::set and std::sort
    bool operator<(const Variant &rhs) const;
    // compares only by address, since variants should be unique
    bool operator==(const Variant &rhs) const;
private:

    friend class LshIO;
    size_t position;
    size_t deletionLength = 0;
    ReferenceString insertionString = "";
//    double probability;
};

// needed for containers in Alignment.h and BacktracingMatrix.h
template<class T>
bool operator<(const std::reference_wrapper<T> &lhs, const std::reference_wrapper<T> &rhs) {
    return (T &) lhs < (T &) rhs;
}

template<class T>
bool operator==(const std::reference_wrapper<T> &lhs, const std::reference_wrapper<T> &rhs) {
    return (T &) lhs == (T &) rhs;
}

template<class T>
std::ostream& operator<<(std::ostream &stream, const std::reference_wrapper<T> &wrap) {
    stream << (T &) wrap;
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const Variant &var);

template<class T>
std::ostream& operator<<(std::ostream& stream, const std::vector<T> &vec) {
    stream << "[";
    for (const T& obj : vec) {
        stream << obj << ",";
    }
    stream << "]";
    return stream;
}

template<class T>
std::ostream& operator<<(std::ostream& stream, const std::set<T> &set) {
    stream << "{";
    for (const T& obj : set) {
        stream << obj << ",";
    }
    stream << "}";
    return stream;
}

template<class T>
bool operator<(const std::vector<T> &vec1, const std::vector<T> &vec2) {
    int max = std::min(vec1.size(), vec2.size());
    for (int i = 0; i < max; i++) {
        if (vec1[i] < vec2[i])
            return true;
        else if (vec1[i] > vec2[i])
            return false;
    }
    if (vec1.size() < vec2.size())
        return true;
    else if (vec1.size() > vec2.size())
        return false;

    return false;
}

#endif // PG583_ALIGN_VARIANT_H
