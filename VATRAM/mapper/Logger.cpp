#include "Logger.h"

Logger::Logger(std::ostream &os/* = std::cerr*/) :
    Logger(os, os, os) {}
Logger::Logger(std::ostream &infoStream, std::ostream &warnStream, std::ostream &errStream) :
    infoStream(infoStream),
    warnStream(warnStream),
    errStream(errStream)
{}

Logger logger;


